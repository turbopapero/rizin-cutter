<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl" sourcelanguage="en">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="14"/>
        <source>About Cutter</source>
        <translation>Over Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="69"/>
        <source>Check for updates on start</source>
        <translation>Controleren op updates bij het opstarten</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="95"/>
        <source>Show version information</source>
        <translation>Versie informatie weergeven</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="108"/>
        <source>Show Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by Rizin&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show radare2 plugin information</source>
        <translation type="vanished">Bekijk de radare2 plugin informatie</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by radare2&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is een gratis en open-source reverse engineering platform wat gebruik maakt van radare2&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Lees meer op &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="128"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:28pt; font-weight:600;&quot;&gt;Cutter&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:28pt; font-weight:600;&quot;&gt;Cutter&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Show plugin information</source>
        <translation type="vanished">Plug-in informatie weergeven</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="82"/>
        <source>Check for updates</source>
        <translation>Naar updates zoeken</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="28"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <source>Using r2-</source>
        <translation type="vanished">U gebruikt r2-</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="28"/>
        <source>Using rizin </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="30"/>
        <source>Optional Features:</source>
        <translation>Optionele functies:</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="47"/>
        <source>License</source>
        <translation>Licentie</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="48"/>
        <source>This Software is released under the GNU General Public License v3.0</source>
        <translation>Deze software is uitgebracht onder de GNU General Public License v3.0 licentie</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="49"/>
        <source>Authors</source>
        <translation>Auteurs</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="50"/>
        <source>Cutter is developed by the community and maintained by its core and development teams.&lt;br/&gt;</source>
        <translation>Cutter is gedreven door de community en wordt onderhouden door de development teams.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="52"/>
        <source>Check our &lt;a href=&apos;https://github.com/rizinorg/cutter/graphs/contributors&apos;&gt;contributors page&lt;/a&gt; for the full list of contributors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="76"/>
        <source>Rizin version information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check our &lt;a href=&apos;https://github.com/radareorg/cutter/graphs/contributors&apos;&gt;contributors page&lt;/a&gt; for the full list of contributors.</source>
        <translation type="vanished">Bekijk onze &lt;a href=&apos;https://github.com/radareorg/cutter/graphs/contributors&apos;&gt;bijdrage pagina&lt;/a&gt; voor een volledige lijst van vrijwilligers.</translation>
    </message>
    <message>
        <source>radare2 version information</source>
        <translation type="vanished">radare2 versie informatie</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="99"/>
        <source>Checking for updates...</source>
        <translation>Aan het zoeken naar updates...</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="109"/>
        <source>Cutter is up to date!</source>
        <translation>Dit is de nieuwste versie van Cutter!</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="150"/>
        <source>Based on Qt %1 (%2, %3 bit)</source>
        <translation>Gebaseerd op Qt %1 (%2, %3 bit)</translation>
    </message>
    <message>
        <source>Timeout error!</source>
        <translation type="vanished">De server doet er te lang over om te antwoorden!</translation>
    </message>
    <message>
        <source>Please check your internet connection and try again.</source>
        <translation type="vanished">Controleer uw internetverbinding en probeer het opnieuw.</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="105"/>
        <source>Error!</source>
        <translation>Er is een fout opgetreden!</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="108"/>
        <source>Version control</source>
        <translation>Versiebeheer</translation>
    </message>
    <message>
        <source>You have latest version and no need to update!</source>
        <translation type="vanished">Er zijn geen updates omdat u de laatste versie in gebruik heeft!</translation>
    </message>
    <message>
        <source>Current version:</source>
        <translation type="vanished">Huidige versie:</translation>
    </message>
    <message>
        <source>Latest version:</source>
        <translation type="vanished">Nieuwste versie:</translation>
    </message>
    <message>
        <source>For update, please check the link:</source>
        <translation type="vanished">Om te updaten, volg de volgende link:</translation>
    </message>
</context>
<context>
    <name>AddressableDockWidget</name>
    <message>
        <location filename="../../widgets/AddressableDockWidget.cpp" line="12"/>
        <source>Sync/unsync offset</source>
        <translation type="unfinished">Sync/unsync offset</translation>
    </message>
</context>
<context>
    <name>AddressableItemContextMenu</name>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="16"/>
        <source>Show in</source>
        <translation>Tonen in</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="17"/>
        <source>Copy address</source>
        <translation>Adres kopiëren</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="18"/>
        <source>Show X-Refs</source>
        <translation>Toon X-Refs</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="19"/>
        <source>Add comment</source>
        <translation>Opmerking toevoegen</translation>
    </message>
</context>
<context>
    <name>AnalClassesModel</name>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="403"/>
        <source>class</source>
        <translation>klasse</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="427"/>
        <source>base</source>
        <translation>base</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="458"/>
        <source>method</source>
        <translation>methode</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="494"/>
        <source>vtable</source>
        <translation>vtable</translation>
    </message>
</context>
<context>
    <name>AnalOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="20"/>
        <source>Analysis</source>
        <translation type="unfinished">Analyse</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="55"/>
        <source>Show verbose information when performing analysis (analysis.verbose)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="68"/>
        <source>Analyze push+ret as jmp (analysis.pushret)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="81"/>
        <source>Verbose output from type analysis (analysis.types.verbose)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="94"/>
        <source>Speculatively set a name for the functions (analysis.autoname)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="107"/>
        <source>Search for new functions following already defined functions (analysis.hasnext)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="120"/>
        <source>Create references for unconditional jumps (analysis.jmp.ref)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="133"/>
        <source>Analyze jump tables in switch statements (analysis.jmp.tbl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="161"/>
        <source>Search boundaries for analysis (analysis.in): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="195"/>
        <source>Pointer depth (analysis.ptrdepth):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="236"/>
        <source>Functions Prelude (analysis.prelude):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="269"/>
        <source>Analyze program</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AnalTask</name>
    <message>
        <location filename="../../common/AnalTask.cpp" line="26"/>
        <source>Analyzing Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="43"/>
        <source>Loading the file...</source>
        <translation>Bestand aan het laden...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="69"/>
        <source>Loading PDB file...</source>
        <translation>PDB bestand aan het laden...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="78"/>
        <source>Loading shellcode...</source>
        <translation>De shellcode wordt ingeladen...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="89"/>
        <source>Executing script...</source>
        <translation>Script aan het uitvoeren...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="98"/>
        <source>Executing analysis...</source>
        <translation>De analyse wordt uitgevoerd...</translation>
    </message>
    <message>
        <source>Analyzing...</source>
        <translation type="vanished">Aan het analyseren...</translation>
    </message>
    <message>
        <source>Running</source>
        <translation type="vanished">Bezig met uitvoeren</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="107"/>
        <source>Analysis complete!</source>
        <translation>De analyse is compleet!</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="109"/>
        <source>Skipping Analysis.</source>
        <translation>De analyse wordt overgeslagen.</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="24"/>
        <source>Initial Analysis</source>
        <translation>Initiële analyse</translation>
    </message>
</context>
<context>
    <name>AppearanceOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="14"/>
        <source>Appearance</source>
        <translation>Uiterlijk</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="28"/>
        <source>Font:</source>
        <translation>Lettertype:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="56"/>
        <source>Select font</source>
        <translation>Selecteer een lettertype</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="76"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="95"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="116"/>
        <source>Language:</source>
        <translation>Taal:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="133"/>
        <source>Interface Theme:</source>
        <translation>Interface Thema:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="160"/>
        <source>Color Theme:</source>
        <translation>Kleurenschema:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="179"/>
        <source>Edit Theme</source>
        <translation>Wijzig Thema</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="193"/>
        <source>Rename</source>
        <translation>Naam Wijzigen</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="241"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="255"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="289"/>
        <source>Use information provided by decompiler when highlighting code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="292"/>
        <source>Decompiler based highlighting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Qt Theme:</source>
        <translation type="vanished">Qt thema:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="147"/>
        <source>Default</source>
        <translation>Standaard</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="152"/>
        <source>Dark</source>
        <translation>Donker</translation>
    </message>
    <message>
        <source>Color Theme</source>
        <translation type="vanished">Thema kleur</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="213"/>
        <source>Copy</source>
        <translation>Kopieer</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="227"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="163"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Taal</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="312"/>
        <source>Save as Default</source>
        <translation>Als standaard opslaan</translation>
    </message>
    <message>
        <source>Enter scheme name</source>
        <translation type="vanished">Voer een naam in voor het kleurenschema</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="136"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="221"/>
        <source>Name:</source>
        <translation>Naam:</translation>
    </message>
    <message>
        <source>Are you sure you want to delete theme </source>
        <translation type="vanished">Ben je zeker dat je het thema wil verwijderen </translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="125"/>
        <source>Theme Editor - &lt;%1&gt;</source>
        <translation>Thema Bewerker - &lt;%1&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="136"/>
        <source>Enter theme name</source>
        <translation>Voer thema naam in</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="137"/>
        <source> - copy</source>
        <translation> - kopiëren</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="143"/>
        <source>Theme Copy</source>
        <translation>Thema kopiëren</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="144"/>
        <source>Theme named %1 already exists.</source>
        <translation>Het thema genaamd %1 bestaat al.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="159"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="168"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="190"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="214"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="229"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="163"/>
        <source>Are you sure you want to delete &lt;b&gt;%1&lt;/b&gt;?</source>
        <translation>Weet je zeker dat je &lt;b&gt;%1&lt;/b&gt; wil verwijderen?</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="185"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="211"/>
        <source>Success</source>
        <translation>Succes</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="186"/>
        <source>Color theme &lt;b&gt;%1&lt;/b&gt; was successfully imported.</source>
        <translation>Kleurenthema &lt;b&gt;%1&lt;/b&gt; is succesvol geïmporteerd.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="212"/>
        <source>Color theme &lt;b&gt;%1&lt;/b&gt; was successfully exported.</source>
        <translation>Kleurenthema &lt;b&gt;%1&lt;/b&gt; is succesvol geëxporteerd.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="221"/>
        <source>Enter new theme name</source>
        <translation>Voer de thema naam in</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="240"/>
        <source>Language settings</source>
        <translation>Taalinstellingen</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="241"/>
        <source>Language will be changed after next application start.</source>
        <translation>De taal zal aangepast worden zodra Cutter opnieuw opgestart wordt.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="245"/>
        <source>Cannot set language, not found in available ones</source>
        <translation>De geselecteerde taal is niet beschikbaar en kan daardoor niet gebruikt worden</translation>
    </message>
</context>
<context>
    <name>AsmOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="14"/>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="65"/>
        <source>Disassembly</source>
        <translation>Disassembly</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="35"/>
        <source>Style</source>
        <translation>Stijl</translation>
    </message>
    <message>
        <source>Show ESIL instead of assembly (asm.esil)</source>
        <translation type="vanished">Toon ESIL in plaats van assembly (asm.esil)</translation>
    </message>
    <message>
        <source>Show pseudocode instead of assembly (asm.pseudo)</source>
        <translation type="vanished">Toon pseudocode in plaats van assembly (asm.pseudo)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="170"/>
        <source>Show offsets (asm.offset)</source>
        <translation>Toon de offsets (asm.offset)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="71"/>
        <source>Display the bytes of each instruction (asm.bytes)</source>
        <translation>Geef de bytes van elke instructie weer (asm.bytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="291"/>
        <source>Comments</source>
        <translation>Commentaar</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="299"/>
        <source>Show opcode description (asm.describe)</source>
        <translation>Toon de opcode beschrijving (asm.describe)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="78"/>
        <source>Syntax (asm.syntax):</source>
        <translation>Syntax (asm.syntax):</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="198"/>
        <source>Lowercase</source>
        <translation>Kleine letters</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="203"/>
        <source>Uppercase (asm.ucase)</source>
        <translation>Hoofdletters (asm.ucase)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="208"/>
        <source>Capitalize (asm.capitalize)</source>
        <translation>Kapitaliseren (asm.capitalize)</translation>
    </message>
    <message>
        <source>Separate bytes with whitespace (asm.bytespace)</source>
        <translation type="vanished">Bytes afzonderen met witruimte (asm.bytespace)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="121"/>
        <source>Indent disassembly based on reflines depth (asm.indent)</source>
        <translation>De disassembly laten inspringen op basis van de reflines (asm.indent)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="98"/>
        <source>Show Disassembly as:</source>
        <translation>Toon Disassembly als:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="190"/>
        <source>Show empty line after every basic block (asm.bb.line)</source>
        <translation>Toon een lege regel na elk basis blok (asm.bb.line)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="152"/>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="307"/>
        <source>Normal</source>
        <translation>Normaal</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="157"/>
        <source>ESIL (asm.esil)</source>
        <translation>ESIL (asm.esil)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="162"/>
        <source>Pseudocode (asm.pseudo)</source>
        <translation>Pseudocode (asm.pseudo)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="216"/>
        <source>Align bytes to the left (asm.lbytes)</source>
        <translation>Bytes naar links uitlijnen (asm.lbytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="223"/>
        <source>Separate bytes with whitespace (asm.bytes.space)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="230"/>
        <source>Display flags&apos; real name (asm.flags.real)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="259"/>
        <source>Show offsets relative to:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="266"/>
        <source>Functions (asm.reloff)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="275"/>
        <source>Flags (asm.reloff.flags)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="312"/>
        <source>Above instructions</source>
        <translation>Bovenstaande instructies</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="317"/>
        <source>Off</source>
        <translation>Uit</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="325"/>
        <source>Show comments:</source>
        <translation>Toon notities:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="475"/>
        <source>Substitute variables (asm.sub.var)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="482"/>
        <source>Substitute entire variable expressions with names (asm.sub.varonly)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="237"/>
        <source>Tabs in assembly (asm.tabs):</source>
        <translation>Tabbladen in assembly (asm.tabs):</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="177"/>
        <source>Tabs before assembly (asm.tabs.off):</source>
        <translation>Tabbladen voor assembly (asm.tabs.off):</translation>
    </message>
    <message>
        <source>Show empty line after every basic block (asm.bbline)</source>
        <translation type="vanished">Toon een lege regel na elk basis blok (asm.bbline)</translation>
    </message>
    <message>
        <source>Show comments at right of assembly (asm.cmt.right)</source>
        <translation type="vanished">Toon de assembly beschrijving aan de rechterkant (asm.cmt.right)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="345"/>
        <source>Column to align comments (asm.cmt.col):</source>
        <translation>De colombreedte aanpassen aan de beschrijvingslengte (asm.cmt.col):</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="355"/>
        <source>Show x-refs (asm.xrefs)</source>
        <translation>Toon x-refs (asm.xrefs)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="362"/>
        <source>Show refpointer information (asm.refptr)</source>
        <translation>Toon refpointer informatie (asm.refptr)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="395"/>
        <source>Metadata</source>
        <translation>Metadata</translation>
    </message>
    <message>
        <source>Show stack pointer (asm.stackptr)</source>
        <translation type="vanished">Toon de stack pointer (asm.stackptr)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="419"/>
        <source>Slow Analysis (asm.slow)</source>
        <translation>Langzame analyse (asm.slow)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="426"/>
        <source>Show jump lines (asm.lines)</source>
        <translation>Toon sprongen in de code met pijlen (asm.lines)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="433"/>
        <source>Show function boundary lines (asm.lines.fcn)</source>
        <translation>Baken de functiegrenzen aan met lijnen (asm.lines.fcn)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="440"/>
        <source>Show offset before flags (asm.flags.off)</source>
        <translation>Toon de offset voor de flags (asm.flags.off)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="447"/>
        <source>Run ESIL emulation analysis (asm.emu)</source>
        <translation>Voer ESIL emulatie analyse uit (asm.emu)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="454"/>
        <source>Show only strings if any in the asm.emu output (emu.str)</source>
        <translation>Toon enkel strings (mits deze er zijn) op basis van asm.emu (emu.str)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="461"/>
        <source>Show size of opcodes in disassembly (asm.size)</source>
        <translation>Toon de grootte van de opcodes in de disassembly (asm.size)</translation>
    </message>
    <message>
        <source>Show bytes (asm.bytes)</source>
        <translation type="vanished">Toon de bytes (asm.bytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="468"/>
        <source>Show variables summary instead of full list (asm.var.summary)</source>
        <translation>Toon de samenvatting van de variabelen in plaats van de volledige lijst (asm.var.summary)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="111"/>
        <source>Number of bytes to display (asm.nbytes):</source>
        <translation>Voer het aantal weer te geven bytes in (asm.nbytes):</translation>
    </message>
    <message>
        <source>Substitute variables (asm.var.sub)</source>
        <translation type="vanished">Vervang variabelen (asm.var.sub)</translation>
    </message>
    <message>
        <source>Substitute entire variable expressions with names (asm.var.subonly)</source>
        <translation type="vanished">Vervang de gehele variabele in assembly code met de naam van de variabele (asm.var.subonly)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="538"/>
        <source>Save as Default</source>
        <translation>Opslaan als standaard</translation>
    </message>
</context>
<context>
    <name>AsyncTaskDialog</name>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.ui" line="14"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.ui" line="20"/>
        <source>Time</source>
        <translation>Tijd</translation>
    </message>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="43"/>
        <source>Running for</source>
        <translation>Verstreken tijd</translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="45"/>
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation>
            <numerusform>%n uur</numerusform>
            <numerusform>%n uur</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="49"/>
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation>
            <numerusform>%n minuut</numerusform>
            <numerusform>%n minuut</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="52"/>
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation>
            <numerusform>%n seconden</numerusform>
            <numerusform>%n seconden</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>AttachProcDialog</name>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="14"/>
        <source>Select process to attach...</source>
        <translation>Selecteer een proces om aan te koppelen...</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="46"/>
        <source>Processes with same name as currently open file:</source>
        <translation>Processen met dezelfde naam als het huidige open bestand:</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="94"/>
        <source>All processes:</source>
        <translation>Alle processen:</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="138"/>
        <source>Quick Filter</source>
        <translation>Snelle filter</translation>
    </message>
</context>
<context>
    <name>BacktraceWidget</name>
    <message>
        <source>Func Name</source>
        <translation type="vanished">Functienaam</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="16"/>
        <source>Function</source>
        <translation>Functie</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="19"/>
        <source>Description</source>
        <translation>Beschrijving</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="20"/>
        <source>Frame Size</source>
        <translation>Frame grootte</translation>
    </message>
</context>
<context>
    <name>Base64EnDecodedWriteDialog</name>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="14"/>
        <source>Base64 Encode/Decode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="24"/>
        <source>String:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="38"/>
        <source>Decode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="48"/>
        <source>Encode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BinClassesModel</name>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="121"/>
        <source>method</source>
        <translation>methode</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="145"/>
        <source>field</source>
        <translation>veld</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="167"/>
        <source>base class</source>
        <translation>basis klasse</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="187"/>
        <source>class</source>
        <translation>klasse</translation>
    </message>
</context>
<context>
    <name>BreakpointModel</name>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="64"/>
        <source>HW %1</source>
        <translation>HW %1</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="66"/>
        <source>SW</source>
        <translation>SW</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="101"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="103"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="105"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="109"/>
        <source>Enabled</source>
        <translation>Ingeschakeld</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="111"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="vanished">Permissies</translation>
    </message>
    <message>
        <source>Hardware bp</source>
        <translation type="vanished">Hardwarematig breakpoint</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="107"/>
        <source>Tracing</source>
        <translation>Tracing</translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="vanished">Actief</translation>
    </message>
</context>
<context>
    <name>BreakpointWidget</name>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="58"/>
        <source>Add new breakpoint</source>
        <translation>Voeg nieuw breakpoint toe</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="65"/>
        <location filename="../../widgets/BreakpointWidget.cpp" line="196"/>
        <source>Delete breakpoint</source>
        <translation>Verwijder breakpoint</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="72"/>
        <source>Delete all breakpoints</source>
        <translation>Verwijder alle breakpoints</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="202"/>
        <source>Toggle breakpoint</source>
        <translation>Zet breakpoint aan/uit</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="208"/>
        <source>Edit</source>
        <translation>Bewerken</translation>
    </message>
</context>
<context>
    <name>BreakpointsDialog</name>
    <message>
        <source>Add breakpoints</source>
        <translation type="vanished">Voeg breakpoints toe</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="14"/>
        <source>Add/Edit breakpoint</source>
        <translation>Voeg toe/bewerk breakpoint</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="22"/>
        <source>Position</source>
        <translation>Positie</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="59"/>
        <source>Condition</source>
        <translation>Voorwaarde</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="91"/>
        <source>?v $.rax-0x6  # break when rax is 6</source>
        <translation>?v $.rax-0x6  # break wanneer rax gelijk is aan 6</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="99"/>
        <source>Module</source>
        <translation>Module</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="126"/>
        <source>Type/Options</source>
        <translation>Type/Opties</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="132"/>
        <source>Enabled</source>
        <translation>Ingeschakeld</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="142"/>
        <source>Software</source>
        <translation>Software</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="152"/>
        <source>Hardware</source>
        <translation>Hardware</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="180"/>
        <source>Read</source>
        <translation>Lees</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="187"/>
        <source>Write</source>
        <translation>Schrijf</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="194"/>
        <source>Execute</source>
        <translation>Uitvoeren</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="206"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="217"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="222"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="227"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="232"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="261"/>
        <source>Action</source>
        <translation>Actie</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="267"/>
        <source>Trace</source>
        <translation>Traceren</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="276"/>
        <source>Command</source>
        <translation>Commando</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="21"/>
        <source>Edit breakpoint</source>
        <translation>Bewerk breakpoint</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="23"/>
        <source>New breakpoint</source>
        <translation>Nieuw breakpoint</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="32"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="32"/>
        <source>Address or expression calculated when creating breakpoint</source>
        <translation>Adres of expressie wat berekend is bij het maken van het breakpoint</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="34"/>
        <source>Named</source>
        <translation>Genoemd</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="34"/>
        <source>Expression - stored as expression</source>
        <translation>Expressie - opgeslagen als expressie</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="35"/>
        <source>Module offset</source>
        <translation>Offset van de module</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="35"/>
        <source>Offset relative to module</source>
        <translation>Relatieve offset van de module</translation>
    </message>
</context>
<context>
    <name>CallGraphWidget</name>
    <message>
        <location filename="../../widgets/CallGraph.cpp" line="23"/>
        <source>Global Callgraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CallGraph.cpp" line="23"/>
        <source>Callgraph</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClassesModel</name>
    <message>
        <source>method</source>
        <translation type="vanished">methode</translation>
    </message>
    <message>
        <source>field</source>
        <translation type="vanished">veld</translation>
    </message>
    <message>
        <source>class</source>
        <translation type="vanished">klasse</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="19"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="21"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="23"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="25"/>
        <source>VTable</source>
        <translation>VTable</translation>
    </message>
</context>
<context>
    <name>ClassesWidget</name>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="79"/>
        <source>Source:</source>
        <translation>Bron:</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="93"/>
        <source>Binary Info (Fixed)</source>
        <translation>Binary informatie (vaste waarde)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="98"/>
        <source>Analysis (Editable)</source>
        <translation>Analyse (bewerkbaar)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="109"/>
        <source>Seek to VTable</source>
        <translation>Opzoeken in de VTable</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="114"/>
        <source>Edit Method</source>
        <translation>Bewerk methode</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="119"/>
        <source>Add Method</source>
        <translation>Voeg methode toe</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="124"/>
        <location filename="../../widgets/ClassesWidget.cpp" line="743"/>
        <source>Create new Class</source>
        <translation>Maak nieuwe klasse</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="129"/>
        <source>Rename Class</source>
        <translation>Hernoem klasse</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="134"/>
        <location filename="../../widgets/ClassesWidget.cpp" line="759"/>
        <source>Delete Class</source>
        <translation>Verwijder klasse</translation>
    </message>
    <message>
        <source>Flags (Editable)</source>
        <translation type="vanished">Flags (wijzigen mogelijk)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="695"/>
        <source>Missing VTable in class</source>
        <translation>Klasse ontbreekt in de VTable</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="696"/>
        <source>The class %1 does not have any VTable!</source>
        <translation>Klasse %1 heeft geen VTable!</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="743"/>
        <source>Class Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="778"/>
        <source>Class name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Class Name</source>
        <translation type="vanished">Klasse Naam</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="760"/>
        <source>Are you sure you want to delete the class %1?</source>
        <translation>Weet je zeker dat je klasse %1 wil verwijderen?</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="777"/>
        <source>Rename Class %1</source>
        <translation>Hernoem klasse %1</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="87"/>
        <source>Val:</source>
        <translation>Val:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="118"/>
        <source>Sat:</source>
        <translation>Sat:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="149"/>
        <source>Hue:</source>
        <translation>Hue:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="184"/>
        <source>Red:</source>
        <translation>Rood:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="215"/>
        <source>Green:</source>
        <translation>Groen:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="246"/>
        <source>Blue:</source>
        <translation>Blauw:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="281"/>
        <source>Hex:</source>
        <translation>Hex:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="288"/>
        <source>\#HHHHHH</source>
        <translation>\#HHHHHH</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="312"/>
        <source>Pick color from screen</source>
        <translation>Kies een kleur van het scherm</translation>
    </message>
</context>
<context>
    <name>ColorSchemeFileSaver</name>
    <message>
        <source>Standard themes not found!</source>
        <translation type="vanished">De standaard themas kunnen niet gevonden worden!</translation>
    </message>
    <message>
        <source>The radare2 standard themes could not be found! This probably means radare2 is not properly installed. If you think it is open an issue please.</source>
        <translation type="vanished">De standaard themas van radare2 kunnen niet gevonden worden. Dit komt waarschijnlijk omdat radare2 niet correct geinstalleerd is. Als u denkt dat het probleem een andere oorzaak heeft, open dan een ticket.</translation>
    </message>
</context>
<context>
    <name>ColorSchemePrefWidget</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Form</translation>
    </message>
    <message>
        <source>Set Default</source>
        <translation type="vanished">Als standaard instellen</translation>
    </message>
</context>
<context>
    <name>ColorThemeEditDialog</name>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="26"/>
        <source>Color Theme:</source>
        <translation>Kleurthema:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="55"/>
        <source>Search</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="35"/>
        <source>Disassembly Preview</source>
        <translation>Disassembly voorbeeld</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="79"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="93"/>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="144"/>
        <source>Unsaved changes</source>
        <translation>Onopgeslagen wijzigingen</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="94"/>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="145"/>
        <source>Are you sure you want to exit without saving? All changes will be lost.</source>
        <translation>Weet je zeker dat je wil afsluiten zonder op te slaan? Alle wijzigingen zullen verloren gaan.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="159"/>
        <source>Theme Editor - &lt;%1&gt;</source>
        <translation>Thema Bewerker - &lt;%1&gt;</translation>
    </message>
</context>
<context>
    <name>ColorThemeWorker</name>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="45"/>
        <source>Standard themes not found</source>
        <translation>De standaard thema&apos;s kunnen niet gevonden worden</translation>
    </message>
    <message>
        <source>The radare2 standard themes could not be found. Most likely, radare2 is not properly installed.</source>
        <translation type="vanished">De standaard thema&apos;s van radare2 kunnen niet gevonden worden. Radare2 is waarschijnlijk niet juist geïnstalleerd.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="74"/>
        <location filename="../../common/ColorThemeWorker.cpp" line="202"/>
        <source>Theme &lt;b&gt;%1&lt;/b&gt; does not exist.</source>
        <translation>Het thema genaamd &lt;b&gt;%1&lt;/b&gt; bestaat niet.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="84"/>
        <source>The file &lt;b&gt;%1&lt;/b&gt; cannot be opened.</source>
        <translation>Het bestand &lt;b&gt;%1&lt;/b&gt; kan niet geopend worden.</translation>
    </message>
    <message>
        <source>You can not delete standard radare2 color themes.</source>
        <translation type="vanished">Je kunt de standaard kleurenthemas van radare2 niet verwijderen.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="46"/>
        <source>The Rizin standard themes could not be found in &apos;%1&apos;. Most likely, Rizin is not properly installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="199"/>
        <source>You can not delete standard Rizin color themes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="207"/>
        <source>You have no permission to write to &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Je hebt geen bevoegdheid om naar &lt;b&gt;%1&lt;/b&gt; te schrijven</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="210"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; can not be opened.</source>
        <translation>Het bestaand &lt;b&gt;%1&lt;/b&gt; kan niet geopend worden.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="213"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; can not be removed.</source>
        <translation>Het bestand &lt;b&gt;%1&lt;/b&gt; kan niet verwijderd worden.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="222"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; does not exist.</source>
        <translation>Het bestand &lt;b&gt;%1&lt;/b&gt; bestaat niet.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="228"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; could not be opened. Please make sure you have access to it and try again.</source>
        <translation>Het bestand &lt;b&gt;%1&lt;/b&gt; kan niet geopend worden. Controleer of je toegang hebt tot het bestand, en probeer opnieuw.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="232"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; is not a Cutter color theme</source>
        <translation>Het bestand &lt;b&gt;%1&lt;/b&gt; is geen Cutter kleurenthema</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="237"/>
        <source>A color theme named &lt;b&gt;%1&lt;/b&gt; already exists.</source>
        <translation>Een kleurenthema genaamd &lt;b&gt;%1&lt;/b&gt; bestaat al.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="243"/>
        <source>Error occurred during importing. Please make sure you have an access to the directory &lt;b&gt;%1&lt;/b&gt; and try again.</source>
        <translation>Er is een fout opgetreden tijdens het importeren van &lt;b&gt;%1&lt;/b&gt;. Controleer of je toegang hebt tot de map en probeer het opnieuw.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="253"/>
        <source>A color theme named &lt;b&gt;&quot;%1&quot;&lt;/b&gt; already exists.</source>
        <translation>Een kleurenschema genaamd &lt;b&gt;&quot;%1&quot;&lt;/b&gt; bestaat al.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="257"/>
        <source>You can not rename standard Rizin themes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can not rename standard radare2 themes.</source>
        <translation type="vanished">Je kunt de standaard radare2 thema&apos;s niet hernoemen.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="263"/>
        <source>Something went wrong during renaming. Please make sure you have access to the directory &lt;b&gt;&quot;%1&quot;&lt;/b&gt;.</source>
        <translation>Er is iets mis gegaan tijdens het hernoemen van &lt;b&gt;&quot;%1&quot;&lt;/b&gt;. Controleer of toegang hebt tot de map.</translation>
    </message>
</context>
<context>
    <name>ComboQuickFilterView</name>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="32"/>
        <source>Quick Filter</source>
        <translation>Snelle filter</translation>
    </message>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="39"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
</context>
<context>
    <name>CommandTask</name>
    <message>
        <location filename="../../common/CommandTask.h" line="23"/>
        <source>Running Command</source>
        <translation>Huidige commando</translation>
    </message>
</context>
<context>
    <name>CommentsDialog</name>
    <message>
        <location filename="../../dialogs/CommentsDialog.ui" line="14"/>
        <source>Comment</source>
        <translation>Opmerking</translation>
    </message>
    <message>
        <location filename="../../dialogs/CommentsDialog.cpp" line="43"/>
        <source>Add Comment at %1</source>
        <translation>Voeg commentaar toe op adres %1</translation>
    </message>
    <message>
        <location filename="../../dialogs/CommentsDialog.cpp" line="45"/>
        <source>Edit Comment at %1</source>
        <translation>Wijzig commentaar op %1</translation>
    </message>
</context>
<context>
    <name>CommentsModel</name>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="157"/>
        <source>Function/Offset</source>
        <translation>Functie/Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="159"/>
        <location filename="../../widgets/CommentsWidget.cpp" line="170"/>
        <source>Comment</source>
        <translation>Opmerking</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="166"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="168"/>
        <source>Function</source>
        <translation>Functie</translation>
    </message>
</context>
<context>
    <name>CommentsWidget</name>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="236"/>
        <source>Horizontal</source>
        <translation>Horizontaal</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="239"/>
        <source>Comments</source>
        <translation>Commentaar</translation>
    </message>
    <message>
        <source>Horizontal view</source>
        <translation type="vanished">Horizontale weergave</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="237"/>
        <source>Vertical</source>
        <translation>Verticaal</translation>
    </message>
    <message>
        <source>Vertical view</source>
        <translation type="vanished">Verticale weergave</translation>
    </message>
</context>
<context>
    <name>Configuration</name>
    <message>
        <location filename="../../common/Configuration.cpp" line="138"/>
        <source>Critical!</source>
        <translation>Kritisch!</translation>
    </message>
    <message>
        <location filename="../../common/Configuration.cpp" line="139"/>
        <source>!!! Settings are not writable! Make sure you have a write access to &quot;%1&quot;</source>
        <translation>!!! De instellingen kunnen niet gewijzigd worden! Controleer of je schrijf toegang hebt tot &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>ConsoleWidget</name>
    <message>
        <source>R2 Console</source>
        <translation type="vanished">R2 Console</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="93"/>
        <source>Rizin Console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="98"/>
        <source>Debugee Input</source>
        <translation>Debugee invoer</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="115"/>
        <source> Type &quot;?&quot; for help</source>
        <translation> Type &quot;?&quot; voor hulp</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="137"/>
        <source> Enter input for the debugee</source>
        <translation> Geef invoer voor de debugee</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="153"/>
        <source>Execute command</source>
        <translation>Voer commando uit</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="159"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.cpp" line="75"/>
        <source>Clear Output</source>
        <translation>Output opschonen</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.cpp" line="84"/>
        <source>Wrap Lines</source>
        <translation>Wrap regels</translation>
    </message>
</context>
<context>
    <name>CutterCore</name>
    <message>
        <location filename="../../core/Cutter.cpp" line="1717"/>
        <source>Starting native debug...</source>
        <translation>Native debugging wordt gestart...</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1760"/>
        <source>Starting emulation...</source>
        <translation>Emulatie starten...</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1814"/>
        <source>Connecting to: </source>
        <translation>Verbinding maken met: </translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1853"/>
        <source>Attaching to process (</source>
        <translation>Koppel aan proces (</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2209"/>
        <source>Creating debug tracepoint...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2245"/>
        <source>Stopping debug session...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2279"/>
        <source>Breakpoint error</source>
        <translation>Breakpoint error</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2279"/>
        <source>Failed to create breakpoint</source>
        <translation>Er is een fout opgetreden bij het maken van het breakpoint</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="3144"/>
        <source>Unknown (%1)</source>
        <translation>Onbekend (%1)</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="3396"/>
        <source>Primitive</source>
        <translation>Primitief</translation>
    </message>
</context>
<context>
    <name>CutterGraphView</name>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="30"/>
        <location filename="../../widgets/CutterGraphView.cpp" line="442"/>
        <source>Export Graph</source>
        <translation type="unfinished">Exporteer Grafiek</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="42"/>
        <source>Layout</source>
        <translation type="unfinished">Opmaak</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="43"/>
        <source>Horizontal</source>
        <translation type="unfinished">Horizontaal</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="47"/>
        <source>Grid narrow</source>
        <translation type="unfinished">Smal raster</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="48"/>
        <source>Grid medium</source>
        <translation type="unfinished">Normaal grid</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="49"/>
        <source>Grid wide</source>
        <translation type="unfinished">Rastergrootte</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="63"/>
        <source>Graphviz polyline</source>
        <translation type="unfinished">Graphviz polyline</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="64"/>
        <source>Graphviz ortho</source>
        <translation type="unfinished">Graphviz ortho</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="65"/>
        <source>Graphviz sfdp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="66"/>
        <source>Graphviz neato</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="67"/>
        <source>Graphviz twopi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="68"/>
        <source>Graphviz circo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="409"/>
        <source>PNG (*.png)</source>
        <translation type="unfinished">PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="410"/>
        <source>JPEG (*.jpg)</source>
        <translation type="unfinished">PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="411"/>
        <source>SVG (*.svg)</source>
        <translation type="unfinished">SVG (*.svg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="417"/>
        <source>Graphviz dot (*.dot)</source>
        <translation type="unfinished">Graphviz dot (*.dot)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="418"/>
        <source>Graph Modelling Language (*.gml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="420"/>
        <source>RZ JSON (*.json)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="421"/>
        <source>SDB key-value (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="427"/>
        <source>Graphviz json (*.json)</source>
        <translation type="unfinished">Graphviz json (*.json)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="429"/>
        <source>Graphviz gif (*.gif)</source>
        <translation type="unfinished">Graphviz gif (*.gif)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="431"/>
        <source>Graphviz png (*.png)</source>
        <translation type="unfinished">Graphviz png (*.png)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="433"/>
        <source>Graphviz jpg (*.jpg)</source>
        <translation type="unfinished">Graphviz jpg (*.jpg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="435"/>
        <source>Graphviz PostScript (*.ps)</source>
        <translation type="unfinished">Graphviz PostScript (*.ps)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="437"/>
        <source>Graphviz svg (*.svg)</source>
        <translation type="unfinished">Graphviz svg (*.svg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="460"/>
        <source>Graph Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="461"/>
        <source>Do you really want to export %1 x %2 = %3 pixel bitmap image? Consider using different format.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CutterSeekable</name>
    <message>
        <location filename="../../widgets/AddressableDockWidget.cpp" line="45"/>
        <source> (unsynced)</source>
        <translation> (niet gesynchroniseerd)</translation>
    </message>
    <message>
        <location filename="../../common/CutterSeekable.cpp" line="73"/>
        <source>More than one (%1) references here. Weird behaviour expected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CutterTreeWidget</name>
    <message>
        <location filename="../../widgets/CutterTreeWidget.cpp" line="19"/>
        <source>%1 Items</source>
        <translation>%1 Items</translation>
    </message>
</context>
<context>
    <name>Dashboard</name>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="116"/>
        <source>OVERVIEW</source>
        <translation>OVERZICHT</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="140"/>
        <source>Info</source>
        <translation>Informatie</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="178"/>
        <source>File:</source>
        <translation>Bestand:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="188"/>
        <location filename="../../widgets/Dashboard.ui" line="217"/>
        <location filename="../../widgets/Dashboard.ui" line="246"/>
        <location filename="../../widgets/Dashboard.ui" line="291"/>
        <location filename="../../widgets/Dashboard.ui" line="320"/>
        <location filename="../../widgets/Dashboard.ui" line="349"/>
        <location filename="../../widgets/Dashboard.ui" line="378"/>
        <location filename="../../widgets/Dashboard.ui" line="391"/>
        <location filename="../../widgets/Dashboard.ui" line="436"/>
        <location filename="../../widgets/Dashboard.ui" line="465"/>
        <location filename="../../widgets/Dashboard.ui" line="494"/>
        <location filename="../../widgets/Dashboard.ui" line="523"/>
        <location filename="../../widgets/Dashboard.ui" line="552"/>
        <location filename="../../widgets/Dashboard.ui" line="581"/>
        <location filename="../../widgets/Dashboard.ui" line="610"/>
        <location filename="../../widgets/Dashboard.ui" line="655"/>
        <location filename="../../widgets/Dashboard.ui" line="668"/>
        <location filename="../../widgets/Dashboard.ui" line="710"/>
        <location filename="../../widgets/Dashboard.ui" line="739"/>
        <location filename="../../widgets/Dashboard.ui" line="768"/>
        <location filename="../../widgets/Dashboard.ui" line="797"/>
        <location filename="../../widgets/Dashboard.ui" line="826"/>
        <location filename="../../widgets/Dashboard.ui" line="855"/>
        <location filename="../../widgets/Dashboard.ui" line="884"/>
        <location filename="../../widgets/Dashboard.ui" line="913"/>
        <location filename="../../widgets/Dashboard.ui" line="942"/>
        <source>--</source>
        <translation>--</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="207"/>
        <source>Format:</source>
        <translation>Formaat:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="236"/>
        <source>Mode:</source>
        <translation>Modus:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="265"/>
        <source>Size:</source>
        <translation>Grootte:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="281"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="310"/>
        <source>Class:</source>
        <translation>Klasse:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="339"/>
        <source>Language:</source>
        <translation>Taal:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="368"/>
        <source>Bits:</source>
        <translation>Bits:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="426"/>
        <source>FD:</source>
        <translation>Bestandsbeschrijving:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="455"/>
        <source>Base addr:</source>
        <translation>Basis adres:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="484"/>
        <source>Virtual addr:</source>
        <translation>Virtuele adres:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="513"/>
        <source>Canary:</source>
        <translation>Canary:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="542"/>
        <source>Crypto:</source>
        <translation>Cryptografie:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="571"/>
        <source>NX bit:</source>
        <translation>NX bit:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="600"/>
        <source>PIC:</source>
        <translation>PIC:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="629"/>
        <source>Static:</source>
        <translation>Statisch:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="645"/>
        <source>Relro:</source>
        <translation>Relro:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="700"/>
        <source>Architecture:</source>
        <translation>Architectuur:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="729"/>
        <source>Machine:</source>
        <translation>Machine:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="758"/>
        <source>OS:</source>
        <translation>Besturingssysteem:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="787"/>
        <source>Subsystem:</source>
        <translation>Subsysteem:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="816"/>
        <source>Stripped:</source>
        <translation>Stripped:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="845"/>
        <source>Relocs:</source>
        <translation>Relocs:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="874"/>
        <source>Endianness:</source>
        <translation>Endianness:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="903"/>
        <source>Compiled:</source>
        <translation>Gecompileerd:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="932"/>
        <source>Compiler:</source>
        <translation>Gecompileerd door:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="993"/>
        <source>Certificates</source>
        <translation>Certificaten</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1006"/>
        <source>Version info</source>
        <translation>Versie informatie</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1034"/>
        <source>Hashes</source>
        <translation>Hashes</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1051"/>
        <source>Libraries</source>
        <translation>Libraries</translation>
    </message>
    <message>
        <source>MD5:</source>
        <translation type="vanished">MD5:</translation>
    </message>
    <message>
        <source>SHA1:</source>
        <translation type="vanished">SHA1:</translation>
    </message>
    <message>
        <source>Entropy:</source>
        <translation type="vanished">Entropie:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1083"/>
        <source>Analysis info</source>
        <translation>Analyse informatie</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1107"/>
        <source>Functions:</source>
        <translation>Functies:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1133"/>
        <source>X-Refs:</source>
        <translation>X-Refs:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1159"/>
        <source>Calls:</source>
        <translation>Aangeroepen functies:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1185"/>
        <source>Strings:</source>
        <translation>Strings:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1211"/>
        <source>Symbols:</source>
        <translation>Symbolen:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1237"/>
        <source>Imports:</source>
        <translation>Geïmporteerde functies:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1263"/>
        <source>Analysis coverage:</source>
        <translation>Dekking van de analyse:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1289"/>
        <source>Code size:</source>
        <translation>Code grootte:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1315"/>
        <source>Coverage percent:</source>
        <translation>Dekking percentage:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="124"/>
        <source>&lt;b&gt;Entropy:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="236"/>
        <location filename="../../widgets/Dashboard.cpp" line="256"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="251"/>
        <source>True</source>
        <translation>Waar</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="253"/>
        <source>False</source>
        <translation>Onwaar</translation>
    </message>
</context>
<context>
    <name>DebugActions</name>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="56"/>
        <source>Start debug</source>
        <translation>Debuggen starten</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="37"/>
        <source>Start emulation</source>
        <translation>Emulatie starten</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="38"/>
        <source>Attach to process</source>
        <translation>Koppel aan proces</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="40"/>
        <source>Stop debug</source>
        <translation>Debuggen stoppen</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="41"/>
        <source>Stop emulation</source>
        <translation>Stop emulatie</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="55"/>
        <source>Restart program</source>
        <translation>Programma herstarten</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="42"/>
        <source>Restart emulation</source>
        <translation>Herstart emuleren</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="54"/>
        <source>Continue</source>
        <translation>Doorgaan</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="43"/>
        <source>Continue until main</source>
        <translation>Doorgaan tot hoofdfunctie</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="39"/>
        <source>Connect to a remote debugger</source>
        <translation>Verbinding maken met een remote debugger</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="44"/>
        <source>Continue until call</source>
        <translation type="unfinished">Continue until call</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="45"/>
        <source>Continue until syscall</source>
        <translation type="unfinished">Continue until syscall</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="46"/>
        <source>Continue backwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="47"/>
        <source>Step</source>
        <translation>Stap</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="48"/>
        <source>Step over</source>
        <translation>Stap over</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="49"/>
        <source>Step out</source>
        <translation type="unfinished">Step out</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="50"/>
        <source>Step backwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="51"/>
        <source>Start trace session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="52"/>
        <source>Stop trace session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="53"/>
        <source>Suspend the process</source>
        <translation>Proces opschorten</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="147"/>
        <source>Debugged process exited (</source>
        <translation>Debugged process is beëindigd (</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="267"/>
        <source>Debug is currently in beta.
</source>
        <translation>Debug is momenteel in betta.
</translation>
    </message>
    <message>
        <source>If you encounter any problems or have suggestions, please submit an issue to https://github.com/radareorg/cutter/issues</source>
        <translation type="vanished">Als u problemen ondervindt of suggesties heeft, rapporteer deze dan op https://github.com/radareorg/cutter/issues</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="295"/>
        <source>Error connecting.</source>
        <translation>Verbindingsfout.</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="377"/>
        <source>File &apos;%1&apos; does not have executable permissions.</source>
        <translation type="unfinished">File &apos;%1&apos; does not have executable permissions.</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="348"/>
        <source>Error attaching. No process selected!</source>
        <translation type="unfinished">Error attaching. No process selected!</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="268"/>
        <source>If you encounter any problems or have suggestions, please submit an issue to https://github.com/rizinorg/cutter/issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="282"/>
        <location filename="../../widgets/DebugActions.cpp" line="357"/>
        <source>Detach from process</source>
        <translation type="unfinished">Detach from process</translation>
    </message>
</context>
<context>
    <name>DebugOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="14"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <source>Debug Plugin:</source>
        <translation type="vanished">Debug Plugin:</translation>
    </message>
    <message>
        <source>Program Arguments:</source>
        <translation type="vanished">Programma argumenten:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="30"/>
        <source>Debug plugin:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="42"/>
        <source>ESIL options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="69"/>
        <source>ESIL stack address:</source>
        <translation>ESIL stack address:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="79"/>
        <source>Hide text when zooming out and it is smaller than the given value. Higher values can increase Performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="82"/>
        <source>ESIL stack size:</source>
        <translation>ESIL stack grootte:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="100"/>
        <source>Trace options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="109"/>
        <source>Trace each step during continue in a trace session (dbg.trace_continue)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="112"/>
        <source>Disabling this option means that stepping back after continue will return to the previous PC. Significantly improves performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="54"/>
        <source>Break esil execution when instruction is invalid (esil.breakoninvalid)</source>
        <translation>Pauzeer het uitvoeren van ESIL wanneer de instructie ongeldig is (esil.breakoninvalid)</translation>
    </message>
</context>
<context>
    <name>DebugToolbar</name>
    <message>
        <source>Start debug</source>
        <translation type="vanished">Debuggen starten</translation>
    </message>
    <message>
        <source>Start emulation</source>
        <translation type="vanished">Emuleren starten</translation>
    </message>
    <message>
        <source>Attach to process</source>
        <translation type="vanished">Koppel aan proces</translation>
    </message>
    <message>
        <source>Stop debug</source>
        <translation type="vanished">Debuggen stoppen</translation>
    </message>
    <message>
        <source>Stop emulation</source>
        <translation type="vanished">Emuleren stoppen</translation>
    </message>
    <message>
        <source>Restart program</source>
        <translation type="vanished">Programma herstarten</translation>
    </message>
    <message>
        <source>Restart emulation</source>
        <translation type="vanished">Emuleren herstarten</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">Doorgaan</translation>
    </message>
    <message>
        <source>Continue until main</source>
        <translation type="vanished">Doorgaan tot de hoofdfunctie</translation>
    </message>
    <message>
        <source>Continue until call</source>
        <translation type="vanished">Doorgaan tot het aanroepen van de volgende functie</translation>
    </message>
    <message>
        <source>Continue until syscall</source>
        <translation type="vanished">Doorgaan tot het aanroepen van de volgende systeemfunctie</translation>
    </message>
    <message>
        <source>Step</source>
        <translation type="vanished">Step</translation>
    </message>
    <message>
        <source>Step over</source>
        <translation type="vanished">Step over</translation>
    </message>
    <message>
        <source>Step out</source>
        <translation type="vanished">Step out</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; does not have executable permissions.</source>
        <translation type="vanished">Het bestand &apos;%1&apos; kan niet uitgevoerd worden omdat het geen permissie heeft om uitgevoerd te worden.</translation>
    </message>
    <message>
        <source>Error attaching. No process selected!</source>
        <translation type="vanished">Het aanhangen van de debugger is fout gegaan: er is geen proces geselecteerd!</translation>
    </message>
    <message>
        <source>Detach from process</source>
        <translation type="vanished">Debugger losmaken van proces</translation>
    </message>
</context>
<context>
    <name>DecompilerContextMenu</name>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="26"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="27"/>
        <source>Copy instruction address (&lt;address&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="28"/>
        <source>Copy address of [flag] (&lt;address&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="29"/>
        <source>Show in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="30"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="161"/>
        <source>Add Comment</source>
        <translation type="unfinished">Commentaar toevoegen</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="31"/>
        <source>Delete comment</source>
        <translation type="unfinished">Commentaar verwijderen</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="32"/>
        <source>Rename function at cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="33"/>
        <source>Delete &lt;name&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="34"/>
        <source>Edit variable &lt;name of variable&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="35"/>
        <source>Show X-Refs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="36"/>
        <source>Add/remove breakpoint</source>
        <translation type="unfinished">Breakpoint toevoegen/verwijderen</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="37"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="192"/>
        <source>Advanced breakpoint</source>
        <translation type="unfinished">Geavanceerd breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="39"/>
        <source>Continue until line</source>
        <translation type="unfinished">Doorgaan tot regel</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="40"/>
        <source>Set PC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="164"/>
        <source>Edit Comment</source>
        <translation type="unfinished">Commentaar wijzigen</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="180"/>
        <source>Add breakpoint</source>
        <translation type="unfinished">Voeg breakpoint toe</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="182"/>
        <source>Remove breakpoint</source>
        <translation type="unfinished">Verwijder breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="184"/>
        <source>Remove all breakpoints in line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="191"/>
        <source>Edit breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="195"/>
        <source>Set %1 here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="207"/>
        <source>Rename function %1</source>
        <translation type="unfinished">Hernoem functie %1</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="212"/>
        <source>Rename %1</source>
        <translation type="unfinished">Hernoem %1</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="213"/>
        <source>Remove %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="216"/>
        <source>Add name to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="221"/>
        <source>Copy instruction address (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="227"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="232"/>
        <source>Copy address of %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="236"/>
        <source>Copy address (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="252"/>
        <source>Edit variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="254"/>
        <source>Rename variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="316"/>
        <source>Can&apos;t rename this variable.&lt;br&gt;Only local variables defined in disassembly can be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="335"/>
        <source>Can&apos;t edit this variable.&lt;br&gt;Only local variables defined in disassembly can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="407"/>
        <source>Define this function at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="408"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="415"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="414"/>
        <source>Rename function %2</source>
        <translation type="unfinished">Hernoem functie %2</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="424"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="449"/>
        <source>Rename %2</source>
        <translation type="unfinished">Hernoem %2</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="425"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="432"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="450"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="432"/>
        <source>Add name to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="443"/>
        <source>Rename local variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="444"/>
        <source>Can&apos;t rename this variable. Only local variables defined in disassembly can be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="468"/>
        <source>Edit local variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="469"/>
        <source>Can&apos;t edit this variable. Only local variables defined in disassembly can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="535"/>
        <source>Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="545"/>
        <source>Debug</source>
        <translation type="unfinished">Debug</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="575"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="577"/>
        <source>Show %1 in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="582"/>
        <source>%1 (%2)</source>
        <translation type="unfinished">%1 (%2)</translation>
    </message>
</context>
<context>
    <name>DecompilerWidget</name>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="14"/>
        <location filename="../../widgets/DecompilerWidget.cpp" line="452"/>
        <source>Decompiler</source>
        <translation>Decompiler</translation>
    </message>
    <message>
        <source>Auto Refresh</source>
        <translation type="vanished">Automatisch vernieuwen</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Herlaad</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="53"/>
        <source>Decompiling...</source>
        <translation>Decompiling...</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="75"/>
        <source>Decompiler:</source>
        <translation type="unfinished">Decompiler:</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="33"/>
        <source>Choose an offset and refresh to get decompiled code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="69"/>
        <source>No Decompiler available.</source>
        <translation>Geen decompiler beschikbaar.</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="262"/>
        <source>No function found at this offset. Seek to a function or define one in order to decompile it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Annuleren</translation>
    </message>
    <message>
        <source>Click Refresh to generate Decompiler from current offset.</source>
        <translation type="vanished">Klik op vernieuwen om pseudocode te genereren op basis van de huidige offset.</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="309"/>
        <source>Cannot decompile at this address (Not a function?)</source>
        <translation>Kan niet decompileren op dit adres (Misschien is het geen functie?)</translation>
    </message>
</context>
<context>
    <name>DisassemblerGraphView</name>
    <message>
        <source>Export Graph</source>
        <translation type="vanished">Exporteer Grafiek</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Sync/unsync offset</translation>
    </message>
    <message>
        <source>Grid narrow</source>
        <translation type="vanished">Smal raster</translation>
    </message>
    <message>
        <source>Grid medium</source>
        <translation type="vanished">Normaal grid</translation>
    </message>
    <message>
        <source>Grid wide</source>
        <translation type="vanished">Rastergrootte</translation>
    </message>
    <message>
        <source>Graphviz polyline</source>
        <translation type="vanished">Graphviz polyline</translation>
    </message>
    <message>
        <source>Graphviz polyline LR</source>
        <translation type="vanished">Graphviz polyline LR</translation>
    </message>
    <message>
        <source>Graphviz ortho</source>
        <translation type="vanished">Graphviz ortho</translation>
    </message>
    <message>
        <source>Graphviz ortho LR</source>
        <translation type="vanished">Graphviz ortho LR</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation type="vanished">Opmaak</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="91"/>
        <source>Highlight block</source>
        <translation>Blok markeren</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="109"/>
        <source>Unhighlight block</source>
        <translation>Blokmarkering opheffen</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="119"/>
        <source>Highlight instruction</source>
        <translation>Instructie markeren</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="123"/>
        <source>Unhighlight instruction</source>
        <translation>Instructiemarkering opheffen</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="199"/>
        <source>No function detected. Cannot display graph.</source>
        <translation>Kan geen grafiek weergeven omdat er geen functie gevonden is.</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="214"/>
        <source>Graph</source>
        <translation>Grafiek</translation>
    </message>
    <message>
        <source>Graphviz dot (*.dot)</source>
        <translation type="vanished">Graphviz dot (*.dot)</translation>
    </message>
    <message>
        <source>Graphviz json (*.json)</source>
        <translation type="vanished">Graphviz json (*.json)</translation>
    </message>
    <message>
        <source>Graphviz gif (*.gif)</source>
        <translation type="vanished">Graphviz gif (*.gif)</translation>
    </message>
    <message>
        <source>Graphviz png (*.png)</source>
        <translation type="vanished">Graphviz png (*.png)</translation>
    </message>
    <message>
        <source>Graphviz jpg (*.jpg)</source>
        <translation type="vanished">Graphviz jpg (*.jpg)</translation>
    </message>
    <message>
        <source>Graphviz PostScript (*.ps)</source>
        <translation type="vanished">Graphviz PostScript (*.ps)</translation>
    </message>
    <message>
        <source>Graphviz svg (*.svg)</source>
        <translation type="vanished">Graphviz svg (*.svg)</translation>
    </message>
    <message>
        <source>Graphiz dot (*.dot)</source>
        <translation type="vanished">Graphiz dot (*.dot)</translation>
    </message>
    <message>
        <source>GIF (*.gif)</source>
        <translation type="vanished">GIF (*.gif)</translation>
    </message>
    <message>
        <source>PNG (*.png)</source>
        <translation type="vanished">PNG (*.png)</translation>
    </message>
    <message>
        <source>JPEG (*.jpg)</source>
        <translation type="vanished">PNG (*.png)</translation>
    </message>
    <message>
        <source>PostScript (*.ps)</source>
        <translation type="vanished">PostScript (*.ps)</translation>
    </message>
    <message>
        <source>SVG (*.svg)</source>
        <translation type="vanished">SVG (*.svg)</translation>
    </message>
    <message>
        <source>JSON (*.json)</source>
        <translation type="vanished">JSON (*.json)</translation>
    </message>
</context>
<context>
    <name>DisassemblyContextMenu</name>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="72"/>
        <source>Copy</source>
        <translation>Kopieer</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="75"/>
        <source>Copy address</source>
        <translation>Kopieer adres</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="79"/>
        <source>Show in</source>
        <translation type="unfinished">Show in</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="84"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="549"/>
        <source>Add Comment</source>
        <translation>Commentaar toevoegen</translation>
    </message>
    <message>
        <source>Add Flag</source>
        <translation type="vanished">Vlag toevoegen</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">Hernoem</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="96"/>
        <source>Edit function</source>
        <translation>Functie aanpassen</translation>
    </message>
    <message>
        <source>Rename Flag/Fcn/Var Used Here</source>
        <translation type="vanished">Hernoem de gebruikte Flag/Functie/Variabele hier</translation>
    </message>
    <message>
        <source>Re-type function local vars</source>
        <translation type="vanished">Re-type function local vars</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="100"/>
        <source>Delete comment</source>
        <translation>Commentaar verwijderen</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="104"/>
        <source>Delete flag</source>
        <translation>Flag verwijderen</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="107"/>
        <source>Undefine function</source>
        <translation>Undefine functie</translation>
    </message>
    <message>
        <source>Define function here...</source>
        <translation type="vanished">Definieer functie hier...</translation>
    </message>
    <message>
        <source>Set to Code</source>
        <translation type="vanished">Zet om naar Code</translation>
    </message>
    <message>
        <source>Set as Code</source>
        <translation type="vanished">Zet om naar Code</translation>
    </message>
    <message>
        <source>Set as String</source>
        <translation type="vanished">Zet om naar String</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="133"/>
        <source>Show X-Refs</source>
        <translation>Laat kruisreferenties zien</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="137"/>
        <source>X-Refs for local variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="141"/>
        <source>Show Options</source>
        <translation>Opties tonen</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="172"/>
        <source>Set Immediate Base to...</source>
        <translation>Zet de Immediate Base naar...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="174"/>
        <source>Binary</source>
        <translation>Binary</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="178"/>
        <source>Octal</source>
        <translation>Octaal</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="182"/>
        <source>Decimal</source>
        <translation>Decimaal</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="186"/>
        <source>Hexadecimal</source>
        <translation>Hexadecimaal</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="190"/>
        <source>Network Port</source>
        <translation>Netwerk poort</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="194"/>
        <source>IP Address</source>
        <translation>IP Adres</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="198"/>
        <source>Syscall</source>
        <translation>Syscall</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="202"/>
        <source>String</source>
        <translation>String</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="209"/>
        <source>Set current bits to...</source>
        <translation>Zet de huidige bits naar...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="424"/>
        <source>Rename local &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="429"/>
        <source>Rename flag &quot;%1&quot; (used here)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="826"/>
        <source>New function at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="827"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="841"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="840"/>
        <source>Rename function %2</source>
        <translation type="unfinished">Hernoem functie %2</translation>
    </message>
    <message>
        <source>Set to Data...</source>
        <translation type="vanished">Zet om naar Data...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="111"/>
        <source>Define function here</source>
        <translation>Definieer hier een functie</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="121"/>
        <source>Structure offset</source>
        <translation>Offset van de structure</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="125"/>
        <source>Link Type to Address</source>
        <translation>Link type naar adres</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="226"/>
        <source>Set as...</source>
        <translation>Instellen als...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="228"/>
        <source>Code</source>
        <translation type="unfinished">Code</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="232"/>
        <source>String...</source>
        <translation>String...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="234"/>
        <source>Auto-detect</source>
        <translation>Automatisch detecteren</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="236"/>
        <source>Remove</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <source>Adanced</source>
        <translation type="vanished">Geavanceerd</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="250"/>
        <source>Data...</source>
        <translation>Data...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="252"/>
        <source>Byte</source>
        <translation>Byte</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="256"/>
        <source>Word</source>
        <translation>Word</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="260"/>
        <source>Dword</source>
        <translation>Dword</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="264"/>
        <source>Qword</source>
        <translation>Qword</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="279"/>
        <source>Edit</source>
        <translation>Bewerken</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="281"/>
        <source>Instruction</source>
        <translation>Instructie</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="285"/>
        <source>Nop Instruction</source>
        <translation>Nop instructie</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="289"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="292"/>
        <source>Reverse Jump</source>
        <translation>Omgekeerde Jump</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="298"/>
        <source>Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="310"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="300"/>
        <source>Add/remove breakpoint</source>
        <translation>Breakpoint toevoegen/verwijderen</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="88"/>
        <source>Rename or add flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="92"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="875"/>
        <source>Re-type Local Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="238"/>
        <source>Advanced</source>
        <translation type="unfinished">Geavanceerd</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="303"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="587"/>
        <source>Advanced breakpoint</source>
        <translation>Geavanceerd breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="312"/>
        <source>Continue until line</source>
        <translation>Doorgaan tot regel</translation>
    </message>
    <message>
        <source>%1 (used here)</source>
        <translation type="vanished">%1 (hier gebruikt)</translation>
    </message>
    <message>
        <source>%1 (%2)</source>
        <translation type="vanished">%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="552"/>
        <source>Edit Comment</source>
        <translation>Commentaar wijzigen</translation>
    </message>
    <message>
        <source>Rename function &quot;%1&quot;</source>
        <translation type="vanished">Hernoem functie &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Rename flag &quot;%1&quot;</source>
        <translation type="vanished">Hernoem flag &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="568"/>
        <source>Edit function &quot;%1&quot;</source>
        <translation>Pas functie &quot;%1&quot; aan</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="414"/>
        <source>Add flag at %1 (used here)</source>
        <translation>Voeg flag toe op %1 (hier gebruikt)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="419"/>
        <source>Rename &quot;%1&quot;</source>
        <translation>Hernoem &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Rename &quot;%1&quot; (used here)</source>
        <translation type="vanished">Hernoem &quot;%1&quot; (hier gebruikt)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="585"/>
        <source>Remove breakpoint</source>
        <translation>Verwijder breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="585"/>
        <source>Add breakpoint</source>
        <translation>Voeg breakpoint toe</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="586"/>
        <source>Edit breakpoint</source>
        <translation type="unfinished">Edit breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="601"/>
        <source>X-Refs for %1</source>
        <translation type="unfinished">X-Refs for %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="701"/>
        <source>Edit Instruction at %1</source>
        <translation>Wijzig instructie op %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="755"/>
        <source>Edit Bytes at %1</source>
        <translation>Wijzig bytes op %1</translation>
    </message>
    <message>
        <source>Write error</source>
        <translation type="vanished">Error tijdens het schrijven</translation>
    </message>
    <message>
        <source>Unable to complete write operation. Consider opening in write mode. 

WARNING: In write mode any changes will be commited to disk</source>
        <translation type="vanished">Er is een fout opgetreden tijdens het wegschrijven van de gewenste informatie. Probeer het bestaand in de schrijf-modus te openen.

LET OP: in de schrijf-modus worden alle wijzigingen meteen doorgevoerd op het bestand</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Reopen in write mode and try again</source>
        <translation type="vanished">Heropen het bestand in de schrijf-modus en probeer het opnieuw</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="938"/>
        <source>Wrong address</source>
        <translation>Verkeerde adres</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="939"/>
        <source>Can&apos;t edit string at this address</source>
        <translation>Kan de string niet wijzigen op dit adres</translation>
    </message>
    <message>
        <source>Add Comment at %1</source>
        <translation type="vanished">Voeg commentaar toe op adres %1</translation>
    </message>
    <message>
        <source>Edit Comment at %1</source>
        <translation type="vanished">Wijzig commentaar op %1</translation>
    </message>
    <message>
        <source>Analyze function at %1</source>
        <translation type="vanished">Analyseer functie op %1</translation>
    </message>
    <message>
        <source>Function name</source>
        <translation type="vanished">Functie naam</translation>
    </message>
    <message>
        <source>Rename function %1</source>
        <translation type="vanished">Hernoem functie %1</translation>
    </message>
    <message>
        <source>Rename flag %1</source>
        <translation type="vanished">Hernoem flag %1</translation>
    </message>
    <message>
        <source>Add flag at %1</source>
        <translation type="vanished">Voeg flag toe op %1</translation>
    </message>
    <message>
        <source>Rename %1</source>
        <translation type="vanished">Hernoem %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="876"/>
        <source>You must be in a function to define variable types.</source>
        <translation type="unfinished">You must be in a function to define variable types.</translation>
    </message>
    <message>
        <source>Set Variable Types for Function: %1</source>
        <translation type="vanished">Set variabele type voor functie: %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="1020"/>
        <source>Edit function %1</source>
        <translation>Pas functie %1 aan</translation>
    </message>
</context>
<context>
    <name>DisassemblyWidget</name>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="670"/>
        <source>More than one (%1) references here. Weird behaviour expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="676"/>
        <source>offsetFrom (%1) differs from refs.at(0).from (%(2))</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="721"/>
        <source>Disassembly</source>
        <translation type="unfinished">Disassembly</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Sync/unsync offset</translation>
    </message>
</context>
<context>
    <name>DuplicateFromOffsetDialog</name>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="20"/>
        <source>Duplicate from offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="28"/>
        <source>Offset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="49"/>
        <source>N bytes:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditFunctionDialog</name>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="14"/>
        <source>Edit Function</source>
        <translation>Pas functie aan</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="43"/>
        <source>Name of function</source>
        <translation>Naam van de functie</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="53"/>
        <source>Start address</source>
        <translation>Start adres</translation>
    </message>
    <message>
        <source>End address</source>
        <translation type="vanished">Eind adres</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="63"/>
        <source>Stack size</source>
        <translation>Stack grootte</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="73"/>
        <source>Calling convention</source>
        <translation>Calling convention</translation>
    </message>
</context>
<context>
    <name>EditInstructionDialog</name>
    <message>
        <location filename="../../dialogs/EditInstructionDialog.ui" line="14"/>
        <source>Edit Instruction</source>
        <translation>Wijzig instructie</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditInstructionDialog.ui" line="76"/>
        <source>Unknown Instruction</source>
        <translation>Onbekende instructie</translation>
    </message>
</context>
<context>
    <name>EditMethodDialog</name>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="25"/>
        <source>Class:</source>
        <translation>Klasse:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="32"/>
        <source>Name:</source>
        <translation>Naam:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="42"/>
        <source>Address:</source>
        <translation>Adres:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="52"/>
        <source>Virtual:</source>
        <translation>Virtual:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="66"/>
        <source>Offset in VTable:</source>
        <translation>Offset in de VTable:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.cpp" line="152"/>
        <source>Create Method</source>
        <translation>Maak methode</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.cpp" line="167"/>
        <source>Edit Method</source>
        <translation>Bewerk methode</translation>
    </message>
</context>
<context>
    <name>EditStringDialog</name>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="23"/>
        <source>Edit string</source>
        <translation>Wijzig string</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="66"/>
        <source>Address:</source>
        <translation>Adres:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="52"/>
        <source>Size:</source>
        <translation>Grootte:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="59"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="99"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Annuleren</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
</context>
<context>
    <name>EditVariablesDialog</name>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoge</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="22"/>
        <source>Modify:</source>
        <translation>Wijzig:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="35"/>
        <source>Name:</source>
        <translation>Naam:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="42"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
    <message>
        <source>Set Variable Types for Function: %1</source>
        <translation type="obsolete">Set Variable Types for Function: %1</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.cpp" line="19"/>
        <source>Edit Variables in Function: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportsModel</name>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="60"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="62"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="64"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="66"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="68"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportsWidget</name>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="138"/>
        <source>Exports</source>
        <translation>Exports</translation>
    </message>
</context>
<context>
    <name>FlagDialog</name>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="14"/>
        <source>Add Flag</source>
        <translation>Vlag toevoegen</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="20"/>
        <source>Add flag at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="38"/>
        <source>Flag:</source>
        <translation>Vlag:</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="61"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="80"/>
        <source>Size:</source>
        <translation>Grootte:</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.cpp" line="24"/>
        <source>Edit flag at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.cpp" line="26"/>
        <source>Add flag at %1</source>
        <translation type="unfinished">Voeg flag toe op %1</translation>
    </message>
</context>
<context>
    <name>FlagsModel</name>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="64"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="66"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="68"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="70"/>
        <source>Real Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="72"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FlagsWidget</name>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="79"/>
        <source>Quick Filter</source>
        <translation>Snelle filters</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="86"/>
        <source>Flagspace:</source>
        <translation>Flagspace:</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="99"/>
        <source>Rename</source>
        <translation>Hernoem</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="102"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="110"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="113"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="214"/>
        <source>Rename flag %1</source>
        <translation type="unfinished">Hernoem flag %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="215"/>
        <source>Flag name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="244"/>
        <source>(all)</source>
        <translation>(alles)</translation>
    </message>
</context>
<context>
    <name>FunctionModel</name>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="123"/>
        <source>Offset: %1</source>
        <translation>Offset: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="125"/>
        <source>Size: %1</source>
        <translation>Grootte: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="127"/>
        <source>Import: %1</source>
        <translation>Importeer: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="128"/>
        <source>true</source>
        <translation>waar</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="128"/>
        <source>false</source>
        <translation>onwaar</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="130"/>
        <source>Nargs: %1</source>
        <translation>Nargs: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="132"/>
        <source>Nbbs: %1</source>
        <translation>Nbbs: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="134"/>
        <source>Nlocals: %1</source>
        <translation>Nlocals: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="281"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cyclomatic complexity: %1</source>
        <translation type="vanished">Cyclomatische complexiteit: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="136"/>
        <source>Call type: %1</source>
        <translation>Call type: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="138"/>
        <source>Edges: %1</source>
        <translation>Edges: %1</translation>
    </message>
    <message>
        <source>Cost: %1</source>
        <translation type="vanished">Cost: %1</translation>
    </message>
    <message>
        <source>Calls/OutDegree: %1</source>
        <translation type="vanished">Calls/OutDegree: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="140"/>
        <source>StackFrame: %1</source>
        <translation>StackFrame: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="142"/>
        <source>Comment: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="221"/>
        <source>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Disassembly voorbeeldweergave&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="226"/>
        <source>&lt;div&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div&gt;&lt;strong&gt;Hoogtepunten&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;div&gt;&lt;strong&gt;Summary&lt;/strong&gt;:&lt;br&gt;</source>
        <translation type="vanished">&lt;div&gt;&lt;strong&gt;Samenvatting&lt;/strong&gt;:&lt;br&gt;</translation>
    </message>
    <message>
        <source>Size:&amp;nbsp;%1,&amp;nbsp;Cyclomatic complexity:&amp;nbsp;%2,&amp;nbsp;Basic blocks:&amp;nbsp;%3</source>
        <translation type="vanished">Grootte:&amp;nbsp;%1&amp;nbsp;Cyclomatische complexiteit:&amp;nbsp;%2&amp;nbsp;Basis blokken:&amp;nbsp;%3</translation>
    </message>
    <message>
        <source>&lt;/div&gt;&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="vanished">&lt;/div&gt;&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Disassembly voorbeeld&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="vanished">&lt;div style=&quot;margin-top:10px;&quot;&gt;&lt;strong&gt;Hoogtepunten&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="257"/>
        <location filename="../../widgets/FunctionsWidget.cpp" line="261"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="263"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="265"/>
        <source>Imp.</source>
        <translation>Imp.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="267"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="269"/>
        <source>Nargs</source>
        <translation>Nargs</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="273"/>
        <source>Nbbs</source>
        <translation>Nbbs</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="271"/>
        <source>Nlocals</source>
        <translation>Nlocals</translation>
    </message>
    <message>
        <source>Cyclo. Comp.</source>
        <translation type="vanished">Cyclo. Compl.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="275"/>
        <source>Call type</source>
        <translation>Call type</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="277"/>
        <source>Edges</source>
        <translation>Edges</translation>
    </message>
    <message>
        <source>Cost</source>
        <translation type="vanished">Cost</translation>
    </message>
    <message>
        <source>Calls/OutDeg.</source>
        <translation type="vanished">Calls/OutDeg.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="279"/>
        <source>StackFrame</source>
        <translation>StackFrame</translation>
    </message>
</context>
<context>
    <name>FunctionsTask</name>
    <message>
        <location filename="../../common/FunctionsTask.h" line="13"/>
        <source>Fetching Functions</source>
        <translation>Instructies ophalen</translation>
    </message>
</context>
<context>
    <name>FunctionsWidget</name>
    <message>
        <source>Add comment</source>
        <translation type="vanished">Commentaar toevoegen</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="442"/>
        <source>Rename</source>
        <translation>Hernoem</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="443"/>
        <source>Undefine</source>
        <translation>Verwijder defenitie</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="447"/>
        <source>Functions</source>
        <translation>Functies</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="549"/>
        <source>Rename function %1</source>
        <translation type="unfinished">Hernoem functie %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="550"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X-Refs</source>
        <translation type="vanished">X-Refs</translation>
    </message>
    <message>
        <source>Cross references</source>
        <translation type="vanished">Kruisverwijzingen</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="444"/>
        <source>Horizontal</source>
        <translation>Horizontaal</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="445"/>
        <source>Vertical</source>
        <translation>Verticaal</translation>
    </message>
</context>
<context>
    <name>GraphOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="14"/>
        <source>Graph</source>
        <translation>Grafiek</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="22"/>
        <source>Graph Block Options </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="31"/>
        <source>The offset of the first instruction of a graph block is shown in the header of the respective graph block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="34"/>
        <source>Show offset of the first instruction in each graph block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="41"/>
        <source>Show offset for each instruction (graph.offset)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="50"/>
        <source>Maximum Line Length:</source>
        <translation>Maximale regellengte:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="70"/>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="80"/>
        <source>Hide text when zooming out and it is smaller than the given value. Higher values can increase Performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="73"/>
        <source>Minimum Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="98"/>
        <source>Graph Layout Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="117"/>
        <source>Vertical</source>
        <translation type="unfinished">Verticaal</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="140"/>
        <source>Horizontal</source>
        <translation type="unfinished">Horizontaal</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="176"/>
        <source>Block spacing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="183"/>
        <source>Edge spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="193"/>
        <source>Bitmap Export Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="202"/>
        <source>Export Transparent Bitmap Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="211"/>
        <source>Graph Bitmap Export Scale: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="218"/>
        <source>%</source>
        <translation type="unfinished">%</translation>
    </message>
    <message>
        <source>Show offsets (graph.offset)</source>
        <translation type="vanished">Toon offsets (graph.offset)</translation>
    </message>
    <message>
        <source>Show offsets (graph.offset) </source>
        <translation type="vanished">Toon offsets (graph.offset) </translation>
    </message>
</context>
<context>
    <name>HeadersModel</name>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="55"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="57"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="59"/>
        <source>Value</source>
        <translation>Waarde</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="61"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HeadersWidget</name>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="120"/>
        <source>Headers</source>
        <translation type="unfinished">Headers</translation>
    </message>
</context>
<context>
    <name>HexWidget</name>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="67"/>
        <source>Hexadecimal</source>
        <translation type="unfinished">Hexadecimal</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="68"/>
        <source>Octal</source>
        <translation type="unfinished">Octal</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="69"/>
        <source>Decimal</source>
        <translation type="unfinished">Decimal</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="70"/>
        <source>Signed decimal</source>
        <translation>Signed decimal</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="71"/>
        <source>Float</source>
        <translation>Float</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="85"/>
        <source>Bytes per row</source>
        <translation>Bytes per rij</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="95"/>
        <source>Power of 2</source>
        <translation>Macht van 2</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="102"/>
        <source>Big Endian</source>
        <translation>Big Endian</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="107"/>
        <source>Bytes as pairs</source>
        <translation>Bytes als paren</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="111"/>
        <source>Copy</source>
        <translation type="unfinished">Copy</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="117"/>
        <source>Copy address</source>
        <translation type="unfinished">Copy address</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="123"/>
        <source>Select range</source>
        <translation>Selecteer bereik</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="130"/>
        <location filename="../../widgets/HexWidget.cpp" line="705"/>
        <source>Write string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="134"/>
        <source>Write length and string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="138"/>
        <location filename="../../widgets/HexWidget.cpp" line="842"/>
        <source>Write wide string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="142"/>
        <source>Write zero terminated string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="146"/>
        <source>Write De\Encoded Base64 string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="151"/>
        <location filename="../../widgets/HexWidget.cpp" line="745"/>
        <source>Write zeros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="155"/>
        <source>Write random bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="159"/>
        <source>Duplicate from offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="163"/>
        <source>Increment/Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="620"/>
        <source>Item size:</source>
        <translation>Item grootte:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="622"/>
        <source>Item format:</source>
        <translation>Item formaat:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="627"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="705"/>
        <location filename="../../widgets/HexWidget.cpp" line="826"/>
        <location filename="../../widgets/HexWidget.cpp" line="842"/>
        <location filename="../../widgets/HexWidget.cpp" line="857"/>
        <source>String:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="745"/>
        <source>Number of zeros:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="769"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="770"/>
        <source>Error occured during decoding your input.
Please, make sure, that it is a valid base64 string and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="793"/>
        <source>Write random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="793"/>
        <source>Number of bytes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="826"/>
        <source>Write Pascal string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="857"/>
        <source>Write zero-terminated string</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HexdumpRangeDialog</name>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="14"/>
        <source>Select Block</source>
        <translation>Selecteer Block</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="47"/>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="79"/>
        <source>Exclusive end address</source>
        <translation>Exclusief eindadres</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="50"/>
        <source>End Address:</source>
        <translation>Eind adres:</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="65"/>
        <source>Start Address:</source>
        <translation>Start adres:</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="99"/>
        <source>Length:</source>
        <translation>Lengte:</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="115"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#ff8585;&quot;&gt;Big selection might cause a delay&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#ff8585;&quot;&gt;Een grote selectie kan het programma vertragen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>HexdumpWidget</name>
    <message>
        <source>0  1  2  3 ...</source>
        <translation type="vanished">0 1 2 3 ...</translation>
    </message>
    <message>
        <source>0123...</source>
        <translation type="vanished">0123...</translation>
    </message>
    <message>
        <source>Offset</source>
        <translation type="obsolete">Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="70"/>
        <source>Parsing</source>
        <translation>Aan het analyseren (parsing)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="50"/>
        <source>Select bytes to display information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="143"/>
        <source>Disassembly</source>
        <translation type="unfinished">Disassembly</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="144"/>
        <source>String</source>
        <translation>String</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="145"/>
        <source>Assembler</source>
        <translation>Assembler</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="146"/>
        <source>C bytes</source>
        <translation>C bytes</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="147"/>
        <source>C bytes with instructions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="148"/>
        <source>C half-words (2 byte)</source>
        <translation>C halve-words (2 byte)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="149"/>
        <source>C words (4 byte)</source>
        <translation>C words (4 byte)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="150"/>
        <source>C dwords (8 byte)</source>
        <translation>C dwords (8byte)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="151"/>
        <source>Python</source>
        <translation>Python</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="152"/>
        <source>JSON</source>
        <translation>JSOn</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="153"/>
        <source>JavaScript</source>
        <translation>JavaScript</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="154"/>
        <source>Yara</source>
        <translation>Yara</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="120"/>
        <source>Endian</source>
        <translation>Endian</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="131"/>
        <source>Little</source>
        <translation>Kleine</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="136"/>
        <source>Big</source>
        <translation>Grote</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="191"/>
        <source>Arch</source>
        <translation>Architectuur</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="220"/>
        <source>Bits</source>
        <translation>Bits</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="231"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="236"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="241"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="318"/>
        <source>SHA256:</source>
        <translation type="unfinished">SHA1: {256:?}</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="357"/>
        <source>Copy SHA256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="396"/>
        <source>Copy CRC32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="466"/>
        <source>MD5:</source>
        <translation type="unfinished">MD5:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="479"/>
        <source>CRC32:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="350"/>
        <source>SHA1:</source>
        <translation type="unfinished">SHA1:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="434"/>
        <source>Entropy:</source>
        <translation>Entropie:</translation>
    </message>
    <message>
        <source>Hexdump side panel</source>
        <translation type="vanished">Paneel met hexdump aan de zijkant</translation>
    </message>
    <message>
        <source>Undefine</source>
        <translation type="vanished">Verwijder defenitie</translation>
    </message>
    <message>
        <source>Copy all</source>
        <translation type="vanished">Alles kopiëren</translation>
    </message>
    <message>
        <source>Copy bytes</source>
        <translation type="vanished">Kopieer bytes</translation>
    </message>
    <message>
        <source>Copy disasm</source>
        <translation type="vanished">Kopieer diassembly</translation>
    </message>
    <message>
        <source>Copy Hexpair</source>
        <translation type="vanished">Kopieer hexpair</translation>
    </message>
    <message>
        <source>Copy ASCII</source>
        <translation type="vanished">Kopieer ASCII</translation>
    </message>
    <message>
        <source>Copy Text</source>
        <translation type="vanished">Kopieer tekst</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="vanished">2</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="vanished">4</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="vanished">8</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Edit</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Plakken</translation>
    </message>
    <message>
        <source>Insert Hex</source>
        <translation type="vanished">Hex invoegen</translation>
    </message>
    <message>
        <source>Insert String</source>
        <translation type="vanished">String invoegen</translation>
    </message>
    <message>
        <source>Hex</source>
        <translation type="vanished">Hex</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation type="obsolete">Octal</translation>
    </message>
    <message>
        <source>Half-word</source>
        <translation type="vanished">Half-word</translation>
    </message>
    <message>
        <source>Word</source>
        <translation type="vanished">Word</translation>
    </message>
    <message>
        <source>Quad-word</source>
        <translation type="vanished">Quad-word</translation>
    </message>
    <message>
        <source>Emoji</source>
        <translation type="vanished">Emoji</translation>
    </message>
    <message>
        <source>1 byte</source>
        <translation type="vanished">1 byte</translation>
    </message>
    <message>
        <source>2 bytes</source>
        <translation type="vanished">2 bytes</translation>
    </message>
    <message>
        <source>4 bytes</source>
        <translation type="vanished">4 bytes</translation>
    </message>
    <message>
        <source>Select Block...</source>
        <translation type="vanished">Selecteer Block...</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="219"/>
        <source>Hexdump</source>
        <translation>Hexdump</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation type="vanished">Kolommen</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">Formaat</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="obsolete">Sync/unsync offset</translation>
    </message>
    <message>
        <source>Error: Could not select range, end address is less then start address</source>
        <translation type="vanished">Error: het eindadres is lager dan het startadres</translation>
    </message>
</context>
<context>
    <name>ImportsModel</name>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="45"/>
        <source>Unsafe</source>
        <translation>Onveilig</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="71"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="73"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="75"/>
        <source>Safety</source>
        <translation>Veiligheid</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="77"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="79"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="81"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportsWidget</name>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="172"/>
        <source>Imports</source>
        <translation type="unfinished">Imports</translation>
    </message>
</context>
<context>
    <name>IncrementDecrementDialog</name>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="14"/>
        <source>Increment/Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="24"/>
        <source>Interpret as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="38"/>
        <source>Value:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="54"/>
        <source>Increment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="64"/>
        <source>Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Byte</source>
        <translation type="unfinished">Byte</translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Word</source>
        <translation type="unfinished">Word</translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Dword</source>
        <translation type="unfinished">Dword</translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Qword</source>
        <translation type="unfinished">Qword</translation>
    </message>
</context>
<context>
    <name>InitialOptionsDialog</name>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="26"/>
        <source>Load Options</source>
        <translation>Opties voor het inladen van het bestand</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="84"/>
        <source>Program:</source>
        <translation>Programma:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="161"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="363"/>
        <source>Analysis: Enabled</source>
        <translation>Analyse: ingeschakeld</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="177"/>
        <source>Level: </source>
        <translation>Level: </translation>
    </message>
    <message>
        <source>Analyze all symbols (aa)</source>
        <translation type="vanished">Analyseer alle symbolen (aa)</translation>
    </message>
    <message>
        <source>Analyze for references (aar)</source>
        <translation type="vanished">Analyseer alle referenties (aar)</translation>
    </message>
    <message>
        <source>Analyze function calls (aac)</source>
        <translation type="vanished">Analyseer functie calls (aac)</translation>
    </message>
    <message>
        <source>Analyze all basic blocks (aab)</source>
        <translation type="vanished">Analyseer alle basis blokken (aab)</translation>
    </message>
    <message>
        <source>Autorename functions based on context (aan)</source>
        <translation type="vanished">Automatisch functies hernoemen gebaseerd op de context (aan)</translation>
    </message>
    <message>
        <source>Experimental:</source>
        <translation type="vanished">Experimenteel:</translation>
    </message>
    <message>
        <source>Emulate code to find computed references (aae)</source>
        <translation type="vanished">Emuleer code om verbanden in de code te ontdekken (aae)</translation>
    </message>
    <message>
        <source>Analyze for consecutive function (aat)</source>
        <translation type="vanished">Analyseer voor de hierop volgende functie (aat)</translation>
    </message>
    <message>
        <source>Type and Argument matching analysis (afta)</source>
        <translation type="vanished">Type and argument vergelijkings analyse (afta)</translation>
    </message>
    <message>
        <source>Analyze code after trap-sleds (aaT)</source>
        <translation type="vanished">Analyseer code na trap-sleds (aaT)</translation>
    </message>
    <message>
        <source>Analyze function preludes (aap)</source>
        <translation type="vanished">Analyseer functie inleidingen (aap)</translation>
    </message>
    <message>
        <source>Analyze jump tables in switch statements (e! anal.jmptbl)</source>
        <translation type="vanished">Analyseer mogelijke jumps in switch statements (e! anal.jmptbl)</translation>
    </message>
    <message>
        <source>Analyze push+ret as jmp (e! anal.pushret)</source>
        <translation type="vanished">Analyseer push+ret als jmp (e! anal.pushret)</translation>
    </message>
    <message>
        <source>Continue analysis after each function (e! anal.hasnext)</source>
        <translation type="vanished">Blijf analyseren na iedere functie (e! anal.hasnext)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="322"/>
        <source>Load in write mode (-w)</source>
        <translation>Binary inladen in schrijfmodus (-w)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="332"/>
        <source>Do not load bin information (-n)</source>
        <translation>Geen bestandsinformatie inladen (-n)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="345"/>
        <source>Use virtual addressing</source>
        <translation>Gebruik virtuele adressering</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="355"/>
        <source>Import demangled symbols</source>
        <translation>Import demangled symbolen</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="372"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="403"/>
        <source>Advanced options</source>
        <translation>Geavanceerde opties</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="433"/>
        <source>CPU options</source>
        <translation>CPU opties</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="448"/>
        <source>Architecture:</source>
        <translation type="unfinished">Architecture:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="459"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="519"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="567"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="612"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="637"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="37"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="473"/>
        <source>CPU:</source>
        <translation>CPU:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="524"/>
        <source>8</source>
        <translation type="unfinished">8</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="529"/>
        <source>16</source>
        <translation type="unfinished">16</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="534"/>
        <source>32</source>
        <translation type="unfinished">32</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="539"/>
        <source>64</source>
        <translation type="unfinished">64</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="553"/>
        <source>Endianness: </source>
        <translation>Endianness: </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="572"/>
        <source>Little</source>
        <translation type="unfinished">Little</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="577"/>
        <source>Big</source>
        <translation type="unfinished">Big</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="598"/>
        <source>Kernel: </source>
        <translation>Kernel: </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="626"/>
        <source>Format:</source>
        <translation type="unfinished">Format:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="680"/>
        <source>Load bin offset (-B)</source>
        <translation>Laad binary offset (-B)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="699"/>
        <source>1024</source>
        <translation>1024</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="718"/>
        <source>Map offset (-m)</source>
        <translation>Map offset (-m)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="734"/>
        <source>0x40000</source>
        <translation type="unfinished">0x40000</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="741"/>
        <source>Load PDB</source>
        <translation>PDB inladen</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="766"/>
        <source>PDB File path</source>
        <translation>Pad van het PDB bestand</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="773"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="815"/>
        <source>Select</source>
        <translation type="unfinished">Select</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="783"/>
        <source>Load script file</source>
        <translation>Laad script bestand</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="808"/>
        <source>Path to Rizin script file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path to radare2 script file</source>
        <translation type="vanished">Pad naar radare2 script bestand</translation>
    </message>
    <message>
        <source>BasicBlock maxsize:</source>
        <translation type="vanished">Maximale grootte van een basisblok:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="871"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="884"/>
        <source>  Ok  </source>
        <translation>  Ok  </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="55"/>
        <source>Analyze all symbols</source>
        <translation>Analyseer alle symbolen</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="56"/>
        <source>Analyze instructions for references</source>
        <translation>Analyseer de instructies voor referenties</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="57"/>
        <source>Analyze function calls</source>
        <translation>Analyseer functie calls</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="58"/>
        <source>Analyze all basic blocks</source>
        <translation>Analyseer alle basis blokken</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="59"/>
        <source>Analyze all objc references</source>
        <translation>Analyseer alle objc referenties</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="60"/>
        <source>Recover class information from RTTI</source>
        <translation>Klasse informatie ophalen van RTTI</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="61"/>
        <source>Autoname functions based on context</source>
        <translation>Automatisch functies hernoemen gebaseerd op de context</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="62"/>
        <source>Emulate code to find computed references</source>
        <translation>Emuleer code om verbanden in de code te ontdekken</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="63"/>
        <source>Analyze all consecutive functions</source>
        <translation>Analyseer alle hierop volgende functies</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="64"/>
        <source>Type and Argument matching analysis</source>
        <translation>Type and argument vergelijkings analyse</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="65"/>
        <source>Analyze code after trap-sleds</source>
        <translation>Analyseer code na trap-sleds</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="66"/>
        <source>Analyze function preludes</source>
        <translation>Analyseer functie inleidingen</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="67"/>
        <source>Analyze jump tables in switch statements</source>
        <translation>Analyseer mogelijke jumps in switch statements</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="70"/>
        <source>Analyze PUSH+RET as JMP</source>
        <translation>Analyseer PUSH+RET als JMP</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="71"/>
        <source>Continue analysis after each function</source>
        <translation>Blijf analyseren na iedere functie</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="343"/>
        <source>No analysis</source>
        <translation>Geen analyse</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="345"/>
        <source>Auto-Analysis (aaa)</source>
        <translation>Automatische analyse (aaa)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="347"/>
        <source>Auto-Analysis Experimental (aaaa)</source>
        <translation>Automatische experimentele analyse (aaaa)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="349"/>
        <source>Advanced</source>
        <translation>Geavanceerd</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="351"/>
        <source>Unknown</source>
        <translation>Onbekend</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="357"/>
        <source>Level</source>
        <translation>Level</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="360"/>
        <source>Analysis: Disabled</source>
        <translation>Analyse: uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="406"/>
        <source>Select PDB file</source>
        <translation>Selecteer een PDB bestand</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="407"/>
        <source>PDB file (*.pdb)</source>
        <translation>PDB bestand (*.pdb)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="407"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="429"/>
        <source>All files (*)</source>
        <translation>Alle bestanden (*)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="428"/>
        <source>Select Rizin script file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="429"/>
        <source>Script file (*.rz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select radare2 script file</source>
        <translation type="vanished">Selecteer een radare2 script bestand</translation>
    </message>
    <message>
        <source>Script file (*.r2)</source>
        <translation type="vanished">Script bestand (*.r2)</translation>
    </message>
</context>
<context>
    <name>InitializationFileEditor</name>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.ui" line="14"/>
        <source>CutterRC Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.ui" line="20"/>
        <source>TextLabel</source>
        <translation type="unfinished">TextLabel</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.cpp" line="35"/>
        <source>Script is loaded from &lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JSDecDecompiler</name>
    <message>
        <location filename="../../common/Decompiler.cpp" line="40"/>
        <source>Failed to parse JSON from jsdec</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JupyterWebView</name>
    <message>
        <source>Jupyter</source>
        <translation type="vanished">Jupyter</translation>
    </message>
</context>
<context>
    <name>JupyterWidget</name>
    <message>
        <source>Jupyter</source>
        <translation type="vanished">Jupyter</translation>
    </message>
    <message>
        <source>Cutter has been built without QtWebEngine.&lt;br /&gt;Open the following URL in your Browser to use Jupyter:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation type="vanished">Cutter is gecompileerd zonder QtWebEngine.&lt;br /&gt;Open de volgende link in uw browser om Jupyter te gebruiken:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <source>An error occurred while opening jupyter. Make sure Jupyter is installed system-wide.</source>
        <translation type="vanished">Een error is opgetreden tijdens het openen van Jupyter. Controleer of Jupyter correct op uw systeem is geïnstalleerd.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Error</translation>
    </message>
</context>
<context>
    <name>LayoutManager</name>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="14"/>
        <source>Layout</source>
        <translation type="unfinished">Opmaak</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="25"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="32"/>
        <location filename="../../dialogs/LayoutManager.cpp" line="60"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="42"/>
        <source>Rename layout error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="43"/>
        <source>&apos;%1&apos; is already used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="45"/>
        <source>Save layout</source>
        <translation type="unfinished">Layout opslaan</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="45"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="61"/>
        <source>Do you want to delete &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinkTypeDialog</name>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialog</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="26"/>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="43"/>
        <source>Enter Address</source>
        <translation>Voer Adres in</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="50"/>
        <source>Structure Type</source>
        <translation>Structuur Type</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="73"/>
        <source>Address/Flag</source>
        <translation>Adres/Vlag</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="10"/>
        <source>Link type to address</source>
        <translation>Link type naar adres</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="13"/>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="57"/>
        <source>(No Type)</source>
        <translation>(Geen type)</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="75"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="75"/>
        <source>The given address is invalid</source>
        <translation>Het gegeven adres is ongeldig</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="106"/>
        <source>Invalid Address</source>
        <translation>Ongeldig adres</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../core/MainWindow.ui" line="33"/>
        <source>Add extra...</source>
        <translation>Voeg extra toe...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="61"/>
        <source>File</source>
        <translation>Bestand</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="68"/>
        <source>Set mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="94"/>
        <location filename="../../core/MainWindow.cpp" line="288"/>
        <source>View</source>
        <translation>Weergave</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="101"/>
        <source>Zoom</source>
        <translation type="unfinished">Zoom</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="110"/>
        <source>Layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="130"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="138"/>
        <location filename="../../core/MainWindow.ui" line="498"/>
        <source>Edit</source>
        <translation type="unfinished">Edit</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="147"/>
        <source>Windows</source>
        <translation>Windows</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="151"/>
        <location filename="../../core/MainWindow.cpp" line="142"/>
        <location filename="../../core/MainWindow.cpp" line="143"/>
        <source>Plugins</source>
        <translation>Plugins</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="156"/>
        <source>Info...</source>
        <translation>Informatie...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="161"/>
        <source>Debug...</source>
        <translation>Debug...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="176"/>
        <source>Debug</source>
        <translation type="unfinished">Debug</translation>
    </message>
    <message>
        <source>Reset Layout</source>
        <translation type="vanished">Layout herstellen</translation>
    </message>
    <message>
        <source>Reset layout</source>
        <translation type="vanished">Layout herstellen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="227"/>
        <source>Zen Mode</source>
        <translation>Zen modus</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="230"/>
        <source>Zen mode</source>
        <translation>Zen modus</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="235"/>
        <source>About</source>
        <translation>Over</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="240"/>
        <source>Report an issue</source>
        <translation>Een probleem melden</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Nieuw</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="248"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="253"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="258"/>
        <location filename="../../core/MainWindow.ui" line="261"/>
        <location filename="../../core/MainWindow.ui" line="847"/>
        <location filename="../../core/MainWindow.cpp" line="1207"/>
        <source>Save layout</source>
        <translation>Layout opslaan</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="266"/>
        <source>Documentation</source>
        <translation>Documentatie</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Openen</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation type="vanished">Ctrl+O</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Opslaan</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="282"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="340"/>
        <source>Undo Seek</source>
        <translation>Seek ongedaan maken</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="352"/>
        <source>Redo Seek</source>
        <translation>Seek opnieuw instellen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="287"/>
        <source>Cut</source>
        <translation>Knip</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="222"/>
        <source>Reset to default layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="245"/>
        <source>New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="271"/>
        <source>Map File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="274"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="279"/>
        <location filename="../../core/MainWindow.cpp" line="712"/>
        <location filename="../../core/MainWindow.cpp" line="735"/>
        <source>Save Project</source>
        <translation type="unfinished">Project opslaan</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="292"/>
        <source>Copy</source>
        <translation type="unfinished">Copy</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="297"/>
        <location filename="../../core/MainWindow.ui" line="503"/>
        <source>Paste</source>
        <translation type="unfinished">Paste</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="302"/>
        <source>Delete</source>
        <translation type="unfinished">Delete</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="307"/>
        <location filename="../../core/MainWindow.ui" line="310"/>
        <source>Select all</source>
        <translation>Alles selecteren</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="315"/>
        <source>Find</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="320"/>
        <location filename="../../core/MainWindow.ui" line="323"/>
        <source>Find next</source>
        <translation>Volgende zoeken</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="328"/>
        <location filename="../../core/MainWindow.ui" line="331"/>
        <source>Find previous</source>
        <translation>Vorige zoeken</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Vorige</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="343"/>
        <source>Go back</source>
        <translation>Terug</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">Volgende</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="363"/>
        <source>Unlock Panels</source>
        <translation>Panelen vrijgeven</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="366"/>
        <source>Toggle panel locks</source>
        <translation>Panelen vrijgeven/vastzetten</translation>
    </message>
    <message>
        <source>Lock/Unlock</source>
        <translation type="vanished">Vrijgeven/vastzetten</translation>
    </message>
    <message>
        <source>Strings</source>
        <translation type="vanished">Strings</translation>
    </message>
    <message>
        <source>Show/Hide Strings panel</source>
        <translation type="vanished">Geef weer/verberg het strings paneel</translation>
    </message>
    <message>
        <source>Sections</source>
        <translation type="vanished">Secties</translation>
    </message>
    <message>
        <source>Show/Hide Sections panel</source>
        <translation type="vanished">Geef weer/verberg het sectie paneel</translation>
    </message>
    <message>
        <source>Segments</source>
        <translation type="vanished">Segmenten</translation>
    </message>
    <message>
        <source>Show/Hide Segments panel</source>
        <translation type="vanished">Geef weer/verberg het segmenten paneel</translation>
    </message>
    <message>
        <source>Functions</source>
        <translation type="vanished">Functies</translation>
    </message>
    <message>
        <source>Show/Hide Functions panel</source>
        <translation type="vanished">Laat zien/verberg het functies paneel</translation>
    </message>
    <message>
        <source>Imports</source>
        <translation type="vanished">Imports</translation>
    </message>
    <message>
        <source>Show/Hide Imports panel</source>
        <translation type="vanished">Geef weer/verberg het import paneel</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation type="vanished">Symbolen</translation>
    </message>
    <message>
        <source>Show/Hide Symbols panel</source>
        <translation type="vanished">Geef weer/verberg het symbolen paneel</translation>
    </message>
    <message>
        <source>Relocs</source>
        <translation type="vanished">Relocs</translation>
    </message>
    <message>
        <source>Show/Hide Relocs panel</source>
        <translation type="vanished">Geef weer/verberg het relocs paneel</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="vanished">Flags</translation>
    </message>
    <message>
        <source>Show/Hide Flags panel</source>
        <translation type="vanished">Geef weer/verberg het flags paneel</translation>
    </message>
    <message>
        <source>Memory</source>
        <translation type="vanished">Geheugen</translation>
    </message>
    <message>
        <source>Show/Hide Memory panel</source>
        <translation type="vanished">Geef weer/verberg het geheugen paneel</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="385"/>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="400"/>
        <location filename="../../core/MainWindow.ui" line="403"/>
        <source>Tabs up/down</source>
        <translation>Tabs omhoog/omlaag</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="412"/>
        <source>Refresh</source>
        <translation>Vernieuwen</translation>
    </message>
    <message>
        <source>Comments</source>
        <translation type="vanished">Commentaar</translation>
    </message>
    <message>
        <source>Show/Hide comments</source>
        <translation type="vanished">Geef weer/verberg opmerkingen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="420"/>
        <source>Show Tabs at the Top</source>
        <translation>Toon tabs bovenin</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="423"/>
        <source>Toggle tab position</source>
        <translation>Zet de tab positie aan/uit</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="428"/>
        <source>Dark Theme</source>
        <translation>Donker thema</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="433"/>
        <location filename="../../core/MainWindow.ui" line="436"/>
        <source>Load layout</source>
        <translation>Layout laden</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="441"/>
        <source>Default Theme</source>
        <translation>Standaard thema</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="446"/>
        <source>Bindiff</source>
        <translation>Bindiff</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="451"/>
        <source>Analysis</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="456"/>
        <source>Test menu</source>
        <translation>Test menu</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="461"/>
        <location filename="../../core/MainWindow.ui" line="464"/>
        <source>Copy hexpair</source>
        <translation>Kopieer hexpair</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="469"/>
        <location filename="../../core/MainWindow.ui" line="472"/>
        <source>Copy text</source>
        <translation>Kopieer tekst</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="477"/>
        <source>Copy ASCII</source>
        <translation type="unfinished">Copy ASCII</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="482"/>
        <location filename="../../core/MainWindow.ui" line="485"/>
        <source>Insert string</source>
        <translation>Voeg string in</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="490"/>
        <location filename="../../core/MainWindow.ui" line="493"/>
        <source>Insert hex</source>
        <translation>Voeg hex in</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="508"/>
        <source>Show/Hide bytes</source>
        <translation>Toon/verberg bytes</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="513"/>
        <source>Switch case</source>
        <translation>Switch case</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="518"/>
        <location filename="../../core/MainWindow.ui" line="521"/>
        <source>Copy all</source>
        <translation>Alles kopiëren</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="526"/>
        <location filename="../../core/MainWindow.ui" line="529"/>
        <source>Copy bytes</source>
        <translation>Kopieer bytes</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="534"/>
        <location filename="../../core/MainWindow.ui" line="537"/>
        <location filename="../../core/MainWindow.ui" line="542"/>
        <location filename="../../core/MainWindow.ui" line="545"/>
        <source>Copy disasm</source>
        <translation>Kopieer Disasm</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="557"/>
        <location filename="../../core/MainWindow.ui" line="560"/>
        <source>Start web server</source>
        <translation>Start web server</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="565"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="570"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="575"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="580"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="585"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="590"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="595"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="600"/>
        <source>Syntax AT&amp;T/Intel</source>
        <translation>Syntax AT&amp;T/Intel</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="605"/>
        <location filename="../../core/MainWindow.ui" line="615"/>
        <source>Rename</source>
        <translation>Hernoem</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="610"/>
        <location filename="../../core/MainWindow.ui" line="620"/>
        <source>Undefine</source>
        <translation>Ongedefinieerd</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="625"/>
        <source>Add comment</source>
        <translation>Opmerking toevoegen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="637"/>
        <location filename="../../core/MainWindow.ui" line="640"/>
        <source>Show/Hide bottom pannel</source>
        <translation>Toon/verberg het onderste paneel</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="645"/>
        <source>Run Rizin script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="717"/>
        <source>Save Project As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="735"/>
        <source>Analyze Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="809"/>
        <source>Commit changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="817"/>
        <source>Write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="820"/>
        <source>Open the file in write mode. Every change to the file will change the original file on disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="828"/>
        <source>Cache mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="831"/>
        <source>Enable cache mode. Changes to the file would not be applied to disk unless you specifically commit them. This is a safer option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="842"/>
        <source>Read-Only mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="852"/>
        <source>Manage layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SDB Browser</source>
        <translation type="vanished">SDB Browser</translation>
    </message>
    <message>
        <source>Run Script</source>
        <translation type="vanished">Script uitvoeren</translation>
    </message>
    <message>
        <source>Dashboard</source>
        <translation type="vanished">Overzicht</translation>
    </message>
    <message>
        <source>Show/Hide Dashboard panel</source>
        <translation type="vanished">Geef weer/verberg het overzichtspaneel</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="650"/>
        <source>Reset Settings</source>
        <translation>Fabrieksinstellingen herstellen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="653"/>
        <source>Reset settings</source>
        <translation>Fabrieksinstellingen herstellen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="658"/>
        <source>Quit</source>
        <translation>Afsluiten</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="661"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="669"/>
        <source>Exports</source>
        <translation>Exports</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="672"/>
        <source>Show/Hide Exports panel</source>
        <translation>Geef weer/verberg het exports paneel</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="677"/>
        <source>Refresh Contents</source>
        <translation>Inhoud bijwerken</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="680"/>
        <source>Refresh contents</source>
        <translation>Inhoud bijwerken</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="688"/>
        <source>Show ESIL rather than assembly</source>
        <translation>Laat ESIL zien in plaats van assembly</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="696"/>
        <source>Show pseudocode rather than assembly</source>
        <translation>Laat pseudocode zien in plaats van assembly</translation>
    </message>
    <message>
        <source>Entry Points</source>
        <translation type="vanished">Entry Points</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="707"/>
        <source>Display offsets</source>
        <translation>Offsets weergeven</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="712"/>
        <source>Preferences</source>
        <translation>Voorkeuren</translation>
    </message>
    <message>
        <source>Save As...</source>
        <translation type="vanished">Opslaan als...</translation>
    </message>
    <message>
        <source>Graph Overview</source>
        <translation type="vanished">Grafiekenoverzicht</translation>
    </message>
    <message>
        <source>Decompiler</source>
        <translation type="obsolete">Decompiler</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="745"/>
        <source>Add Hexdump</source>
        <translation>Hexdump toevoegen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="750"/>
        <source>Add Decompiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="755"/>
        <source>Add Disassembly</source>
        <translation>Voeg disassembly toe</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="760"/>
        <source>Add Graph</source>
        <translation>Voeg grafiek toe</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="771"/>
        <source>Grouped dock dragging</source>
        <translation>Gegroepeerd dock slepen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="776"/>
        <source>Zoom In</source>
        <translation>Inzoomen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="779"/>
        <source>Ctrl++</source>
        <translation>Ctrl++</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="787"/>
        <source>Zoom Out</source>
        <translation>Uitzoomen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="790"/>
        <source>Ctrl+-</source>
        <translation>Ctrl+-</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="798"/>
        <source>Reset</source>
        <translation>Herstellen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="801"/>
        <source>Ctrl+=</source>
        <translation>Ctrl+=</translation>
    </message>
    <message>
        <source>Tmp</source>
        <translation type="vanished">Tmp</translation>
    </message>
    <message>
        <source>Disassembly</source>
        <translation type="obsolete">Disassembly</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="725"/>
        <source>Graph</source>
        <translation type="unfinished">Graph</translation>
    </message>
    <message>
        <source>Pseudocode</source>
        <translation type="vanished">Pseudocode</translation>
    </message>
    <message>
        <source>Hexdump</source>
        <translation type="obsolete">Hexdump</translation>
    </message>
    <message>
        <source>Sidebar</source>
        <translation type="vanished">Sidebar</translation>
    </message>
    <message>
        <source>Console</source>
        <translation type="vanished">Console</translation>
    </message>
    <message>
        <source>Stack</source>
        <translation type="vanished">Stack</translation>
    </message>
    <message>
        <source>Registers</source>
        <translation type="vanished">Registers</translation>
    </message>
    <message>
        <source>Backtrace</source>
        <translation type="vanished">Backtrace</translation>
    </message>
    <message>
        <source>Threads</source>
        <translation type="vanished">Threads</translation>
    </message>
    <message>
        <source>Processes</source>
        <translation type="vanished">Processen</translation>
    </message>
    <message>
        <source>Memory map</source>
        <translation type="vanished">Memory map</translation>
    </message>
    <message>
        <source>Breakpoints</source>
        <translation type="vanished">Breakpoints</translation>
    </message>
    <message>
        <source>Register References</source>
        <translation type="vanished">Register Referenties</translation>
    </message>
    <message>
        <source>Classes</source>
        <translation type="vanished">Klasses</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="730"/>
        <source>Import PDB</source>
        <translation>Importeer PDB</translation>
    </message>
    <message>
        <source>Analyze</source>
        <translation type="vanished">Analyseer</translation>
    </message>
    <message>
        <source>Resources</source>
        <translation type="vanished">Resources</translation>
    </message>
    <message>
        <source>VTables</source>
        <translation type="vanished">VTables</translation>
    </message>
    <message>
        <source>Show/Hide VTables panel</source>
        <translation type="vanished">Geef weer/verberg het VTables paneel</translation>
    </message>
    <message>
        <source>Types</source>
        <translation type="vanished">Types</translation>
    </message>
    <message>
        <source>Show/Hide Types panel</source>
        <translation type="vanished">Geef weer/verberg het types paneel</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Zoeken</translation>
    </message>
    <message>
        <source>Show/Hide Search panel</source>
        <translation type="vanished">Geef weer/verberg het zoekpaneel</translation>
    </message>
    <message>
        <source>Headers</source>
        <translation type="vanished">Headers</translation>
    </message>
    <message>
        <source>Show/Hide Headers panel</source>
        <translation type="vanished">Geef weer/verberg het headers paneel</translation>
    </message>
    <message>
        <source>Zignatures</source>
        <translation type="vanished">Zignatures</translation>
    </message>
    <message>
        <source>Show/Hide Zignatures panel</source>
        <translation type="vanished">Geef weer/verberg het Zignatures paneel</translation>
    </message>
    <message>
        <source>Jupyter</source>
        <translation type="obsolete">Jupyter</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="740"/>
        <location filename="../../core/MainWindow.cpp" line="1697"/>
        <source>Export as code</source>
        <translation>Exporteer als code</translation>
    </message>
    <message>
        <source>Hexdump view</source>
        <translation type="vanished">Hexdump view</translation>
    </message>
    <message>
        <source>Disassembly view</source>
        <translation type="vanished">Disassembly view</translation>
    </message>
    <message>
        <source>Graph view</source>
        <translation type="vanished">Graph view</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="533"/>
        <source>Script loading</source>
        <translation>Script is aan het laden</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="534"/>
        <source>Do you want to load the &apos;%1&apos; script?</source>
        <translation>Wil je het script &apos;%1&apos; inladen?</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="552"/>
        <source>Cannot open file!</source>
        <translation>Kan het bestand niet openen!</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="553"/>
        <source>Could not open the file! Make sure the file exists and that you have the correct permissions.</source>
        <translation>Kan het bestand niet openen! Zorg ervoor dat het bestand bestaat en dat je de juiste permissies hebt.</translation>
    </message>
    <message>
        <source> &gt; Populating UI</source>
        <translation type="vanished"> &gt; Gebruikersinterface aan het laden</translation>
    </message>
    <message>
        <source> &gt; Finished, happy reversing :)</source>
        <translation type="vanished"> &gt; Gereed, veel plezier :)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="764"/>
        <source>Do you really want to exit?
Save your project before closing!</source>
        <translation>Weet je zeker dat je wil afsluiten?
Sla je project op voordat je afsluit!</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1038"/>
        <source>New disassembly</source>
        <translation>Nieuwe disassembly</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1040"/>
        <source>New graph</source>
        <translation>Nieuwe grafiek</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1042"/>
        <source>New hexdump</source>
        <translation>Nieuwe hexdump</translation>
    </message>
    <message>
        <source>Select radare2 script</source>
        <translation type="vanished">Selecteer een radare2 script</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="251"/>
        <source>No plugins are installed. Check the plugins section on Cutter documentation to learn more.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="256"/>
        <source>The installed plugins didn&apos;t add entries to this menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="616"/>
        <source>Failed to open project: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="619"/>
        <source>Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1043"/>
        <source>New Decompiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1204"/>
        <source>Save layout error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1205"/>
        <source>&apos;%1&apos; is not a valid name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1207"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1514"/>
        <source>Select Rizin script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1557"/>
        <source>Do you really want to clear all settings?</source>
        <translation>Weet je zeker dat je de instellingen wilt herstellen?</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1651"/>
        <source>Select PDB file</source>
        <translation>Selecteer een PDB bestand</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1652"/>
        <source>PDB file (*.pdb)</source>
        <translation>PDB bestand (*.pdb)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1652"/>
        <source>All files (*)</source>
        <translation>Alle bestanden (*)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1662"/>
        <source>%1 loaded.</source>
        <translation>%1 geladen.</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1672"/>
        <source>C uin8_t array (*.c)</source>
        <translation>C uin8_t array (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1674"/>
        <source>C uin16_t array (*.c)</source>
        <translation>C uin16_t array (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1676"/>
        <source>C uin32_t array (*.c)</source>
        <translation>C uin32_t array (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1678"/>
        <source>C uin64_t array (*.c)</source>
        <translation>C uin64_t array (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1680"/>
        <source>C string (*.c)</source>
        <translation>C string (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1682"/>
        <source>Shell-script that reconstructs the bin (*.sh)</source>
        <translation>Shell-script dat de binary nabootst (*.sh)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1684"/>
        <source>JSON array (*.json)</source>
        <translation>JSON array (*.json)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1686"/>
        <source>JavaScript array (*.js)</source>
        <translation>JavaScript array (*.js)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1688"/>
        <source>Python array (*.py)</source>
        <translation>Python array (*.py)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1690"/>
        <source>Print &apos;wx&apos; Rizin commands (*.rz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print &apos;wx&apos; r2 commands (*.r2)</source>
        <translation type="vanished">Geef &apos;wx&apos; r2 commando&apos;s weer (*.r2)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1692"/>
        <source>GAS .byte blob (*.asm, *.s)</source>
        <translation>GAS .byte blob (*.asm, *.s)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1694"/>
        <source>.bytes with instructions in comments (*.txt)</source>
        <translation>.bytes met instructies in de comments (*.txt)</translation>
    </message>
    <message>
        <source>Project saved: %1</source>
        <translation type="vanished">Project opgeslagen: %1</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="736"/>
        <source>Failed to save project: %1</source>
        <translation>Er is een error opgetreden bij het opslaan van %1</translation>
    </message>
    <message>
        <source>Project saved:</source>
        <translation type="vanished">Project opgeslagen:</translation>
    </message>
</context>
<context>
    <name>MapFileDialog</name>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="14"/>
        <source>Map New File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="26"/>
        <source>File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="52"/>
        <location filename="../../dialogs/MapFileDialog.cpp" line="18"/>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="59"/>
        <source>Map address:</source>
        <translation type="unfinished">Map adres:</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="72"/>
        <source>0x40000</source>
        <translation type="unfinished">0x40000</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.cpp" line="36"/>
        <source>Map new file file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.cpp" line="36"/>
        <source>Failed to map a new file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MemoryDockWidget</name>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="obsolete">Sync/unsync offset</translation>
    </message>
</context>
<context>
    <name>MemoryMapModel</name>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="58"/>
        <source>Offset start</source>
        <translation>Offset start</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="60"/>
        <source>Offset end</source>
        <translation>Offset einde</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="62"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="64"/>
        <source>Permissions</source>
        <translation type="unfinished">Permissions</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="66"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MemoryMapWidget</name>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="123"/>
        <source>Memory Map</source>
        <translation>Geheugen Map</translation>
    </message>
</context>
<context>
    <name>MultitypeFileSaveDialog</name>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="24"/>
        <source>Detect type (*)</source>
        <translation>Type detectie (*)</translation>
    </message>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="63"/>
        <source>File save error</source>
        <translation>Fout bij opslaan bestand</translation>
    </message>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="64"/>
        <source>Unrecognized extension &apos;%1&apos;</source>
        <translation>Niet herkende extensie &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>NativeDebugDialog</name>
    <message>
        <location filename="../../dialogs/NativeDebugDialog.ui" line="35"/>
        <source>Command line arguments:</source>
        <translation>Opdrachtregelparameters:</translation>
    </message>
</context>
<context>
    <name>NewFileDialog</name>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="20"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="149"/>
        <source>Open File</source>
        <translation>Bestand openen</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="79"/>
        <source>About</source>
        <translation type="unfinished">About</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="182"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="390"/>
        <source>Select</source>
        <translation>Selecteer</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="169"/>
        <source>&lt;b&gt;Select new file&lt;b&gt;</source>
        <translation>&lt;b&gt;Selecteer nieuw bestand&lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="202"/>
        <source>&lt;b&gt;IO&lt;/b&gt;</source>
        <translation>&lt;b&gt;IO&lt;/b&gt;</translation>
    </message>
    <message>
        <source>://</source>
        <translation type="vanished">://</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="296"/>
        <source>Don&apos;t open any file</source>
        <translation>Niks openen</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="303"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="360"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="501"/>
        <source>Open</source>
        <translation type="unfinished">Open</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="316"/>
        <source>Open Shellcode</source>
        <translation>Shellcode openen</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="328"/>
        <source>&lt;b&gt;Paste Shellcode&lt;b&gt;</source>
        <translation>&lt;b&gt;Shellcode plakken&lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="370"/>
        <source>Projects</source>
        <translation>Projecten</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="410"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Open Project&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="534"/>
        <source>Clear all projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Projects path (dir.projects):&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Locatie van de projecten (dir.projects):&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="519"/>
        <source>Remove item</source>
        <translation>Item wissen</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="524"/>
        <source>Clear all</source>
        <translation>Alles wissen</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="529"/>
        <source>Delete project</source>
        <translation>Project verwijderen</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="92"/>
        <source>Select file</source>
        <translation>Bestand selecteren</translation>
    </message>
    <message>
        <source>Select project path (dir.projects)</source>
        <translation type="vanished">Selecteer project pad (dir.projects)</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Toestemming geweigerd</translation>
    </message>
    <message>
        <source>You do not have write access to &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="vanished">U heeft geen schrijfrechten voor &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Delete the project &quot;%1&quot; from disk ?</source>
        <translation type="vanished">Verwijder het project &quot;%1&quot; van de harde schijf?</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="104"/>
        <source>Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="287"/>
        <source>Open a file with no extra treatment.</source>
        <translation>Bestand enkel openen.</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="319"/>
        <source>Select a new program or a previous one before continuing.</source>
        <translation>Selecteer een programma voordat u doorgaat.</translation>
    </message>
</context>
<context>
    <name>Omnibar</name>
    <message>
        <location filename="../../widgets/Omnibar.cpp" line="15"/>
        <source>Type flag name or address here</source>
        <translation>Geef de flag naam of het adres hier in</translation>
    </message>
</context>
<context>
    <name>OpenFileDialog</name>
    <message>
        <source>Open file</source>
        <translation type="vanished">Bestand openen</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="vanished">Bestand selecteren</translation>
    </message>
    <message>
        <source>Map address:</source>
        <translation type="vanished">Map adres:</translation>
    </message>
    <message>
        <source>File:</source>
        <translation type="obsolete">File:</translation>
    </message>
    <message>
        <source>Map address</source>
        <translation type="vanished">Map adres</translation>
    </message>
    <message>
        <source>0x40000</source>
        <translation type="vanished">0x40000</translation>
    </message>
    <message>
        <source>Failed to open file</source>
        <translation type="vanished">Kon bestand niet openen</translation>
    </message>
</context>
<context>
    <name>PluginsOptionsWidget</name>
    <message>
        <source>Plugins are loaded from &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="vanished">Plugins worden geladen van &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="28"/>
        <source>Plugins are loaded from &lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Version</source>
        <translation type="unfinished">Version</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Author</source>
        <translation type="unfinished">Author</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="48"/>
        <source>Show Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show radare2 plugin information</source>
        <translation type="obsolete">Show radare2 plugin information</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.ui" line="6"/>
        <source>Preferences</source>
        <translation>Voorkeuren</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="28"/>
        <source>Disassembly</source>
        <translation type="unfinished">Disassembly</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="34"/>
        <source>Debug</source>
        <translation type="unfinished">Debug</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="35"/>
        <source>Appearance</source>
        <translation type="unfinished">Appearance</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="36"/>
        <source>Plugins</source>
        <translation type="unfinished">Plugins</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="37"/>
        <source>Initialization Script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="39"/>
        <source>Analysis</source>
        <translation type="unfinished">Analyse</translation>
    </message>
</context>
<context>
    <name>ProcessModel</name>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="71"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="73"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="75"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="77"/>
        <source>Path</source>
        <translation>Pad</translation>
    </message>
</context>
<context>
    <name>ProcessesWidget</name>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="26"/>
        <source>PID</source>
        <translation type="unfinished">PID</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="27"/>
        <source>UID</source>
        <translation type="unfinished">UID</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="28"/>
        <source>Status</source>
        <translation type="unfinished">Status</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="29"/>
        <source>Path</source>
        <translation type="unfinished">Path</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="172"/>
        <source>Unable to switch to the requested process.</source>
        <translation>Kan niet overschakelen naar het aangevraagde proces.</translation>
    </message>
</context>
<context>
    <name>PseudocodeWidget</name>
    <message>
        <source>Pseudocode</source>
        <translation type="obsolete">Pseudocode</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Refresh</translation>
    </message>
    <message>
        <source>Decompiler:</source>
        <translation type="vanished">Decompiler:</translation>
    </message>
    <message>
        <source>r2dec</source>
        <translation type="vanished">r2dec</translation>
    </message>
    <message>
        <source>pdc</source>
        <translation type="vanished">pdc</translation>
    </message>
    <message>
        <source>Click Refresh to generate Pseudocode from current offset.</source>
        <translation type="vanished">Klik Vernieuwen om pseudocode te genereren op basis van de huidige offset.</translation>
    </message>
    <message>
        <source>Cannot decompile at</source>
        <translation type="vanished">Kan niet decompileren op</translation>
    </message>
    <message>
        <source>(Not a function?)</source>
        <translation type="vanished">(Geen functie?)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>A Qt and C++ GUI for radare2 reverse engineering framework</source>
        <translation type="vanished">A QT en C++ GUI voor het radare2 reverse engineering framework</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="97"/>
        <source>The version used to compile Cutter (%1) does not match the binary version of rizin (%2). This could result in unexpected behaviour. Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="317"/>
        <source>A Qt and C++ GUI for rizin reverse engineering framework</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="320"/>
        <source>Filename to open.</source>
        <translation>Bestandsnaam op te openen.</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="324"/>
        <source>Automatically open file and optionally start analysis. Needs filename to be specified. May be a value between 0 and 2: 0 = no analysis, 1 = aaa, 2 = aaaa (experimental)</source>
        <translation>Bestand automatisch openen en start de analyse (optioneel). Bestandsnaam moet gegeven zijn. De waarde moet tussen de 0 en 2 liggen: 0 = geen analyse, 1 = aaa, 2 = aaaa (experimenteel)</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="327"/>
        <source>level</source>
        <translation>level</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="331"/>
        <source>Force using a specific file format (bin plugin)</source>
        <translation>Forceer gebruik van een specifiek bestandsformaat (bin plugin)</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="332"/>
        <source>name</source>
        <translation>naam</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="336"/>
        <source>Load binary at a specific base address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="337"/>
        <source>base address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="340"/>
        <source>Run script file</source>
        <translation>Voer script uit</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="340"/>
        <source>file</source>
        <translation>bestand</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="343"/>
        <source>Load project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="344"/>
        <source>project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="348"/>
        <source>Open file in write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="352"/>
        <source>PYTHONHOME to use for embedded python interpreter</source>
        <translation>PYTHONHOME om te gebruiken voor de ingebouwde python interpreter</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="358"/>
        <source>Disable output redirection. Some of the output in console widget will not be visible. Use this option when debuging a crash or freeze and output  redirection is causing some messages to be lost.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="364"/>
        <source>Do not load plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="368"/>
        <source>Do not load Cutter plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="372"/>
        <source>Do not load rizin plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PYTHONHOME to use for Jupyter</source>
        <translation type="vanished">PYTHONHOME om te gebruiken voor Jupyter</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="96"/>
        <source>Version mismatch!</source>
        <translation>Versies komen niet overeen!</translation>
    </message>
    <message>
        <source>The version used to compile Cutter (%1) does not match the binary version of radare2 (%2). This could result in unexpected behaviour. Are you sure you want to continue?</source>
        <translation type="vanished">The versie die gebruikt wordt om Cutter te compileren (%1) komt niet overeen met de versie van radare2 (%2). Dit kan resulteren in onverwachte uitkomsten. Weet je zeker dat je door wil gaan?</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="386"/>
        <source>Invalid Analysis Level. May be a value between 0 and 2.</source>
        <translation>Ongeldig analyse level. De waarde moet tussen de 0 en 2 liggen.</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="406"/>
        <source>Filename must be specified to start analysis automatically.</source>
        <translation>Bestandsnaam moet gegeven zijn om de automatische analyse te starten.</translation>
    </message>
    <message>
        <source>Color of comment generated by radare2</source>
        <translation type="vanished">De kleur van commentaar welke gegenereerd is door radare2</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="410"/>
        <source>Comment</source>
        <translation type="unfinished">Comment</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="410"/>
        <source>Color of comment generated by Rizin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="411"/>
        <source>Comment created by user</source>
        <translation>Commentaar wat door de gebruiker gemaakt is</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="411"/>
        <source>Color of user Comment</source>
        <translation>Kleur van een stuk commentaar van de gebruiker</translation>
    </message>
    <message>
        <source>Color of function arguments</source>
        <translation type="vanished">Kleur van de functie argumenten</translation>
    </message>
    <message>
        <source>Arguments</source>
        <translation type="vanished">Argumenten</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="413"/>
        <source>Color of names of functions</source>
        <translation>Kleur van functienamen</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="413"/>
        <source>Function name</source>
        <translation type="unfinished">Function name</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="414"/>
        <source>Color of function location</source>
        <translation>Kleur van functie locaties</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="414"/>
        <source>Function location</source>
        <translation>Functie locatie</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="416"/>
        <source>Color of ascii line in left side that shows what opcodes are belong to function</source>
        <translation>De kleur van de ascii lijn aan de linker kant die laat zien welke opcodes tot de functie behoren</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="418"/>
        <source>Function line</source>
        <translation>Functie Lijn</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="420"/>
        <source>Color of flags (similar to bookmarks for offset)</source>
        <translation>Kleur van de flags (vergelijkbaar met bladwijzers voor de offset)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="420"/>
        <source>Flag</source>
        <translation>Flag</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="421"/>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="422"/>
        <source>Help</source>
        <translation type="unfinished">Help</translation>
    </message>
    <message>
        <source>flow</source>
        <translation type="vanished">flow</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="424"/>
        <source>flow2</source>
        <translation>flow2</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="425"/>
        <location filename="../../widgets/ColorThemeListView.cpp" line="427"/>
        <source>Info</source>
        <translation type="unfinished">Info</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="425"/>
        <source>prompt</source>
        <translation>prompt</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="426"/>
        <source>Color of offsets</source>
        <translation>Kleur van de offsets</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="426"/>
        <source>Offset</source>
        <translation type="unfinished">Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="427"/>
        <source>input</source>
        <translation>invoer</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="428"/>
        <source>Invalid opcode color</source>
        <translation>Ongelde kleur voor de opcodes</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="428"/>
        <source>invalid</source>
        <translation>ongeldig</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="429"/>
        <source>other</source>
        <translation>overig</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="430"/>
        <source>0x00 opcode color</source>
        <translation>0x00 opcode kleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="431"/>
        <source>0x7f opcode color</source>
        <translation>0x7f opcode kleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="432"/>
        <source>0xff opcode color</source>
        <translation>0xff opcode kleur</translation>
    </message>
    <message>
        <source>arithmetic color (+, -, *, / etc.)</source>
        <translation type="vanished">kleur voor rekenkundige onderdelen (+, -, *, / etc.)</translation>
    </message>
    <message>
        <source>bin</source>
        <translation type="vanished">bin</translation>
    </message>
    <message>
        <source>btext</source>
        <translation type="vanished">btext</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="443"/>
        <source>push opcode color</source>
        <translation>push opcode kleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="444"/>
        <source>pop opcode color</source>
        <translation>pop opcode kleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="445"/>
        <source>Cryptographic color</source>
        <translation>Kleur voor cryptografie</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="446"/>
        <source>jmp instructions color</source>
        <translation>jmp instructions kleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="450"/>
        <source>call instructions color (ccall, rcall, call etc)</source>
        <translation>call instructions kleur (ccall, rcall, call etc)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="451"/>
        <source>nop opcode color</source>
        <translation>nop opcode kleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="452"/>
        <source>ret opcode color</source>
        <translation>ret opcode kleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="453"/>
        <source>Color of interrupts</source>
        <translation>Kleur van de interrupts</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="453"/>
        <source>Interrupts</source>
        <translation>Interrupts</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="454"/>
        <source>swi opcode color</source>
        <translation>swi opcode kleur</translation>
    </message>
    <message>
        <source>cmp opcode color</source>
        <translation type="vanished">cmp opcode kleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="458"/>
        <source>Registers color</source>
        <translation>Register kleuren</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="458"/>
        <source>Register</source>
        <translation>Register</translation>
    </message>
    <message>
        <source>Numeric constants color</source>
        <translation type="vanished">Nummerieke constanten kleur</translation>
    </message>
    <message>
        <source>Numbers</source>
        <translation type="vanished">Getallen</translation>
    </message>
    <message>
        <source>mov instructions color (mov, movd, movw etc</source>
        <translation type="vanished">mov instructie kleur (mov, movd, movw etc</translation>
    </message>
    <message>
        <source>mov</source>
        <translation type="vanished">mov</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="423"/>
        <source>Color of lines showing jump destination</source>
        <translation>De kleuren van de lijnen laten de bestemming van de jump zien</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="423"/>
        <source>Flow</source>
        <translation>Flow</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="434"/>
        <source>Color of arithmetic opcodes (add, div, mul etc)</source>
        <translation>De kleur van rekenkundige opcodes (add, div, mul etc)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="435"/>
        <source>Arithmetic</source>
        <translation>Arithmetic</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="437"/>
        <source>Color of binary operations (and, or, xor etc).</source>
        <translation>Kleur van binaire bewerkingen (and, or, xor etc).</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="437"/>
        <source>Binary</source>
        <translation type="unfinished">Binary</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="439"/>
        <source>Color of object names, commas between operators, squared brackets and operators inside them.</source>
        <translation>De kleur van objectnamen, komma&apos;s tussen operatoren, vierkante blokhaken en operators daarbinnen.</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="442"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="448"/>
        <source>Color of conditional jump opcodes such as je, jg, jne etc</source>
        <translation>De kleur van conditionele jump opcodes zoals je, jg, jne etc</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="449"/>
        <source>Conditional jump</source>
        <translation>Conditionele jump</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="456"/>
        <source>Color of compare instructions such as test and cmp</source>
        <translation>Kleur van vergelijkingsinstructies zoals test en cmp</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="457"/>
        <source>Compare instructions</source>
        <translation>Vergelijkingsinstructies</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="461"/>
        <source>Color of numeric constants and object pointers</source>
        <translation>De kleur van numerieke constanten en object pointers</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="461"/>
        <source>Constants</source>
        <translation>Constanten</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="463"/>
        <source>Color of move instructions such as mov, movd, lea etc</source>
        <translation>De kleur van move instructies zoals mov, mod, lea etc</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="464"/>
        <source>Move instructions</source>
        <translation>Move instructies</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="465"/>
        <source>Function variable color</source>
        <translation>Functie variabele kleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="465"/>
        <source>Function variable</source>
        <translation>Function variabele</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="467"/>
        <source>Function variable (local or argument) type color</source>
        <translation>Functie variabele (lokaal or argument) type kleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="468"/>
        <source>Variable type</source>
        <translation>Variabele type</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="470"/>
        <source>Function variable address color</source>
        <translation>Functie variabele adres kleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="470"/>
        <source>Variable address</source>
        <translation>Variabele adres</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="482"/>
        <source>In graph view jump arrow true</source>
        <translation>In het grafiekoverzicht de jump pijlen weergeven</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="482"/>
        <source>Arrow true</source>
        <translation>Pijlen aanzetten</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="484"/>
        <source>In graph view jump arrow false</source>
        <translation>In het grafiekoverzicht de jump pijlen niet weergeven</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="484"/>
        <source>Arrow false</source>
        <translation>Pijlen niet weergeven</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="486"/>
        <source>In graph view jump arrow (no condition)</source>
        <translation>In de grafiekweergave de jump pijl weergeven (jump zonder conditie)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="486"/>
        <source>Arrow</source>
        <translation>Pijl</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="490"/>
        <source>Background color of Graph Overview&apos;s node</source>
        <translation>Achtergrondkleur van Grafiek Overzicht nodes</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="491"/>
        <source>Graph Overview node</source>
        <translation>Grafiek overzicht node</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="493"/>
        <source>Fill color of Graph Overview&apos;s selection</source>
        <translation>De vulkleur van het de grafiek van de selectie</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="494"/>
        <source>Graph Overview fill</source>
        <translation>Grafiek overzicht vulkleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="496"/>
        <source>Border color of Graph Overview&apos;s selection</source>
        <translation>De randkleur van de grafiek overview selectie</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="497"/>
        <source>Graph Overview border</source>
        <translation>Grafiek overzicht rand</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="500"/>
        <source>General background color</source>
        <translation>Algemene achtergrondkleur</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="500"/>
        <source>Background</source>
        <translation>Achtergrond</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="502"/>
        <source>Background color of non-focused graph node</source>
        <translation>Achtergrondkleur van niet-gefocusede grafiek nodes</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="503"/>
        <source>Node background</source>
        <translation>Node achtergrond</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="511"/>
        <source>Background color of selected word</source>
        <translation>Achtergrondkleur van een geselecteerde word</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="512"/>
        <source>Main function color</source>
        <translation>Main functie kleur</translation>
    </message>
    <message>
        <source>Alt. background</source>
        <translation type="vanished">Alternatieve achtergrond</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="505"/>
        <source>Background of current graph node</source>
        <translation>Achtergrond van de huidige grafieknode</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="505"/>
        <source>Current graph node</source>
        <translation>Huidige grafiek node</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="507"/>
        <source>Color of node border in graph view</source>
        <translation>Kleur van de noderand in grafiekweergave</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="507"/>
        <source>Node border</source>
        <translation>Node rand</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="509"/>
        <source>Selected line background color</source>
        <translation>Achtergrond kleur voor de huidige regel</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="509"/>
        <source>Line highlight</source>
        <translation>Regel highlight</translation>
    </message>
    <message>
        <source>Highlighted word text color</source>
        <translation type="vanished">Highlighted word text color</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="511"/>
        <source>Word higlight</source>
        <translation>Word higlight</translation>
    </message>
    <message>
        <source>Color of main function color</source>
        <translation type="vanished">Kleur van de main functie</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="512"/>
        <source>Main</source>
        <translation>Main</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="521"/>
        <source>Code section color in navigation bar</source>
        <translation>Kleur van de code in de navigatie balk</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="521"/>
        <source>Navbar code</source>
        <translation>Navbar code</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="523"/>
        <source>Empty section color in navigation bar</source>
        <translation>Kleur van een lege sectie in de navigatie bar</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="523"/>
        <source>Navbar empty</source>
        <translation>Lege navbar</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="524"/>
        <source>ucall</source>
        <translation>ucall</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="525"/>
        <source>ujmp</source>
        <translation>ujmp</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="526"/>
        <source>Breakpoint background</source>
        <translation>Achtergrondkleur van een breakpoint</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="100"/>
        <source>Crash</source>
        <translation>Crash</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="101"/>
        <source>Cutter received a signal it can&apos;t handle and will close.&lt;br/&gt;Would you like to create a crash dump for a bug report?</source>
        <translation>Cutter heeft een signaal ontvangen dat het niet kan afhandelen. Wil je een crash dump en bug report maken?</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="104"/>
        <source>Create a Crash Dump</source>
        <translation>Maak een Crash Dump</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="105"/>
        <location filename="../../common/CrashHandler.cpp" line="148"/>
        <source>Quit</source>
        <translation type="unfinished">Quit</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="119"/>
        <source>Choose a directory to save the crash dump in</source>
        <translation>Kies een map om de crash dump in op te slaan</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="124"/>
        <source>Minidump (*.dmp)</source>
        <translation>Minidump (*.dmp)</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="133"/>
        <source>Save Crash Dump</source>
        <translation>Crash dump opslaan</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="134"/>
        <source>Failed to write to %1.&lt;br/&gt;Please make sure you have access to that directory and try again.</source>
        <translation>Het schrijven naar %1.&lt;br/&gt;Let op dat je toegang hebt tot de map, en probeer het opnieuw.</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="142"/>
        <source>Success</source>
        <translation type="unfinished">Success</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="143"/>
        <source>&lt;a href=&quot;%1&quot;&gt;Crash dump&lt;/a&gt; was successfully created.</source>
        <translation>De &lt;a href=&quot;%1&quot;&gt;Crash dump&lt;/a&gt; is gemaakt.</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="147"/>
        <source>Open an Issue</source>
        <translation>Rapporteer een probleem</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="156"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="157"/>
        <source>Error occurred during crash dump creation.</source>
        <translation>Er is een fout opgetreden bij het maken van de crash dump.</translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="53"/>
        <source>Write error</source>
        <translation type="unfinished">Error tijdens het schrijven</translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="54"/>
        <source>Your file is opened in read-only mode. Editing is only available when the file is opened in either Write or Cache modes.

WARNING: In Write mode, any changes will be committed to the file on disk. For safety, please consider using Cache mode and then commit the changes manually via File -&gt; Commit modifications to disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="60"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="62"/>
        <source>Reopen in Write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="64"/>
        <source>Enable Cache mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="99"/>
        <source>Uncomitted changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="100"/>
        <source>It seems that you have changes or patches that are not committed to the file.
Do you want to commit them now?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickFilterView</name>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="41"/>
        <source>Quick Filter</source>
        <translation type="unfinished">Quick Filter</translation>
    </message>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="61"/>
        <source>X</source>
        <translation>X</translation>
    </message>
</context>
<context>
    <name>R2DecDecompiler</name>
    <message>
        <source>Failed to parse JSON from r2dec</source>
        <translation type="vanished">Fout bij het verwerken van JSON van r2dec</translation>
    </message>
</context>
<context>
    <name>R2PluginsDialog</name>
    <message>
        <source>radare2 plugin information</source>
        <translation type="vanished">radare2 plugin informatie</translation>
    </message>
    <message>
        <source>RBin</source>
        <translation type="vanished">RBin</translation>
    </message>
    <message>
        <source>RBin plugins</source>
        <translation type="vanished">RBin plugins</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="obsolete">Description</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">Licentie</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Type</translation>
    </message>
    <message>
        <source>RIO</source>
        <translation type="vanished">RIO</translation>
    </message>
    <message>
        <source>RIO plugins</source>
        <translation type="vanished">RIO plugins</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="obsolete">Permissions</translation>
    </message>
    <message>
        <source>RCore</source>
        <translation type="vanished">RCore</translation>
    </message>
    <message>
        <source>RCore plugins</source>
        <translation type="vanished">RCore plugins</translation>
    </message>
    <message>
        <source>RAsm</source>
        <translation type="vanished">RAsm</translation>
    </message>
    <message>
        <source>RAsm plugins</source>
        <translation type="vanished">RAsm plugins</translation>
    </message>
    <message>
        <source>Architecture</source>
        <translation type="vanished">Architectuur</translation>
    </message>
    <message>
        <source>CPU&apos;s</source>
        <translation type="vanished">CPU&apos;s</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="vanished">Versie</translation>
    </message>
    <message>
        <source>Author</source>
        <translation type="vanished">Auteur</translation>
    </message>
    <message>
        <source>Cutter</source>
        <translation type="obsolete">Cutter</translation>
    </message>
    <message>
        <source>Cutter plugins</source>
        <translation type="vanished">Cutter plugins</translation>
    </message>
</context>
<context>
    <name>R2TaskDialog</name>
    <message>
        <source>R2 Task</source>
        <translation type="vanished">R2 Taak</translation>
    </message>
    <message>
        <source>R2 task in progress..</source>
        <translation type="vanished">R2 taak in uitvoering..</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Time</translation>
    </message>
    <message>
        <source>Running for</source>
        <translation type="obsolete">Running for</translation>
    </message>
    <message numerus="yes">
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation type="obsolete">
            <numerusform>%n hour</numerusform>
            <numerusform>%n hour</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation type="obsolete">
            <numerusform>%n minute</numerusform>
            <numerusform>%n minute</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation type="obsolete">
            <numerusform>%n seconds</numerusform>
            <numerusform>%n seconds</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RawAddrDock</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="500"/>
        <source>Raw</source>
        <translation>Raw</translation>
    </message>
</context>
<context>
    <name>RegisterRefModel</name>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="67"/>
        <source>Register</source>
        <translation>Register</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="69"/>
        <source>Value</source>
        <translation type="unfinished">Value</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="71"/>
        <source>Reference</source>
        <translation>Referentie</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="73"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegisterRefsWidget</name>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="137"/>
        <source>Copy register value</source>
        <translation>Kopieer registerwaarde</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="138"/>
        <source>Copy register reference</source>
        <translation>Kopieer register adres</translation>
    </message>
</context>
<context>
    <name>RelocsModel</name>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="54"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="56"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="58"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="60"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RelocsWidget</name>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="131"/>
        <source>Relocs</source>
        <translation type="unfinished">Relocs</translation>
    </message>
</context>
<context>
    <name>RemoteDebugDialog</name>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="43"/>
        <source>Debugger:</source>
        <translation>Debugger:</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="82"/>
        <source>IP or Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="159"/>
        <source>Remove item</source>
        <translation type="unfinished">Item wissen</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="164"/>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="167"/>
        <source>Remove all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GDB</source>
        <translation type="vanished">GDB</translation>
    </message>
    <message>
        <source>WinDbg - Pipe</source>
        <translation type="vanished">WinDbg - Pipe</translation>
    </message>
    <message>
        <source>IP:</source>
        <translation type="vanished">IP:</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="75"/>
        <source>Port:</source>
        <translation>Poort:</translation>
    </message>
    <message>
        <source>Path:</source>
        <translation type="vanished">Pad:</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="57"/>
        <source>Invalid debugger</source>
        <translation>Ongeldige debugger</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="68"/>
        <source>Invalid IP address</source>
        <translation>Ongeldig IP-adres</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="81"/>
        <source>Path does not exist</source>
        <translation>Pad bestaat niet</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="94"/>
        <source>Invalid port</source>
        <translation>Ongeldige poort</translation>
    </message>
</context>
<context>
    <name>RenameDialog</name>
    <message>
        <source>Name:</source>
        <translation type="vanished">Naam:</translation>
    </message>
</context>
<context>
    <name>ResourcesModel</name>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="76"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="78"/>
        <source>Vaddr</source>
        <translation>Vaddr</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="80"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="82"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="84"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="86"/>
        <source>Lang</source>
        <translation>Taal</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="88"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ResourcesWidget</name>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="118"/>
        <source>Resources</source>
        <translation type="unfinished">Resources</translation>
    </message>
</context>
<context>
    <name>RizinGraphWidget</name>
    <message>
        <location filename="../../widgets/RizinGraphWidget.ui" line="57"/>
        <source>ag...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="21"/>
        <source>Data reference graph (aga)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="22"/>
        <source>Global data references graph (agA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="26"/>
        <source>Imports graph (agi)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="27"/>
        <source>References graph (agr)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="28"/>
        <source>Global references graph (agR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="29"/>
        <source>Cross references graph (agx)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="30"/>
        <source>Custom graph (agg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="31"/>
        <source>User command</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RizinPluginsDialog</name>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="14"/>
        <source>Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="24"/>
        <source>RzBin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="30"/>
        <source>RzBin plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="41"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="82"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="123"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="154"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="46"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="87"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="128"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="174"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="51"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="92"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="179"/>
        <source>License</source>
        <translation type="unfinished">Licentie</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="56"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="65"/>
        <source>RzIO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="71"/>
        <source>RzIO plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="97"/>
        <source>Permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="106"/>
        <source>RzCore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="112"/>
        <source>RzCore plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="137"/>
        <source>RzAsm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="143"/>
        <source>RzAsm plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="159"/>
        <source>Architecture</source>
        <translation type="unfinished">Architectuur</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="164"/>
        <source>CPU&apos;s</source>
        <translation type="unfinished">CPU&apos;s</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="169"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="184"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RizinTaskDialog</name>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="14"/>
        <source>Rizin Task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="20"/>
        <source>Rizin task in progress..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="27"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="32"/>
        <source>Running for</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="34"/>
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="38"/>
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="41"/>
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RunScriptTask</name>
    <message>
        <location filename="../../common/RunScriptTask.cpp" line="18"/>
        <source>Executing script...</source>
        <translation type="unfinished">Executing script...</translation>
    </message>
    <message>
        <location filename="../../common/RunScriptTask.h" line="15"/>
        <source>Run Script</source>
        <translation type="unfinished">Run Script</translation>
    </message>
</context>
<context>
    <name>SaveProjectDialog</name>
    <message>
        <source>Save Project</source>
        <translation type="vanished">Project opslaan</translation>
    </message>
    <message>
        <source>Project name (prj.name):</source>
        <translation type="vanished">Project naam (prj.name):</translation>
    </message>
    <message>
        <source>Projects path (dir.projects):</source>
        <translation type="vanished">Pad voor projecten (dir.projects):</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="obsolete">Select</translation>
    </message>
    <message>
        <source>Use simple project saving style (prj.simple, recommended)</source>
        <translation type="vanished">Sla op als simpel project (prj.simple, aangeraden)</translation>
    </message>
    <message>
        <source>Save the target binary inside the project directory (prj.files)</source>
        <translation type="vanished">Sla de binary op binnen de projectmap (prj.files)</translation>
    </message>
    <message>
        <source>Project is a git repo and saving is committing (prj.git)</source>
        <translation type="vanished">Het project is een git repository en opslaan staat gelijk aan commiten (prj.git)</translation>
    </message>
    <message>
        <source>Use ZIP format for project files (prj.zip)</source>
        <translation type="vanished">Gebruik het ZIP formaat voor projecten (prj.zip)</translation>
    </message>
    <message>
        <source>Select project path (dir.projects)</source>
        <translation type="obsolete">Select project path (dir.projects)</translation>
    </message>
    <message>
        <source>Save project</source>
        <translation type="vanished">Sla project op</translation>
    </message>
    <message>
        <source>Invalid project name.</source>
        <translation type="vanished">Ongeldige projectnaam.</translation>
    </message>
</context>
<context>
    <name>SdbDock</name>
    <message>
        <source>Key</source>
        <translation type="vanished">Key</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Value</translation>
    </message>
</context>
<context>
    <name>SdbWidget</name>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="17"/>
        <source>SDB Browser</source>
        <translation type="unfinished">SDB Browser</translation>
    </message>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="120"/>
        <source>Key</source>
        <translation type="unfinished">Key</translation>
    </message>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="125"/>
        <source>Value</source>
        <translation type="unfinished">Value</translation>
    </message>
</context>
<context>
    <name>SearchModel</name>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="96"/>
        <source>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Weergave&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="115"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="117"/>
        <source>Offset</source>
        <translation type="unfinished">Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="119"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="121"/>
        <source>Data</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="123"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="83"/>
        <source>Search</source>
        <translation type="unfinished">Search</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="90"/>
        <source>Search for:</source>
        <translation>Zoek naar:</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="100"/>
        <source>Search in:</source>
        <translation>Zoeken in:</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="251"/>
        <source>asm code</source>
        <translation>asm code</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="252"/>
        <source>string</source>
        <translation>string</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="253"/>
        <source>hex string</source>
        <translation>hex string</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="254"/>
        <source>ROP gadgets</source>
        <translation>ROP gadgets</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="255"/>
        <source>32bit value</source>
        <translation>32bit waarde</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="282"/>
        <source>No results found for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="285"/>
        <source>No Results Found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SectionsModel</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="93"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="95"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="101"/>
        <source>Virtual Size</source>
        <translation>Virtuele grootte</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="97"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="99"/>
        <source>End Address</source>
        <translation>Eind adres</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="103"/>
        <source>Permissions</source>
        <translation type="unfinished">Permissions</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="105"/>
        <source>Entropy</source>
        <translation>Entropie</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="107"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SegmentsModel</name>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="81"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="83"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="85"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="87"/>
        <source>End Address</source>
        <translation>Eind adres</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="89"/>
        <source>Permissions</source>
        <translation type="unfinished">Permissions</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="91"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetFunctionVarTypes</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Dialog</translation>
    </message>
    <message>
        <source>Set Type To:</source>
        <translation type="vanished">Zet type:</translation>
    </message>
    <message>
        <source>Set Name To:</source>
        <translation type="vanished">Zet naam:</translation>
    </message>
    <message>
        <source>Modify:</source>
        <translation type="vanished">Wijzig:</translation>
    </message>
    <message>
        <source>You must be in a function to define variable types.</source>
        <translation type="vanished">Alleen binnen een functie kunnen variabelen gedefinieerd worden.</translation>
    </message>
</context>
<context>
    <name>SetToDataDialog</name>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="14"/>
        <source>Set to Data</source>
        <translation>Zet om naar Data</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="20"/>
        <location filename="../../dialogs/SetToDataDialog.ui" line="34"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="27"/>
        <source>Start address</source>
        <translation>Start adres</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="41"/>
        <source>End address</source>
        <translation>Eind adres</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="48"/>
        <source>Item size</source>
        <translation>Item grootte</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="55"/>
        <source>Number of items</source>
        <translation>Aantal items</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="62"/>
        <location filename="../../dialogs/SetToDataDialog.ui" line="69"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
</context>
<context>
    <name>SideBar</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Form</translation>
    </message>
    <message>
        <source>Script</source>
        <translation type="vanished">Script</translation>
    </message>
    <message>
        <source>X</source>
        <translation type="vanished">X</translation>
    </message>
    <message>
        <source>example.py</source>
        <translation type="vanished">example.py</translation>
    </message>
    <message>
        <source>Execution finished</source>
        <translation type="vanished">Klaar met uitvoeren</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation type="vanished">Rekenmachine</translation>
    </message>
    <message>
        <source>Assembler</source>
        <translation type="obsolete">Assembler</translation>
    </message>
    <message>
        <source>Assembly</source>
        <translation type="vanished">Assembly</translation>
    </message>
    <message>
        <source>v</source>
        <translation type="vanished">v</translation>
    </message>
    <message>
        <source>^</source>
        <translation type="vanished">^</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation type="obsolete">Hexadecimal</translation>
    </message>
    <message>
        <source>Toogle resposiveness</source>
        <translation type="vanished">Zet responsive interface aan/uit</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Instellingen</translation>
    </message>
</context>
<context>
    <name>SidebarWidget</name>
    <message>
        <source> Function:</source>
        <translation type="vanished"> Functie:</translation>
    </message>
    <message>
        <source>Offset info:</source>
        <translation type="vanished">Offset informatie:</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="obsolete">Info</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Value</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>Opcode description:</source>
        <translation type="vanished">Opcode beschrijving:</translation>
    </message>
    <message>
        <source>Function registers info:</source>
        <translation type="vanished">Functie registers informatie:</translation>
    </message>
    <message>
        <source>X-Refs to current address:</source>
        <translation type="vanished">X-Refs naar het huidige adres:</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">Address</translation>
    </message>
    <message>
        <source>Instruction</source>
        <translation type="obsolete">Instruction</translation>
    </message>
    <message>
        <source>X-Refs from current address:</source>
        <translation type="vanished">X-Refs vanaf het huidige adres:</translation>
    </message>
</context>
<context>
    <name>SimpleTextGraphView</name>
    <message>
        <location filename="../../widgets/SimpleTextGraphView.cpp" line="29"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StackModel</name>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="216"/>
        <source>Offset</source>
        <translation type="unfinished">Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="218"/>
        <source>Value</source>
        <translation type="unfinished">Value</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="220"/>
        <source>Reference</source>
        <translation type="unfinished">Reference</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="222"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StackWidget</name>
    <message>
        <source>Offset</source>
        <translation type="obsolete">Offset</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Value</translation>
    </message>
    <message>
        <source>Reference</source>
        <translation type="obsolete">Reference</translation>
    </message>
    <message>
        <source>Seek to this offset</source>
        <translation type="vanished">Seek naar deze offset</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="33"/>
        <source>Edit stack value...</source>
        <translation>Wijzig stack waarde...</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="109"/>
        <source>Edit stack at %1</source>
        <translation>Wijzig stack op %1</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="138"/>
        <source>Stack position</source>
        <translation>Stack positie</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="140"/>
        <source>Pointed memory</source>
        <translation>Verwezen geheugen</translation>
    </message>
</context>
<context>
    <name>StringsModel</name>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="67"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="69"/>
        <source>String</source>
        <translation type="unfinished">String</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="71"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="73"/>
        <source>Length</source>
        <translation>Lengte</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="75"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="77"/>
        <source>Section</source>
        <translation>Selectie</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="79"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StringsTask</name>
    <message>
        <location filename="../../common/StringsTask.h" line="13"/>
        <source>Searching for Strings</source>
        <translation>Zoeken naar Strings</translation>
    </message>
</context>
<context>
    <name>StringsWidget</name>
    <message>
        <source>Copy Address</source>
        <translation type="vanished">Kopieer adres</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.ui" line="76"/>
        <source>Copy String</source>
        <translation>Kopieer String</translation>
    </message>
    <message>
        <source>Xrefs</source>
        <translation type="vanished">Xrefs</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.ui" line="81"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="161"/>
        <source>Section:</source>
        <translation>Sectie:</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="244"/>
        <source>(all)</source>
        <translation type="unfinished">(all)</translation>
    </message>
</context>
<context>
    <name>SymbolsModel</name>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="58"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="60"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="62"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="64"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SymbolsWidget</name>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="123"/>
        <source>Symbols</source>
        <translation type="unfinished">Symbols</translation>
    </message>
</context>
<context>
    <name>ThreadsWidget</name>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="24"/>
        <source>PID</source>
        <translation type="unfinished">PID</translation>
    </message>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="25"/>
        <source>Status</source>
        <translation type="unfinished">Status</translation>
    </message>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="26"/>
        <source>Path</source>
        <translation type="unfinished">Path</translation>
    </message>
</context>
<context>
    <name>TypesInteractionDialog</name>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialog</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="23"/>
        <source>Load From File:</source>
        <translation>Laden uit bestand:</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="37"/>
        <source>Select File</source>
        <translation>Selecteer bestand</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="50"/>
        <source>Enter Types Manually</source>
        <translation>Voer types handmatig in</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="32"/>
        <source>Select file</source>
        <translation type="unfinished">Select file</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="40"/>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="68"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="69"/>
        <source>There was some error while loading new types</source>
        <translation>Er is een fout opgetreden bij het laden van nieuwe types</translation>
    </message>
</context>
<context>
    <name>TypesModel</name>
    <message>
        <source>Type</source>
        <translation type="obsolete">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="62"/>
        <source>Type / Name</source>
        <translation>Type / Naam</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="64"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="66"/>
        <source>Format</source>
        <translation type="unfinished">Format</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="68"/>
        <source>Category</source>
        <translation>Categorie</translation>
    </message>
</context>
<context>
    <name>TypesWidget</name>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="76"/>
        <location filename="../../widgets/TypesWidget.ui" line="79"/>
        <source>Export Types</source>
        <translation>Export Types</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="84"/>
        <location filename="../../widgets/TypesWidget.ui" line="87"/>
        <location filename="../../widgets/TypesWidget.cpp" line="286"/>
        <source>Load New Types</source>
        <translation>Nieuwe types laden</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="92"/>
        <location filename="../../widgets/TypesWidget.ui" line="95"/>
        <source>Delete Type</source>
        <translation>Verwijder type</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="100"/>
        <source>Link Type to Address</source>
        <translation type="unfinished">Link Type to Address</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="140"/>
        <source>Category</source>
        <translation>Categorie</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="185"/>
        <source>View Type</source>
        <translation>Bekijk type</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="186"/>
        <source>Edit Type</source>
        <translation>Bewerk type</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="217"/>
        <source>(All)</source>
        <translation>(Alles)</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="266"/>
        <source>Save File</source>
        <translation>Bestand opslaan</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="273"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="302"/>
        <source>Edit Type: </source>
        <translation>Bewerk type: </translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="305"/>
        <location filename="../../widgets/TypesWidget.cpp" line="355"/>
        <source>View Type: </source>
        <translation>Bekijk type: </translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="305"/>
        <location filename="../../widgets/TypesWidget.cpp" line="355"/>
        <source> (Read Only)</source>
        <translation> (Alleen-Lezen)</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="324"/>
        <source>Cutter</source>
        <translation type="unfinished">Cutter</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="324"/>
        <source>Are you sure you want to delete &quot;%1&quot;?</source>
        <translation>Weet u zeker dat u &quot;%1&quot; wilt verwijderen?</translation>
    </message>
</context>
<context>
    <name>UpdateWorker</name>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="34"/>
        <source>Time limit exceeded during version check. Please check your internet connection and try again.</source>
        <translation>Het tijdslimiet is overschreden tijdens de versiecontrole. Controleer uw internetverbinding en probeer het opnieuw.</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="81"/>
        <source>Version control</source>
        <translation type="unfinished">Version control</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="82"/>
        <source>There is an update available for Cutter.&lt;br/&gt;</source>
        <translation>Er is een update beschikbaar voor Cutter.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="82"/>
        <source>Current version:</source>
        <translation type="unfinished">Current version:</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="83"/>
        <source>Latest version:</source>
        <translation type="unfinished">Latest version:</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="85"/>
        <source>For update, please check the link:&lt;br/&gt;</source>
        <translation>Om te updaten, volg de volgende link:&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="89"/>
        <source>or click &quot;Download&quot; to download latest version of Cutter.</source>
        <translation>of klik &quot;Download&quot; om de laatste versie van Cutter te downloaden.</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="92"/>
        <source>Don&apos;t check for updates</source>
        <translation>Niet controleren op updates</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="96"/>
        <source>Download</source>
        <translation>Downloaden</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="103"/>
        <source>Choose directory for downloading</source>
        <translation>Kies een map om de update te downloaden</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="108"/>
        <source>Downloading update...</source>
        <translation>Update downloaden...</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="108"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancel</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="116"/>
        <source>Download finished!</source>
        <translation>Klaar met downloaden!</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="117"/>
        <source>Latest version of Cutter was succesfully downloaded!</source>
        <translation>De laatste versie van Cutter is succesvol gedownload!</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="119"/>
        <source>Open file</source>
        <translation type="unfinished">Open file</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="120"/>
        <source>Open download folder</source>
        <translation>Open downloadmap</translation>
    </message>
</context>
<context>
    <name>VTableModel</name>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="63"/>
        <source>VTable</source>
        <translation>VTable</translation>
    </message>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="84"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="86"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
</context>
<context>
    <name>VersionInfoDialog</name>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="50"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="64"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="94"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="126"/>
        <source>Key</source>
        <translation type="unfinished">Key</translation>
    </message>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="99"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="131"/>
        <source>Value</source>
        <translation type="unfinished">Value</translation>
    </message>
</context>
<context>
    <name>VirtualAddrDock</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="508"/>
        <source>Virtual</source>
        <translation>Virtueel</translation>
    </message>
</context>
<context>
    <name>VisualNavbar</name>
    <message>
        <location filename="../../widgets/VisualNavbar.cpp" line="29"/>
        <source>Visual navigation bar</source>
        <translation>Visuele navigatiebar</translation>
    </message>
</context>
<context>
    <name>WelcomeDialog</name>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="20"/>
        <source>Welcome to Cutter</source>
        <translation>Welkom bij Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="83"/>
        <source>Cutter</source>
        <translation type="unfinished">Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="104"/>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="19"/>
        <source>Version </source>
        <translation>Versie </translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="168"/>
        <source>About</source>
        <translation type="unfinished">About</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="203"/>
        <source>Native Theme</source>
        <translation>Standaard Thema</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="208"/>
        <source>Dark Theme</source>
        <translation type="unfinished">Dark Theme</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="213"/>
        <source>Midnight Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="218"/>
        <source>Light Theme</source>
        <translation>Licht Thema</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="229"/>
        <source>Check for updates on start</source>
        <translation type="unfinished">Check for updates on start</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="298"/>
        <source>Community</source>
        <translation>Gemeenschap</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="320"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/cutter_re&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@cutter_re&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/cutter_re&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@cutter_re &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="355"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;/span&gt;&lt;a href=&quot;https://github.com/rizinorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and report bugs or contribute code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Kom samen met duizenden reverse engineers in onze gemeenschap: &lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter op &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;/span&gt;&lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and report bugs or contribute code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Wil jij helpen met het verbeteren van Cutter?&lt;br/&gt;Bezoek onze &lt;/span&gt;&lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github pagina&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; en rapporteer fouten of werk mee aan de code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="417"/>
        <source>Continue</source>
        <translation type="unfinished">Continue</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;	&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;#cutter on &lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;	&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;#cutter on &lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt; and report bugs or contribute code.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wil je Cutter nog beter maken?&lt;br/&gt;Bezoek onze &lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Github pagina&lt;/span&gt;&lt;/a&gt; en rapporteer fouten of draag bij aan de code.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="394"/>
        <source>Contributing</source>
        <translation>Bijdragen</translation>
    </message>
    <message>
        <source>Continue 🢒</source>
        <translation type="vanished">Doorgaan</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="70"/>
        <source>Language settings</source>
        <translation type="unfinished">Language settings</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="71"/>
        <source>Language will be changed after next application start.</source>
        <translation type="unfinished">Language will be changed after next application start.</translation>
    </message>
</context>
<context>
    <name>XrefModel</name>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="287"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="291"/>
        <source>Code</source>
        <translation type="unfinished">Code</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="293"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="289"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
</context>
<context>
    <name>XrefsDialog</name>
    <message>
        <source>Address</source>
        <translation type="obsolete">Address</translation>
    </message>
    <message>
        <source>Code</source>
        <translation type="obsolete">Code</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Type</translation>
    </message>
    <message>
        <source>X-Refs to %1:</source>
        <translation type="vanished">X-Refs to %1:</translation>
    </message>
    <message>
        <source>X-Refs from %1:</source>
        <translation type="vanished">X-Refs from %1:</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="145"/>
        <source>X-Refs to %1 (%2 results):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="147"/>
        <source>X-Refs from %1 (%2 results):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="152"/>
        <source>Writes to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="153"/>
        <source>Reads from %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="164"/>
        <location filename="../../dialogs/XrefsDialog.cpp" line="183"/>
        <source>X-Refs for %1</source>
        <translation>X-Refs for %1</translation>
    </message>
</context>
<context>
    <name>ZignaturesModel</name>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="66"/>
        <source>Offset</source>
        <translation type="unfinished">Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="68"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="70"/>
        <source>Bytes</source>
        <translation type="unfinished">Bytes</translation>
    </message>
</context>
</TS>
