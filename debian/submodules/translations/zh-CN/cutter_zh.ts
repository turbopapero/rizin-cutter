<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh-CN" sourcelanguage="en">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="14"/>
        <source>About Cutter</source>
        <translation>关于 Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="69"/>
        <source>Check for updates on start</source>
        <translation>启动时检查更新</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="95"/>
        <source>Show version information</source>
        <translation>显示版本信息</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="108"/>
        <source>Show Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by Rizin&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show radare2 plugin information</source>
        <translation type="vanished">显示 radare2 插件信息</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by radare2&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by radare2&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="128"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:28pt; font-weight:600;&quot;&gt;Cutter&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:28pt; font-weight:600;&quot;&gt;Cutter&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Show plugin information</source>
        <translation type="vanished">显示插件信息</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="82"/>
        <source>Check for updates</source>
        <translation>检查更新</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="28"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <source>Using r2-</source>
        <translation type="vanished">使用 r2-</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="28"/>
        <source>Using rizin </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="30"/>
        <source>Optional Features:</source>
        <translation>可选功能：</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="47"/>
        <source>License</source>
        <translation>许可</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="48"/>
        <source>This Software is released under the GNU General Public License v3.0</source>
        <translation>本软件根据 GNU 通用公共许可证 v3.0 发布。</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="49"/>
        <source>Authors</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="50"/>
        <source>Cutter is developed by the community and maintained by its core and development teams.&lt;br/&gt;</source>
        <translation>Cutter 由社区开发，并由其核心和开发团队维护。&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="52"/>
        <source>Check our &lt;a href=&apos;https://github.com/rizinorg/cutter/graphs/contributors&apos;&gt;contributors page&lt;/a&gt; for the full list of contributors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="76"/>
        <source>Rizin version information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check our &lt;a href=&apos;https://github.com/radareorg/cutter/graphs/contributors&apos;&gt;contributors page&lt;/a&gt; for the full list of contributors.</source>
        <translation type="vanished">查看我们的 &lt;a href=&apos;https://github.com/radareorg/cutter/graphs/contributors&apos;&gt;贡献者页面&lt;/a&gt; 以获取全部贡献者列表。</translation>
    </message>
    <message>
        <source>radare2 version information</source>
        <translation type="vanished">radare2 版本信息</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="99"/>
        <source>Checking for updates...</source>
        <translation>正在检查更新…</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="109"/>
        <source>Cutter is up to date!</source>
        <translation>Cutter 已是最新版本！</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="150"/>
        <source>Based on Qt %1 (%2, %3 bit)</source>
        <translation>基于 Qt %1 (%2, %3 bit)</translation>
    </message>
    <message>
        <source>Timeout error!</source>
        <translation type="vanished">超时错误！</translation>
    </message>
    <message>
        <source>Please check your internet connection and try again.</source>
        <translation type="vanished">请检查网络连接，然后重试。</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="105"/>
        <source>Error!</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="108"/>
        <source>Version control</source>
        <translation>版本控制</translation>
    </message>
    <message>
        <source>You have latest version and no need to update!</source>
        <translation type="vanished">已是最新版本，无需更新！</translation>
    </message>
    <message>
        <source>Current version:</source>
        <translation type="vanished">当前版本：</translation>
    </message>
    <message>
        <source>Latest version:</source>
        <translation type="vanished">最新版本：</translation>
    </message>
    <message>
        <source>For update, please check the link:</source>
        <translation type="vanished">相关更新，请检查链接：</translation>
    </message>
</context>
<context>
    <name>AddressableDockWidget</name>
    <message>
        <location filename="../../widgets/AddressableDockWidget.cpp" line="12"/>
        <source>Sync/unsync offset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddressableItemContextMenu</name>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="16"/>
        <source>Show in</source>
        <translation>显示</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="17"/>
        <source>Copy address</source>
        <translation>复制地址</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="18"/>
        <source>Show X-Refs</source>
        <translation>显示 X-Refs</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="19"/>
        <source>Add comment</source>
        <translation>添加注释</translation>
    </message>
</context>
<context>
    <name>AnalClassesModel</name>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="403"/>
        <source>class</source>
        <translation>class</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="427"/>
        <source>base</source>
        <translation>base</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="458"/>
        <source>method</source>
        <translation>method</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="494"/>
        <source>vtable</source>
        <translation>vtable</translation>
    </message>
</context>
<context>
    <name>AnalOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="20"/>
        <source>Analysis</source>
        <translation type="unfinished">分析</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="55"/>
        <source>Show verbose information when performing analysis (analysis.verbose)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="68"/>
        <source>Analyze push+ret as jmp (analysis.pushret)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="81"/>
        <source>Verbose output from type analysis (analysis.types.verbose)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="94"/>
        <source>Speculatively set a name for the functions (analysis.autoname)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="107"/>
        <source>Search for new functions following already defined functions (analysis.hasnext)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="120"/>
        <source>Create references for unconditional jumps (analysis.jmp.ref)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="133"/>
        <source>Analyze jump tables in switch statements (analysis.jmp.tbl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="161"/>
        <source>Search boundaries for analysis (analysis.in): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="195"/>
        <source>Pointer depth (analysis.ptrdepth):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="236"/>
        <source>Functions Prelude (analysis.prelude):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="269"/>
        <source>Analyze program</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AnalTask</name>
    <message>
        <location filename="../../common/AnalTask.cpp" line="26"/>
        <source>Analyzing Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="43"/>
        <source>Loading the file...</source>
        <translation>正在加载文件...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="69"/>
        <source>Loading PDB file...</source>
        <translation>正在加载 PDB 文件...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="78"/>
        <source>Loading shellcode...</source>
        <translation>正在加载 shellcode ...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="89"/>
        <source>Executing script...</source>
        <translation>正在执行脚本...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="98"/>
        <source>Executing analysis...</source>
        <translation>执行分析...</translation>
    </message>
    <message>
        <source>Analyzing...</source>
        <translation type="vanished">分析中…</translation>
    </message>
    <message>
        <source>Running</source>
        <translation type="vanished">运行中</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="107"/>
        <source>Analysis complete!</source>
        <translation>分析完成！</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="109"/>
        <source>Skipping Analysis.</source>
        <translation>跳过分析。</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="24"/>
        <source>Initial Analysis</source>
        <translation>初步分析</translation>
    </message>
</context>
<context>
    <name>AppearanceOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="14"/>
        <source>Appearance</source>
        <translation>外观</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="28"/>
        <source>Font:</source>
        <translation>字体：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="56"/>
        <source>Select font</source>
        <translation>选择字体</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="76"/>
        <source>Zoom</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="95"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="116"/>
        <source>Language:</source>
        <translation>语言：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="133"/>
        <source>Interface Theme:</source>
        <translation>界面主题：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="160"/>
        <source>Color Theme:</source>
        <translation>颜色主题：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="179"/>
        <source>Edit Theme</source>
        <translation>编辑主题</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="193"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="241"/>
        <source>Export</source>
        <translation>导出</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="255"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="289"/>
        <source>Use information provided by decompiler when highlighting code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="292"/>
        <source>Decompiler based highlighting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Qt Theme:</source>
        <translation type="vanished">Qt 主题：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="147"/>
        <source>Default</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="152"/>
        <source>Dark</source>
        <translation>深色</translation>
    </message>
    <message>
        <source>Color Theme</source>
        <translation type="vanished">颜色主题</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="213"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="227"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="163"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">语言</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="312"/>
        <source>Save as Default</source>
        <translation>另存为默认</translation>
    </message>
    <message>
        <source>Enter scheme name</source>
        <translation type="vanished">输入方案名</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="136"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="221"/>
        <source>Name:</source>
        <translation>名字：</translation>
    </message>
    <message>
        <source>Are you sure you want to delete theme </source>
        <translation type="vanished">你确定要删除该主题么？ </translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="125"/>
        <source>Theme Editor - &lt;%1&gt;</source>
        <translation>主题编辑器 - &lt;%1&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="136"/>
        <source>Enter theme name</source>
        <translation>输入主题名称</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="137"/>
        <source> - copy</source>
        <translation> - copy</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="143"/>
        <source>Theme Copy</source>
        <translation>主题复制</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="144"/>
        <source>Theme named %1 already exists.</source>
        <translation>名为 %1 的主题已存在。</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="159"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="168"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="190"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="214"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="229"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="163"/>
        <source>Are you sure you want to delete &lt;b&gt;%1&lt;/b&gt;?</source>
        <translation>您确定你要删除 &lt;b&gt;%1&lt;/b&gt;？</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="185"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="211"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="186"/>
        <source>Color theme &lt;b&gt;%1&lt;/b&gt; was successfully imported.</source>
        <translation>颜色主题 &lt;b&gt;%1&lt;/b&gt; 已成功导入。</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="212"/>
        <source>Color theme &lt;b&gt;%1&lt;/b&gt; was successfully exported.</source>
        <translation>颜色主题 &lt;b&gt;%1&lt;/b&gt; 已成功导出。</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="221"/>
        <source>Enter new theme name</source>
        <translation>输入新主题的名称</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="240"/>
        <source>Language settings</source>
        <translation>语言设置</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="241"/>
        <source>Language will be changed after next application start.</source>
        <translation>在下次启动应用程序后，语言将被更改。</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="245"/>
        <source>Cannot set language, not found in available ones</source>
        <translation>无法设置语言，在可用语言中找不到。</translation>
    </message>
</context>
<context>
    <name>AsmOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="14"/>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="65"/>
        <source>Disassembly</source>
        <translation>反汇编</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="35"/>
        <source>Style</source>
        <translation>样式</translation>
    </message>
    <message>
        <source>Show ESIL instead of assembly (asm.esil)</source>
        <translation type="vanished">显示 ESIL 而不是汇编 (asm.esil)</translation>
    </message>
    <message>
        <source>Show pseudocode instead of assembly (asm.pseudo)</source>
        <translation type="vanished">显示伪代码而不是汇编 (asm.pseudo)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="170"/>
        <source>Show offsets (asm.offset)</source>
        <translation>显示偏移(asm.offset)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="71"/>
        <source>Display the bytes of each instruction (asm.bytes)</source>
        <translation>显示每条指令的字节 (asm.bytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="291"/>
        <source>Comments</source>
        <translation>注释</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="299"/>
        <source>Show opcode description (asm.describe)</source>
        <translation>显示 opcode 的描述 (asm.describe)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="78"/>
        <source>Syntax (asm.syntax):</source>
        <translation>语法 (asm.syntax)：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="198"/>
        <source>Lowercase</source>
        <translation>小写字母</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="203"/>
        <source>Uppercase (asm.ucase)</source>
        <translation>大写 (asm.ucase)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="208"/>
        <source>Capitalize (asm.capitalize)</source>
        <translation>首字母大写 (asm.capitalize)</translation>
    </message>
    <message>
        <source>Separate bytes with whitespace (asm.bytespace)</source>
        <translation type="vanished">使用空格分隔字节 (asm.bytespace)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="121"/>
        <source>Indent disassembly based on reflines depth (asm.indent)</source>
        <translation>基于调用关系缩进反汇编结果 (asm.indent)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="98"/>
        <source>Show Disassembly as:</source>
        <translation>将反汇编显示为：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="190"/>
        <source>Show empty line after every basic block (asm.bb.line)</source>
        <translation>在每个基本方块后显示空行 (asm.bb.line)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="152"/>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="307"/>
        <source>Normal</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="157"/>
        <source>ESIL (asm.esil)</source>
        <translation>ESIL (asm.esil)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="162"/>
        <source>Pseudocode (asm.pseudo)</source>
        <translation>伪代码</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="216"/>
        <source>Align bytes to the left (asm.lbytes)</source>
        <translation>字节按左对齐 (asm.lbytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="223"/>
        <source>Separate bytes with whitespace (asm.bytes.space)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="230"/>
        <source>Display flags&apos; real name (asm.flags.real)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="259"/>
        <source>Show offsets relative to:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="266"/>
        <source>Functions (asm.reloff)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="275"/>
        <source>Flags (asm.reloff.flags)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="312"/>
        <source>Above instructions</source>
        <translation>在反汇编结果上方</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="317"/>
        <source>Off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="325"/>
        <source>Show comments:</source>
        <translation>显示注释：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="475"/>
        <source>Substitute variables (asm.sub.var)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="482"/>
        <source>Substitute entire variable expressions with names (asm.sub.varonly)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="237"/>
        <source>Tabs in assembly (asm.tabs):</source>
        <translation>程序集中的标签(asm.tabs)：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="177"/>
        <source>Tabs before assembly (asm.tabs.off):</source>
        <translation>Tabs before assembly (asm.tabs.off):</translation>
    </message>
    <message>
        <source>Show empty line after every basic block (asm.bbline)</source>
        <translation type="vanished">Show empty line after every basic block (asm.bbline)</translation>
    </message>
    <message>
        <source>Show comments at right of assembly (asm.cmt.right)</source>
        <translation type="vanished">在汇编右侧显示注释 (asm.cmt.right)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="345"/>
        <source>Column to align comments (asm.cmt.col):</source>
        <translation>Column to align comments (asm.cmt.col):</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="355"/>
        <source>Show x-refs (asm.xrefs)</source>
        <translation>显示 x-refs (asm.xrefs)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="362"/>
        <source>Show refpointer information (asm.refptr)</source>
        <translation>显示引用指针信息(asm.refptr)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="395"/>
        <source>Metadata</source>
        <translation>Metadata</translation>
    </message>
    <message>
        <source>Show stack pointer (asm.stackptr)</source>
        <translation type="vanished">显示栈指针 (asm.stackptr)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="419"/>
        <source>Slow Analysis (asm.slow)</source>
        <translation>Slow Analysis (asm.slow)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="426"/>
        <source>Show jump lines (asm.lines)</source>
        <translation>Show jump lines (asm.lines)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="433"/>
        <source>Show function boundary lines (asm.lines.fcn)</source>
        <translation>Show function boundary lines (asm.lines.fcn)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="440"/>
        <source>Show offset before flags (asm.flags.off)</source>
        <translation>在分析结果前显示偏移 (asm.flags.off)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="447"/>
        <source>Run ESIL emulation analysis (asm.emu)</source>
        <translation>运行 ESIL 模拟分析(asm.emu)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="454"/>
        <source>Show only strings if any in the asm.emu output (emu.str)</source>
        <translation>只显示在 asm.emu 输出的字符串 (emu.str)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="461"/>
        <source>Show size of opcodes in disassembly (asm.size)</source>
        <translation>显示反汇编操作码的大小（asm.size）</translation>
    </message>
    <message>
        <source>Show bytes (asm.bytes)</source>
        <translation type="vanished">显示字节 (asm.bytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="468"/>
        <source>Show variables summary instead of full list (asm.var.summary)</source>
        <translation>显示变量摘要而不是完整列表  (asm.var.summary)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="111"/>
        <source>Number of bytes to display (asm.nbytes):</source>
        <translation>要显示的字节数 (asm.nbytes)：</translation>
    </message>
    <message>
        <source>Substitute variables (asm.var.sub)</source>
        <translation type="vanished">替代变量 (asm.var.sub)</translation>
    </message>
    <message>
        <source>Substitute entire variable expressions with names (asm.var.subonly)</source>
        <translation type="vanished">Substitute entire variable expressions with names (asm.var.subonly)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="538"/>
        <source>Save as Default</source>
        <translation>另存为默认</translation>
    </message>
</context>
<context>
    <name>AsyncTaskDialog</name>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.ui" line="14"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.ui" line="20"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="43"/>
        <source>Running for</source>
        <translation>正在运行</translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="45"/>
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation>
            <numerusform>%n 时</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="49"/>
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation>
            <numerusform>%n 分</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="52"/>
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation>
            <numerusform>%n 秒</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>AttachProcDialog</name>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="14"/>
        <source>Select process to attach...</source>
        <translation>选择要附加的进程...</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="46"/>
        <source>Processes with same name as currently open file:</source>
        <translation>与当前打开的文件同名称的进程</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="94"/>
        <source>All processes:</source>
        <translation>所有进程</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="138"/>
        <source>Quick Filter</source>
        <translation>快速筛选</translation>
    </message>
</context>
<context>
    <name>BacktraceWidget</name>
    <message>
        <source>Func Name</source>
        <translation type="vanished">函数名称</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="16"/>
        <source>Function</source>
        <translation>Function</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="19"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="20"/>
        <source>Frame Size</source>
        <translation>帧大小</translation>
    </message>
</context>
<context>
    <name>Base64EnDecodedWriteDialog</name>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="14"/>
        <source>Base64 Encode/Decode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="24"/>
        <source>String:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="38"/>
        <source>Decode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="48"/>
        <source>Encode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BinClassesModel</name>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="121"/>
        <source>method</source>
        <translation>方法</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="145"/>
        <source>field</source>
        <translation>字段</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="167"/>
        <source>base class</source>
        <translation>基类</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="187"/>
        <source>class</source>
        <translation>类</translation>
    </message>
</context>
<context>
    <name>BreakpointModel</name>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="64"/>
        <source>HW %1</source>
        <translation>硬件断点 %1</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="66"/>
        <source>SW</source>
        <translation>软件断点</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="101"/>
        <source>Offset</source>
        <translation>偏移</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="103"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="105"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="109"/>
        <source>Enabled</source>
        <translation>已启用</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="111"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="vanished">权限</translation>
    </message>
    <message>
        <source>Hardware bp</source>
        <translation type="vanished">硬件断点</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="107"/>
        <source>Tracing</source>
        <translation>跟踪</translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="vanished">激活</translation>
    </message>
</context>
<context>
    <name>BreakpointWidget</name>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="58"/>
        <source>Add new breakpoint</source>
        <translation>添加新断点</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="65"/>
        <location filename="../../widgets/BreakpointWidget.cpp" line="196"/>
        <source>Delete breakpoint</source>
        <translation>删除断点</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="72"/>
        <source>Delete all breakpoints</source>
        <translation>删除所有断点</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="202"/>
        <source>Toggle breakpoint</source>
        <translation>切换断点</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="208"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
</context>
<context>
    <name>BreakpointsDialog</name>
    <message>
        <source>Add breakpoints</source>
        <translation type="vanished">添加断点</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="14"/>
        <source>Add/Edit breakpoint</source>
        <translation>添加/编辑断点</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="22"/>
        <source>Position</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="59"/>
        <source>Condition</source>
        <translation>条件</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="91"/>
        <source>?v $.rax-0x6  # break when rax is 6</source>
        <translation>?v $.rax-0x6  # 当 rax 为 6 时中断</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="99"/>
        <source>Module</source>
        <translation>模块</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="126"/>
        <source>Type/Options</source>
        <translation>类型/选项</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="132"/>
        <source>Enabled</source>
        <translation>已启用</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="142"/>
        <source>Software</source>
        <translation>软件</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="152"/>
        <source>Hardware</source>
        <translation>硬件</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="180"/>
        <source>Read</source>
        <translation>读</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="187"/>
        <source>Write</source>
        <translation>写</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="194"/>
        <source>Execute</source>
        <translation>执行</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="206"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="217"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="222"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="227"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="232"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="261"/>
        <source>Action</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="267"/>
        <source>Trace</source>
        <translation>追踪</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="276"/>
        <source>Command</source>
        <translation>命令</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="21"/>
        <source>Edit breakpoint</source>
        <translation>编辑断点</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="23"/>
        <source>New breakpoint</source>
        <translation>新断点</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="32"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="32"/>
        <source>Address or expression calculated when creating breakpoint</source>
        <translation>创建断点时计算的地址或表达式</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="34"/>
        <source>Named</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="34"/>
        <source>Expression - stored as expression</source>
        <translation>表达式 - 作为表达式保存</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="35"/>
        <source>Module offset</source>
        <translation>模块偏移</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="35"/>
        <source>Offset relative to module</source>
        <translation>相对于模块的偏移</translation>
    </message>
</context>
<context>
    <name>CallGraphWidget</name>
    <message>
        <location filename="../../widgets/CallGraph.cpp" line="23"/>
        <source>Global Callgraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CallGraph.cpp" line="23"/>
        <source>Callgraph</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClassesModel</name>
    <message>
        <source>method</source>
        <translation type="vanished">方法</translation>
    </message>
    <message>
        <source>field</source>
        <translation type="vanished">field</translation>
    </message>
    <message>
        <source>class</source>
        <translation type="vanished">class</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="19"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="21"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="23"/>
        <source>Offset</source>
        <translation>偏移量</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="25"/>
        <source>VTable</source>
        <translation>VTable</translation>
    </message>
</context>
<context>
    <name>ClassesWidget</name>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="79"/>
        <source>Source:</source>
        <translation>Source:</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="93"/>
        <source>Binary Info (Fixed)</source>
        <translation>二进制信息(固定的)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="98"/>
        <source>Analysis (Editable)</source>
        <translation>分析(可编辑)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="109"/>
        <source>Seek to VTable</source>
        <translation>寻找VTable</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="114"/>
        <source>Edit Method</source>
        <translation>编辑方法</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="119"/>
        <source>Add Method</source>
        <translation>添加方法</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="124"/>
        <location filename="../../widgets/ClassesWidget.cpp" line="743"/>
        <source>Create new Class</source>
        <translation>创建新类</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="129"/>
        <source>Rename Class</source>
        <translation>重命名类</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="134"/>
        <location filename="../../widgets/ClassesWidget.cpp" line="759"/>
        <source>Delete Class</source>
        <translation>删除类</translation>
    </message>
    <message>
        <source>Flags (Editable)</source>
        <translation type="obsolete">Flags (Editable)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="695"/>
        <source>Missing VTable in class</source>
        <translation>类中缺少VTable</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="696"/>
        <source>The class %1 does not have any VTable!</source>
        <translation>类 %1 没有任何VTable！</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="743"/>
        <source>Class Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="778"/>
        <source>Class name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Class Name</source>
        <translation type="vanished">类名</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="760"/>
        <source>Are you sure you want to delete the class %1?</source>
        <translation>您确定要删除类 %1 吗？</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="777"/>
        <source>Rename Class %1</source>
        <translation>重命名类 %1</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="14"/>
        <source>Form</source>
        <translation>窗体</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="87"/>
        <source>Val:</source>
        <translation>值:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="118"/>
        <source>Sat:</source>
        <translation>饱和度：</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="149"/>
        <source>Hue:</source>
        <translation>色调:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="184"/>
        <source>Red:</source>
        <translation>红色:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="215"/>
        <source>Green:</source>
        <translation>绿色:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="246"/>
        <source>Blue:</source>
        <translation>蓝色:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="281"/>
        <source>Hex:</source>
        <translation>十六进制值：</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="288"/>
        <source>\#HHHHHH</source>
        <translation>\#HHHHHH</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="312"/>
        <source>Pick color from screen</source>
        <translation>从屏幕上拾取颜色</translation>
    </message>
</context>
<context>
    <name>ColorSchemeFileSaver</name>
    <message>
        <source>Standard themes not found!</source>
        <translation type="vanished">未找到标准主题！</translation>
    </message>
    <message>
        <source>The radare2 standard themes could not be found! This probably means radare2 is not properly installed. If you think it is open an issue please.</source>
        <translation type="vanished">找不到 radare2 标准主题，原因可能是没有正确安装 radare2。</translation>
    </message>
</context>
<context>
    <name>ColorSchemePrefWidget</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Form</translation>
    </message>
    <message>
        <source>Set Default</source>
        <translation type="vanished">设置为默认</translation>
    </message>
</context>
<context>
    <name>ColorThemeEditDialog</name>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="26"/>
        <source>Color Theme:</source>
        <translation>颜色主题：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="55"/>
        <source>Search</source>
        <translation>搜索​​​​</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="35"/>
        <source>Disassembly Preview</source>
        <translation>反汇编预览</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="79"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="93"/>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="144"/>
        <source>Unsaved changes</source>
        <translation>未保存更改</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="94"/>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="145"/>
        <source>Are you sure you want to exit without saving? All changes will be lost.</source>
        <translation>您确定要不保存就退出吗？ 所有更改都将丢失。</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="159"/>
        <source>Theme Editor - &lt;%1&gt;</source>
        <translation>主题编辑器 - &lt;%1&gt;</translation>
    </message>
</context>
<context>
    <name>ColorThemeWorker</name>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="45"/>
        <source>Standard themes not found</source>
        <translation>找不到标准主题</translation>
    </message>
    <message>
        <source>The radare2 standard themes could not be found. Most likely, radare2 is not properly installed.</source>
        <translation type="vanished">找不到 radare2 标准主题，原因可能是没有正确安装 radare2。</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="74"/>
        <location filename="../../common/ColorThemeWorker.cpp" line="202"/>
        <source>Theme &lt;b&gt;%1&lt;/b&gt; does not exist.</source>
        <translation>主题 &lt;b&gt;%1&lt;/b&gt; 不存在。</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="84"/>
        <source>The file &lt;b&gt;%1&lt;/b&gt; cannot be opened.</source>
        <translation>该文件 &lt;b&gt;%1&lt;/b&gt; 无法打开。</translation>
    </message>
    <message>
        <source>You can not delete standard radare2 color themes.</source>
        <translation type="vanished">您不能删除 radare2 标准颜色主题。</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="46"/>
        <source>The Rizin standard themes could not be found in &apos;%1&apos;. Most likely, Rizin is not properly installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="199"/>
        <source>You can not delete standard Rizin color themes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="207"/>
        <source>You have no permission to write to &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>您没有权限写入 &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="210"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; can not be opened.</source>
        <translation>文件 &lt;b&gt;%1&lt;/b&gt; 无法打开。</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="213"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; can not be removed.</source>
        <translation>文件 &lt;b&gt;%1&lt;/b&gt; 无法删除。</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="222"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; does not exist.</source>
        <translation>文件 &lt;b&gt;%1&lt;/b&gt; 不存在。</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="228"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; could not be opened. Please make sure you have access to it and try again.</source>
        <translation>文件 &lt;b&gt;%1&lt;/b&gt; 无法打开。 请确保您有权限访问它，然后重试。</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="232"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; is not a Cutter color theme</source>
        <translation>文件 &lt;b&gt;%1&lt;/b&gt; 不是 Cutter 颜色主题</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="237"/>
        <source>A color theme named &lt;b&gt;%1&lt;/b&gt; already exists.</source>
        <translation>名为 &lt;b&gt;%1&lt;/b&gt; 的颜色主题已存在。</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="243"/>
        <source>Error occurred during importing. Please make sure you have an access to the directory &lt;b&gt;%1&lt;/b&gt; and try again.</source>
        <translation>导入过程中发生错误，请确保您有权限访问目录 &lt;b&gt;%1&lt;/b&gt; ，然后重试。</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="253"/>
        <source>A color theme named &lt;b&gt;&quot;%1&quot;&lt;/b&gt; already exists.</source>
        <translation>名为 &lt;b&gt;&quot;%1&quot;&lt;/b&gt; 的颜色主题已存在。</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="257"/>
        <source>You can not rename standard Rizin themes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can not rename standard radare2 themes.</source>
        <translation type="vanished">您不能重命名 radare2 标准主题。</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="263"/>
        <source>Something went wrong during renaming. Please make sure you have access to the directory &lt;b&gt;&quot;%1&quot;&lt;/b&gt;.</source>
        <translation>重命名过程中发生错误， 请确保您有权限访问目录 &lt;b&gt;&quot;%1&quot;&lt;/b&gt;。</translation>
    </message>
</context>
<context>
    <name>ComboQuickFilterView</name>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="14"/>
        <source>Form</source>
        <translation>窗体</translation>
    </message>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="32"/>
        <source>Quick Filter</source>
        <translation>快速筛选器</translation>
    </message>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="39"/>
        <source>TextLabel</source>
        <translation>文本标签</translation>
    </message>
</context>
<context>
    <name>CommandTask</name>
    <message>
        <location filename="../../common/CommandTask.h" line="23"/>
        <source>Running Command</source>
        <translation>运行命令</translation>
    </message>
</context>
<context>
    <name>CommentsDialog</name>
    <message>
        <location filename="../../dialogs/CommentsDialog.ui" line="14"/>
        <source>Comment</source>
        <translation>注释</translation>
    </message>
    <message>
        <location filename="../../dialogs/CommentsDialog.cpp" line="43"/>
        <source>Add Comment at %1</source>
        <translation>在 %1 添加注释</translation>
    </message>
    <message>
        <location filename="../../dialogs/CommentsDialog.cpp" line="45"/>
        <source>Edit Comment at %1</source>
        <translation>在 %1 编辑注释</translation>
    </message>
</context>
<context>
    <name>CommentsModel</name>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="157"/>
        <source>Function/Offset</source>
        <translation>函数/偏移</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="159"/>
        <location filename="../../widgets/CommentsWidget.cpp" line="170"/>
        <source>Comment</source>
        <translation>注释</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="166"/>
        <source>Offset</source>
        <translation>偏移</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="168"/>
        <source>Function</source>
        <translation>函数</translation>
    </message>
</context>
<context>
    <name>CommentsWidget</name>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="236"/>
        <source>Horizontal</source>
        <translation>水平</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="239"/>
        <source>Comments</source>
        <translation>注释</translation>
    </message>
    <message>
        <source>Horizontal view</source>
        <translation type="vanished">水平视图</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="237"/>
        <source>Vertical</source>
        <translation>垂直</translation>
    </message>
    <message>
        <source>Vertical view</source>
        <translation type="vanished">垂直视图</translation>
    </message>
</context>
<context>
    <name>Configuration</name>
    <message>
        <location filename="../../common/Configuration.cpp" line="138"/>
        <source>Critical!</source>
        <translation>严重！</translation>
    </message>
    <message>
        <location filename="../../common/Configuration.cpp" line="139"/>
        <source>!!! Settings are not writable! Make sure you have a write access to &quot;%1&quot;</source>
        <translation>!!! 设置不可写！ 请确保您对 &quot;%1&quot; 具有写入权限。</translation>
    </message>
</context>
<context>
    <name>ConsoleWidget</name>
    <message>
        <source>R2 Console</source>
        <translation type="vanished">R2 控制台</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="93"/>
        <source>Rizin Console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="98"/>
        <source>Debugee Input</source>
        <translation>调试输入</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="115"/>
        <source> Type &quot;?&quot; for help</source>
        <translation>输入 &quot;?&quot; 获取帮助</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="137"/>
        <source> Enter input for the debugee</source>
        <translation> 在此键入调试器参数</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="153"/>
        <source>Execute command</source>
        <translation>执行命令</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="159"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.cpp" line="75"/>
        <source>Clear Output</source>
        <translation>清空输出</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.cpp" line="84"/>
        <source>Wrap Lines</source>
        <translation>换行</translation>
    </message>
</context>
<context>
    <name>CutterCore</name>
    <message>
        <location filename="../../core/Cutter.cpp" line="1717"/>
        <source>Starting native debug...</source>
        <translation>正在开始本地调试...</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1760"/>
        <source>Starting emulation...</source>
        <translation>正在开始模拟...</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1814"/>
        <source>Connecting to: </source>
        <translation>连接到：</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1853"/>
        <source>Attaching to process (</source>
        <translation>附加到进程 (</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2209"/>
        <source>Creating debug tracepoint...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2245"/>
        <source>Stopping debug session...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2279"/>
        <source>Breakpoint error</source>
        <translation>断点错误</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2279"/>
        <source>Failed to create breakpoint</source>
        <translation>创建断点失败</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="3144"/>
        <source>Unknown (%1)</source>
        <translation>未知 (%1)</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="3396"/>
        <source>Primitive</source>
        <translation>Primitive</translation>
    </message>
</context>
<context>
    <name>CutterGraphView</name>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="30"/>
        <location filename="../../widgets/CutterGraphView.cpp" line="442"/>
        <source>Export Graph</source>
        <translation type="unfinished">导出图表</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="42"/>
        <source>Layout</source>
        <translation type="unfinished">布局</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="43"/>
        <source>Horizontal</source>
        <translation type="unfinished">水平</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="47"/>
        <source>Grid narrow</source>
        <translation type="unfinished">Grid narrow</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="48"/>
        <source>Grid medium</source>
        <translation type="unfinished">Grid medium</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="49"/>
        <source>Grid wide</source>
        <translation type="unfinished">Grid wide</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="63"/>
        <source>Graphviz polyline</source>
        <translation type="unfinished">Graphviz polyline</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="64"/>
        <source>Graphviz ortho</source>
        <translation type="unfinished">Graphviz ortho</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="65"/>
        <source>Graphviz sfdp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="66"/>
        <source>Graphviz neato</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="67"/>
        <source>Graphviz twopi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="68"/>
        <source>Graphviz circo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="409"/>
        <source>PNG (*.png)</source>
        <translation type="unfinished">PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="410"/>
        <source>JPEG (*.jpg)</source>
        <translation type="unfinished">JPEG (*.jpg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="411"/>
        <source>SVG (*.svg)</source>
        <translation type="unfinished">SVG (*.svg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="417"/>
        <source>Graphviz dot (*.dot)</source>
        <translation type="unfinished">Graphviz dot (*.dot)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="418"/>
        <source>Graph Modelling Language (*.gml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="420"/>
        <source>RZ JSON (*.json)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="421"/>
        <source>SDB key-value (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="427"/>
        <source>Graphviz json (*.json)</source>
        <translation type="unfinished">Graphviz json (*.json)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="429"/>
        <source>Graphviz gif (*.gif)</source>
        <translation type="unfinished">Graphviz gif (*.gif)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="431"/>
        <source>Graphviz png (*.png)</source>
        <translation type="unfinished">Graphviz png (*.png)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="433"/>
        <source>Graphviz jpg (*.jpg)</source>
        <translation type="unfinished">Graphviz jpg (*.jpg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="435"/>
        <source>Graphviz PostScript (*.ps)</source>
        <translation type="unfinished">Graphviz PostScript (*.ps)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="437"/>
        <source>Graphviz svg (*.svg)</source>
        <translation type="unfinished">Graphviz svg (*.svg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="460"/>
        <source>Graph Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="461"/>
        <source>Do you really want to export %1 x %2 = %3 pixel bitmap image? Consider using different format.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CutterSeekable</name>
    <message>
        <location filename="../../widgets/AddressableDockWidget.cpp" line="45"/>
        <source> (unsynced)</source>
        <translation> (未同步)</translation>
    </message>
    <message>
        <location filename="../../common/CutterSeekable.cpp" line="73"/>
        <source>More than one (%1) references here. Weird behaviour expected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CutterTreeWidget</name>
    <message>
        <location filename="../../widgets/CutterTreeWidget.cpp" line="19"/>
        <source>%1 Items</source>
        <translation>%1 项</translation>
    </message>
</context>
<context>
    <name>Dashboard</name>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="116"/>
        <source>OVERVIEW</source>
        <translation>总览</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="140"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="178"/>
        <source>File:</source>
        <translation>File:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="188"/>
        <location filename="../../widgets/Dashboard.ui" line="217"/>
        <location filename="../../widgets/Dashboard.ui" line="246"/>
        <location filename="../../widgets/Dashboard.ui" line="291"/>
        <location filename="../../widgets/Dashboard.ui" line="320"/>
        <location filename="../../widgets/Dashboard.ui" line="349"/>
        <location filename="../../widgets/Dashboard.ui" line="378"/>
        <location filename="../../widgets/Dashboard.ui" line="391"/>
        <location filename="../../widgets/Dashboard.ui" line="436"/>
        <location filename="../../widgets/Dashboard.ui" line="465"/>
        <location filename="../../widgets/Dashboard.ui" line="494"/>
        <location filename="../../widgets/Dashboard.ui" line="523"/>
        <location filename="../../widgets/Dashboard.ui" line="552"/>
        <location filename="../../widgets/Dashboard.ui" line="581"/>
        <location filename="../../widgets/Dashboard.ui" line="610"/>
        <location filename="../../widgets/Dashboard.ui" line="655"/>
        <location filename="../../widgets/Dashboard.ui" line="668"/>
        <location filename="../../widgets/Dashboard.ui" line="710"/>
        <location filename="../../widgets/Dashboard.ui" line="739"/>
        <location filename="../../widgets/Dashboard.ui" line="768"/>
        <location filename="../../widgets/Dashboard.ui" line="797"/>
        <location filename="../../widgets/Dashboard.ui" line="826"/>
        <location filename="../../widgets/Dashboard.ui" line="855"/>
        <location filename="../../widgets/Dashboard.ui" line="884"/>
        <location filename="../../widgets/Dashboard.ui" line="913"/>
        <location filename="../../widgets/Dashboard.ui" line="942"/>
        <source>--</source>
        <translation>--</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="207"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="236"/>
        <source>Mode:</source>
        <translation>Mode:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="265"/>
        <source>Size:</source>
        <translation>Size:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="281"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="310"/>
        <source>Class:</source>
        <translation>Class:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="339"/>
        <source>Language:</source>
        <translation>Language:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="368"/>
        <source>Bits:</source>
        <translation>Bits:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="426"/>
        <source>FD:</source>
        <translation>FD:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="455"/>
        <source>Base addr:</source>
        <translation>Base addr:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="484"/>
        <source>Virtual addr:</source>
        <translation>Virtual addr:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="513"/>
        <source>Canary:</source>
        <translation>Canary:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="542"/>
        <source>Crypto:</source>
        <translation>Crypto:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="571"/>
        <source>NX bit:</source>
        <translation>NX bit:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="600"/>
        <source>PIC:</source>
        <translation>PIC:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="629"/>
        <source>Static:</source>
        <translation>Static:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="645"/>
        <source>Relro:</source>
        <translation>Relro:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="700"/>
        <source>Architecture:</source>
        <translation>Architecture:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="729"/>
        <source>Machine:</source>
        <translation>Machine:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="758"/>
        <source>OS:</source>
        <translation>OS:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="787"/>
        <source>Subsystem:</source>
        <translation>Subsystem:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="816"/>
        <source>Stripped:</source>
        <translation>文件是否已精简：</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="845"/>
        <source>Relocs:</source>
        <translation>是否有重定位表：</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="874"/>
        <source>Endianness:</source>
        <translation>Endianness:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="903"/>
        <source>Compiled:</source>
        <translation>Compiled:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="932"/>
        <source>Compiler:</source>
        <translation>Compiler:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="993"/>
        <source>Certificates</source>
        <translation>证书</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1006"/>
        <source>Version info</source>
        <translation>版本信息</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1034"/>
        <source>Hashes</source>
        <translation>哈希值</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1051"/>
        <source>Libraries</source>
        <translation>Libraries</translation>
    </message>
    <message>
        <source>MD5:</source>
        <translation type="vanished">MD5:</translation>
    </message>
    <message>
        <source>SHA1:</source>
        <translation type="vanished">SHA1:</translation>
    </message>
    <message>
        <source>Entropy:</source>
        <translation type="vanished">熵：</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1083"/>
        <source>Analysis info</source>
        <translation>分析信息</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1107"/>
        <source>Functions:</source>
        <translation>Functions:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1133"/>
        <source>X-Refs:</source>
        <translation>交叉引用：</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1159"/>
        <source>Calls:</source>
        <translation>Calls:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1185"/>
        <source>Strings:</source>
        <translation>Strings:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1211"/>
        <source>Symbols:</source>
        <translation>Symbols:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1237"/>
        <source>Imports:</source>
        <translation>Imports:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1263"/>
        <source>Analysis coverage:</source>
        <translation>Analysis coverage:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1289"/>
        <source>Code size:</source>
        <translation>Code size:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1315"/>
        <source>Coverage percent:</source>
        <translation>Coverage percent:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="124"/>
        <source>&lt;b&gt;Entropy:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="236"/>
        <location filename="../../widgets/Dashboard.cpp" line="256"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="251"/>
        <source>True</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="253"/>
        <source>False</source>
        <translation>否</translation>
    </message>
</context>
<context>
    <name>DebugActions</name>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="56"/>
        <source>Start debug</source>
        <translation>开始调试</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="37"/>
        <source>Start emulation</source>
        <translation>开始模拟</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="38"/>
        <source>Attach to process</source>
        <translation>附加到进程</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="40"/>
        <source>Stop debug</source>
        <translation>停止调试</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="41"/>
        <source>Stop emulation</source>
        <translation>停止模拟</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="55"/>
        <source>Restart program</source>
        <translation>重启程序</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="42"/>
        <source>Restart emulation</source>
        <translation>重新模拟</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="54"/>
        <source>Continue</source>
        <translation>继续</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="43"/>
        <source>Continue until main</source>
        <translation>执行到 main</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="39"/>
        <source>Connect to a remote debugger</source>
        <translation>连接到远程调试器</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="44"/>
        <source>Continue until call</source>
        <translation>执行到 call</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="45"/>
        <source>Continue until syscall</source>
        <translation>执行到 syscall</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="46"/>
        <source>Continue backwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="47"/>
        <source>Step</source>
        <translation>步进</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="48"/>
        <source>Step over</source>
        <translation>步过</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="49"/>
        <source>Step out</source>
        <translation>步出</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="50"/>
        <source>Step backwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="51"/>
        <source>Start trace session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="52"/>
        <source>Stop trace session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="53"/>
        <source>Suspend the process</source>
        <translation>暂停进程</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="147"/>
        <source>Debugged process exited (</source>
        <translation>调试进程已退出 (</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="267"/>
        <source>Debug is currently in beta.
</source>
        <translation>调试功能目前处于测试阶段。
</translation>
    </message>
    <message>
        <source>If you encounter any problems or have suggestions, please submit an issue to https://github.com/radareorg/cutter/issues</source>
        <translation type="vanished">如果您遇到问题或有建议，请到 https://github.com/radareorg/cutter/issues 提交。</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="295"/>
        <source>Error connecting.</source>
        <translation>连接错误。</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="377"/>
        <source>File &apos;%1&apos; does not have executable permissions.</source>
        <translation>文件 &apos;%1&apos; 没有可执行权限。</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="348"/>
        <source>Error attaching. No process selected!</source>
        <translation>附加错误，未选择进程！</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="268"/>
        <source>If you encounter any problems or have suggestions, please submit an issue to https://github.com/rizinorg/cutter/issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="282"/>
        <location filename="../../widgets/DebugActions.cpp" line="357"/>
        <source>Detach from process</source>
        <translation>脱离进程</translation>
    </message>
</context>
<context>
    <name>DebugOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="14"/>
        <source>Debug</source>
        <translation>调试</translation>
    </message>
    <message>
        <source>Debug Plugin:</source>
        <translation type="vanished">调试插件：</translation>
    </message>
    <message>
        <source>Program Arguments:</source>
        <translation type="vanished">程序参数：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="30"/>
        <source>Debug plugin:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="42"/>
        <source>ESIL options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="69"/>
        <source>ESIL stack address:</source>
        <translation>ESIL 堆栈地址：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="79"/>
        <source>Hide text when zooming out and it is smaller than the given value. Higher values can increase Performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="82"/>
        <source>ESIL stack size:</source>
        <translation>ESIL 堆栈大小：</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="100"/>
        <source>Trace options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="109"/>
        <source>Trace each step during continue in a trace session (dbg.trace_continue)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="112"/>
        <source>Disabling this option means that stepping back after continue will return to the previous PC. Significantly improves performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="54"/>
        <source>Break esil execution when instruction is invalid (esil.breakoninvalid)</source>
        <translation>指令无效时中断 ESIL 执行 (esil.breakoninvalid)</translation>
    </message>
</context>
<context>
    <name>DebugToolbar</name>
    <message>
        <source>Start debug</source>
        <translation type="vanished">开始调试</translation>
    </message>
    <message>
        <source>Start emulation</source>
        <translation type="vanished">开始模拟</translation>
    </message>
    <message>
        <source>Attach to process</source>
        <translation type="vanished">附加到进程</translation>
    </message>
    <message>
        <source>Stop debug</source>
        <translation type="vanished">停止调试</translation>
    </message>
    <message>
        <source>Stop emulation</source>
        <translation type="vanished">停止模拟</translation>
    </message>
    <message>
        <source>Restart program</source>
        <translation type="vanished">重启程序</translation>
    </message>
    <message>
        <source>Restart emulation</source>
        <translation type="vanished">重新模拟</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <source>Continue until main</source>
        <translation type="vanished">执行到 main</translation>
    </message>
    <message>
        <source>Continue until call</source>
        <translation type="vanished">执行到 call</translation>
    </message>
    <message>
        <source>Continue until syscall</source>
        <translation type="vanished">执行到 syscall</translation>
    </message>
    <message>
        <source>Step</source>
        <translation type="vanished">步进</translation>
    </message>
    <message>
        <source>Step over</source>
        <translation type="vanished">步过</translation>
    </message>
    <message>
        <source>Step out</source>
        <translation type="vanished">步出</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; does not have executable permissions.</source>
        <translation type="vanished">文件 &apos;%1&apos; 没有可执行权限。</translation>
    </message>
    <message>
        <source>Error attaching. No process selected!</source>
        <translation type="vanished">附加错误，未选择进程！</translation>
    </message>
    <message>
        <source>Detach from process</source>
        <translation type="vanished">脱离进程</translation>
    </message>
</context>
<context>
    <name>DecompilerContextMenu</name>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="26"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="27"/>
        <source>Copy instruction address (&lt;address&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="28"/>
        <source>Copy address of [flag] (&lt;address&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="29"/>
        <source>Show in</source>
        <translation type="unfinished">显示</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="30"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="161"/>
        <source>Add Comment</source>
        <translation type="unfinished">添加注释</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="31"/>
        <source>Delete comment</source>
        <translation type="unfinished">删除注释</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="32"/>
        <source>Rename function at cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="33"/>
        <source>Delete &lt;name&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="34"/>
        <source>Edit variable &lt;name of variable&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="35"/>
        <source>Show X-Refs</source>
        <translation type="unfinished">显示 X-Refs</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="36"/>
        <source>Add/remove breakpoint</source>
        <translation type="unfinished">添加/移除断点</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="37"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="192"/>
        <source>Advanced breakpoint</source>
        <translation type="unfinished">高级断点</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="39"/>
        <source>Continue until line</source>
        <translation type="unfinished">执行到行</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="40"/>
        <source>Set PC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="164"/>
        <source>Edit Comment</source>
        <translation type="unfinished">编辑注释</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="180"/>
        <source>Add breakpoint</source>
        <translation type="unfinished">添加断点</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="182"/>
        <source>Remove breakpoint</source>
        <translation type="unfinished">移除断点</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="184"/>
        <source>Remove all breakpoints in line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="191"/>
        <source>Edit breakpoint</source>
        <translation type="unfinished">编辑断点</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="195"/>
        <source>Set %1 here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="207"/>
        <source>Rename function %1</source>
        <translation type="unfinished">重命名函数 %1</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="212"/>
        <source>Rename %1</source>
        <translation type="unfinished">重命名 %1</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="213"/>
        <source>Remove %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="216"/>
        <source>Add name to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="221"/>
        <source>Copy instruction address (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="227"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="232"/>
        <source>Copy address of %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="236"/>
        <source>Copy address (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="252"/>
        <source>Edit variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="254"/>
        <source>Rename variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="316"/>
        <source>Can&apos;t rename this variable.&lt;br&gt;Only local variables defined in disassembly can be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="335"/>
        <source>Can&apos;t edit this variable.&lt;br&gt;Only local variables defined in disassembly can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="407"/>
        <source>Define this function at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="408"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="415"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="414"/>
        <source>Rename function %2</source>
        <translation type="unfinished">重命名函数 %2</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="424"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="449"/>
        <source>Rename %2</source>
        <translation type="unfinished">重命名 %2</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="425"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="432"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="450"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="432"/>
        <source>Add name to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="443"/>
        <source>Rename local variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="444"/>
        <source>Can&apos;t rename this variable. Only local variables defined in disassembly can be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="468"/>
        <source>Edit local variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="469"/>
        <source>Can&apos;t edit this variable. Only local variables defined in disassembly can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="535"/>
        <source>Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="545"/>
        <source>Debug</source>
        <translation type="unfinished">调试</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="575"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="577"/>
        <source>Show %1 in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="582"/>
        <source>%1 (%2)</source>
        <translation type="unfinished">%1 (%2)</translation>
    </message>
</context>
<context>
    <name>DecompilerWidget</name>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="14"/>
        <location filename="../../widgets/DecompilerWidget.cpp" line="452"/>
        <source>Decompiler</source>
        <translation>反编译器</translation>
    </message>
    <message>
        <source>Auto Refresh</source>
        <translation type="vanished">自动刷新</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">刷新</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="53"/>
        <source>Decompiling...</source>
        <translation>正在反编译...</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="75"/>
        <source>Decompiler:</source>
        <translation>反编译器：</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="33"/>
        <source>Choose an offset and refresh to get decompiled code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="69"/>
        <source>No Decompiler available.</source>
        <translation>没有可用的反编译器。</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="262"/>
        <source>No function found at this offset. Seek to a function or define one in order to decompile it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Click Refresh to generate Decompiler from current offset.</source>
        <translation type="vanished">单击刷新从当前偏移量生成反编译器。</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="309"/>
        <source>Cannot decompile at this address (Not a function?)</source>
        <translation>无法在此地址反编译 (不是函数？)</translation>
    </message>
</context>
<context>
    <name>DisassemblerGraphView</name>
    <message>
        <source>Export Graph</source>
        <translation type="vanished">导出图表</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">同步/取消同步偏移</translation>
    </message>
    <message>
        <source>Grid narrow</source>
        <translation type="obsolete">Grid narrow</translation>
    </message>
    <message>
        <source>Grid medium</source>
        <translation type="obsolete">Grid medium</translation>
    </message>
    <message>
        <source>Grid wide</source>
        <translation type="obsolete">Grid wide</translation>
    </message>
    <message>
        <source>Graphviz polyline</source>
        <translation type="obsolete">Graphviz polyline</translation>
    </message>
    <message>
        <source>Graphviz polyline LR</source>
        <translation type="obsolete">Graphviz polyline LR</translation>
    </message>
    <message>
        <source>Graphviz ortho</source>
        <translation type="obsolete">Graphviz ortho</translation>
    </message>
    <message>
        <source>Graphviz ortho LR</source>
        <translation type="obsolete">Graphviz ortho LR</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation type="vanished">布局</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="91"/>
        <source>Highlight block</source>
        <translation>高亮显示块</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="109"/>
        <source>Unhighlight block</source>
        <translation>取消高亮显示块</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="119"/>
        <source>Highlight instruction</source>
        <translation>高亮指令</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="123"/>
        <source>Unhighlight instruction</source>
        <translation>取消高亮指令</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="199"/>
        <source>No function detected. Cannot display graph.</source>
        <translation>未检测到函数，无法显示图表。</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="214"/>
        <source>Graph</source>
        <translation>图表</translation>
    </message>
    <message>
        <source>Graphviz dot (*.dot)</source>
        <translation type="vanished">Graphviz dot (*.dot)</translation>
    </message>
    <message>
        <source>Graphviz json (*.json)</source>
        <translation type="vanished">Graphviz json (*.json)</translation>
    </message>
    <message>
        <source>Graphviz gif (*.gif)</source>
        <translation type="vanished">Graphviz gif (*.gif)</translation>
    </message>
    <message>
        <source>Graphviz png (*.png)</source>
        <translation type="vanished">Graphviz png (*.png)</translation>
    </message>
    <message>
        <source>Graphviz jpg (*.jpg)</source>
        <translation type="vanished">Graphviz jpg (*.jpg)</translation>
    </message>
    <message>
        <source>Graphviz PostScript (*.ps)</source>
        <translation type="vanished">Graphviz PostScript (*.ps)</translation>
    </message>
    <message>
        <source>Graphviz svg (*.svg)</source>
        <translation type="vanished">Graphviz svg (*.svg)</translation>
    </message>
    <message>
        <source>Graphiz dot (*.dot)</source>
        <translation type="vanished">Graphiz dot (*.dot)</translation>
    </message>
    <message>
        <source>GIF (*.gif)</source>
        <translation type="vanished">GIF (*.gif)</translation>
    </message>
    <message>
        <source>PNG (*.png)</source>
        <translation type="vanished">PNG (*.png)</translation>
    </message>
    <message>
        <source>JPEG (*.jpg)</source>
        <translation type="vanished">JPEG (*.jpg)</translation>
    </message>
    <message>
        <source>PostScript (*.ps)</source>
        <translation type="vanished">Postscript (*.ps)</translation>
    </message>
    <message>
        <source>SVG (*.svg)</source>
        <translation type="vanished">SVG (*.svg)</translation>
    </message>
    <message>
        <source>JSON (*.json)</source>
        <translation type="vanished">JSON (*.json)</translation>
    </message>
</context>
<context>
    <name>DisassemblyContextMenu</name>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="72"/>
        <source>Copy</source>
        <translation>拷贝</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="75"/>
        <source>Copy address</source>
        <translation>复制地址</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="79"/>
        <source>Show in</source>
        <translation>显示</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="84"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="549"/>
        <source>Add Comment</source>
        <translation>添加注释</translation>
    </message>
    <message>
        <source>Add Flag</source>
        <translation type="vanished">添加 Flag</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="96"/>
        <source>Edit function</source>
        <translation>编辑函数</translation>
    </message>
    <message>
        <source>Rename Flag/Fcn/Var Used Here</source>
        <translation type="vanished">重命名此处的标志/函数/变量</translation>
    </message>
    <message>
        <source>Re-type function local vars</source>
        <translation type="vanished">重新输入函数局部变量</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="100"/>
        <source>Delete comment</source>
        <translation>删除注释</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="104"/>
        <source>Delete flag</source>
        <translation>删除标志</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="107"/>
        <source>Undefine function</source>
        <translation>取消定义函数</translation>
    </message>
    <message>
        <source>Define function here...</source>
        <translation type="vanished">在此处定义函数...</translation>
    </message>
    <message>
        <source>Set to Code</source>
        <translation type="vanished">设置为代码</translation>
    </message>
    <message>
        <source>Set as Code</source>
        <translation type="obsolete">Set as Code</translation>
    </message>
    <message>
        <source>Set as String</source>
        <translation type="obsolete">Set as String</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="133"/>
        <source>Show X-Refs</source>
        <translation>显示 X-Refs</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="137"/>
        <source>X-Refs for local variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="141"/>
        <source>Show Options</source>
        <translation>显示选项</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="172"/>
        <source>Set Immediate Base to...</source>
        <translation type="unfinished">Set Immediate Base to...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="174"/>
        <source>Binary</source>
        <translation>二进制</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="178"/>
        <source>Octal</source>
        <translation>八进制</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="182"/>
        <source>Decimal</source>
        <translation>十进制</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="186"/>
        <source>Hexadecimal</source>
        <translation>十六进制</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="190"/>
        <source>Network Port</source>
        <translation>网络端口</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="194"/>
        <source>IP Address</source>
        <translation>IP 地址</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="198"/>
        <source>Syscall</source>
        <translation>系统调用</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="202"/>
        <source>String</source>
        <translation>字符串</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="209"/>
        <source>Set current bits to...</source>
        <translation>将当前 bits 设置为...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="424"/>
        <source>Rename local &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="429"/>
        <source>Rename flag &quot;%1&quot; (used here)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="826"/>
        <source>New function at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="827"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="841"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="840"/>
        <source>Rename function %2</source>
        <translation type="unfinished">重命名函数 %2</translation>
    </message>
    <message>
        <source>Set to Data...</source>
        <translation type="vanished">设置为数据...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="111"/>
        <source>Define function here</source>
        <translation>在此处定义函数</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="121"/>
        <source>Structure offset</source>
        <translation>结构偏移</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="125"/>
        <source>Link Type to Address</source>
        <translation>指向地址的链接类型</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="226"/>
        <source>Set as...</source>
        <translation>设置为...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="228"/>
        <source>Code</source>
        <translation>代码</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="232"/>
        <source>String...</source>
        <translation>字符串...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="234"/>
        <source>Auto-detect</source>
        <translation>自动检测</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="236"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <source>Adanced</source>
        <translation type="vanished">高级</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="250"/>
        <source>Data...</source>
        <translation>数据...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="252"/>
        <source>Byte</source>
        <translation type="unfinished">Byte</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="256"/>
        <source>Word</source>
        <translation>Word</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="260"/>
        <source>Dword</source>
        <translation>Dword</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="264"/>
        <source>Qword</source>
        <translation>Qword</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="279"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="281"/>
        <source>Instruction</source>
        <translation>指令</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="285"/>
        <source>Nop Instruction</source>
        <translation>Nop 指令</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="289"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="292"/>
        <source>Reverse Jump</source>
        <translation>反转跳转</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="298"/>
        <source>Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="310"/>
        <source>Debug</source>
        <translation>调试</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="300"/>
        <source>Add/remove breakpoint</source>
        <translation>添加/移除断点</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="88"/>
        <source>Rename or add flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="92"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="875"/>
        <source>Re-type Local Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="238"/>
        <source>Advanced</source>
        <translation type="unfinished">高级</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="303"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="587"/>
        <source>Advanced breakpoint</source>
        <translation>高级断点</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="312"/>
        <source>Continue until line</source>
        <translation>执行到行</translation>
    </message>
    <message>
        <source>%1 (used here)</source>
        <translation type="vanished">%1 (在此处使用)</translation>
    </message>
    <message>
        <source>%1 (%2)</source>
        <translation type="vanished">%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="552"/>
        <source>Edit Comment</source>
        <translation>编辑注释</translation>
    </message>
    <message>
        <source>Rename function &quot;%1&quot;</source>
        <translation type="vanished">重命名函数 &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Rename flag &quot;%1&quot;</source>
        <translation type="vanished">重命名标志 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="568"/>
        <source>Edit function &quot;%1&quot;</source>
        <translation>编辑函数 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="414"/>
        <source>Add flag at %1 (used here)</source>
        <translation>在 %1 添加标志 (在此处使用)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="419"/>
        <source>Rename &quot;%1&quot;</source>
        <translation>重命名 &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Rename &quot;%1&quot; (used here)</source>
        <translation type="vanished">重命名 &quot;%1&quot; (在此处使用)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="585"/>
        <source>Remove breakpoint</source>
        <translation>移除断点</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="585"/>
        <source>Add breakpoint</source>
        <translation>添加断点</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="586"/>
        <source>Edit breakpoint</source>
        <translation>编辑断点</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="601"/>
        <source>X-Refs for %1</source>
        <translation type="unfinished">%1 的外部参考</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="701"/>
        <source>Edit Instruction at %1</source>
        <translation>在 %1 编辑指令</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="755"/>
        <source>Edit Bytes at %1</source>
        <translation>在 %1 编辑 Bytes</translation>
    </message>
    <message>
        <source>Write error</source>
        <translation type="vanished">写入错误</translation>
    </message>
    <message>
        <source>Unable to complete write operation. Consider opening in write mode. 

WARNING: In write mode any changes will be commited to disk</source>
        <translation type="vanished">无法完成写入操作，可尝试以写入模式打开。

警告：在写入模式下，所有修改都将保存到磁盘文件。</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Reopen in write mode and try again</source>
        <translation type="vanished">以写入模式打开，然后重试。</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="938"/>
        <source>Wrong address</source>
        <translation>错误的地址</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="939"/>
        <source>Can&apos;t edit string at this address</source>
        <translation>可以在这个地址编辑字符串</translation>
    </message>
    <message>
        <source>Add Comment at %1</source>
        <translation type="obsolete">Add Comment at %1</translation>
    </message>
    <message>
        <source>Edit Comment at %1</source>
        <translation type="obsolete">Edit Comment at %1</translation>
    </message>
    <message>
        <source>Analyze function at %1</source>
        <translation type="vanished">在 %1 分析函数</translation>
    </message>
    <message>
        <source>Function name</source>
        <translation type="vanished">函数名称</translation>
    </message>
    <message>
        <source>Rename function %1</source>
        <translation type="vanished">重命名函数 %1</translation>
    </message>
    <message>
        <source>Rename flag %1</source>
        <translation type="vanished">重命名标志 %1</translation>
    </message>
    <message>
        <source>Add flag at %1</source>
        <translation type="vanished">在 %1 添加标志</translation>
    </message>
    <message>
        <source>Rename %1</source>
        <translation type="vanished">重命名 %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="876"/>
        <source>You must be in a function to define variable types.</source>
        <translation>您必须在函数中才能定义变量类型。</translation>
    </message>
    <message>
        <source>Set Variable Types for Function: %1</source>
        <translation type="obsolete">Set Variable Types for Function: %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="1020"/>
        <source>Edit function %1</source>
        <translation>编辑函数 %1</translation>
    </message>
</context>
<context>
    <name>DisassemblyWidget</name>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="670"/>
        <source>More than one (%1) references here. Weird behaviour expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="676"/>
        <source>offsetFrom (%1) differs from refs.at(0).from (%(2))</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="721"/>
        <source>Disassembly</source>
        <translation>反汇编</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">同步/取消同步偏移</translation>
    </message>
</context>
<context>
    <name>DuplicateFromOffsetDialog</name>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="20"/>
        <source>Duplicate from offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="28"/>
        <source>Offset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="49"/>
        <source>N bytes:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditFunctionDialog</name>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="14"/>
        <source>Edit Function</source>
        <translation>编辑函数</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="43"/>
        <source>Name of function</source>
        <translation>函数名称</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="53"/>
        <source>Start address</source>
        <translation>起始地址</translation>
    </message>
    <message>
        <source>End address</source>
        <translation type="vanished">结束地址</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="63"/>
        <source>Stack size</source>
        <translation>堆栈大小</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="73"/>
        <source>Calling convention</source>
        <translation>调用约定</translation>
    </message>
</context>
<context>
    <name>EditInstructionDialog</name>
    <message>
        <location filename="../../dialogs/EditInstructionDialog.ui" line="14"/>
        <source>Edit Instruction</source>
        <translation>编辑指令</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditInstructionDialog.ui" line="76"/>
        <source>Unknown Instruction</source>
        <translation>未知指令</translation>
    </message>
</context>
<context>
    <name>EditMethodDialog</name>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="25"/>
        <source>Class:</source>
        <translation>类:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="32"/>
        <source>Name:</source>
        <translation>名称：</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="42"/>
        <source>Address:</source>
        <translation>地址：</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="52"/>
        <source>Virtual:</source>
        <translation>虚拟：</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="66"/>
        <source>Offset in VTable:</source>
        <translation>VTable中的偏移量:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.cpp" line="152"/>
        <source>Create Method</source>
        <translation>创建 Method</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.cpp" line="167"/>
        <source>Edit Method</source>
        <translation>编辑 Method</translation>
    </message>
</context>
<context>
    <name>EditStringDialog</name>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="23"/>
        <source>Edit string</source>
        <translation>编辑字符串</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="66"/>
        <source>Address:</source>
        <translation>地址:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="52"/>
        <source>Size:</source>
        <translation>大小:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="59"/>
        <source>Type:</source>
        <translation>类型:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="99"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>EditVariablesDialog</name>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="22"/>
        <source>Modify:</source>
        <translation>修改：</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="35"/>
        <source>Name:</source>
        <translation>名称：</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="42"/>
        <source>Type:</source>
        <translation>类型:</translation>
    </message>
    <message>
        <source>Set Variable Types for Function: %1</source>
        <translation type="vanished">设置函数的变量类型：%1</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.cpp" line="19"/>
        <source>Edit Variables in Function: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportsModel</name>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="60"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="62"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="64"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="66"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="68"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>ExportsWidget</name>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="138"/>
        <source>Exports</source>
        <translation>导出表</translation>
    </message>
</context>
<context>
    <name>FlagDialog</name>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="14"/>
        <source>Add Flag</source>
        <translation>添加 Flag</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="20"/>
        <source>Add flag at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="38"/>
        <source>Flag:</source>
        <translation>标志：</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="61"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="80"/>
        <source>Size:</source>
        <translation>大小:</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.cpp" line="24"/>
        <source>Edit flag at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.cpp" line="26"/>
        <source>Add flag at %1</source>
        <translation type="unfinished">在 %1 添加标志</translation>
    </message>
</context>
<context>
    <name>FlagsModel</name>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="64"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="66"/>
        <source>Offset</source>
        <translation>偏移量</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="68"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="70"/>
        <source>Real Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="72"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>FlagsWidget</name>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="79"/>
        <source>Quick Filter</source>
        <translation>快速筛选器</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="86"/>
        <source>Flagspace:</source>
        <translation>标志空间：</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="99"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="102"/>
        <source>N</source>
        <translation type="unfinished">N</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="110"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="113"/>
        <source>Del</source>
        <translation type="unfinished">Del</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="214"/>
        <source>Rename flag %1</source>
        <translation type="unfinished">重命名标志 %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="215"/>
        <source>Flag name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="244"/>
        <source>(all)</source>
        <translation>(所有)</translation>
    </message>
</context>
<context>
    <name>FunctionModel</name>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="123"/>
        <source>Offset: %1</source>
        <translation>偏移：%1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="125"/>
        <source>Size: %1</source>
        <translation>大小：%1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="127"/>
        <source>Import: %1</source>
        <translation>导入：%1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="128"/>
        <source>true</source>
        <translation type="unfinished">true</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="128"/>
        <source>false</source>
        <translation type="unfinished">false</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="130"/>
        <source>Nargs: %1</source>
        <translation type="unfinished">Nargs: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="132"/>
        <source>Nbbs: %1</source>
        <translation type="unfinished">Nbbs: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="134"/>
        <source>Nlocals: %1</source>
        <translation type="unfinished">Nlocals: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="281"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
    <message>
        <source>Cyclomatic complexity: %1</source>
        <translation type="obsolete">Cyclomatic complexity: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="136"/>
        <source>Call type: %1</source>
        <translation>Call type: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="138"/>
        <source>Edges: %1</source>
        <translation type="unfinished">Edges: %1</translation>
    </message>
    <message>
        <source>Cost: %1</source>
        <translation type="obsolete">Cost: %1</translation>
    </message>
    <message>
        <source>Calls/OutDegree: %1</source>
        <translation type="obsolete">Calls/OutDegree: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="140"/>
        <source>StackFrame: %1</source>
        <translation>StackFrame: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="142"/>
        <source>Comment: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="221"/>
        <source>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;反汇编预览&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="226"/>
        <source>&lt;div&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div&gt;&lt;strong&gt;高亮&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;div&gt;&lt;strong&gt;Summary&lt;/strong&gt;:&lt;br&gt;</source>
        <translation type="obsolete">&lt;div&gt;&lt;strong&gt;Summary&lt;/strong&gt;:&lt;br&gt;</translation>
    </message>
    <message>
        <source>Size:&amp;nbsp;%1,&amp;nbsp;Cyclomatic complexity:&amp;nbsp;%2,&amp;nbsp;Basic blocks:&amp;nbsp;%3</source>
        <translation type="obsolete">Size:&amp;nbsp;%1,&amp;nbsp;Cyclomatic complexity:&amp;nbsp;%2,&amp;nbsp;Basic blocks:&amp;nbsp;%3</translation>
    </message>
    <message>
        <source>&lt;/div&gt;&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="obsolete">&lt;/div&gt;&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="obsolete">&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="257"/>
        <location filename="../../widgets/FunctionsWidget.cpp" line="261"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="263"/>
        <source>Size</source>
        <translation>Size</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="265"/>
        <source>Imp.</source>
        <translation type="unfinished">Imp.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="267"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="269"/>
        <source>Nargs</source>
        <translation type="unfinished">Nargs</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="273"/>
        <source>Nbbs</source>
        <translation>Nbbs框架</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="271"/>
        <source>Nlocals</source>
        <translation>非本地</translation>
    </message>
    <message>
        <source>Cyclo. Comp.</source>
        <translation type="obsolete">Cyclo. Comp.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="275"/>
        <source>Call type</source>
        <translation>Call type</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="277"/>
        <source>Edges</source>
        <translation>边</translation>
    </message>
    <message>
        <source>Cost</source>
        <translation type="obsolete">Cost</translation>
    </message>
    <message>
        <source>Calls/OutDeg.</source>
        <translation type="obsolete">Calls/OutDeg.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="279"/>
        <source>StackFrame</source>
        <translation>StackFrame</translation>
    </message>
</context>
<context>
    <name>FunctionsTask</name>
    <message>
        <location filename="../../common/FunctionsTask.h" line="13"/>
        <source>Fetching Functions</source>
        <translation>正在获取函数</translation>
    </message>
</context>
<context>
    <name>FunctionsWidget</name>
    <message>
        <source>Add comment</source>
        <translation type="vanished">添加注释</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="442"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="443"/>
        <source>Undefine</source>
        <translation>未定义</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="447"/>
        <source>Functions</source>
        <translation>函数</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="549"/>
        <source>Rename function %1</source>
        <translation type="unfinished">重命名函数 %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="550"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X-Refs</source>
        <translation type="obsolete">X-Refs</translation>
    </message>
    <message>
        <source>Cross references</source>
        <translation type="obsolete">Cross references</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="444"/>
        <source>Horizontal</source>
        <translation>水平</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="445"/>
        <source>Vertical</source>
        <translation>垂直</translation>
    </message>
</context>
<context>
    <name>GraphOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="14"/>
        <source>Graph</source>
        <translation type="unfinished">Graph</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="22"/>
        <source>Graph Block Options </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="31"/>
        <source>The offset of the first instruction of a graph block is shown in the header of the respective graph block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="34"/>
        <source>Show offset of the first instruction in each graph block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="41"/>
        <source>Show offset for each instruction (graph.offset)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="50"/>
        <source>Maximum Line Length:</source>
        <translation type="unfinished">Maximum Line Length:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="70"/>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="80"/>
        <source>Hide text when zooming out and it is smaller than the given value. Higher values can increase Performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="73"/>
        <source>Minimum Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="98"/>
        <source>Graph Layout Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="117"/>
        <source>Vertical</source>
        <translation type="unfinished">垂直</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="140"/>
        <source>Horizontal</source>
        <translation type="unfinished">水平</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="176"/>
        <source>Block spacing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="183"/>
        <source>Edge spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="193"/>
        <source>Bitmap Export Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="202"/>
        <source>Export Transparent Bitmap Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="211"/>
        <source>Graph Bitmap Export Scale: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="218"/>
        <source>%</source>
        <translation type="unfinished">%</translation>
    </message>
    <message>
        <source>Show offsets (graph.offset)</source>
        <translation type="vanished">Show offsets (graph.offset)</translation>
    </message>
    <message>
        <source>Show offsets (graph.offset) </source>
        <translation type="obsolete">Show offsets (graph.offset) </translation>
    </message>
</context>
<context>
    <name>HeadersModel</name>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="55"/>
        <source>Offset</source>
        <translation>偏移量</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="57"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="59"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="61"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>HeadersWidget</name>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="120"/>
        <source>Headers</source>
        <translation type="unfinished">Headers</translation>
    </message>
</context>
<context>
    <name>HexWidget</name>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="67"/>
        <source>Hexadecimal</source>
        <translation>十六进制</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="68"/>
        <source>Octal</source>
        <translation>八进制</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="69"/>
        <source>Decimal</source>
        <translation>十进制</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="70"/>
        <source>Signed decimal</source>
        <translation>有符号小数</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="71"/>
        <source>Float</source>
        <translation>浮点数</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="85"/>
        <source>Bytes per row</source>
        <translation>每行字节数</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="95"/>
        <source>Power of 2</source>
        <translation type="unfinished">Power of 2</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="102"/>
        <source>Big Endian</source>
        <translation>大端</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="107"/>
        <source>Bytes as pairs</source>
        <translation type="unfinished">Bytes as pairs</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="111"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="117"/>
        <source>Copy address</source>
        <translation>复制地址</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="123"/>
        <source>Select range</source>
        <translation>选择范围</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="130"/>
        <location filename="../../widgets/HexWidget.cpp" line="705"/>
        <source>Write string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="134"/>
        <source>Write length and string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="138"/>
        <location filename="../../widgets/HexWidget.cpp" line="842"/>
        <source>Write wide string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="142"/>
        <source>Write zero terminated string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="146"/>
        <source>Write De\Encoded Base64 string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="151"/>
        <location filename="../../widgets/HexWidget.cpp" line="745"/>
        <source>Write zeros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="155"/>
        <source>Write random bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="159"/>
        <source>Duplicate from offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="163"/>
        <source>Increment/Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="620"/>
        <source>Item size:</source>
        <translation type="unfinished">Item size:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="622"/>
        <source>Item format:</source>
        <translation type="unfinished">Item format:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="627"/>
        <source>Edit</source>
        <translation type="unfinished">编辑</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="705"/>
        <location filename="../../widgets/HexWidget.cpp" line="826"/>
        <location filename="../../widgets/HexWidget.cpp" line="842"/>
        <location filename="../../widgets/HexWidget.cpp" line="857"/>
        <source>String:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="745"/>
        <source>Number of zeros:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="769"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="770"/>
        <source>Error occured during decoding your input.
Please, make sure, that it is a valid base64 string and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="793"/>
        <source>Write random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="793"/>
        <source>Number of bytes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="826"/>
        <source>Write Pascal string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="857"/>
        <source>Write zero-terminated string</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HexdumpRangeDialog</name>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="14"/>
        <source>Select Block</source>
        <translation>选择块</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="47"/>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="79"/>
        <source>Exclusive end address</source>
        <translation type="unfinished">Exclusive end address</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="50"/>
        <source>End Address:</source>
        <translation>结束地址：</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="65"/>
        <source>Start Address:</source>
        <translation>起始地址：</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="99"/>
        <source>Length:</source>
        <translation>长度：</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="115"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#ff8585;&quot;&gt;Big selection might cause a delay&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#ff8585;&quot;&gt;选择过多可能会导致延迟&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>HexdumpWidget</name>
    <message>
        <source>0  1  2  3 ...</source>
        <translation type="vanished">0  1  2  3 ...</translation>
    </message>
    <message>
        <source>0123...</source>
        <translation type="vanished">0123...</translation>
    </message>
    <message>
        <source>Offset</source>
        <translation type="vanished">偏移量</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="70"/>
        <source>Parsing</source>
        <translation>解析</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="50"/>
        <source>Select bytes to display information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="143"/>
        <source>Disassembly</source>
        <translation>反汇编</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="144"/>
        <source>String</source>
        <translation>字符串</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="145"/>
        <source>Assembler</source>
        <translation>汇编</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="146"/>
        <source>C bytes</source>
        <translation>C bytes</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="147"/>
        <source>C bytes with instructions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="148"/>
        <source>C half-words (2 byte)</source>
        <translation>C half-words (2 byte)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="149"/>
        <source>C words (4 byte)</source>
        <translation>C words (4 byte)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="150"/>
        <source>C dwords (8 byte)</source>
        <translation>C dwords (8 byte)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="151"/>
        <source>Python</source>
        <translation>Python</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="152"/>
        <source>JSON</source>
        <translation>JSON</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="153"/>
        <source>JavaScript</source>
        <translation>JavaScript</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="154"/>
        <source>Yara</source>
        <translation type="unfinished">Yara</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="120"/>
        <source>Endian</source>
        <translation>大小端</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="131"/>
        <source>Little</source>
        <translation>小端</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="136"/>
        <source>Big</source>
        <translation>大端</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="191"/>
        <source>Arch</source>
        <translation>架构</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="220"/>
        <source>Bits</source>
        <translation>位数</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="231"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="236"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="241"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="318"/>
        <source>SHA256:</source>
        <translation type="unfinished">SHA1: {256:?}</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="357"/>
        <source>Copy SHA256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="396"/>
        <source>Copy CRC32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="466"/>
        <source>MD5:</source>
        <translation>MD5:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="479"/>
        <source>CRC32:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="350"/>
        <source>SHA1:</source>
        <translation>SHA1:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="434"/>
        <source>Entropy:</source>
        <translation>熵：</translation>
    </message>
    <message>
        <source>Hexdump side panel</source>
        <translation type="obsolete">Hexdump side panel</translation>
    </message>
    <message>
        <source>Undefine</source>
        <translation type="obsolete">Undefine</translation>
    </message>
    <message>
        <source>Copy all</source>
        <translation type="vanished">复制所有</translation>
    </message>
    <message>
        <source>Copy bytes</source>
        <translation type="vanished">复制 bytes</translation>
    </message>
    <message>
        <source>Copy disasm</source>
        <translation type="obsolete">Copy disasm</translation>
    </message>
    <message>
        <source>Copy Hexpair</source>
        <translation type="obsolete">Copy Hexpair</translation>
    </message>
    <message>
        <source>Copy ASCII</source>
        <translation type="obsolete">Copy ASCII</translation>
    </message>
    <message>
        <source>Copy Text</source>
        <translation type="obsolete">Copy Text</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="vanished">8</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">编辑</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="obsolete">Paste</translation>
    </message>
    <message>
        <source>Insert Hex</source>
        <translation type="obsolete">Insert Hex</translation>
    </message>
    <message>
        <source>Insert String</source>
        <translation type="obsolete">Insert String</translation>
    </message>
    <message>
        <source>Hex</source>
        <translation type="obsolete">Hex</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation type="vanished">八进制</translation>
    </message>
    <message>
        <source>Half-word</source>
        <translation type="obsolete">Half-word</translation>
    </message>
    <message>
        <source>Word</source>
        <translation type="obsolete">Word</translation>
    </message>
    <message>
        <source>Quad-word</source>
        <translation type="obsolete">Quad-word</translation>
    </message>
    <message>
        <source>Emoji</source>
        <translation type="obsolete">Emoji</translation>
    </message>
    <message>
        <source>1 byte</source>
        <translation type="obsolete">1 byte</translation>
    </message>
    <message>
        <source>2 bytes</source>
        <translation type="obsolete">2 bytes</translation>
    </message>
    <message>
        <source>4 bytes</source>
        <translation type="obsolete">4 bytes</translation>
    </message>
    <message>
        <source>Select Block...</source>
        <translation type="obsolete">Select Block...</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="219"/>
        <source>Hexdump</source>
        <translation type="unfinished">Hexdump</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation type="obsolete">Columns</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">同步/取消同步偏移</translation>
    </message>
    <message>
        <source>Error: Could not select range, end address is less then start address</source>
        <translation type="obsolete">Error: Could not select range, end address is less then start address</translation>
    </message>
</context>
<context>
    <name>ImportsModel</name>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="45"/>
        <source>Unsafe</source>
        <translation>不安全</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="71"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="73"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="75"/>
        <source>Safety</source>
        <translation>安全</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="77"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="79"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="81"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>ImportsWidget</name>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="172"/>
        <source>Imports</source>
        <translation>导入表</translation>
    </message>
</context>
<context>
    <name>IncrementDecrementDialog</name>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="14"/>
        <source>Increment/Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="24"/>
        <source>Interpret as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="38"/>
        <source>Value:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="54"/>
        <source>Increment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="64"/>
        <source>Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Byte</source>
        <translation type="unfinished">Byte</translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Word</source>
        <translation type="unfinished">Word</translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Dword</source>
        <translation type="unfinished">Dword</translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Qword</source>
        <translation type="unfinished">Qword</translation>
    </message>
</context>
<context>
    <name>InitialOptionsDialog</name>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="26"/>
        <source>Load Options</source>
        <translation>加载选项</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="84"/>
        <source>Program:</source>
        <translation>程序：</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="161"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="363"/>
        <source>Analysis: Enabled</source>
        <translation>分析：已启用</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="177"/>
        <source>Level: </source>
        <translation>级别：</translation>
    </message>
    <message>
        <source>Analyze all symbols (aa)</source>
        <translation type="obsolete">Analyze all symbols (aa)</translation>
    </message>
    <message>
        <source>Analyze for references (aar)</source>
        <translation type="obsolete">Analyze for references (aar)</translation>
    </message>
    <message>
        <source>Analyze function calls (aac)</source>
        <translation type="obsolete">Analyze function calls (aac)</translation>
    </message>
    <message>
        <source>Analyze all basic blocks (aab)</source>
        <translation type="obsolete">Analyze all basic blocks (aab)</translation>
    </message>
    <message>
        <source>Autorename functions based on context (aan)</source>
        <translation type="obsolete">Autorename functions based on context (aan)</translation>
    </message>
    <message>
        <source>Experimental:</source>
        <translation type="obsolete">Experimental:</translation>
    </message>
    <message>
        <source>Emulate code to find computed references (aae)</source>
        <translation type="obsolete">Emulate code to find computed references (aae)</translation>
    </message>
    <message>
        <source>Analyze for consecutive function (aat)</source>
        <translation type="obsolete">Analyze for consecutive function (aat)</translation>
    </message>
    <message>
        <source>Type and Argument matching analysis (afta)</source>
        <translation type="obsolete">Type and Argument matching analysis (afta)</translation>
    </message>
    <message>
        <source>Analyze code after trap-sleds (aaT)</source>
        <translation type="obsolete">Analyze code after trap-sleds (aaT)</translation>
    </message>
    <message>
        <source>Analyze function preludes (aap)</source>
        <translation type="obsolete">Analyze function preludes (aap)</translation>
    </message>
    <message>
        <source>Analyze jump tables in switch statements (e! anal.jmptbl)</source>
        <translation type="obsolete">Analyze jump tables in switch statements (e! anal.jmptbl)</translation>
    </message>
    <message>
        <source>Analyze push+ret as jmp (e! anal.pushret)</source>
        <translation type="obsolete">Analyze push+ret as jmp (e! anal.pushret)</translation>
    </message>
    <message>
        <source>Continue analysis after each function (e! anal.hasnext)</source>
        <translation type="obsolete">Continue analysis after each function (e! anal.hasnext)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="322"/>
        <source>Load in write mode (-w)</source>
        <translation>以写入模式加载 (-w)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="332"/>
        <source>Do not load bin information (-n)</source>
        <translation>不加载二进制文件信息 (-n)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="345"/>
        <source>Use virtual addressing</source>
        <translation>使用虚拟地址</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="355"/>
        <source>Import demangled symbols</source>
        <translation>导入 demangled 符号</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="372"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="403"/>
        <source>Advanced options</source>
        <translation>高级选项</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="433"/>
        <source>CPU options</source>
        <translation>CPU 选项</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="448"/>
        <source>Architecture:</source>
        <translation>Architecture:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="459"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="519"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="567"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="612"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="637"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="37"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="473"/>
        <source>CPU:</source>
        <translation>CPU:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="524"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="529"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="534"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="539"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="553"/>
        <source>Endianness: </source>
        <translation>Endianness: </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="572"/>
        <source>Little</source>
        <translation>小端序</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="577"/>
        <source>Big</source>
        <translation>大端序</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="598"/>
        <source>Kernel: </source>
        <translation type="unfinished">Kernel: </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="626"/>
        <source>Format:</source>
        <translation type="unfinished">Format:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="680"/>
        <source>Load bin offset (-B)</source>
        <translation type="unfinished">Load bin offset (-B)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="699"/>
        <source>1024</source>
        <translation>1024</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="718"/>
        <source>Map offset (-m)</source>
        <translation type="unfinished">Map offset (-m)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="734"/>
        <source>0x40000</source>
        <translation>0x40000</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="741"/>
        <source>Load PDB</source>
        <translation>加载 PDB 文件</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="766"/>
        <source>PDB File path</source>
        <translation>PDB 文件路径</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="773"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="815"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="783"/>
        <source>Load script file</source>
        <translation>加载脚本文件</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="808"/>
        <source>Path to Rizin script file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path to radare2 script file</source>
        <translation type="vanished">Radare2 脚本文件路径</translation>
    </message>
    <message>
        <source>BasicBlock maxsize:</source>
        <translation type="obsolete">BasicBlock maxsize:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="871"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="884"/>
        <source>  Ok  </source>
        <translation>  确定  </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="55"/>
        <source>Analyze all symbols</source>
        <translation>[分析所有符号] Analyze all symbols</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="56"/>
        <source>Analyze instructions for references</source>
        <translation>[分析指令引用] Analyze instructions for references</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="57"/>
        <source>Analyze function calls</source>
        <translation>[分析函数调用] Analyze function calls</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="58"/>
        <source>Analyze all basic blocks</source>
        <translation>[分析所有基本块] Analyze all basic blocks</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="59"/>
        <source>Analyze all objc references</source>
        <translation>[分析所有对象引用] Analyze all objc references</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="60"/>
        <source>Recover class information from RTTI</source>
        <translation>[从 RTTI 恢复 class 信息] Recover class information from RTTI</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="61"/>
        <source>Autoname functions based on context</source>
        <translation>[基于上下文自动命名函数] Autoname functions based on context</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="62"/>
        <source>Emulate code to find computed references</source>
        <translation>[模拟代码以查找计算引用] Emulate code to find computed references</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="63"/>
        <source>Analyze all consecutive functions</source>
        <translation>[分析所有连续函数] Analyze all consecutive functions</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="64"/>
        <source>Type and Argument matching analysis</source>
        <translation>[类型和参数匹配分析] Type and Argument matching analysis</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="65"/>
        <source>Analyze code after trap-sleds</source>
        <translation type="unfinished">Analyze code after trap-sleds</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="66"/>
        <source>Analyze function preludes</source>
        <translation type="unfinished">Analyze function preludes</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="67"/>
        <source>Analyze jump tables in switch statements</source>
        <translation>[分析 switch 语句中的跳转表] Analyze jump tables in switch statements</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="70"/>
        <source>Analyze PUSH+RET as JMP</source>
        <translation>[将 PUSH + RET 分析为 JMP] Analyze PUSH+RET as JMP</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="71"/>
        <source>Continue analysis after each function</source>
        <translation>[每个函数执行完后继续分析] Continue analysis after each function</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="343"/>
        <source>No analysis</source>
        <translation>不进行分析</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="345"/>
        <source>Auto-Analysis (aaa)</source>
        <translation>自动分析 (aaa)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="347"/>
        <source>Auto-Analysis Experimental (aaaa)</source>
        <translation>自动分析-实验性 (aaaa)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="349"/>
        <source>Advanced</source>
        <translation>高级</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="351"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="357"/>
        <source>Level</source>
        <translation>级别</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="360"/>
        <source>Analysis: Disabled</source>
        <translation>分析：已禁用</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="406"/>
        <source>Select PDB file</source>
        <translation>选择 PDB 文件</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="407"/>
        <source>PDB file (*.pdb)</source>
        <translation>PDB 文件 (*.pdb)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="407"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="429"/>
        <source>All files (*)</source>
        <translation>所有文件 (*)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="428"/>
        <source>Select Rizin script file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="429"/>
        <source>Script file (*.rz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select radare2 script file</source>
        <translation type="vanished">选择 radare2 脚本文件</translation>
    </message>
    <message>
        <source>Script file (*.r2)</source>
        <translation type="vanished">脚本文件 (*.r2)</translation>
    </message>
</context>
<context>
    <name>InitializationFileEditor</name>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.ui" line="14"/>
        <source>CutterRC Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.ui" line="20"/>
        <source>TextLabel</source>
        <translation type="unfinished">文本标签</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.cpp" line="35"/>
        <source>Script is loaded from &lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JSDecDecompiler</name>
    <message>
        <location filename="../../common/Decompiler.cpp" line="40"/>
        <source>Failed to parse JSON from jsdec</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JupyterWebView</name>
    <message>
        <source>Jupyter</source>
        <translation type="obsolete">Jupyter</translation>
    </message>
</context>
<context>
    <name>JupyterWidget</name>
    <message>
        <source>Jupyter</source>
        <translation type="obsolete">Jupyter</translation>
    </message>
    <message>
        <source>Cutter has been built without QtWebEngine.&lt;br /&gt;Open the following URL in your Browser to use Jupyter:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation type="obsolete">Cutter has been built without QtWebEngine.&lt;br /&gt;Open the following URL in your Browser to use Jupyter:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <source>An error occurred while opening jupyter. Make sure Jupyter is installed system-wide.</source>
        <translation type="obsolete">An error occurred while opening jupyter. Make sure Jupyter is installed system-wide.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
</context>
<context>
    <name>LayoutManager</name>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="14"/>
        <source>Layout</source>
        <translation type="unfinished">布局</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="25"/>
        <source>Rename</source>
        <translation type="unfinished">重命名</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="32"/>
        <location filename="../../dialogs/LayoutManager.cpp" line="60"/>
        <source>Delete</source>
        <translation type="unfinished">删除</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="42"/>
        <source>Rename layout error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="43"/>
        <source>&apos;%1&apos; is already used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="45"/>
        <source>Save layout</source>
        <translation type="unfinished">保存布局</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="45"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="61"/>
        <source>Do you want to delete &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinkTypeDialog</name>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="26"/>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="43"/>
        <source>Enter Address</source>
        <translation>输入地址</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="50"/>
        <source>Structure Type</source>
        <translation>结构类型</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="73"/>
        <source>Address/Flag</source>
        <translation>地址/标志</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="10"/>
        <source>Link type to address</source>
        <translation>Link type to address</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="13"/>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="57"/>
        <source>(No Type)</source>
        <translation>(无类型)</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="75"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="75"/>
        <source>The given address is invalid</source>
        <translation>指定的地址无效</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="106"/>
        <source>Invalid Address</source>
        <translation>无效地址</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../core/MainWindow.ui" line="33"/>
        <source>Add extra...</source>
        <translation>添加额外的...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="61"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="68"/>
        <source>Set mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="94"/>
        <location filename="../../core/MainWindow.cpp" line="288"/>
        <source>View</source>
        <translation>视图</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="101"/>
        <source>Zoom</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="110"/>
        <source>Layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="130"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="138"/>
        <location filename="../../core/MainWindow.ui" line="498"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="147"/>
        <source>Windows</source>
        <translation>窗口</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="151"/>
        <location filename="../../core/MainWindow.cpp" line="142"/>
        <location filename="../../core/MainWindow.cpp" line="143"/>
        <source>Plugins</source>
        <translation>插件</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="156"/>
        <source>Info...</source>
        <translation>PE 文件信息...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="161"/>
        <source>Debug...</source>
        <translation>调试...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="176"/>
        <source>Debug</source>
        <translation>调试</translation>
    </message>
    <message>
        <source>Reset Layout</source>
        <translation type="vanished">重置布局</translation>
    </message>
    <message>
        <source>Reset layout</source>
        <translation type="vanished">重置布局</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="227"/>
        <source>Zen Mode</source>
        <translation type="unfinished">Zen Mode</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="230"/>
        <source>Zen mode</source>
        <translation type="unfinished">Zen mode</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="235"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="240"/>
        <source>Report an issue</source>
        <translation>报告问题</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">新建</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="248"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="253"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="258"/>
        <location filename="../../core/MainWindow.ui" line="261"/>
        <location filename="../../core/MainWindow.ui" line="847"/>
        <location filename="../../core/MainWindow.cpp" line="1207"/>
        <source>Save layout</source>
        <translation>保存布局</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="266"/>
        <source>Documentation</source>
        <translation>文档</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">打开</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation type="vanished">Ctrl+O</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="282"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="340"/>
        <source>Undo Seek</source>
        <translation>撤消</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="352"/>
        <source>Redo Seek</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="287"/>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="222"/>
        <source>Reset to default layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="245"/>
        <source>New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="271"/>
        <source>Map File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="274"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="279"/>
        <location filename="../../core/MainWindow.cpp" line="712"/>
        <location filename="../../core/MainWindow.cpp" line="735"/>
        <source>Save Project</source>
        <translation type="unfinished">保存项目</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="292"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="297"/>
        <location filename="../../core/MainWindow.ui" line="503"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="302"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="307"/>
        <location filename="../../core/MainWindow.ui" line="310"/>
        <source>Select all</source>
        <translation>选择所有</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="315"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="320"/>
        <location filename="../../core/MainWindow.ui" line="323"/>
        <source>Find next</source>
        <translation>查找下一个</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="328"/>
        <location filename="../../core/MainWindow.ui" line="331"/>
        <source>Find previous</source>
        <translation>查找上一个</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="obsolete">Back</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="343"/>
        <source>Go back</source>
        <translation>返回</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="obsolete">Forward</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="363"/>
        <source>Unlock Panels</source>
        <translation>解锁面板</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="366"/>
        <source>Toggle panel locks</source>
        <translation>切换面板锁定</translation>
    </message>
    <message>
        <source>Lock/Unlock</source>
        <translation type="vanished">锁定/解锁</translation>
    </message>
    <message>
        <source>Strings</source>
        <translation type="vanished">字符串</translation>
    </message>
    <message>
        <source>Show/Hide Strings panel</source>
        <translation type="vanished">显示/隐藏字符串面板</translation>
    </message>
    <message>
        <source>Sections</source>
        <translation type="vanished">区段</translation>
    </message>
    <message>
        <source>Show/Hide Sections panel</source>
        <translation type="vanished">显示/隐藏区段面板</translation>
    </message>
    <message>
        <source>Segments</source>
        <translation type="vanished">Segments</translation>
    </message>
    <message>
        <source>Show/Hide Segments panel</source>
        <translation type="vanished">显示/隐藏 Segments 面板</translation>
    </message>
    <message>
        <source>Functions</source>
        <translation type="vanished">函数</translation>
    </message>
    <message>
        <source>Show/Hide Functions panel</source>
        <translation type="vanished">显示/隐藏函数面板</translation>
    </message>
    <message>
        <source>Imports</source>
        <translation type="vanished">导入表</translation>
    </message>
    <message>
        <source>Show/Hide Imports panel</source>
        <translation type="vanished">显示/隐藏导入表面板</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation type="vanished">符号</translation>
    </message>
    <message>
        <source>Show/Hide Symbols panel</source>
        <translation type="vanished">显示/隐藏符号面板</translation>
    </message>
    <message>
        <source>Relocs</source>
        <translation type="vanished">重定位表</translation>
    </message>
    <message>
        <source>Show/Hide Relocs panel</source>
        <translation type="vanished">显示/隐藏重定位表面板</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="vanished">Flags</translation>
    </message>
    <message>
        <source>Show/Hide Flags panel</source>
        <translation type="vanished">显示/隐藏标志面板</translation>
    </message>
    <message>
        <source>Memory</source>
        <translation type="vanished">内存</translation>
    </message>
    <message>
        <source>Show/Hide Memory panel</source>
        <translation type="vanished">显示/隐藏内存面板</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="385"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="400"/>
        <location filename="../../core/MainWindow.ui" line="403"/>
        <source>Tabs up/down</source>
        <translation type="unfinished">Tabs up/down</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="412"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <source>Comments</source>
        <translation type="vanished">注释</translation>
    </message>
    <message>
        <source>Show/Hide comments</source>
        <translation type="vanished">显示/隐藏注释</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="420"/>
        <source>Show Tabs at the Top</source>
        <translation>在顶部显示 Tabs</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="423"/>
        <source>Toggle tab position</source>
        <translation>切换 tab 位置</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="428"/>
        <source>Dark Theme</source>
        <translation>深色主题</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="433"/>
        <location filename="../../core/MainWindow.ui" line="436"/>
        <source>Load layout</source>
        <translation>加载布局</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="441"/>
        <source>Default Theme</source>
        <translation>默认主题</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="446"/>
        <source>Bindiff</source>
        <translation type="unfinished">Bindiff</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="451"/>
        <source>Analysis</source>
        <translation>分析</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="456"/>
        <source>Test menu</source>
        <translation>测试菜单</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="461"/>
        <location filename="../../core/MainWindow.ui" line="464"/>
        <source>Copy hexpair</source>
        <translation>复制十六进制</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="469"/>
        <location filename="../../core/MainWindow.ui" line="472"/>
        <source>Copy text</source>
        <translation>复制文本</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="477"/>
        <source>Copy ASCII</source>
        <translation>复制 ASCII</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="482"/>
        <location filename="../../core/MainWindow.ui" line="485"/>
        <source>Insert string</source>
        <translation>插入字符串</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="490"/>
        <location filename="../../core/MainWindow.ui" line="493"/>
        <source>Insert hex</source>
        <translation>插入 HEX</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="508"/>
        <source>Show/Hide bytes</source>
        <translation>显示/隐藏 bytes</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="513"/>
        <source>Switch case</source>
        <translation type="unfinished">Switch case</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="518"/>
        <location filename="../../core/MainWindow.ui" line="521"/>
        <source>Copy all</source>
        <translation>全部拷贝</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="526"/>
        <location filename="../../core/MainWindow.ui" line="529"/>
        <source>Copy bytes</source>
        <translation type="unfinished">Copy bytes</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="534"/>
        <location filename="../../core/MainWindow.ui" line="537"/>
        <location filename="../../core/MainWindow.ui" line="542"/>
        <location filename="../../core/MainWindow.ui" line="545"/>
        <source>Copy disasm</source>
        <translation>复制反汇编</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="557"/>
        <location filename="../../core/MainWindow.ui" line="560"/>
        <source>Start web server</source>
        <translation>启动 Web 服务器</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="565"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="570"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="575"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="580"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="585"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="590"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="595"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="600"/>
        <source>Syntax AT&amp;T/Intel</source>
        <translation>语法 AT&amp;T/Intel</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="605"/>
        <location filename="../../core/MainWindow.ui" line="615"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="610"/>
        <location filename="../../core/MainWindow.ui" line="620"/>
        <source>Undefine</source>
        <translation>未定义</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="625"/>
        <source>Add comment</source>
        <translation>添加注释</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="637"/>
        <location filename="../../core/MainWindow.ui" line="640"/>
        <source>Show/Hide bottom pannel</source>
        <translation>显示/隐藏底部面板</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="645"/>
        <source>Run Rizin script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="717"/>
        <source>Save Project As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="735"/>
        <source>Analyze Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="809"/>
        <source>Commit changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="817"/>
        <source>Write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="820"/>
        <source>Open the file in write mode. Every change to the file will change the original file on disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="828"/>
        <source>Cache mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="831"/>
        <source>Enable cache mode. Changes to the file would not be applied to disk unless you specifically commit them. This is a safer option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="842"/>
        <source>Read-Only mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="852"/>
        <source>Manage layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SDB Browser</source>
        <translation type="vanished">SDB 浏览器</translation>
    </message>
    <message>
        <source>Run Script</source>
        <translation type="vanished">运行脚本</translation>
    </message>
    <message>
        <source>Dashboard</source>
        <translation type="vanished">仪表盘</translation>
    </message>
    <message>
        <source>Show/Hide Dashboard panel</source>
        <translation type="vanished">显示/隐藏仪表盘面板</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="650"/>
        <source>Reset Settings</source>
        <translation>重置设置</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="653"/>
        <source>Reset settings</source>
        <translation>重置设置</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="658"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="661"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="669"/>
        <source>Exports</source>
        <translation>导出表</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="672"/>
        <source>Show/Hide Exports panel</source>
        <translation>显示/隐藏导出表面板</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="677"/>
        <source>Refresh Contents</source>
        <translation>刷新内容</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="680"/>
        <source>Refresh contents</source>
        <translation>刷新内容</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="688"/>
        <source>Show ESIL rather than assembly</source>
        <translation>显示 ESIL 而不是汇编</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="696"/>
        <source>Show pseudocode rather than assembly</source>
        <translation>显示伪代码而不是汇编</translation>
    </message>
    <message>
        <source>Entry Points</source>
        <translation type="vanished">入口点</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="707"/>
        <source>Display offsets</source>
        <translation>显示偏移</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="712"/>
        <source>Preferences</source>
        <translation>首选项</translation>
    </message>
    <message>
        <source>Save As...</source>
        <translation type="vanished">另存为...</translation>
    </message>
    <message>
        <source>Graph Overview</source>
        <translation type="vanished">图表总览</translation>
    </message>
    <message>
        <source>Decompiler</source>
        <translation type="vanished">反编译器</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="745"/>
        <source>Add Hexdump</source>
        <translation type="unfinished">Add Hexdump</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="750"/>
        <source>Add Decompiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="755"/>
        <source>Add Disassembly</source>
        <translation type="unfinished">Add Disassembly</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="760"/>
        <source>Add Graph</source>
        <translation type="unfinished">Add Graph</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="771"/>
        <source>Grouped dock dragging</source>
        <translation>Grouped dock dragging</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="776"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="779"/>
        <source>Ctrl++</source>
        <translation>Ctrl++</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="787"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="790"/>
        <source>Ctrl+-</source>
        <translation>Ctrl+-</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="798"/>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="801"/>
        <source>Ctrl+=</source>
        <translation>Ctrl+=</translation>
    </message>
    <message>
        <source>Tmp</source>
        <translation type="obsolete">Tmp</translation>
    </message>
    <message>
        <source>Disassembly</source>
        <translation type="vanished">反汇编</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="725"/>
        <source>Graph</source>
        <translation>图表</translation>
    </message>
    <message>
        <source>Pseudocode</source>
        <translation type="obsolete">Pseudocode</translation>
    </message>
    <message>
        <source>Hexdump</source>
        <translation type="obsolete">Hexdump</translation>
    </message>
    <message>
        <source>Sidebar</source>
        <translation type="obsolete">Sidebar</translation>
    </message>
    <message>
        <source>Console</source>
        <translation type="vanished">控制台</translation>
    </message>
    <message>
        <source>Stack</source>
        <translation type="vanished">堆栈</translation>
    </message>
    <message>
        <source>Registers</source>
        <translation type="vanished">寄存器</translation>
    </message>
    <message>
        <source>Backtrace</source>
        <translation type="vanished">回溯</translation>
    </message>
    <message>
        <source>Threads</source>
        <translation type="vanished">线程</translation>
    </message>
    <message>
        <source>Processes</source>
        <translation type="vanished">进程</translation>
    </message>
    <message>
        <source>Memory map</source>
        <translation type="vanished">内存布局</translation>
    </message>
    <message>
        <source>Breakpoints</source>
        <translation type="vanished">断点</translation>
    </message>
    <message>
        <source>Register References</source>
        <translation type="obsolete">Register References</translation>
    </message>
    <message>
        <source>Classes</source>
        <translation type="vanished">类</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="730"/>
        <source>Import PDB</source>
        <translation>导入 PDB</translation>
    </message>
    <message>
        <source>Analyze</source>
        <translation type="vanished">分析</translation>
    </message>
    <message>
        <source>Resources</source>
        <translation type="vanished">资源</translation>
    </message>
    <message>
        <source>VTables</source>
        <translation type="vanished">VTables</translation>
    </message>
    <message>
        <source>Show/Hide VTables panel</source>
        <translation type="vanished">显示/隐藏 VTables 面板</translation>
    </message>
    <message>
        <source>Types</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>Show/Hide Types panel</source>
        <translation type="vanished">显示/隐藏类型面板</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索​​​​</translation>
    </message>
    <message>
        <source>Show/Hide Search panel</source>
        <translation type="vanished">显示/隐藏搜索面板</translation>
    </message>
    <message>
        <source>Headers</source>
        <translation type="obsolete">Headers</translation>
    </message>
    <message>
        <source>Show/Hide Headers panel</source>
        <translation type="vanished">显示/隐藏 Headers 面板</translation>
    </message>
    <message>
        <source>Zignatures</source>
        <translation type="obsolete">Zignatures</translation>
    </message>
    <message>
        <source>Show/Hide Zignatures panel</source>
        <translation type="obsolete">Show/Hide Zignatures panel</translation>
    </message>
    <message>
        <source>Jupyter</source>
        <translation type="obsolete">Jupyter</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="740"/>
        <location filename="../../core/MainWindow.cpp" line="1697"/>
        <source>Export as code</source>
        <translation>导出为代码</translation>
    </message>
    <message>
        <source>Hexdump view</source>
        <translation type="obsolete">Hexdump view</translation>
    </message>
    <message>
        <source>Disassembly view</source>
        <translation type="obsolete">Disassembly view</translation>
    </message>
    <message>
        <source>Graph view</source>
        <translation type="obsolete">Graph view</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="533"/>
        <source>Script loading</source>
        <translation>脚本加载</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="534"/>
        <source>Do you want to load the &apos;%1&apos; script?</source>
        <translation>您是否要加载 &apos;%1&apos; 脚本？</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="552"/>
        <source>Cannot open file!</source>
        <translation>无法打开文件！</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="553"/>
        <source>Could not open the file! Make sure the file exists and that you have the correct permissions.</source>
        <translation>无法打开文件！ 请确保文件存在并且您具有相应的权限。</translation>
    </message>
    <message>
        <source> &gt; Populating UI</source>
        <translation type="obsolete"> &gt; Populating UI</translation>
    </message>
    <message>
        <source> &gt; Finished, happy reversing :)</source>
        <translation type="obsolete"> &gt; Finished, happy reversing :)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="764"/>
        <source>Do you really want to exit?
Save your project before closing!</source>
        <translation>项目尚未保存！
您确定要退出吗？</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1038"/>
        <source>New disassembly</source>
        <translation type="unfinished">New disassembly</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1040"/>
        <source>New graph</source>
        <translation type="unfinished">New graph</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1042"/>
        <source>New hexdump</source>
        <translation type="unfinished">New hexdump</translation>
    </message>
    <message>
        <source>Select radare2 script</source>
        <translation type="vanished">选择 radare2 脚本</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="251"/>
        <source>No plugins are installed. Check the plugins section on Cutter documentation to learn more.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="256"/>
        <source>The installed plugins didn&apos;t add entries to this menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="616"/>
        <source>Failed to open project: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="619"/>
        <source>Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1043"/>
        <source>New Decompiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1204"/>
        <source>Save layout error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1205"/>
        <source>&apos;%1&apos; is not a valid name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1207"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1514"/>
        <source>Select Rizin script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1557"/>
        <source>Do you really want to clear all settings?</source>
        <translation>您真的要清除所有设置吗？</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1651"/>
        <source>Select PDB file</source>
        <translation>选择 PDB 文件</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1652"/>
        <source>PDB file (*.pdb)</source>
        <translation>PDB 文件 (*.pdb)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1652"/>
        <source>All files (*)</source>
        <translation>所有文件 (*)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1662"/>
        <source>%1 loaded.</source>
        <translation>%1 已加载。</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1672"/>
        <source>C uin8_t array (*.c)</source>
        <translation>C uin8_t 数组 (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1674"/>
        <source>C uin16_t array (*.c)</source>
        <translation>C uin16_t 数组 (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1676"/>
        <source>C uin32_t array (*.c)</source>
        <translation>C uin32_t 数组 (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1678"/>
        <source>C uin64_t array (*.c)</source>
        <translation>C uin64_t 数组 (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1680"/>
        <source>C string (*.c)</source>
        <translation>C string (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1682"/>
        <source>Shell-script that reconstructs the bin (*.sh)</source>
        <translation type="unfinished">Shell-script that reconstructs the bin (*.sh)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1684"/>
        <source>JSON array (*.json)</source>
        <translation>JSON 数组 (*.json)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1686"/>
        <source>JavaScript array (*.js)</source>
        <translation>JavaScript 数组 (*.js)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1688"/>
        <source>Python array (*.py)</source>
        <translation>Python 数组 (*.py)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1690"/>
        <source>Print &apos;wx&apos; Rizin commands (*.rz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print &apos;wx&apos; r2 commands (*.r2)</source>
        <translation type="vanished">打印 &apos;wx&apos; r2 命令 (*.r2)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1692"/>
        <source>GAS .byte blob (*.asm, *.s)</source>
        <translation>GAS .byte blob (*.asm, *.s)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1694"/>
        <source>.bytes with instructions in comments (*.txt)</source>
        <translation>.bytes 带注释说明 (*.txt)</translation>
    </message>
    <message>
        <source>Project saved: %1</source>
        <translation type="vanished">已保存项目：%1</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="736"/>
        <source>Failed to save project: %1</source>
        <translation>无法保存项目：%1</translation>
    </message>
    <message>
        <source>Project saved:</source>
        <translation type="obsolete">Project saved:</translation>
    </message>
</context>
<context>
    <name>MapFileDialog</name>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="14"/>
        <source>Map New File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="26"/>
        <source>File:</source>
        <translation type="unfinished">File:</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="52"/>
        <location filename="../../dialogs/MapFileDialog.cpp" line="18"/>
        <source>Select file</source>
        <translation type="unfinished">选择文件</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="59"/>
        <source>Map address:</source>
        <translation type="unfinished">Map address:</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="72"/>
        <source>0x40000</source>
        <translation type="unfinished">0x40000</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.cpp" line="36"/>
        <source>Map new file file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.cpp" line="36"/>
        <source>Failed to map a new file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MemoryDockWidget</name>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="obsolete">Sync/unsync offset</translation>
    </message>
</context>
<context>
    <name>MemoryMapModel</name>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="58"/>
        <source>Offset start</source>
        <translation>偏移起始</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="60"/>
        <source>Offset end</source>
        <translation>偏移结束</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="62"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="64"/>
        <source>Permissions</source>
        <translation>权限</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="66"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>MemoryMapWidget</name>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="123"/>
        <source>Memory Map</source>
        <translation>内存布局</translation>
    </message>
</context>
<context>
    <name>MultitypeFileSaveDialog</name>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="24"/>
        <source>Detect type (*)</source>
        <translation>检测类型 (*)</translation>
    </message>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="63"/>
        <source>File save error</source>
        <translation>文件保存错误</translation>
    </message>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="64"/>
        <source>Unrecognized extension &apos;%1&apos;</source>
        <translation>无法识别的扩展名 &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>NativeDebugDialog</name>
    <message>
        <location filename="../../dialogs/NativeDebugDialog.ui" line="35"/>
        <source>Command line arguments:</source>
        <translation>命令行参数：</translation>
    </message>
</context>
<context>
    <name>NewFileDialog</name>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="20"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="149"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="79"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="182"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="390"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="169"/>
        <source>&lt;b&gt;Select new file&lt;b&gt;</source>
        <translation>&lt;b&gt; 选择文件 &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="202"/>
        <source>&lt;b&gt;IO&lt;/b&gt;</source>
        <translation>&lt;b&gt;IO&lt;/b&gt;</translation>
    </message>
    <message>
        <source>://</source>
        <translation type="vanished">://</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="296"/>
        <source>Don&apos;t open any file</source>
        <translation>不打开任何文件</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="303"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="360"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="501"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="316"/>
        <source>Open Shellcode</source>
        <translation>打开 Shellcode</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="328"/>
        <source>&lt;b&gt;Paste Shellcode&lt;b&gt;</source>
        <translation>&lt;b&gt;粘贴 Shellcode&lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="370"/>
        <source>Projects</source>
        <translation>项目</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="410"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Open Project&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="534"/>
        <source>Clear all projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Projects path (dir.projects):&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;项目路径 (dir.projects):&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="519"/>
        <source>Remove item</source>
        <translation>删除项目</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="524"/>
        <source>Clear all</source>
        <translation>清除所有</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="529"/>
        <source>Delete project</source>
        <translation>删除项目</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="92"/>
        <source>Select file</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <source>Select project path (dir.projects)</source>
        <translation type="vanished">选择项目路径 (dir.projects)</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">没有权限</translation>
    </message>
    <message>
        <source>You do not have write access to &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="vanished">您没有权限访问 &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Delete the project &quot;%1&quot; from disk ?</source>
        <translation type="vanished">从磁盘上删除 &quot;%1&quot; 项目？</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="104"/>
        <source>Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="287"/>
        <source>Open a file with no extra treatment.</source>
        <translation>打开文件，不需要额外处理。</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="319"/>
        <source>Select a new program or a previous one before continuing.</source>
        <translation>选择一个新程序或上一个程序，然后继续。</translation>
    </message>
</context>
<context>
    <name>Omnibar</name>
    <message>
        <location filename="../../widgets/Omnibar.cpp" line="15"/>
        <source>Type flag name or address here</source>
        <translation>在此处输入标志名称或地址</translation>
    </message>
</context>
<context>
    <name>OpenFileDialog</name>
    <message>
        <source>Open file</source>
        <translation type="vanished">打开文件</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="vanished">选择文件</translation>
    </message>
    <message>
        <source>Map address:</source>
        <translation type="obsolete">Map address:</translation>
    </message>
    <message>
        <source>File:</source>
        <translation type="vanished">File:</translation>
    </message>
    <message>
        <source>Map address</source>
        <translation type="obsolete">Map address</translation>
    </message>
    <message>
        <source>0x40000</source>
        <translation type="vanished">0x40000</translation>
    </message>
    <message>
        <source>Failed to open file</source>
        <translation type="vanished">无法打开文件</translation>
    </message>
</context>
<context>
    <name>PluginsOptionsWidget</name>
    <message>
        <source>Plugins are loaded from &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="vanished">从加载 &lt;b&gt;%1&lt;/b&gt; 插件</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="28"/>
        <source>Plugins are loaded from &lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Author</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="48"/>
        <source>Show Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show radare2 plugin information</source>
        <translation type="vanished">显示 radare2 插件信息</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.ui" line="6"/>
        <source>Preferences</source>
        <translation>首选项</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="28"/>
        <source>Disassembly</source>
        <translation>反汇编</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="34"/>
        <source>Debug</source>
        <translation>调试</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="35"/>
        <source>Appearance</source>
        <translation>外观</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="36"/>
        <source>Plugins</source>
        <translation>插件</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="37"/>
        <source>Initialization Script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="39"/>
        <source>Analysis</source>
        <translation type="unfinished">分析</translation>
    </message>
</context>
<context>
    <name>ProcessModel</name>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="71"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="73"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="75"/>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="77"/>
        <source>Path</source>
        <translation>路径</translation>
    </message>
</context>
<context>
    <name>ProcessesWidget</name>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="26"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="27"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="28"/>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="29"/>
        <source>Path</source>
        <translation>路径</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="172"/>
        <source>Unable to switch to the requested process.</source>
        <translation>无法切换到请求的进程。</translation>
    </message>
</context>
<context>
    <name>PseudocodeWidget</name>
    <message>
        <source>Pseudocode</source>
        <translation type="obsolete">Pseudocode</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Refresh</translation>
    </message>
    <message>
        <source>Decompiler:</source>
        <translation type="obsolete">Decompiler:</translation>
    </message>
    <message>
        <source>r2dec</source>
        <translation type="obsolete">r2dec</translation>
    </message>
    <message>
        <source>pdc</source>
        <translation type="obsolete">pdc</translation>
    </message>
    <message>
        <source>Click Refresh to generate Pseudocode from current offset.</source>
        <translation type="obsolete">Click Refresh to generate Pseudocode from current offset.</translation>
    </message>
    <message>
        <source>Cannot decompile at</source>
        <translation type="obsolete">Cannot decompile at</translation>
    </message>
    <message>
        <source>(Not a function?)</source>
        <translation type="obsolete">(Not a function?)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>A Qt and C++ GUI for radare2 reverse engineering framework</source>
        <translation type="vanished">用于 Radar2 逆向工程框架的 Qt 和C ++ GUI</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="97"/>
        <source>The version used to compile Cutter (%1) does not match the binary version of rizin (%2). This could result in unexpected behaviour. Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="317"/>
        <source>A Qt and C++ GUI for rizin reverse engineering framework</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="320"/>
        <source>Filename to open.</source>
        <translation>要打开的文件名。</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="324"/>
        <source>Automatically open file and optionally start analysis. Needs filename to be specified. May be a value between 0 and 2: 0 = no analysis, 1 = aaa, 2 = aaaa (experimental)</source>
        <translation>自动打开文件并有选择地开始分析。需要指定文件名。可以是 0 到 2 之间的值：0 =不分析，1 = aaa，2 = aaaa（实验性）</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="327"/>
        <source>level</source>
        <translation>级别</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="331"/>
        <source>Force using a specific file format (bin plugin)</source>
        <translation>强制使用特定的文件格式 (bin plugin)</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="332"/>
        <source>name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="336"/>
        <source>Load binary at a specific base address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="337"/>
        <source>base address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="340"/>
        <source>Run script file</source>
        <translation>运行脚本文件</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="340"/>
        <source>file</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="343"/>
        <source>Load project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="344"/>
        <source>project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="348"/>
        <source>Open file in write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="352"/>
        <source>PYTHONHOME to use for embedded python interpreter</source>
        <translation>PYTHONHOME 用于嵌入式 python 解释器</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="358"/>
        <source>Disable output redirection. Some of the output in console widget will not be visible. Use this option when debuging a crash or freeze and output  redirection is causing some messages to be lost.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="364"/>
        <source>Do not load plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="368"/>
        <source>Do not load Cutter plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="372"/>
        <source>Do not load rizin plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PYTHONHOME to use for Jupyter</source>
        <translation type="obsolete">PYTHONHOME to use for Jupyter</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="96"/>
        <source>Version mismatch!</source>
        <translation>版本不匹配！</translation>
    </message>
    <message>
        <source>The version used to compile Cutter (%1) does not match the binary version of radare2 (%2). This could result in unexpected behaviour. Are you sure you want to continue?</source>
        <translation type="vanished">用于编译Cutter (%1) 的版本与 Radar2 (%2) 的二进制版本不匹配， 这可能会导致意外的行为。 你确定你要继续吗？</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="386"/>
        <source>Invalid Analysis Level. May be a value between 0 and 2.</source>
        <translation>无效的分析级别，有效的级别是 0 到 2 之间的值。</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="406"/>
        <source>Filename must be specified to start analysis automatically.</source>
        <translation>必须指定文件名才能自动开始分析。</translation>
    </message>
    <message>
        <source>Color of comment generated by radare2</source>
        <translation type="obsolete">Color of comment generated by radare2</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="410"/>
        <source>Comment</source>
        <translation>注释</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="410"/>
        <source>Color of comment generated by Rizin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="411"/>
        <source>Comment created by user</source>
        <translation>用户创建的注释</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="411"/>
        <source>Color of user Comment</source>
        <translation>用户注释的颜色</translation>
    </message>
    <message>
        <source>Color of function arguments</source>
        <translation type="obsolete">Color of function arguments</translation>
    </message>
    <message>
        <source>Arguments</source>
        <translation type="obsolete">Arguments</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="413"/>
        <source>Color of names of functions</source>
        <translation>函数名称的颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="413"/>
        <source>Function name</source>
        <translation>函数名称</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="414"/>
        <source>Color of function location</source>
        <translation>函数位置的颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="414"/>
        <source>Function location</source>
        <translation>函数位置</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="416"/>
        <source>Color of ascii line in left side that shows what opcodes are belong to function</source>
        <translation>左侧ascii 线的 颜色，显示哪些操作码属于函数。</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="418"/>
        <source>Function line</source>
        <translation>函数线</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="420"/>
        <source>Color of flags (similar to bookmarks for offset)</source>
        <translation>标志的颜色 (类似于用于偏移的书签)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="420"/>
        <source>Flag</source>
        <translation>标志</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="421"/>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="422"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <source>flow</source>
        <translation type="obsolete">flow</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="424"/>
        <source>flow2</source>
        <translation type="unfinished">flow2</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="425"/>
        <location filename="../../widgets/ColorThemeListView.cpp" line="427"/>
        <source>Info</source>
        <translation type="unfinished">Info</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="425"/>
        <source>prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="426"/>
        <source>Color of offsets</source>
        <translation>颜色偏移</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="426"/>
        <source>Offset</source>
        <translation>偏移量</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="427"/>
        <source>input</source>
        <translation>输入</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="428"/>
        <source>Invalid opcode color</source>
        <translation>无效的 opcode 颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="428"/>
        <source>invalid</source>
        <translation>无效</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="429"/>
        <source>other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="430"/>
        <source>0x00 opcode color</source>
        <translation>0x00 opcode color</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="431"/>
        <source>0x7f opcode color</source>
        <translation>0x7f opcode color</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="432"/>
        <source>0xff opcode color</source>
        <translation>0xff opcode color</translation>
    </message>
    <message>
        <source>arithmetic color (+, -, *, / etc.)</source>
        <translation type="obsolete">arithmetic color (+, -, *, / etc.)</translation>
    </message>
    <message>
        <source>bin</source>
        <translation type="obsolete">bin</translation>
    </message>
    <message>
        <source>btext</source>
        <translation type="obsolete">btext</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="443"/>
        <source>push opcode color</source>
        <translation>push opcode color</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="444"/>
        <source>pop opcode color</source>
        <translation>pop opcode color</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="445"/>
        <source>Cryptographic color</source>
        <translation type="unfinished">Cryptographic color</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="446"/>
        <source>jmp instructions color</source>
        <translation>jmp 指令颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="450"/>
        <source>call instructions color (ccall, rcall, call etc)</source>
        <translation>call 指令颜色 (ccall, rcall, call etc)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="451"/>
        <source>nop opcode color</source>
        <translation type="unfinished">nop opcode color</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="452"/>
        <source>ret opcode color</source>
        <translation type="unfinished">ret opcode color</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="453"/>
        <source>Color of interrupts</source>
        <translation>中断颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="453"/>
        <source>Interrupts</source>
        <translation>中断</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="454"/>
        <source>swi opcode color</source>
        <translation type="unfinished">swi opcode color</translation>
    </message>
    <message>
        <source>cmp opcode color</source>
        <translation type="obsolete">cmp opcode color</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="458"/>
        <source>Registers color</source>
        <translation>寄存器颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="458"/>
        <source>Register</source>
        <translation>寄存器</translation>
    </message>
    <message>
        <source>Numeric constants color</source>
        <translation type="obsolete">Numeric constants color</translation>
    </message>
    <message>
        <source>Numbers</source>
        <translation type="obsolete">Numbers</translation>
    </message>
    <message>
        <source>mov instructions color (mov, movd, movw etc</source>
        <translation type="obsolete">mov instructions color (mov, movd, movw etc</translation>
    </message>
    <message>
        <source>mov</source>
        <translation type="obsolete">mov</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="423"/>
        <source>Color of lines showing jump destination</source>
        <translation>显示跳转目标的线条颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="423"/>
        <source>Flow</source>
        <translation type="unfinished">Flow</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="434"/>
        <source>Color of arithmetic opcodes (add, div, mul etc)</source>
        <translation>算术 opcodes 的颜色，例如：add,、div、mul、etc</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="435"/>
        <source>Arithmetic</source>
        <translation>算术</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="437"/>
        <source>Color of binary operations (and, or, xor etc).</source>
        <translation>二进制运算的颜色，例如：and、or、xor、etc</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="437"/>
        <source>Binary</source>
        <translation>二进制</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="439"/>
        <source>Color of object names, commas between operators, squared brackets and operators inside them.</source>
        <translation>对象名称的颜色，运算符之间的逗号，方括号和其中的运算符。</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="442"/>
        <source>Text</source>
        <translation>文本</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="448"/>
        <source>Color of conditional jump opcodes such as je, jg, jne etc</source>
        <translation>条件跳转 opcodes 的颜色，例如：je、jg、jne</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="449"/>
        <source>Conditional jump</source>
        <translation>条件跳转</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="456"/>
        <source>Color of compare instructions such as test and cmp</source>
        <translation>比较指令的颜色，例如： test、cmp</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="457"/>
        <source>Compare instructions</source>
        <translation>比较指令</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="461"/>
        <source>Color of numeric constants and object pointers</source>
        <translation>数字常量和对象指针的颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="461"/>
        <source>Constants</source>
        <translation>常量</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="463"/>
        <source>Color of move instructions such as mov, movd, lea etc</source>
        <translation>移动指令的颜色，例如：mov、movd、lea </translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="464"/>
        <source>Move instructions</source>
        <translation>移动指令</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="465"/>
        <source>Function variable color</source>
        <translation>函数变量的颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="465"/>
        <source>Function variable</source>
        <translation>函数变量</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="467"/>
        <source>Function variable (local or argument) type color</source>
        <translation>函数变量（局部或参数）类型的颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="468"/>
        <source>Variable type</source>
        <translation>变量类型</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="470"/>
        <source>Function variable address color</source>
        <translation>函数变量地址的颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="470"/>
        <source>Variable address</source>
        <translation>变量地址</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="482"/>
        <source>In graph view jump arrow true</source>
        <translation>在图表视图中跳转箭头为 true</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="482"/>
        <source>Arrow true</source>
        <translation>箭头 true</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="484"/>
        <source>In graph view jump arrow false</source>
        <translation>在图表视图中跳转箭头为 false</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="484"/>
        <source>Arrow false</source>
        <translation>箭头 false</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="486"/>
        <source>In graph view jump arrow (no condition)</source>
        <translation>在图表视图中跳箭头 (无条件)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="486"/>
        <source>Arrow</source>
        <translation>箭头</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="490"/>
        <source>Background color of Graph Overview&apos;s node</source>
        <translation>图表概览 &apos; 节点的背景颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="491"/>
        <source>Graph Overview node</source>
        <translation>图表总览节点</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="493"/>
        <source>Fill color of Graph Overview&apos;s selection</source>
        <translation>填充图表总览选择的颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="494"/>
        <source>Graph Overview fill</source>
        <translation>图表总览填充</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="496"/>
        <source>Border color of Graph Overview&apos;s selection</source>
        <translation>图表总览选择的边框颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="497"/>
        <source>Graph Overview border</source>
        <translation>图表总览边框</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="500"/>
        <source>General background color</source>
        <translation>一般背景色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="500"/>
        <source>Background</source>
        <translation>背景</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="502"/>
        <source>Background color of non-focused graph node</source>
        <translation>非焦点图节点的背景色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="503"/>
        <source>Node background</source>
        <translation>节点背景</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="511"/>
        <source>Background color of selected word</source>
        <translation>所选单词的背景色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="512"/>
        <source>Main function color</source>
        <translation>Main 函数的颜色</translation>
    </message>
    <message>
        <source>Alt. background</source>
        <translation type="obsolete">Alt. background</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="505"/>
        <source>Background of current graph node</source>
        <translation>当前图表节点的背景</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="505"/>
        <source>Current graph node</source>
        <translation>当前图表节点</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="507"/>
        <source>Color of node border in graph view</source>
        <translation>图表视图中节点边框的颜色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="507"/>
        <source>Node border</source>
        <translation>节点边框</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="509"/>
        <source>Selected line background color</source>
        <translation>选定线的背景色</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="509"/>
        <source>Line highlight</source>
        <translation>行高亮</translation>
    </message>
    <message>
        <source>Highlighted word text color</source>
        <translation type="obsolete">Highlighted word text color</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="511"/>
        <source>Word higlight</source>
        <translation>单词高亮</translation>
    </message>
    <message>
        <source>Color of main function color</source>
        <translation type="obsolete">Color of main function color</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="512"/>
        <source>Main</source>
        <translation type="unfinished">Main</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="521"/>
        <source>Code section color in navigation bar</source>
        <translation>Code section color in navigation bar</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="521"/>
        <source>Navbar code</source>
        <translation>导航栏代码</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="523"/>
        <source>Empty section color in navigation bar</source>
        <translation>Empty section color in navigation bar</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="523"/>
        <source>Navbar empty</source>
        <translation>导航栏为空</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="524"/>
        <source>ucall</source>
        <translation type="unfinished">ucall</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="525"/>
        <source>ujmp</source>
        <translation type="unfinished">ujmp</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="526"/>
        <source>Breakpoint background</source>
        <translation>断点背景</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="100"/>
        <source>Crash</source>
        <translation>故障</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="101"/>
        <source>Cutter received a signal it can&apos;t handle and will close.&lt;br/&gt;Would you like to create a crash dump for a bug report?</source>
        <translation>Cutter 收到一个无法处理的信号，它将关闭。&lt;br/&gt;您想为错误报告创建故障转储吗？</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="104"/>
        <source>Create a Crash Dump</source>
        <translation>创建故障转储</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="105"/>
        <location filename="../../common/CrashHandler.cpp" line="148"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="119"/>
        <source>Choose a directory to save the crash dump in</source>
        <translation>选择一个目录来保存故障转储</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="124"/>
        <source>Minidump (*.dmp)</source>
        <translation>Minidump (*.dmp)</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="133"/>
        <source>Save Crash Dump</source>
        <translation>保存故障转储</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="134"/>
        <source>Failed to write to %1.&lt;br/&gt;Please make sure you have access to that directory and try again.</source>
        <translation>写入 %1 失败，&lt;br/&gt;请确保您有相应权限访问该目录，然后重试。</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="142"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="143"/>
        <source>&lt;a href=&quot;%1&quot;&gt;Crash dump&lt;/a&gt; was successfully created.</source>
        <translation>&lt;a href=&quot;%1&quot;&gt;崩溃转储&lt;/a&gt; 已成功创建。</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="147"/>
        <source>Open an Issue</source>
        <translation>打开 Issue</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="156"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="157"/>
        <source>Error occurred during crash dump creation.</source>
        <translation>故障转储创建期间发生错误。</translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="53"/>
        <source>Write error</source>
        <translation type="unfinished">写入错误</translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="54"/>
        <source>Your file is opened in read-only mode. Editing is only available when the file is opened in either Write or Cache modes.

WARNING: In Write mode, any changes will be committed to the file on disk. For safety, please consider using Cache mode and then commit the changes manually via File -&gt; Commit modifications to disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="60"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="62"/>
        <source>Reopen in Write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="64"/>
        <source>Enable Cache mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="99"/>
        <source>Uncomitted changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="100"/>
        <source>It seems that you have changes or patches that are not committed to the file.
Do you want to commit them now?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickFilterView</name>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Form</translation>
    </message>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="41"/>
        <source>Quick Filter</source>
        <translation>快速筛选器</translation>
    </message>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="61"/>
        <source>X</source>
        <translation type="unfinished">X</translation>
    </message>
</context>
<context>
    <name>R2DecDecompiler</name>
    <message>
        <source>Failed to parse JSON from r2dec</source>
        <translation type="vanished">无法从 r2dec 解析 JSON</translation>
    </message>
</context>
<context>
    <name>R2PluginsDialog</name>
    <message>
        <source>radare2 plugin information</source>
        <translation type="vanished">Radar2 插件信息</translation>
    </message>
    <message>
        <source>RBin</source>
        <translation type="vanished">RBin</translation>
    </message>
    <message>
        <source>RBin plugins</source>
        <translation type="vanished">RBin 插件</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">描述</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">许可</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>RIO</source>
        <translation type="vanished">RIO</translation>
    </message>
    <message>
        <source>RIO plugins</source>
        <translation type="vanished">RIO 插件</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="vanished">权限</translation>
    </message>
    <message>
        <source>RCore</source>
        <translation type="vanished">RCore</translation>
    </message>
    <message>
        <source>RCore plugins</source>
        <translation type="vanished">RCore 插件</translation>
    </message>
    <message>
        <source>RAsm</source>
        <translation type="obsolete">RAsm</translation>
    </message>
    <message>
        <source>RAsm plugins</source>
        <translation type="vanished">RAsm 插件</translation>
    </message>
    <message>
        <source>Architecture</source>
        <translation type="vanished">架构</translation>
    </message>
    <message>
        <source>CPU&apos;s</source>
        <translation type="obsolete">CPU&apos;s</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Author</source>
        <translation type="vanished">作者</translation>
    </message>
    <message>
        <source>Cutter</source>
        <translation type="vanished">Cutter</translation>
    </message>
    <message>
        <source>Cutter plugins</source>
        <translation type="obsolete">Cutter plugins</translation>
    </message>
</context>
<context>
    <name>R2TaskDialog</name>
    <message>
        <source>R2 Task</source>
        <translation type="vanished">R2 任务</translation>
    </message>
    <message>
        <source>R2 task in progress..</source>
        <translation type="vanished">R2 任务正在进行中..</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时间</translation>
    </message>
    <message>
        <source>Running for</source>
        <translation type="obsolete">Running for</translation>
    </message>
    <message numerus="yes">
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation type="vanished">
            <numerusform>%n 小时</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation type="vanished">
            <numerusform>%n 分钟</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation type="vanished">
            <numerusform>%n 秒</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RawAddrDock</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="500"/>
        <source>Raw</source>
        <translation type="unfinished">Raw</translation>
    </message>
</context>
<context>
    <name>RegisterRefModel</name>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="67"/>
        <source>Register</source>
        <translation>寄存器</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="69"/>
        <source>Value</source>
        <translation>数值</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="71"/>
        <source>Reference</source>
        <translation type="unfinished">Reference</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="73"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>RegisterRefsWidget</name>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="137"/>
        <source>Copy register value</source>
        <translation>复制寄存器值</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="138"/>
        <source>Copy register reference</source>
        <translation type="unfinished">Copy register reference</translation>
    </message>
</context>
<context>
    <name>RelocsModel</name>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="54"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="56"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="58"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="60"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>RelocsWidget</name>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="131"/>
        <source>Relocs</source>
        <translation type="unfinished">Relocs</translation>
    </message>
</context>
<context>
    <name>RemoteDebugDialog</name>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="43"/>
        <source>Debugger:</source>
        <translation>调试器：</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="82"/>
        <source>IP or Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="159"/>
        <source>Remove item</source>
        <translation type="unfinished">删除项目</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="164"/>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="167"/>
        <source>Remove all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GDB</source>
        <translation type="vanished">GDB</translation>
    </message>
    <message>
        <source>WinDbg - Pipe</source>
        <translation type="obsolete">WinDbg - Pipe</translation>
    </message>
    <message>
        <source>IP:</source>
        <translation type="obsolete">IP:</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="75"/>
        <source>Port:</source>
        <translation>端口：</translation>
    </message>
    <message>
        <source>Path:</source>
        <translation type="vanished">路径：</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="57"/>
        <source>Invalid debugger</source>
        <translation>无效的调试器</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="68"/>
        <source>Invalid IP address</source>
        <translation>IP 地址无效</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="81"/>
        <source>Path does not exist</source>
        <translation>路径不存在</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="94"/>
        <source>Invalid port</source>
        <translation>无效的端口</translation>
    </message>
</context>
<context>
    <name>RenameDialog</name>
    <message>
        <source>Name:</source>
        <translation type="vanished">名称：</translation>
    </message>
</context>
<context>
    <name>ResourcesModel</name>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="76"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="78"/>
        <source>Vaddr</source>
        <translation>虚拟地址</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="80"/>
        <source>Index</source>
        <translation>索引</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="82"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="84"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="86"/>
        <source>Lang</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="88"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>ResourcesWidget</name>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="118"/>
        <source>Resources</source>
        <translation type="unfinished">Resources</translation>
    </message>
</context>
<context>
    <name>RizinGraphWidget</name>
    <message>
        <location filename="../../widgets/RizinGraphWidget.ui" line="57"/>
        <source>ag...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="21"/>
        <source>Data reference graph (aga)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="22"/>
        <source>Global data references graph (agA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="26"/>
        <source>Imports graph (agi)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="27"/>
        <source>References graph (agr)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="28"/>
        <source>Global references graph (agR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="29"/>
        <source>Cross references graph (agx)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="30"/>
        <source>Custom graph (agg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="31"/>
        <source>User command</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RizinPluginsDialog</name>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="14"/>
        <source>Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="24"/>
        <source>RzBin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="30"/>
        <source>RzBin plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="41"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="82"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="123"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="154"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="46"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="87"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="128"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="174"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="51"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="92"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="179"/>
        <source>License</source>
        <translation type="unfinished">许可</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="56"/>
        <source>Type</source>
        <translation type="unfinished">类型</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="65"/>
        <source>RzIO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="71"/>
        <source>RzIO plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="97"/>
        <source>Permissions</source>
        <translation type="unfinished">权限</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="106"/>
        <source>RzCore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="112"/>
        <source>RzCore plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="137"/>
        <source>RzAsm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="143"/>
        <source>RzAsm plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="159"/>
        <source>Architecture</source>
        <translation type="unfinished">架构</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="164"/>
        <source>CPU&apos;s</source>
        <translation type="unfinished">CPU&apos;s</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="169"/>
        <source>Version</source>
        <translation type="unfinished">版本</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="184"/>
        <source>Author</source>
        <translation type="unfinished">作者</translation>
    </message>
</context>
<context>
    <name>RizinTaskDialog</name>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="14"/>
        <source>Rizin Task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="20"/>
        <source>Rizin task in progress..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="27"/>
        <source>Time</source>
        <translation type="unfinished">时间</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="32"/>
        <source>Running for</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="34"/>
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="38"/>
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="41"/>
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation type="unfinished">
            <numerusform>%n 秒</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RunScriptTask</name>
    <message>
        <location filename="../../common/RunScriptTask.cpp" line="18"/>
        <source>Executing script...</source>
        <translation type="unfinished">Executing script...</translation>
    </message>
    <message>
        <location filename="../../common/RunScriptTask.h" line="15"/>
        <source>Run Script</source>
        <translation type="unfinished">Run Script</translation>
    </message>
</context>
<context>
    <name>SaveProjectDialog</name>
    <message>
        <source>Save Project</source>
        <translation type="vanished">保存项目</translation>
    </message>
    <message>
        <source>Project name (prj.name):</source>
        <translation type="vanished">项目名称 (prj.name):</translation>
    </message>
    <message>
        <source>Projects path (dir.projects):</source>
        <translation type="vanished">项目路径 (dir.projects):</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="obsolete">Select</translation>
    </message>
    <message>
        <source>Use simple project saving style (prj.simple, recommended)</source>
        <translation type="vanished">使用简单的项目保存样式 (prj.simple, recommended)</translation>
    </message>
    <message>
        <source>Save the target binary inside the project directory (prj.files)</source>
        <translation type="vanished">将目标二进制文件保存在项目目录中 (prj.files)</translation>
    </message>
    <message>
        <source>Project is a git repo and saving is committing (prj.git)</source>
        <translation type="vanished">Project is a git repo and saving is committing (prj.git)</translation>
    </message>
    <message>
        <source>Use ZIP format for project files (prj.zip)</source>
        <translation type="vanished">Use ZIP format for project files (prj.zip)</translation>
    </message>
    <message>
        <source>Select project path (dir.projects)</source>
        <translation type="vanished">选择项目路径 (dir.projects)</translation>
    </message>
    <message>
        <source>Save project</source>
        <translation type="vanished">保存项目</translation>
    </message>
    <message>
        <source>Invalid project name.</source>
        <translation type="vanished">无效的项目名称。</translation>
    </message>
</context>
<context>
    <name>SdbDock</name>
    <message>
        <source>Key</source>
        <translation type="obsolete">Key</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Value</translation>
    </message>
</context>
<context>
    <name>SdbWidget</name>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="17"/>
        <source>SDB Browser</source>
        <translation>SDB 浏览器</translation>
    </message>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="120"/>
        <source>Key</source>
        <translation type="unfinished">Key</translation>
    </message>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="125"/>
        <source>Value</source>
        <translation>Value</translation>
    </message>
</context>
<context>
    <name>SearchModel</name>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="96"/>
        <source>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;预览&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="115"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="117"/>
        <source>Offset</source>
        <translation type="unfinished">Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="119"/>
        <source>Code</source>
        <translation>代码</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="121"/>
        <source>Data</source>
        <translation>数据</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="123"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="83"/>
        <source>Search</source>
        <translation type="unfinished">Search</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="90"/>
        <source>Search for:</source>
        <translation>搜索:</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="100"/>
        <source>Search in:</source>
        <translation>搜索自:</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="251"/>
        <source>asm code</source>
        <translation>asm code</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="252"/>
        <source>string</source>
        <translation>string</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="253"/>
        <source>hex string</source>
        <translation>hex string</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="254"/>
        <source>ROP gadgets</source>
        <translation>ROP gadgets</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="255"/>
        <source>32bit value</source>
        <translation>32bit value</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="282"/>
        <source>No results found for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="285"/>
        <source>No Results Found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SectionsModel</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="93"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="95"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="101"/>
        <source>Virtual Size</source>
        <translation>虚拟大小</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="97"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="99"/>
        <source>End Address</source>
        <translation>结束地址</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="103"/>
        <source>Permissions</source>
        <translation>权限</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="105"/>
        <source>Entropy</source>
        <translation>熵</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="107"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>SegmentsModel</name>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="81"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="83"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="85"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="87"/>
        <source>End Address</source>
        <translation>结束地址</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="89"/>
        <source>Permissions</source>
        <translation>权限</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="91"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>SetFunctionVarTypes</name>
    <message>
        <source>Dialog</source>
        <translation type="obsolete">Dialog</translation>
    </message>
    <message>
        <source>Set Type To:</source>
        <translation type="obsolete">Set Type To:</translation>
    </message>
    <message>
        <source>Set Name To:</source>
        <translation type="obsolete">Set Name To:</translation>
    </message>
    <message>
        <source>Modify:</source>
        <translation type="obsolete">Modify:</translation>
    </message>
    <message>
        <source>You must be in a function to define variable types.</source>
        <translation type="obsolete">You must be in a function to define variable types.</translation>
    </message>
</context>
<context>
    <name>SetToDataDialog</name>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="14"/>
        <source>Set to Data</source>
        <translation>设置为数据</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="20"/>
        <location filename="../../dialogs/SetToDataDialog.ui" line="34"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="27"/>
        <source>Start address</source>
        <translation>起始地址</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="41"/>
        <source>End address</source>
        <translation>结束地址</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="48"/>
        <source>Item size</source>
        <translation type="unfinished">Item size</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="55"/>
        <source>Number of items</source>
        <translation type="unfinished">Number of items</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="62"/>
        <location filename="../../dialogs/SetToDataDialog.ui" line="69"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>SideBar</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Form</translation>
    </message>
    <message>
        <source>Script</source>
        <translation type="obsolete">Script</translation>
    </message>
    <message>
        <source>X</source>
        <translation type="obsolete">X</translation>
    </message>
    <message>
        <source>example.py</source>
        <translation type="obsolete">example.py</translation>
    </message>
    <message>
        <source>Execution finished</source>
        <translation type="obsolete">Execution finished</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation type="obsolete">Calculator</translation>
    </message>
    <message>
        <source>Assembler</source>
        <translation type="obsolete">Assembler</translation>
    </message>
    <message>
        <source>Assembly</source>
        <translation type="obsolete">Assembly</translation>
    </message>
    <message>
        <source>v</source>
        <translation type="obsolete">v</translation>
    </message>
    <message>
        <source>^</source>
        <translation type="obsolete">^</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation type="vanished">十六进制</translation>
    </message>
    <message>
        <source>Toogle resposiveness</source>
        <translation type="obsolete">Toogle resposiveness</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Settings</translation>
    </message>
</context>
<context>
    <name>SidebarWidget</name>
    <message>
        <source> Function:</source>
        <translation type="obsolete"> Function:</translation>
    </message>
    <message>
        <source>Offset info:</source>
        <translation type="obsolete">Offset info:</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="obsolete">Info</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Value</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Opcode description:</source>
        <translation type="obsolete">Opcode description:</translation>
    </message>
    <message>
        <source>Function registers info:</source>
        <translation type="obsolete">Function registers info:</translation>
    </message>
    <message>
        <source>X-Refs to current address:</source>
        <translation type="obsolete">X-Refs to current address:</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="vanished">地址</translation>
    </message>
    <message>
        <source>Instruction</source>
        <translation type="obsolete">Instruction</translation>
    </message>
    <message>
        <source>X-Refs from current address:</source>
        <translation type="obsolete">X-Refs from current address:</translation>
    </message>
</context>
<context>
    <name>SimpleTextGraphView</name>
    <message>
        <location filename="../../widgets/SimpleTextGraphView.cpp" line="29"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StackModel</name>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="216"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="218"/>
        <source>Value</source>
        <translation>Value</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="220"/>
        <source>Reference</source>
        <translation>引用</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="222"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>StackWidget</name>
    <message>
        <source>Offset</source>
        <translation type="vanished">偏移量</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">值</translation>
    </message>
    <message>
        <source>Reference</source>
        <translation type="obsolete">Reference</translation>
    </message>
    <message>
        <source>Seek to this offset</source>
        <translation type="obsolete">Seek to this offset</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="33"/>
        <source>Edit stack value...</source>
        <translation>编辑堆栈值...</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="109"/>
        <source>Edit stack at %1</source>
        <translation>在 %1 编辑堆栈</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="138"/>
        <source>Stack position</source>
        <translation>堆栈位置</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="140"/>
        <source>Pointed memory</source>
        <translation type="unfinished">Pointed memory</translation>
    </message>
</context>
<context>
    <name>StringsModel</name>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="67"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="69"/>
        <source>String</source>
        <translation>字符串</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="71"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="73"/>
        <source>Length</source>
        <translation>长度</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="75"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="77"/>
        <source>Section</source>
        <translation>区段</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="79"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>StringsTask</name>
    <message>
        <location filename="../../common/StringsTask.h" line="13"/>
        <source>Searching for Strings</source>
        <translation>搜索字符串</translation>
    </message>
</context>
<context>
    <name>StringsWidget</name>
    <message>
        <source>Copy Address</source>
        <translation type="obsolete">Copy Address</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.ui" line="76"/>
        <source>Copy String</source>
        <translation>复制字符串</translation>
    </message>
    <message>
        <source>Xrefs</source>
        <translation type="obsolete">Xrefs</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.ui" line="81"/>
        <source>Filter</source>
        <translation>过滤</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="161"/>
        <source>Section:</source>
        <translation type="unfinished">Section:</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="244"/>
        <source>(all)</source>
        <translation>(所有)</translation>
    </message>
</context>
<context>
    <name>SymbolsModel</name>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="58"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="60"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="62"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="64"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
</context>
<context>
    <name>SymbolsWidget</name>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="123"/>
        <source>Symbols</source>
        <translation>符号</translation>
    </message>
</context>
<context>
    <name>ThreadsWidget</name>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="24"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="25"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="26"/>
        <source>Path</source>
        <translation>Path</translation>
    </message>
</context>
<context>
    <name>TypesInteractionDialog</name>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="23"/>
        <source>Load From File:</source>
        <translation>从文件加载：</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="37"/>
        <source>Select File</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="50"/>
        <source>Enter Types Manually</source>
        <translation>手动输入类型</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="32"/>
        <source>Select file</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="40"/>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="68"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="69"/>
        <source>There was some error while loading new types</source>
        <translation>加载新类型时出错</translation>
    </message>
</context>
<context>
    <name>TypesModel</name>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="62"/>
        <source>Type / Name</source>
        <translation>类型/名称</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="64"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="66"/>
        <source>Format</source>
        <translation>格式</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="68"/>
        <source>Category</source>
        <translation>类别</translation>
    </message>
</context>
<context>
    <name>TypesWidget</name>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="76"/>
        <location filename="../../widgets/TypesWidget.ui" line="79"/>
        <source>Export Types</source>
        <translation>导出类型</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="84"/>
        <location filename="../../widgets/TypesWidget.ui" line="87"/>
        <location filename="../../widgets/TypesWidget.cpp" line="286"/>
        <source>Load New Types</source>
        <translation>加载新类型</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="92"/>
        <location filename="../../widgets/TypesWidget.ui" line="95"/>
        <source>Delete Type</source>
        <translation>删除类型</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="100"/>
        <source>Link Type to Address</source>
        <translation type="unfinished">Link Type to Address</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="140"/>
        <source>Category</source>
        <translation>类别</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="185"/>
        <source>View Type</source>
        <translation>查看类型</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="186"/>
        <source>Edit Type</source>
        <translation>编辑类型</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="217"/>
        <source>(All)</source>
        <translation>(所有)</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="266"/>
        <source>Save File</source>
        <translation>保存文件</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="273"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="302"/>
        <source>Edit Type: </source>
        <translation>编辑类型：</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="305"/>
        <location filename="../../widgets/TypesWidget.cpp" line="355"/>
        <source>View Type: </source>
        <translation>查看类型：</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="305"/>
        <location filename="../../widgets/TypesWidget.cpp" line="355"/>
        <source> (Read Only)</source>
        <translation> (只读)</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="324"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="324"/>
        <source>Are you sure you want to delete &quot;%1&quot;?</source>
        <translation>您确定要删除 &quot;%1&quot;？</translation>
    </message>
</context>
<context>
    <name>UpdateWorker</name>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="34"/>
        <source>Time limit exceeded during version check. Please check your internet connection and try again.</source>
        <translation>版本检查超时，请检查您的网络连接，然后重试。</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="81"/>
        <source>Version control</source>
        <translation>版本控制</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="82"/>
        <source>There is an update available for Cutter.&lt;br/&gt;</source>
        <translation>Cutter 有可用更新。&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="82"/>
        <source>Current version:</source>
        <translation>当前版本:</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="83"/>
        <source>Latest version:</source>
        <translation>最新版本:</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="85"/>
        <source>For update, please check the link:&lt;br/&gt;</source>
        <translation>有关更新，请检查链接：&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="89"/>
        <source>or click &quot;Download&quot; to download latest version of Cutter.</source>
        <translation>或点击下载，以下载最新版本的 Cutter。</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="92"/>
        <source>Don&apos;t check for updates</source>
        <translation>不要检查更新</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="96"/>
        <source>Download</source>
        <translation>下载</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="103"/>
        <source>Choose directory for downloading</source>
        <translation>选择下载目录</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="108"/>
        <source>Downloading update...</source>
        <translation>正在下载更新...</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="108"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="116"/>
        <source>Download finished!</source>
        <translation>下载完成！</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="117"/>
        <source>Latest version of Cutter was succesfully downloaded!</source>
        <translation>最新版本的 Cutter 已成功下载！</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="119"/>
        <source>Open file</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="120"/>
        <source>Open download folder</source>
        <translation>打开下载文件夹</translation>
    </message>
</context>
<context>
    <name>VTableModel</name>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="63"/>
        <source>VTable</source>
        <translation>VTable</translation>
    </message>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="84"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="86"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
</context>
<context>
    <name>VersionInfoDialog</name>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="50"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="64"/>
        <source>TextLabel</source>
        <translation>文本标签</translation>
    </message>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="94"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="126"/>
        <source>Key</source>
        <translation type="unfinished">Key</translation>
    </message>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="99"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="131"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
</context>
<context>
    <name>VirtualAddrDock</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="508"/>
        <source>Virtual</source>
        <translation>Virtual</translation>
    </message>
</context>
<context>
    <name>VisualNavbar</name>
    <message>
        <location filename="../../widgets/VisualNavbar.cpp" line="29"/>
        <source>Visual navigation bar</source>
        <translation type="unfinished">Visual navigation bar</translation>
    </message>
</context>
<context>
    <name>WelcomeDialog</name>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="20"/>
        <source>Welcome to Cutter</source>
        <translation>欢迎使用 Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="83"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="104"/>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="19"/>
        <source>Version </source>
        <translation>版本 </translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="168"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="203"/>
        <source>Native Theme</source>
        <translation>原生主题</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="208"/>
        <source>Dark Theme</source>
        <translation>深色主题</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="213"/>
        <source>Midnight Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="218"/>
        <source>Light Theme</source>
        <translation>浅色主题</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="229"/>
        <source>Check for updates on start</source>
        <translation>在启动时检查更新</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="298"/>
        <source>Community</source>
        <translation>社区</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="320"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/cutter_re&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@cutter_re&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/cutter_re&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@cutter_re &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="355"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;/span&gt;&lt;a href=&quot;https://github.com/rizinorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and report bugs or contribute code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;/span&gt;&lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and report bugs or contribute code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;/span&gt;&lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and report bugs or contribute code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="417"/>
        <source>Continue</source>
        <translation>继续</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;	&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;#cutter on &lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;	&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;#cutter on &lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt; and report bugs or contribute code.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt; and report bugs or contribute code.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="394"/>
        <source>Contributing</source>
        <translation>贡献</translation>
    </message>
    <message>
        <source>Continue 🢒</source>
        <translation type="obsolete">Continue 🢒</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="70"/>
        <source>Language settings</source>
        <translation>语言设置</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="71"/>
        <source>Language will be changed after next application start.</source>
        <translation>语言将在下一次应用程序启动后更改。</translation>
    </message>
</context>
<context>
    <name>XrefModel</name>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="287"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="291"/>
        <source>Code</source>
        <translation>代码</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="293"/>
        <source>Comment</source>
        <translation type="unfinished">注释</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="289"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
</context>
<context>
    <name>XrefsDialog</name>
    <message>
        <source>Address</source>
        <translation type="vanished">地址</translation>
    </message>
    <message>
        <source>Code</source>
        <translation type="obsolete">Code</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>X-Refs to %1:</source>
        <translation type="vanished">%1的外部参照：</translation>
    </message>
    <message>
        <source>X-Refs from %1:</source>
        <translation type="vanished">来自%1的外部参照：</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="145"/>
        <source>X-Refs to %1 (%2 results):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="147"/>
        <source>X-Refs from %1 (%2 results):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="152"/>
        <source>Writes to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="153"/>
        <source>Reads from %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="164"/>
        <location filename="../../dialogs/XrefsDialog.cpp" line="183"/>
        <source>X-Refs for %1</source>
        <translation>%1 的外部参考</translation>
    </message>
</context>
<context>
    <name>ZignaturesModel</name>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="66"/>
        <source>Offset</source>
        <translation>偏移量</translation>
    </message>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="68"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="70"/>
        <source>Bytes</source>
        <translation>字节</translation>
    </message>
</context>
</TS>
