<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="14"/>
        <source>About Cutter</source>
        <translation>О Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="69"/>
        <source>Check for updates on start</source>
        <translation>Проверять обновления при старте</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="95"/>
        <source>Show version information</source>
        <translation>Показать сведения о версии</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="108"/>
        <source>Show Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by Rizin&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show radare2 plugin information</source>
        <translation type="vanished">Показать информацию о плагинах radare2</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by radare2&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter - бесплатная платформа для реверс-инжиниринга, с открытым исходным кодом, использующая radare2&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Читать подробнее &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="128"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:28pt; font-weight:600;&quot;&gt;Cutter&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:28pt; font-weight:600;&quot;&gt;Cutter&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Show plugin information</source>
        <translation type="vanished">Показать информацию о плагинах</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="82"/>
        <source>Check for updates</source>
        <translation>Проверить обновления</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="28"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <source>Using r2-</source>
        <translation type="vanished">Использование r2-</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="28"/>
        <source>Using rizin </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="30"/>
        <source>Optional Features:</source>
        <translation>Дополнительные возможности:</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="47"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="48"/>
        <source>This Software is released under the GNU General Public License v3.0</source>
        <translation>Это программное обеспечение распространяется под лицензией GNU General Public License v3.0</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="49"/>
        <source>Authors</source>
        <translation>Авторы</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="50"/>
        <source>Cutter is developed by the community and maintained by its core and development teams.&lt;br/&gt;</source>
        <translation>Cutter разработан сообществом и поддерживается его командами ядра и разработки.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="52"/>
        <source>Check our &lt;a href=&apos;https://github.com/rizinorg/cutter/graphs/contributors&apos;&gt;contributors page&lt;/a&gt; for the full list of contributors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="76"/>
        <source>Rizin version information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check our &lt;a href=&apos;https://github.com/radareorg/cutter/graphs/contributors&apos;&gt;contributors page&lt;/a&gt; for the full list of contributors.</source>
        <translation type="vanished">Полный список участников можно просмотреть на нашей &lt;a href=&apos;https://github.com/radareorg/cutter/graphs/contributors&apos;&gt;странице участников&lt;/a&gt;.</translation>
    </message>
    <message>
        <source>radare2 version information</source>
        <translation type="vanished">Информация о версии radare2</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="99"/>
        <source>Checking for updates...</source>
        <translation>Проверка обновлений...</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="109"/>
        <source>Cutter is up to date!</source>
        <translation>Cutter обновлён!</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="150"/>
        <source>Based on Qt %1 (%2, %3 bit)</source>
        <translation>Основано на Qt %1 (%2, %3-битном)</translation>
    </message>
    <message>
        <source>Timeout error!</source>
        <translation type="vanished">Превышение времени ожидания!</translation>
    </message>
    <message>
        <source>Please check your internet connection and try again.</source>
        <translation type="vanished">Пожалуйста, проверьте ваше интернет-соединение и попробуйте еще раз.</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="105"/>
        <source>Error!</source>
        <translation>Ошибка!</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="108"/>
        <source>Version control</source>
        <translation>Управление версиями</translation>
    </message>
    <message>
        <source>You have latest version and no need to update!</source>
        <translation type="vanished">У вас установлена последняя версия, и в обновлении нет необходимости!</translation>
    </message>
    <message>
        <source>Current version:</source>
        <translation type="vanished">Текущая версия:</translation>
    </message>
    <message>
        <source>Latest version:</source>
        <translation type="vanished">Последняя версия:</translation>
    </message>
    <message>
        <source>For update, please check the link:</source>
        <translation type="vanished">Для обновления перейдите по ссылке:</translation>
    </message>
</context>
<context>
    <name>AddressableDockWidget</name>
    <message>
        <location filename="../../widgets/AddressableDockWidget.cpp" line="12"/>
        <source>Sync/unsync offset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddressableItemContextMenu</name>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="16"/>
        <source>Show in</source>
        <translation>Показать в</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="17"/>
        <source>Copy address</source>
        <translation>Копировать адрес</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="18"/>
        <source>Show X-Refs</source>
        <translation>Показать перекрестные ссылки</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="19"/>
        <source>Add comment</source>
        <translation>Добавить комментарий</translation>
    </message>
</context>
<context>
    <name>AnalClassesModel</name>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="403"/>
        <source>class</source>
        <translation>класс</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="427"/>
        <source>base</source>
        <translation>базовый</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="458"/>
        <source>method</source>
        <translation>метод</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="494"/>
        <source>vtable</source>
        <translation>vtable</translation>
    </message>
</context>
<context>
    <name>AnalOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="20"/>
        <source>Analysis</source>
        <translation type="unfinished">Анализ</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="55"/>
        <source>Show verbose information when performing analysis (analysis.verbose)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="68"/>
        <source>Analyze push+ret as jmp (analysis.pushret)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="81"/>
        <source>Verbose output from type analysis (analysis.types.verbose)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="94"/>
        <source>Speculatively set a name for the functions (analysis.autoname)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="107"/>
        <source>Search for new functions following already defined functions (analysis.hasnext)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="120"/>
        <source>Create references for unconditional jumps (analysis.jmp.ref)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="133"/>
        <source>Analyze jump tables in switch statements (analysis.jmp.tbl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="161"/>
        <source>Search boundaries for analysis (analysis.in): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="195"/>
        <source>Pointer depth (analysis.ptrdepth):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="236"/>
        <source>Functions Prelude (analysis.prelude):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="269"/>
        <source>Analyze program</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AnalTask</name>
    <message>
        <location filename="../../common/AnalTask.cpp" line="26"/>
        <source>Analyzing Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="43"/>
        <source>Loading the file...</source>
        <translation>Загрузка файла...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="69"/>
        <source>Loading PDB file...</source>
        <translation>Загрузка PDB файла...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="78"/>
        <source>Loading shellcode...</source>
        <translation>Загрузка шеллкода...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="89"/>
        <source>Executing script...</source>
        <translation>Выполнение скрипта...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="98"/>
        <source>Executing analysis...</source>
        <translation>Выполнение анализа...</translation>
    </message>
    <message>
        <source>Analyzing...</source>
        <translation type="vanished">Анализ...</translation>
    </message>
    <message>
        <source>Running</source>
        <translation type="vanished">Выполняется</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="107"/>
        <source>Analysis complete!</source>
        <translation>Анализ завершён!</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="109"/>
        <source>Skipping Analysis.</source>
        <translation>Пропуск анализа.</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="24"/>
        <source>Initial Analysis</source>
        <translation>Первоначальный анализ</translation>
    </message>
</context>
<context>
    <name>AppearanceOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="14"/>
        <source>Appearance</source>
        <translation>Внешний вид</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="28"/>
        <source>Font:</source>
        <translation>Шрифт:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="56"/>
        <source>Select font</source>
        <translation>Выбрать шрифт</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="76"/>
        <source>Zoom</source>
        <translation>Масштаб</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="95"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="116"/>
        <source>Language:</source>
        <translation>Язык:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="133"/>
        <source>Interface Theme:</source>
        <translation>Тема оформления:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="160"/>
        <source>Color Theme:</source>
        <translation>Цветовая тема:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="179"/>
        <source>Edit Theme</source>
        <translation>Редактировать тему</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="193"/>
        <source>Rename</source>
        <translation>Переименовать</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="241"/>
        <source>Export</source>
        <translation>Экспорт</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="255"/>
        <source>Import</source>
        <translation>Импорт</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="289"/>
        <source>Use information provided by decompiler when highlighting code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="292"/>
        <source>Decompiler based highlighting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Qt Theme:</source>
        <translation type="vanished">Тема Qt:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="147"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="152"/>
        <source>Dark</source>
        <translation>Темный</translation>
    </message>
    <message>
        <source>Color Theme</source>
        <translation type="vanished">Цветовая схема</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="213"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="227"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="163"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Язык</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="312"/>
        <source>Save as Default</source>
        <translation>Сохранить по умолчанию</translation>
    </message>
    <message>
        <source>Enter scheme name</source>
        <translation type="vanished">Введите название цветовой схемы</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="136"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="221"/>
        <source>Name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <source>Are you sure you want to delete theme </source>
        <translation type="vanished">Вы уверены, что хотите удалить тему </translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="125"/>
        <source>Theme Editor - &lt;%1&gt;</source>
        <translation>Редактор тем - &lt;%1&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="136"/>
        <source>Enter theme name</source>
        <translation>Введите название темы</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="137"/>
        <source> - copy</source>
        <translation> - копировать</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="143"/>
        <source>Theme Copy</source>
        <translation>Скопировать тему</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="144"/>
        <source>Theme named %1 already exists.</source>
        <translation>Тема с именем %1 уже существует.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="159"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="168"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="190"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="214"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="229"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="163"/>
        <source>Are you sure you want to delete &lt;b&gt;%1&lt;/b&gt;?</source>
        <translation>Вы уверены, что хотите удалить &lt;b&gt;%1&lt;/b&gt;?</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="185"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="211"/>
        <source>Success</source>
        <translation>Действие выполнено успешно</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="186"/>
        <source>Color theme &lt;b&gt;%1&lt;/b&gt; was successfully imported.</source>
        <translation>Цветовая тема &lt;b&gt;%1&lt;/b&gt; успешно импортирована.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="212"/>
        <source>Color theme &lt;b&gt;%1&lt;/b&gt; was successfully exported.</source>
        <translation>Цветовая тема &lt;b&gt;%1&lt;/b&gt; была успешно экспортирована.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="221"/>
        <source>Enter new theme name</source>
        <translation>Введите название темы</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="240"/>
        <source>Language settings</source>
        <translation>Языковые параметры</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="241"/>
        <source>Language will be changed after next application start.</source>
        <translation>Язык будет изменен после следующего запуска приложения.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="245"/>
        <source>Cannot set language, not found in available ones</source>
        <translation>Выбранный язык недоступен</translation>
    </message>
</context>
<context>
    <name>AsmOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="14"/>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="65"/>
        <source>Disassembly</source>
        <translation>Дизассемблер</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="35"/>
        <source>Style</source>
        <translation>Стиль</translation>
    </message>
    <message>
        <source>Show ESIL instead of assembly (asm.esil)</source>
        <translation type="vanished">Показать ESIL, вместо ассемблерного листинга (asm.esil)</translation>
    </message>
    <message>
        <source>Show pseudocode instead of assembly (asm.pseudo)</source>
        <translation type="vanished">Показать псевдокод, вместо ассемблерного листинга (asm.pseudo)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="170"/>
        <source>Show offsets (asm.offset)</source>
        <translation>Показать смещения (asm.offset)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="71"/>
        <source>Display the bytes of each instruction (asm.bytes)</source>
        <translation>Отображение байтов каждой инструкции (asm.bytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="291"/>
        <source>Comments</source>
        <translation>Комментарии</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="299"/>
        <source>Show opcode description (asm.describe)</source>
        <translation>Показать описание опкода (asm.describe)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="78"/>
        <source>Syntax (asm.syntax):</source>
        <translation>Синтаксис (asm.syntax):</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="198"/>
        <source>Lowercase</source>
        <translation>Строчный</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="203"/>
        <source>Uppercase (asm.ucase)</source>
        <translation>Верхний регистр (asm.ucase)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="208"/>
        <source>Capitalize (asm.capitalize)</source>
        <translation>Заглавная буква (asm.capitalize)</translation>
    </message>
    <message>
        <source>Separate bytes with whitespace (asm.bytespace)</source>
        <translation type="vanished">Отделить байты пробелом (asm.bytespace)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="121"/>
        <source>Indent disassembly based on reflines depth (asm.indent)</source>
        <translation>Отступ в листинге основанный на глубине линий ссылок (asm.indent)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="98"/>
        <source>Show Disassembly as:</source>
        <translation>Показать листинг как:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="190"/>
        <source>Show empty line after every basic block (asm.bb.line)</source>
        <translation>Показывать пустую строку после каждого базового блока (asm.bb.line)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="152"/>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="307"/>
        <source>Normal</source>
        <translation>Нормально</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="157"/>
        <source>ESIL (asm.esil)</source>
        <translation>ESIL (asm.esil)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="162"/>
        <source>Pseudocode (asm.pseudo)</source>
        <translation>Псевдокод (asm.pseudo)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="216"/>
        <source>Align bytes to the left (asm.lbytes)</source>
        <translation>Выравнивание байт по левой стороне (asm.lbytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="223"/>
        <source>Separate bytes with whitespace (asm.bytes.space)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="230"/>
        <source>Display flags&apos; real name (asm.flags.real)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="259"/>
        <source>Show offsets relative to:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="266"/>
        <source>Functions (asm.reloff)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="275"/>
        <source>Flags (asm.reloff.flags)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="312"/>
        <source>Above instructions</source>
        <translation>Инструкции выше</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="317"/>
        <source>Off</source>
        <translation>Выключено</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="325"/>
        <source>Show comments:</source>
        <translation>Показать комментарии:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="475"/>
        <source>Substitute variables (asm.sub.var)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="482"/>
        <source>Substitute entire variable expressions with names (asm.sub.varonly)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="237"/>
        <source>Tabs in assembly (asm.tabs):</source>
        <translation>Отступов после инструкции (asm.tabs):</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="177"/>
        <source>Tabs before assembly (asm.tabs.off):</source>
        <translation>Отступов до инструкции (asm.tabs.off):</translation>
    </message>
    <message>
        <source>Show empty line after every basic block (asm.bbline)</source>
        <translation type="vanished">Показывать пустую строку после каждого базового блока (asm.bbline)</translation>
    </message>
    <message>
        <source>Show comments at right of assembly (asm.cmt.right)</source>
        <translation type="vanished">Показывать комментарии справа от инструкций (asm.cmt.right)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="345"/>
        <source>Column to align comments (asm.cmt.col):</source>
        <translation>Столбец для выравнивания комментариев (asm.cmt.col):</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="355"/>
        <source>Show x-refs (asm.xrefs)</source>
        <translation>Показать x-refs (asm.xrefs)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="362"/>
        <source>Show refpointer information (asm.refptr)</source>
        <translation>Показывать информацию об указателе (asm.refptr)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="395"/>
        <source>Metadata</source>
        <translation>Метаданные</translation>
    </message>
    <message>
        <source>Show stack pointer (asm.stackptr)</source>
        <translation type="vanished">Показать указатель стека (asm.stackptr)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="419"/>
        <source>Slow Analysis (asm.slow)</source>
        <translation>Медленный анализ (asm.slow)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="426"/>
        <source>Show jump lines (asm.lines)</source>
        <translation>Показать прыжки (asm.lines)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="433"/>
        <source>Show function boundary lines (asm.lines.fcn)</source>
        <translation>Показывать границы функции (asm.lines.fcn)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="440"/>
        <source>Show offset before flags (asm.flags.off)</source>
        <translation>Показать смещение перед флагами (asm.flags.off)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="447"/>
        <source>Run ESIL emulation analysis (asm.emu)</source>
        <translation>Запустить анализ с помощью эмуляции ESIL (asm.emu)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="454"/>
        <source>Show only strings if any in the asm.emu output (emu.str)</source>
        <translation>Показывать только строки, если они находятся в выводе asm.emu (emu.str)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="461"/>
        <source>Show size of opcodes in disassembly (asm.size)</source>
        <translation>Показать размер опкодов в дизассемблере (asm.size)</translation>
    </message>
    <message>
        <source>Show bytes (asm.bytes)</source>
        <translation type="vanished">Показать байты (asm.bytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="468"/>
        <source>Show variables summary instead of full list (asm.var.summary)</source>
        <translation>Краткое отображение переменных вместо списка (asm.var.summary)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="111"/>
        <source>Number of bytes to display (asm.nbytes):</source>
        <translation>Количество байт для отображения (asm.nbytes):</translation>
    </message>
    <message>
        <source>Substitute variables (asm.var.sub)</source>
        <translation type="vanished">Заменить переменные (asm.var.sub)</translation>
    </message>
    <message>
        <source>Substitute entire variable expressions with names (asm.var.subonly)</source>
        <translation type="vanished">Заменить все переменные выражения именами (asm.var.subonly)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="538"/>
        <source>Save as Default</source>
        <translation>Сохранить по умолчанию</translation>
    </message>
</context>
<context>
    <name>AsyncTaskDialog</name>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.ui" line="14"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.ui" line="20"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="43"/>
        <source>Running for</source>
        <translation>Запущен</translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="45"/>
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation>
            <numerusform>%n час</numerusform>
            <numerusform>%n часов</numerusform>
            <numerusform>%n часов</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="49"/>
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation>
            <numerusform>%n минута</numerusform>
            <numerusform>%n минуты</numerusform>
            <numerusform>%n минут</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="52"/>
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation>
            <numerusform>%n секунда</numerusform>
            <numerusform>%n секунды</numerusform>
            <numerusform>%n секунд</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>AttachProcDialog</name>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="14"/>
        <source>Select process to attach...</source>
        <translation>Выбрать процесс для присоединения...</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="46"/>
        <source>Processes with same name as currently open file:</source>
        <translation>Процессы с тем же именем как текущий открытый файл:</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="94"/>
        <source>All processes:</source>
        <translation>Все процессы:</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="138"/>
        <source>Quick Filter</source>
        <translation>Быстрая фильтрация</translation>
    </message>
</context>
<context>
    <name>BacktraceWidget</name>
    <message>
        <source>Func Name</source>
        <translation type="vanished">Имя функции</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="16"/>
        <source>Function</source>
        <translation>Функция</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="19"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="20"/>
        <source>Frame Size</source>
        <translation>Размер кадра</translation>
    </message>
</context>
<context>
    <name>Base64EnDecodedWriteDialog</name>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="14"/>
        <source>Base64 Encode/Decode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="24"/>
        <source>String:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="38"/>
        <source>Decode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="48"/>
        <source>Encode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BinClassesModel</name>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="121"/>
        <source>method</source>
        <translation>метод</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="145"/>
        <source>field</source>
        <translation>поле</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="167"/>
        <source>base class</source>
        <translation>базовый класс</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="187"/>
        <source>class</source>
        <translation>класс</translation>
    </message>
</context>
<context>
    <name>BreakpointModel</name>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="64"/>
        <source>HW %1</source>
        <translation>HW %1</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="66"/>
        <source>SW</source>
        <translation>SW</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="101"/>
        <source>Offset</source>
        <translation>Смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="103"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="105"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="109"/>
        <source>Enabled</source>
        <translation>Включено</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="111"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="vanished">Права доступа</translation>
    </message>
    <message>
        <source>Hardware bp</source>
        <translation type="vanished">Аппаратная точка останова</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="107"/>
        <source>Tracing</source>
        <translation>Трассировка</translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="vanished">Активный</translation>
    </message>
</context>
<context>
    <name>BreakpointWidget</name>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="58"/>
        <source>Add new breakpoint</source>
        <translation>Добавление точки останова</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="65"/>
        <location filename="../../widgets/BreakpointWidget.cpp" line="196"/>
        <source>Delete breakpoint</source>
        <translation>Удалить точку останова</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="72"/>
        <source>Delete all breakpoints</source>
        <translation>Удалить все точки останова</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="202"/>
        <source>Toggle breakpoint</source>
        <translation>Переключить состояние точки останова</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="208"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
</context>
<context>
    <name>BreakpointsDialog</name>
    <message>
        <source>Add breakpoints</source>
        <translation type="vanished">Добавление точки останова</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="14"/>
        <source>Add/Edit breakpoint</source>
        <translation>Добавление/редактирование точки останова</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="22"/>
        <source>Position</source>
        <translation>Позиция</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="59"/>
        <source>Condition</source>
        <translation>Условие</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="91"/>
        <source>?v $.rax-0x6  # break when rax is 6</source>
        <translation>?v $.rax-0x6 # остановиться, когда rax равно 6</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="99"/>
        <source>Module</source>
        <translation>Модуль</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="126"/>
        <source>Type/Options</source>
        <translation>Тип/Опции</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="132"/>
        <source>Enabled</source>
        <translation>Включено</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="142"/>
        <source>Software</source>
        <translation>Софтварный</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="152"/>
        <source>Hardware</source>
        <translation>Аппаратное обеспечение</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="180"/>
        <source>Read</source>
        <translation>Чтение</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="187"/>
        <source>Write</source>
        <translation>Запись</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="194"/>
        <source>Execute</source>
        <translation>Исполнение</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="206"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="217"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="222"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="227"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="232"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="261"/>
        <source>Action</source>
        <translation>Действие</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="267"/>
        <source>Trace</source>
        <translation>Трассировка</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="276"/>
        <source>Command</source>
        <translation>Команда</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="21"/>
        <source>Edit breakpoint</source>
        <translation>Редактирование точки останова</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="23"/>
        <source>New breakpoint</source>
        <translation>Добавление точки останова</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="32"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="32"/>
        <source>Address or expression calculated when creating breakpoint</source>
        <translation>Адрес или выражение, вычисляемое при создании точки останова</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="34"/>
        <source>Named</source>
        <translation>Именованый</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="34"/>
        <source>Expression - stored as expression</source>
        <translation>Выражение - сохранено как выражение</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="35"/>
        <source>Module offset</source>
        <translation>Смещение модуля</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="35"/>
        <source>Offset relative to module</source>
        <translation>Смещение относительно модуля</translation>
    </message>
</context>
<context>
    <name>CallGraphWidget</name>
    <message>
        <location filename="../../widgets/CallGraph.cpp" line="23"/>
        <source>Global Callgraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CallGraph.cpp" line="23"/>
        <source>Callgraph</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClassesModel</name>
    <message>
        <source>method</source>
        <translation type="vanished">method</translation>
    </message>
    <message>
        <source>field</source>
        <translation type="vanished">field</translation>
    </message>
    <message>
        <source>class</source>
        <translation type="vanished">class</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="19"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="21"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="23"/>
        <source>Offset</source>
        <translation>Смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="25"/>
        <source>VTable</source>
        <translation>Таблица виртуальных методов</translation>
    </message>
</context>
<context>
    <name>ClassesWidget</name>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="79"/>
        <source>Source:</source>
        <translation>Исходник:</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="93"/>
        <source>Binary Info (Fixed)</source>
        <translation>Двоичные информация (фиксированная)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="98"/>
        <source>Analysis (Editable)</source>
        <translation>Анализ (редактируемы)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="109"/>
        <source>Seek to VTable</source>
        <translation>Искать таблицу виртуальных методов</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="114"/>
        <source>Edit Method</source>
        <translation>Редактировать метод</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="119"/>
        <source>Add Method</source>
        <translation>Добавить метод</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="124"/>
        <location filename="../../widgets/ClassesWidget.cpp" line="743"/>
        <source>Create new Class</source>
        <translation>Создать новый класс</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="129"/>
        <source>Rename Class</source>
        <translation>Переименовать класс</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="134"/>
        <location filename="../../widgets/ClassesWidget.cpp" line="759"/>
        <source>Delete Class</source>
        <translation>Удалить класс</translation>
    </message>
    <message>
        <source>Flags (Editable)</source>
        <translation type="vanished">Флаги (редактируемые)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="695"/>
        <source>Missing VTable in class</source>
        <translation>В классе отсутствует таблица виртуальных методов</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="696"/>
        <source>The class %1 does not have any VTable!</source>
        <translation>Класс %1 не имеет VTable!</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="743"/>
        <source>Class Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="778"/>
        <source>Class name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Class Name</source>
        <translation type="vanished">Имя класса</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="760"/>
        <source>Are you sure you want to delete the class %1?</source>
        <translation>Вы уверенны что хотите удалить класс %1?</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="777"/>
        <source>Rename Class %1</source>
        <translation>Переименовать класс %1</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="87"/>
        <source>Val:</source>
        <translation>Ярк:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="118"/>
        <source>Sat:</source>
        <translation>Насыщ:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="149"/>
        <source>Hue:</source>
        <translation>Тон:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="184"/>
        <source>Red:</source>
        <translation>Красный:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="215"/>
        <source>Green:</source>
        <translation>Зеленый:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="246"/>
        <source>Blue:</source>
        <translation>Синий:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="281"/>
        <source>Hex:</source>
        <translation>Шестнадцатеричный код:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="288"/>
        <source>\#HHHHHH</source>
        <translation>\#HHHHHH</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="312"/>
        <source>Pick color from screen</source>
        <translation>Выбрать цвет на экране</translation>
    </message>
</context>
<context>
    <name>ColorSchemeFileSaver</name>
    <message>
        <source>Standard themes not found!</source>
        <translation type="vanished">Стандартные темы не найдены!</translation>
    </message>
    <message>
        <source>The radare2 standard themes could not be found! This probably means radare2 is not properly installed. If you think it is open an issue please.</source>
        <translation type="vanished">Стандартные темы radare2 не найдены! Вероятно, radare2 неправильно установлен. Если вы считаете, что он установлен правильно, пожалуйста сообщите об ошибке.</translation>
    </message>
</context>
<context>
    <name>ColorSchemePrefWidget</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Форма</translation>
    </message>
    <message>
        <source>Set Default</source>
        <translation type="vanished">Установить по умолчанию</translation>
    </message>
</context>
<context>
    <name>ColorThemeEditDialog</name>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="26"/>
        <source>Color Theme:</source>
        <translation>Цветовая схема:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="55"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="35"/>
        <source>Disassembly Preview</source>
        <translation>Предварительный просмотр листинга</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="79"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="93"/>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="144"/>
        <source>Unsaved changes</source>
        <translation>Несохраненные изменения</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="94"/>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="145"/>
        <source>Are you sure you want to exit without saving? All changes will be lost.</source>
        <translation>Вы уверены, что хотите выйти без сохранения? Все изменения будут потеряны.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="159"/>
        <source>Theme Editor - &lt;%1&gt;</source>
        <translation>Редактор тем - &lt;%1&gt;</translation>
    </message>
</context>
<context>
    <name>ColorThemeWorker</name>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="45"/>
        <source>Standard themes not found</source>
        <translation>Стандартные темы не найдены</translation>
    </message>
    <message>
        <source>The radare2 standard themes could not be found. Most likely, radare2 is not properly installed.</source>
        <translation type="vanished">Не удалось найти стандартные темы radare2. Скорее всего, radare2 установлен неправильно.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="74"/>
        <location filename="../../common/ColorThemeWorker.cpp" line="202"/>
        <source>Theme &lt;b&gt;%1&lt;/b&gt; does not exist.</source>
        <translation>Тема &lt;b&gt;%1&lt;/b&gt; не существует.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="84"/>
        <source>The file &lt;b&gt;%1&lt;/b&gt; cannot be opened.</source>
        <translation>Файл &lt;b&gt;%1&lt;/b&gt; не может быть открыт.</translation>
    </message>
    <message>
        <source>You can not delete standard radare2 color themes.</source>
        <translation type="vanished">Вы не можете удалить стандартные цветовые темы radare2.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="46"/>
        <source>The Rizin standard themes could not be found in &apos;%1&apos;. Most likely, Rizin is not properly installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="199"/>
        <source>You can not delete standard Rizin color themes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="207"/>
        <source>You have no permission to write to &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>У вас нет разрешения на запись в &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="210"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; can not be opened.</source>
        <translation>Файл &lt;b&gt;%1&lt;/b&gt; не может быть открыт.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="213"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; can not be removed.</source>
        <translation>Файл &lt;b&gt;%1&lt;/b&gt; не может быть удален.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="222"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; does not exist.</source>
        <translation>Файл &lt;b&gt;%1&lt;/b&gt; не существует.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="228"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; could not be opened. Please make sure you have access to it and try again.</source>
        <translation>Файл &lt;b&gt;%1&lt;/b&gt; не может быть открыт. Пожалуйста, убедитесь, что у вас есть доступ к нему и повторите попытку.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="232"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; is not a Cutter color theme</source>
        <translation>Файл &lt;b&gt;%1&lt;/b&gt; не является цветовой темой Cutter</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="237"/>
        <source>A color theme named &lt;b&gt;%1&lt;/b&gt; already exists.</source>
        <translation>Цветовая тема &lt;b&gt;%1&lt;/b&gt; уже существует.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="243"/>
        <source>Error occurred during importing. Please make sure you have an access to the directory &lt;b&gt;%1&lt;/b&gt; and try again.</source>
        <translation>Произошла ошибка во время импорта. Пожалуйста, убедитесь, что у вас есть доступ к каталогу &lt;b&gt;%1&lt;/b&gt; и повторите попытку.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="253"/>
        <source>A color theme named &lt;b&gt;&quot;%1&quot;&lt;/b&gt; already exists.</source>
        <translation>Цветовая тема &lt;b&gt;&quot;%1&quot;&lt;/b&gt; уже существует.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="257"/>
        <source>You can not rename standard Rizin themes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can not rename standard radare2 themes.</source>
        <translation type="vanished">Вы не можете переименовать стандартные темы radare2.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="263"/>
        <source>Something went wrong during renaming. Please make sure you have access to the directory &lt;b&gt;&quot;%1&quot;&lt;/b&gt;.</source>
        <translation>Что-то пошло не так во время переименования. Пожалуйста, убедитесь, что у вас есть доступ к каталогу &lt;b&gt;&quot;%1&quot;&lt;/b&gt;.</translation>
    </message>
</context>
<context>
    <name>ComboQuickFilterView</name>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="32"/>
        <source>Quick Filter</source>
        <translation>Быстрый фильтр</translation>
    </message>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="39"/>
        <source>TextLabel</source>
        <translation>Текстовая метка</translation>
    </message>
</context>
<context>
    <name>CommandTask</name>
    <message>
        <location filename="../../common/CommandTask.h" line="23"/>
        <source>Running Command</source>
        <translation>Выполнение команды</translation>
    </message>
</context>
<context>
    <name>CommentsDialog</name>
    <message>
        <location filename="../../dialogs/CommentsDialog.ui" line="14"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../../dialogs/CommentsDialog.cpp" line="43"/>
        <source>Add Comment at %1</source>
        <translation>Добавить комментарий к %1</translation>
    </message>
    <message>
        <location filename="../../dialogs/CommentsDialog.cpp" line="45"/>
        <source>Edit Comment at %1</source>
        <translation>Изменить комментарий к %1</translation>
    </message>
</context>
<context>
    <name>CommentsModel</name>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="157"/>
        <source>Function/Offset</source>
        <translation>Функция/смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="159"/>
        <location filename="../../widgets/CommentsWidget.cpp" line="170"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="166"/>
        <source>Offset</source>
        <translation>Смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="168"/>
        <source>Function</source>
        <translation>Функция</translation>
    </message>
</context>
<context>
    <name>CommentsWidget</name>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="236"/>
        <source>Horizontal</source>
        <translation>Горизонтально</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="239"/>
        <source>Comments</source>
        <translation>Комментарии</translation>
    </message>
    <message>
        <source>Horizontal view</source>
        <translation type="vanished">Горизонтальное отображение</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="237"/>
        <source>Vertical</source>
        <translation>По вертикали</translation>
    </message>
    <message>
        <source>Vertical view</source>
        <translation type="vanished">Вертикальное отображение</translation>
    </message>
</context>
<context>
    <name>Configuration</name>
    <message>
        <location filename="../../common/Configuration.cpp" line="138"/>
        <source>Critical!</source>
        <translation>Критично!</translation>
    </message>
    <message>
        <location filename="../../common/Configuration.cpp" line="139"/>
        <source>!!! Settings are not writable! Make sure you have a write access to &quot;%1&quot;</source>
        <translation>!!! Сохранение настроек недоступно! Убедитесь, что у вас есть доступ на запись &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>ConsoleWidget</name>
    <message>
        <source>R2 Console</source>
        <translation type="vanished">R2 консоль</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="93"/>
        <source>Rizin Console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="98"/>
        <source>Debugee Input</source>
        <translation>Ввод отладки</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="115"/>
        <source> Type &quot;?&quot; for help</source>
        <translation> Введите &quot;?&quot; для получения помощи</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="137"/>
        <source> Enter input for the debugee</source>
        <translation> Введите текст, который будет передан в отлаживаемую программу</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="153"/>
        <source>Execute command</source>
        <translation>Выполнить команду</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="159"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.cpp" line="75"/>
        <source>Clear Output</source>
        <translation>Очистить вывод</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.cpp" line="84"/>
        <source>Wrap Lines</source>
        <translation>Переносить строки</translation>
    </message>
</context>
<context>
    <name>CutterCore</name>
    <message>
        <location filename="../../core/Cutter.cpp" line="1717"/>
        <source>Starting native debug...</source>
        <translation>Запуск нативной отладки...</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1760"/>
        <source>Starting emulation...</source>
        <translation>Запуск эмуляции...</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1814"/>
        <source>Connecting to: </source>
        <translation>Подключение к: </translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1853"/>
        <source>Attaching to process (</source>
        <translation>Присоединиться к процессу (</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2209"/>
        <source>Creating debug tracepoint...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2245"/>
        <source>Stopping debug session...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2279"/>
        <source>Breakpoint error</source>
        <translation>Ошибка точки останова</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2279"/>
        <source>Failed to create breakpoint</source>
        <translation>Не удалось создать точку останова</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="3144"/>
        <source>Unknown (%1)</source>
        <translation>Неизвестный (%1)</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="3396"/>
        <source>Primitive</source>
        <translation>Простой</translation>
    </message>
</context>
<context>
    <name>CutterGraphView</name>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="30"/>
        <location filename="../../widgets/CutterGraphView.cpp" line="442"/>
        <source>Export Graph</source>
        <translation type="unfinished">Экспорт графа</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="42"/>
        <source>Layout</source>
        <translation type="unfinished">Схема расположения</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="43"/>
        <source>Horizontal</source>
        <translation type="unfinished">Горизонтально</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="47"/>
        <source>Grid narrow</source>
        <translation type="unfinished">Узкая сетка</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="48"/>
        <source>Grid medium</source>
        <translation type="unfinished">Средняя сетка</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="49"/>
        <source>Grid wide</source>
        <translation type="unfinished">Широкая сетка</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="63"/>
        <source>Graphviz polyline</source>
        <translation type="unfinished">Полилиния graphviz</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="64"/>
        <source>Graphviz ortho</source>
        <translation type="unfinished">Graphviz орто</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="65"/>
        <source>Graphviz sfdp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="66"/>
        <source>Graphviz neato</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="67"/>
        <source>Graphviz twopi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="68"/>
        <source>Graphviz circo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="409"/>
        <source>PNG (*.png)</source>
        <translation type="unfinished">PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="410"/>
        <source>JPEG (*.jpg)</source>
        <translation type="unfinished">JPEG (*.jpg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="411"/>
        <source>SVG (*.svg)</source>
        <translation type="unfinished">SVG (*.svg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="417"/>
        <source>Graphviz dot (*.dot)</source>
        <translation type="unfinished">Graphviz dot (*.dot)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="418"/>
        <source>Graph Modelling Language (*.gml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="420"/>
        <source>RZ JSON (*.json)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="421"/>
        <source>SDB key-value (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="427"/>
        <source>Graphviz json (*.json)</source>
        <translation type="unfinished">Graphviz json (*.json)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="429"/>
        <source>Graphviz gif (*.gif)</source>
        <translation type="unfinished">Graphviz gif (*.gif)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="431"/>
        <source>Graphviz png (*.png)</source>
        <translation type="unfinished">Graphviz png (*.png)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="433"/>
        <source>Graphviz jpg (*.jpg)</source>
        <translation type="unfinished">Graphviz jpg (*.jpg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="435"/>
        <source>Graphviz PostScript (*.ps)</source>
        <translation type="unfinished">Graphviz PostScript (*.ps)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="437"/>
        <source>Graphviz svg (*.svg)</source>
        <translation type="unfinished">Graphviz svg (*.svg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="460"/>
        <source>Graph Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="461"/>
        <source>Do you really want to export %1 x %2 = %3 pixel bitmap image? Consider using different format.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CutterSeekable</name>
    <message>
        <location filename="../../widgets/AddressableDockWidget.cpp" line="45"/>
        <source> (unsynced)</source>
        <translation> (не синхронизировано)</translation>
    </message>
    <message>
        <location filename="../../common/CutterSeekable.cpp" line="73"/>
        <source>More than one (%1) references here. Weird behaviour expected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CutterTreeWidget</name>
    <message>
        <location filename="../../widgets/CutterTreeWidget.cpp" line="19"/>
        <source>%1 Items</source>
        <translation>%1 элементов</translation>
    </message>
</context>
<context>
    <name>Dashboard</name>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="116"/>
        <source>OVERVIEW</source>
        <translation>ОБЗОР</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="140"/>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="178"/>
        <source>File:</source>
        <translation>Файл:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="188"/>
        <location filename="../../widgets/Dashboard.ui" line="217"/>
        <location filename="../../widgets/Dashboard.ui" line="246"/>
        <location filename="../../widgets/Dashboard.ui" line="291"/>
        <location filename="../../widgets/Dashboard.ui" line="320"/>
        <location filename="../../widgets/Dashboard.ui" line="349"/>
        <location filename="../../widgets/Dashboard.ui" line="378"/>
        <location filename="../../widgets/Dashboard.ui" line="391"/>
        <location filename="../../widgets/Dashboard.ui" line="436"/>
        <location filename="../../widgets/Dashboard.ui" line="465"/>
        <location filename="../../widgets/Dashboard.ui" line="494"/>
        <location filename="../../widgets/Dashboard.ui" line="523"/>
        <location filename="../../widgets/Dashboard.ui" line="552"/>
        <location filename="../../widgets/Dashboard.ui" line="581"/>
        <location filename="../../widgets/Dashboard.ui" line="610"/>
        <location filename="../../widgets/Dashboard.ui" line="655"/>
        <location filename="../../widgets/Dashboard.ui" line="668"/>
        <location filename="../../widgets/Dashboard.ui" line="710"/>
        <location filename="../../widgets/Dashboard.ui" line="739"/>
        <location filename="../../widgets/Dashboard.ui" line="768"/>
        <location filename="../../widgets/Dashboard.ui" line="797"/>
        <location filename="../../widgets/Dashboard.ui" line="826"/>
        <location filename="../../widgets/Dashboard.ui" line="855"/>
        <location filename="../../widgets/Dashboard.ui" line="884"/>
        <location filename="../../widgets/Dashboard.ui" line="913"/>
        <location filename="../../widgets/Dashboard.ui" line="942"/>
        <source>--</source>
        <translation>--</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="207"/>
        <source>Format:</source>
        <translation>Формат:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="236"/>
        <source>Mode:</source>
        <translation>Режим:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="265"/>
        <source>Size:</source>
        <translation>Размер:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="281"/>
        <source>Type:</source>
        <translation>Тип:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="310"/>
        <source>Class:</source>
        <translation>Класс:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="339"/>
        <source>Language:</source>
        <translation>Язык:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="368"/>
        <source>Bits:</source>
        <translation>Биты:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="426"/>
        <source>FD:</source>
        <translation>FD:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="455"/>
        <source>Base addr:</source>
        <translation>Базовый адрес:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="484"/>
        <source>Virtual addr:</source>
        <translation>Виртуальный адрес:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="513"/>
        <source>Canary:</source>
        <translation>Canary:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="542"/>
        <source>Crypto:</source>
        <translation>Crypto:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="571"/>
        <source>NX bit:</source>
        <translation>NX bit:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="600"/>
        <source>PIC:</source>
        <translation>PIC:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="629"/>
        <source>Static:</source>
        <translation>Static:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="645"/>
        <source>Relro:</source>
        <translation>Relro:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="700"/>
        <source>Architecture:</source>
        <translation>Архитектура:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="729"/>
        <source>Machine:</source>
        <translation>Устройство:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="758"/>
        <source>OS:</source>
        <translation>ОС:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="787"/>
        <source>Subsystem:</source>
        <translation>Подсистема:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="816"/>
        <source>Stripped:</source>
        <translation>Пустой:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="845"/>
        <source>Relocs:</source>
        <translation>Relocs:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="874"/>
        <source>Endianness:</source>
        <translation>Порядок байтов:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="903"/>
        <source>Compiled:</source>
        <translation>Скомпилирован:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="932"/>
        <source>Compiler:</source>
        <translation>Компилятор:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="993"/>
        <source>Certificates</source>
        <translation>Сертификаты</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1006"/>
        <source>Version info</source>
        <translation>Информация о версии</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1034"/>
        <source>Hashes</source>
        <translation>Хэши</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1051"/>
        <source>Libraries</source>
        <translation>Библиотеки</translation>
    </message>
    <message>
        <source>MD5:</source>
        <translation type="vanished">MD5:</translation>
    </message>
    <message>
        <source>SHA1:</source>
        <translation type="vanished">SHA1:</translation>
    </message>
    <message>
        <source>Entropy:</source>
        <translation type="vanished">Энтропия:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1083"/>
        <source>Analysis info</source>
        <translation>Информация об анализе</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1107"/>
        <source>Functions:</source>
        <translation>Функции:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1133"/>
        <source>X-Refs:</source>
        <translation>Перекрестные ссылки:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1159"/>
        <source>Calls:</source>
        <translation>Вызовы:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1185"/>
        <source>Strings:</source>
        <translation>Строки:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1211"/>
        <source>Symbols:</source>
        <translation>Символы:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1237"/>
        <source>Imports:</source>
        <translation>Импорт:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1263"/>
        <source>Analysis coverage:</source>
        <translation>Покрытие анализом:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1289"/>
        <source>Code size:</source>
        <translation>Размер кода:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1315"/>
        <source>Coverage percent:</source>
        <translation>Процент покрытия:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="124"/>
        <source>&lt;b&gt;Entropy:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="236"/>
        <location filename="../../widgets/Dashboard.cpp" line="256"/>
        <source>N/A</source>
        <translation>Н/Д</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="251"/>
        <source>True</source>
        <translation>True</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="253"/>
        <source>False</source>
        <translation>False</translation>
    </message>
</context>
<context>
    <name>DebugActions</name>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="56"/>
        <source>Start debug</source>
        <translation>Начать отладку</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="37"/>
        <source>Start emulation</source>
        <translation>Начать эмуляцию</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="38"/>
        <source>Attach to process</source>
        <translation>Присоединиться к процессу</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="40"/>
        <source>Stop debug</source>
        <translation>Остановить отладку</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="41"/>
        <source>Stop emulation</source>
        <translation>Остановить эмуляцию</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="55"/>
        <source>Restart program</source>
        <translation>Перезапустить программу</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="42"/>
        <source>Restart emulation</source>
        <translation>Перезапустить эмуляцию</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="54"/>
        <source>Continue</source>
        <translation>Продолжить</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="43"/>
        <source>Continue until main</source>
        <translation>Продолжить выполнение до достижения main</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="39"/>
        <source>Connect to a remote debugger</source>
        <translation>Подключиться к удалённому отладчику</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="44"/>
        <source>Continue until call</source>
        <translation>Продолжить выполнение до достижения вызова</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="45"/>
        <source>Continue until syscall</source>
        <translation>Продолжить выполнение до достижения syscall</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="46"/>
        <source>Continue backwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="47"/>
        <source>Step</source>
        <translation>Шаг</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="48"/>
        <source>Step over</source>
        <translation>Перешагнуть</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="49"/>
        <source>Step out</source>
        <translation>Шаг с выходом</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="50"/>
        <source>Step backwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="51"/>
        <source>Start trace session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="52"/>
        <source>Stop trace session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="53"/>
        <source>Suspend the process</source>
        <translation>Приостановить процесс</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="147"/>
        <source>Debugged process exited (</source>
        <translation>Отлаживаемый процесс завершился (</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="267"/>
        <source>Debug is currently in beta.
</source>
        <translation>Отладка в настоящее время находится в бета-версии.
</translation>
    </message>
    <message>
        <source>If you encounter any problems or have suggestions, please submit an issue to https://github.com/radareorg/cutter/issues</source>
        <translation type="vanished">Если у вас возникли проблемы или у вас есть предложения, пожалуйста, зарегистрируйте issue на https://github.com/radareorg/cutter/issues</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="295"/>
        <source>Error connecting.</source>
        <translation>Ошибка подключения.</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="377"/>
        <source>File &apos;%1&apos; does not have executable permissions.</source>
        <translation>Файл &apos;%1&apos; не имеет разрешения на исполнение.</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="348"/>
        <source>Error attaching. No process selected!</source>
        <translation>Ошибка присоединения. Процесс не выбран!</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="268"/>
        <source>If you encounter any problems or have suggestions, please submit an issue to https://github.com/rizinorg/cutter/issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="282"/>
        <location filename="../../widgets/DebugActions.cpp" line="357"/>
        <source>Detach from process</source>
        <translation>Отсоединиться от процесса</translation>
    </message>
</context>
<context>
    <name>DebugOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="14"/>
        <source>Debug</source>
        <translation>Отладка</translation>
    </message>
    <message>
        <source>Debug Plugin:</source>
        <translation type="vanished">Отладка плагина:</translation>
    </message>
    <message>
        <source>Program Arguments:</source>
        <translation type="vanished">Аргументы программы:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="30"/>
        <source>Debug plugin:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="42"/>
        <source>ESIL options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="69"/>
        <source>ESIL stack address:</source>
        <translation>ESIL адрес стека:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="79"/>
        <source>Hide text when zooming out and it is smaller than the given value. Higher values can increase Performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="82"/>
        <source>ESIL stack size:</source>
        <translation>ESIL размер стека:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="100"/>
        <source>Trace options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="109"/>
        <source>Trace each step during continue in a trace session (dbg.trace_continue)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="112"/>
        <source>Disabling this option means that stepping back after continue will return to the previous PC. Significantly improves performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="54"/>
        <source>Break esil execution when instruction is invalid (esil.breakoninvalid)</source>
        <translation>Прервать выполнение Esil, когда инструкция будет неверной (esil.breakoninvalid)</translation>
    </message>
</context>
<context>
    <name>DebugToolbar</name>
    <message>
        <source>Start debug</source>
        <translation type="vanished">Начать отладку</translation>
    </message>
    <message>
        <source>Start emulation</source>
        <translation type="vanished">Начать эмуляцию</translation>
    </message>
    <message>
        <source>Attach to process</source>
        <translation type="vanished">Присоединиться к процессу</translation>
    </message>
    <message>
        <source>Stop debug</source>
        <translation type="vanished">Остановить отладку</translation>
    </message>
    <message>
        <source>Stop emulation</source>
        <translation type="vanished">Остановить эмуляцию</translation>
    </message>
    <message>
        <source>Restart program</source>
        <translation type="vanished">Перезапустить программу</translation>
    </message>
    <message>
        <source>Restart emulation</source>
        <translation type="vanished">Перезапустить эмуляцию</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">Продолжить выполнение</translation>
    </message>
    <message>
        <source>Continue until main</source>
        <translation type="vanished">Продолжить выполнение до достижения main</translation>
    </message>
    <message>
        <source>Continue until call</source>
        <translation type="vanished">Продолжить выполнение до достижения вызова</translation>
    </message>
    <message>
        <source>Continue until syscall</source>
        <translation type="vanished">Продолжить выполнение до достижения syscall</translation>
    </message>
    <message>
        <source>Step</source>
        <translation type="vanished">Шаг</translation>
    </message>
    <message>
        <source>Step over</source>
        <translation type="vanished">Шаг с обходом</translation>
    </message>
    <message>
        <source>Step out</source>
        <translation type="vanished">Шаг с выходом</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; does not have executable permissions.</source>
        <translation type="vanished">Файл &apos;%1&apos; не имеет прав на выполнение.</translation>
    </message>
    <message>
        <source>Error attaching. No process selected!</source>
        <translation type="vanished">Ошибка присоединения. Процесс не выбран!</translation>
    </message>
    <message>
        <source>Detach from process</source>
        <translation type="vanished">Отсоединиться от процесса</translation>
    </message>
</context>
<context>
    <name>DecompilerContextMenu</name>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="26"/>
        <source>Copy</source>
        <translation type="unfinished">Копировать</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="27"/>
        <source>Copy instruction address (&lt;address&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="28"/>
        <source>Copy address of [flag] (&lt;address&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="29"/>
        <source>Show in</source>
        <translation type="unfinished">Показать в</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="30"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="161"/>
        <source>Add Comment</source>
        <translation type="unfinished">Добавить комментарий</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="31"/>
        <source>Delete comment</source>
        <translation type="unfinished">Удалить комментарий</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="32"/>
        <source>Rename function at cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="33"/>
        <source>Delete &lt;name&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="34"/>
        <source>Edit variable &lt;name of variable&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="35"/>
        <source>Show X-Refs</source>
        <translation type="unfinished">Показать перекрестные ссылки</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="36"/>
        <source>Add/remove breakpoint</source>
        <translation type="unfinished">Добавить/Удалить точки останова</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="37"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="192"/>
        <source>Advanced breakpoint</source>
        <translation type="unfinished">Расширенное создание точки останова</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="39"/>
        <source>Continue until line</source>
        <translation type="unfinished">Продолжить выполнение до достижения строки</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="40"/>
        <source>Set PC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="164"/>
        <source>Edit Comment</source>
        <translation type="unfinished">Редактировать комментарий</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="180"/>
        <source>Add breakpoint</source>
        <translation type="unfinished">Добавление точки останова</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="182"/>
        <source>Remove breakpoint</source>
        <translation type="unfinished">Удаление точки останова</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="184"/>
        <source>Remove all breakpoints in line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="191"/>
        <source>Edit breakpoint</source>
        <translation type="unfinished">Редактирование точки останова</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="195"/>
        <source>Set %1 here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="207"/>
        <source>Rename function %1</source>
        <translation type="unfinished">Переименовать функцию %1</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="212"/>
        <source>Rename %1</source>
        <translation type="unfinished">Переименовать %1</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="213"/>
        <source>Remove %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="216"/>
        <source>Add name to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="221"/>
        <source>Copy instruction address (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="227"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="232"/>
        <source>Copy address of %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="236"/>
        <source>Copy address (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="252"/>
        <source>Edit variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="254"/>
        <source>Rename variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="316"/>
        <source>Can&apos;t rename this variable.&lt;br&gt;Only local variables defined in disassembly can be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="335"/>
        <source>Can&apos;t edit this variable.&lt;br&gt;Only local variables defined in disassembly can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="407"/>
        <source>Define this function at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="408"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="415"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="414"/>
        <source>Rename function %2</source>
        <translation type="unfinished">Переименовать функцию %2</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="424"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="449"/>
        <source>Rename %2</source>
        <translation type="unfinished">Переименовать %2</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="425"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="432"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="450"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="432"/>
        <source>Add name to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="443"/>
        <source>Rename local variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="444"/>
        <source>Can&apos;t rename this variable. Only local variables defined in disassembly can be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="468"/>
        <source>Edit local variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="469"/>
        <source>Can&apos;t edit this variable. Only local variables defined in disassembly can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="535"/>
        <source>Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="545"/>
        <source>Debug</source>
        <translation type="unfinished">Отладка</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="575"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="577"/>
        <source>Show %1 in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="582"/>
        <source>%1 (%2)</source>
        <translation type="unfinished">%1 (%2)</translation>
    </message>
</context>
<context>
    <name>DecompilerWidget</name>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="14"/>
        <location filename="../../widgets/DecompilerWidget.cpp" line="452"/>
        <source>Decompiler</source>
        <translation>Декомпилятор</translation>
    </message>
    <message>
        <source>Auto Refresh</source>
        <translation type="vanished">Автообновление</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Обновить</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="53"/>
        <source>Decompiling...</source>
        <translation>Декомпиляция...</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="75"/>
        <source>Decompiler:</source>
        <translation>Декомпилятор:</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="33"/>
        <source>Choose an offset and refresh to get decompiled code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="69"/>
        <source>No Decompiler available.</source>
        <translation>Декомпилятор недоступен.</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="262"/>
        <source>No function found at this offset. Seek to a function or define one in order to decompile it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Отмена</translation>
    </message>
    <message>
        <source>Click Refresh to generate Decompiler from current offset.</source>
        <translation type="vanished">Нажмите обновить для генерации декомпилированного кода.</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="309"/>
        <source>Cannot decompile at this address (Not a function?)</source>
        <translation>Невозможно декомпилировать по этому адресу (не функция?)</translation>
    </message>
</context>
<context>
    <name>DisassemblerGraphView</name>
    <message>
        <source>Export Graph</source>
        <translation type="vanished">Экспорт графа</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Синхронизация/Десинхронизация смещения</translation>
    </message>
    <message>
        <source>Grid narrow</source>
        <translation type="vanished">Узкая сетка</translation>
    </message>
    <message>
        <source>Grid medium</source>
        <translation type="vanished">Средняя сетка</translation>
    </message>
    <message>
        <source>Grid wide</source>
        <translation type="vanished">Широкая сетка</translation>
    </message>
    <message>
        <source>Graphviz polyline</source>
        <translation type="vanished">Полилиния graphviz</translation>
    </message>
    <message>
        <source>Graphviz polyline LR</source>
        <translation type="vanished">Полилиния Graphviz LR</translation>
    </message>
    <message>
        <source>Graphviz ortho</source>
        <translation type="vanished">Graphviz орто</translation>
    </message>
    <message>
        <source>Graphviz ortho LR</source>
        <translation type="vanished">Graphviz орто LR</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation type="vanished">Схема расположения</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="91"/>
        <source>Highlight block</source>
        <translation>Подсветить блок</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="109"/>
        <source>Unhighlight block</source>
        <translation>Убрать подсветку блока</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="119"/>
        <source>Highlight instruction</source>
        <translation>Подсветить инструкцию</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="123"/>
        <source>Unhighlight instruction</source>
        <translation>Отменить подсветку инструкции</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="199"/>
        <source>No function detected. Cannot display graph.</source>
        <translation>Функция не найдена. Не удается отобразить график.</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="214"/>
        <source>Graph</source>
        <translation>Граф</translation>
    </message>
    <message>
        <source>Graphviz dot (*.dot)</source>
        <translation type="vanished">Graphviz dot (*.dot)</translation>
    </message>
    <message>
        <source>Graphviz json (*.json)</source>
        <translation type="vanished">Graphviz json (*.json)</translation>
    </message>
    <message>
        <source>Graphviz gif (*.gif)</source>
        <translation type="vanished">Graphviz gif (*.gif)</translation>
    </message>
    <message>
        <source>Graphviz png (*.png)</source>
        <translation type="vanished">Graphviz png (*.png)</translation>
    </message>
    <message>
        <source>Graphviz jpg (*.jpg)</source>
        <translation type="vanished">Graphviz jpg (*.jpg)</translation>
    </message>
    <message>
        <source>Graphviz PostScript (*.ps)</source>
        <translation type="vanished">Graphviz PostScript (*.ps)</translation>
    </message>
    <message>
        <source>Graphviz svg (*.svg)</source>
        <translation type="vanished">Graphviz svg (*.svg)</translation>
    </message>
    <message>
        <source>Graphiz dot (*.dot)</source>
        <translation type="vanished">Graphiz dot (*.dot)</translation>
    </message>
    <message>
        <source>GIF (*.gif)</source>
        <translation type="vanished">GIF (*.gif)</translation>
    </message>
    <message>
        <source>PNG (*.png)</source>
        <translation type="vanished">PNG (*.png)</translation>
    </message>
    <message>
        <source>JPEG (*.jpg)</source>
        <translation type="vanished">JPEG (*.jpg)</translation>
    </message>
    <message>
        <source>PostScript (*.ps)</source>
        <translation type="vanished">PostScript (*.ps)</translation>
    </message>
    <message>
        <source>SVG (*.svg)</source>
        <translation type="vanished">SVG (*.svg)</translation>
    </message>
    <message>
        <source>JSON (*.json)</source>
        <translation type="vanished">JSON (*.json)</translation>
    </message>
</context>
<context>
    <name>DisassemblyContextMenu</name>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="72"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="75"/>
        <source>Copy address</source>
        <translation>Скопировать адрес</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="79"/>
        <source>Show in</source>
        <translation>Показать в</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="84"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="549"/>
        <source>Add Comment</source>
        <translation>Добавить комментарий</translation>
    </message>
    <message>
        <source>Add Flag</source>
        <translation type="vanished">Добавить флаг</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">Переименовать</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="96"/>
        <source>Edit function</source>
        <translation>Изменить функцию</translation>
    </message>
    <message>
        <source>Rename Flag/Fcn/Var Used Here</source>
        <translation type="vanished">Переименовать Флаг/Функцию/Переменную использованную здесь</translation>
    </message>
    <message>
        <source>Re-type function local vars</source>
        <translation type="vanished">Переопределить тип локальных переменных</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="100"/>
        <source>Delete comment</source>
        <translation>Удалить комментарий</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="104"/>
        <source>Delete flag</source>
        <translation>Удалить флаг</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="107"/>
        <source>Undefine function</source>
        <translation>Неопределенная функция</translation>
    </message>
    <message>
        <source>Define function here...</source>
        <translation type="vanished">Определить здесь функцию...</translation>
    </message>
    <message>
        <source>Set to Code</source>
        <translation type="vanished">Отобразить как код</translation>
    </message>
    <message>
        <source>Set as Code</source>
        <translation type="vanished">Отобразить как код</translation>
    </message>
    <message>
        <source>Set as String</source>
        <translation type="vanished">Установить как строку</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="133"/>
        <source>Show X-Refs</source>
        <translation>Показать перекрестные ссылки</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="137"/>
        <source>X-Refs for local variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="141"/>
        <source>Show Options</source>
        <translation>Показать параметры</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="172"/>
        <source>Set Immediate Base to...</source>
        <translation>Отобразить смещение как...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="174"/>
        <source>Binary</source>
        <translation>Двоичная</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="178"/>
        <source>Octal</source>
        <translation>Восьмеричная</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="182"/>
        <source>Decimal</source>
        <translation>Десятичная</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="186"/>
        <source>Hexadecimal</source>
        <translation>Шестнадцатеричная</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="190"/>
        <source>Network Port</source>
        <translation>Сетевой порт</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="194"/>
        <source>IP Address</source>
        <translation>IP адрес</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="198"/>
        <source>Syscall</source>
        <translation>Syscall</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="202"/>
        <source>String</source>
        <translation>Строка</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="209"/>
        <source>Set current bits to...</source>
        <translation>Установить разрядность инструкций...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="424"/>
        <source>Rename local &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="429"/>
        <source>Rename flag &quot;%1&quot; (used here)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="826"/>
        <source>New function at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="827"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="841"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="840"/>
        <source>Rename function %2</source>
        <translation type="unfinished">Переименовать функцию %2</translation>
    </message>
    <message>
        <source>Set to Data...</source>
        <translation type="vanished">Отобразить данные как...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="111"/>
        <source>Define function here</source>
        <translation>Определить здесь функцию</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="121"/>
        <source>Structure offset</source>
        <translation>Смещение структуры</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="125"/>
        <source>Link Type to Address</source>
        <translation>Тип ссылки на адрес</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="226"/>
        <source>Set as...</source>
        <translation>Установить как...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="228"/>
        <source>Code</source>
        <translation>Код</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="232"/>
        <source>String...</source>
        <translation>Строка...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="234"/>
        <source>Auto-detect</source>
        <translation>Автоопределение</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="236"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Adanced</source>
        <translation type="vanished">Дополнительно</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="250"/>
        <source>Data...</source>
        <translation>Данные...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="252"/>
        <source>Byte</source>
        <translation>Byte</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="256"/>
        <source>Word</source>
        <translation>Слово</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="260"/>
        <source>Dword</source>
        <translation>Dword</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="264"/>
        <source>Qword</source>
        <translation>Qword</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="279"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="281"/>
        <source>Instruction</source>
        <translation>Инструкция</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="285"/>
        <source>Nop Instruction</source>
        <translation>Nop инструкция</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="289"/>
        <source>Bytes</source>
        <translation>Байтов</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="292"/>
        <source>Reverse Jump</source>
        <translation>Обратный прыжок</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="298"/>
        <source>Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="310"/>
        <source>Debug</source>
        <translation>Отладка</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="300"/>
        <source>Add/remove breakpoint</source>
        <translation>Добавить/Удалить точки останова</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="88"/>
        <source>Rename or add flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="92"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="875"/>
        <source>Re-type Local Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="238"/>
        <source>Advanced</source>
        <translation type="unfinished">Дополнительно</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="303"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="587"/>
        <source>Advanced breakpoint</source>
        <translation>Расширенное создание точки останова</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="312"/>
        <source>Continue until line</source>
        <translation>Продолжить выполнение до достижения строки</translation>
    </message>
    <message>
        <source>%1 (used here)</source>
        <translation type="vanished">%1 (используется здесь)</translation>
    </message>
    <message>
        <source>%1 (%2)</source>
        <translation type="vanished">%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="552"/>
        <source>Edit Comment</source>
        <translation>Редактировать комментарий</translation>
    </message>
    <message>
        <source>Rename function &quot;%1&quot;</source>
        <translation type="vanished">Переименовать функцию &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Rename flag &quot;%1&quot;</source>
        <translation type="vanished">Переименовать флаг &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="568"/>
        <source>Edit function &quot;%1&quot;</source>
        <translation>Переименовать функцию &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="414"/>
        <source>Add flag at %1 (used here)</source>
        <translation>Добавить флаг по адресу %1 (используется здесь)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="419"/>
        <source>Rename &quot;%1&quot;</source>
        <translation>Переименовать &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Rename &quot;%1&quot; (used here)</source>
        <translation type="vanished">Переименовать &quot;%1&quot; (используемую здесь)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="585"/>
        <source>Remove breakpoint</source>
        <translation>Удаление точки останова</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="585"/>
        <source>Add breakpoint</source>
        <translation>Добавление точки останова</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="586"/>
        <source>Edit breakpoint</source>
        <translation>Редактирование точки останова</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="601"/>
        <source>X-Refs for %1</source>
        <translation type="unfinished">Перекрестные ссылки для %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="701"/>
        <source>Edit Instruction at %1</source>
        <translation>Изменить инструкцию по адресу %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="755"/>
        <source>Edit Bytes at %1</source>
        <translation>Изменить байты по адресу %1</translation>
    </message>
    <message>
        <source>Write error</source>
        <translation type="vanished">Ошибка записи</translation>
    </message>
    <message>
        <source>Unable to complete write operation. Consider opening in write mode. 

WARNING: In write mode any changes will be commited to disk</source>
        <translation type="vanished">Не удалось завершить операцию записи. Рассмотрите возможность открытия в режиме записи.

ПРЕДУПРЕЖДЕНИЕ. В режиме записи любые изменения будут записаны на диск</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Reopen in write mode and try again</source>
        <translation type="vanished">Откройте в режиме записи и попробуйте снова</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="938"/>
        <source>Wrong address</source>
        <translation>Неправильный адрес</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="939"/>
        <source>Can&apos;t edit string at this address</source>
        <translation>Невозможно отредактировать строку по этому адресу</translation>
    </message>
    <message>
        <source>Add Comment at %1</source>
        <translation type="vanished">Добавление комментария по адресу %1</translation>
    </message>
    <message>
        <source>Edit Comment at %1</source>
        <translation type="vanished">Изменить комментарий по адресу %1</translation>
    </message>
    <message>
        <source>Analyze function at %1</source>
        <translation type="vanished">Анализ функции по адресу %1</translation>
    </message>
    <message>
        <source>Function name</source>
        <translation type="vanished">Имя функции</translation>
    </message>
    <message>
        <source>Rename function %1</source>
        <translation type="vanished">Переименовать функцию %1</translation>
    </message>
    <message>
        <source>Rename flag %1</source>
        <translation type="vanished">Переименовать флаг %1</translation>
    </message>
    <message>
        <source>Add flag at %1</source>
        <translation type="vanished">Добавить флаг по адресу %1</translation>
    </message>
    <message>
        <source>Rename %1</source>
        <translation type="vanished">Переименовать %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="876"/>
        <source>You must be in a function to define variable types.</source>
        <translation>Вы должны быть в функции для определения типов переменных.</translation>
    </message>
    <message>
        <source>Set Variable Types for Function: %1</source>
        <translation type="vanished">Установить тип переменной для функции: %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="1020"/>
        <source>Edit function %1</source>
        <translation>Изменить функцию %1</translation>
    </message>
</context>
<context>
    <name>DisassemblyWidget</name>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="670"/>
        <source>More than one (%1) references here. Weird behaviour expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="676"/>
        <source>offsetFrom (%1) differs from refs.at(0).from (%(2))</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="721"/>
        <source>Disassembly</source>
        <translation>Дизассемблированный код</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Синхронизация/десинхронизация смещения</translation>
    </message>
</context>
<context>
    <name>DuplicateFromOffsetDialog</name>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="20"/>
        <source>Duplicate from offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="28"/>
        <source>Offset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="49"/>
        <source>N bytes:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditFunctionDialog</name>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="14"/>
        <source>Edit Function</source>
        <translation>Изменить функцию</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="43"/>
        <source>Name of function</source>
        <translation>Имя функции</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="53"/>
        <source>Start address</source>
        <translation>Начальный адрес</translation>
    </message>
    <message>
        <source>End address</source>
        <translation type="vanished">Конечный адрес</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="63"/>
        <source>Stack size</source>
        <translation>Размер стека</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="73"/>
        <source>Calling convention</source>
        <translation>Соглашение о вызове</translation>
    </message>
</context>
<context>
    <name>EditInstructionDialog</name>
    <message>
        <location filename="../../dialogs/EditInstructionDialog.ui" line="14"/>
        <source>Edit Instruction</source>
        <translation>Изменить инструкцию</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditInstructionDialog.ui" line="76"/>
        <source>Unknown Instruction</source>
        <translation>Неизвестная инструкция</translation>
    </message>
</context>
<context>
    <name>EditMethodDialog</name>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="25"/>
        <source>Class:</source>
        <translation>Класс:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="32"/>
        <source>Name:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="42"/>
        <source>Address:</source>
        <translation>Адрес:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="52"/>
        <source>Virtual:</source>
        <translation>Виртуальный:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="66"/>
        <source>Offset in VTable:</source>
        <translation>Смещение в таблице виртуальных методов:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.cpp" line="152"/>
        <source>Create Method</source>
        <translation>Создать метод</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.cpp" line="167"/>
        <source>Edit Method</source>
        <translation>Редактировать метод</translation>
    </message>
</context>
<context>
    <name>EditStringDialog</name>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="23"/>
        <source>Edit string</source>
        <translation>Редактировать строку</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="66"/>
        <source>Address:</source>
        <translation>Адрес:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="52"/>
        <source>Size:</source>
        <translation>Размер:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="59"/>
        <source>Type:</source>
        <translation>Тип:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="99"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Отмена</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
</context>
<context>
    <name>EditVariablesDialog</name>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="22"/>
        <source>Modify:</source>
        <translation>Изменить:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="35"/>
        <source>Name:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="42"/>
        <source>Type:</source>
        <translation>Тип:</translation>
    </message>
    <message>
        <source>Set Variable Types for Function: %1</source>
        <translation type="vanished">Установка типов переменных для функции: %1</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.cpp" line="19"/>
        <source>Edit Variables in Function: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportsModel</name>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="60"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="62"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="64"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="66"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="68"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>ExportsWidget</name>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="138"/>
        <source>Exports</source>
        <translation>Экспорт</translation>
    </message>
</context>
<context>
    <name>FlagDialog</name>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="14"/>
        <source>Add Flag</source>
        <translation>Добавить флаг</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="20"/>
        <source>Add flag at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="38"/>
        <source>Flag:</source>
        <translation>Флаг:</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="61"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="80"/>
        <source>Size:</source>
        <translation>Размер:</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.cpp" line="24"/>
        <source>Edit flag at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.cpp" line="26"/>
        <source>Add flag at %1</source>
        <translation type="unfinished">Добавить флаг по адресу %1</translation>
    </message>
</context>
<context>
    <name>FlagsModel</name>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="64"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="66"/>
        <source>Offset</source>
        <translation>Смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="68"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="70"/>
        <source>Real Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="72"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>FlagsWidget</name>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="79"/>
        <source>Quick Filter</source>
        <translation>Быстрая фильтрация</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="86"/>
        <source>Flagspace:</source>
        <translation>Flagspace:</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="99"/>
        <source>Rename</source>
        <translation>Переименовать</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="102"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="110"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="113"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="214"/>
        <source>Rename flag %1</source>
        <translation type="unfinished">Переименовать флаг %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="215"/>
        <source>Flag name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="244"/>
        <source>(all)</source>
        <translation>(all)</translation>
    </message>
</context>
<context>
    <name>FunctionModel</name>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="123"/>
        <source>Offset: %1</source>
        <translation>Смещение %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="125"/>
        <source>Size: %1</source>
        <translation>Размер: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="127"/>
        <source>Import: %1</source>
        <translation>Импорт: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="128"/>
        <source>true</source>
        <translation>true</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="128"/>
        <source>false</source>
        <translation>неверно</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="130"/>
        <source>Nargs: %1</source>
        <translation>Nargs: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="132"/>
        <source>Nbbs: %1</source>
        <translation>Nbbs: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="134"/>
        <source>Nlocals: %1</source>
        <translation>Nlocals: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="281"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
    <message>
        <source>Cyclomatic complexity: %1</source>
        <translation type="vanished">Цикломатическая сложность: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="136"/>
        <source>Call type: %1</source>
        <translation>Тип вызова: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="138"/>
        <source>Edges: %1</source>
        <translation>Края: %1</translation>
    </message>
    <message>
        <source>Cost: %1</source>
        <translation type="vanished">Сумма: %1</translation>
    </message>
    <message>
        <source>Calls/OutDegree: %1</source>
        <translation type="vanished">Calls/OutDegree: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="140"/>
        <source>StackFrame: %1</source>
        <translation>Кадр стека: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="142"/>
        <source>Comment: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="221"/>
        <source>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Дизассемблерный предпросмотр&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="226"/>
        <source>&lt;div&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;div&gt;&lt;strong&gt;Summary&lt;/strong&gt;:&lt;br&gt;</source>
        <translation type="vanished">&lt;div&gt;&lt;strong&gt;Отчет&lt;/strong&gt;:&lt;br&gt;</translation>
    </message>
    <message>
        <source>Size:&amp;nbsp;%1,&amp;nbsp;Cyclomatic complexity:&amp;nbsp;%2,&amp;nbsp;Basic blocks:&amp;nbsp;%3</source>
        <translation type="vanished">Размер:&amp;nbsp;%1,&amp;nbsp;Цикломатическая сложность:&amp;nbsp;%2,&amp;nbsp;Основные блоки:&amp;nbsp;%3</translation>
    </message>
    <message>
        <source>&lt;/div&gt;&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="vanished">&lt;/div&gt;&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Дизассемблерный предпросмотр&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="vanished">&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Выделенные&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="257"/>
        <location filename="../../widgets/FunctionsWidget.cpp" line="261"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="263"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="265"/>
        <source>Imp.</source>
        <translation>Imp.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="267"/>
        <source>Offset</source>
        <translation>Смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="269"/>
        <source>Nargs</source>
        <translation>Nargs</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="273"/>
        <source>Nbbs</source>
        <translation>Nbbs</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="271"/>
        <source>Nlocals</source>
        <translation>Nlocals</translation>
    </message>
    <message>
        <source>Cyclo. Comp.</source>
        <translation type="vanished">Цикл. сложн.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="275"/>
        <source>Call type</source>
        <translation>Тип вызова</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="277"/>
        <source>Edges</source>
        <translation>Края</translation>
    </message>
    <message>
        <source>Cost</source>
        <translation type="vanished">Сумма</translation>
    </message>
    <message>
        <source>Calls/OutDeg.</source>
        <translation type="vanished">Calls/OutDeg.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="279"/>
        <source>StackFrame</source>
        <translation>Кадр стека</translation>
    </message>
</context>
<context>
    <name>FunctionsTask</name>
    <message>
        <location filename="../../common/FunctionsTask.h" line="13"/>
        <source>Fetching Functions</source>
        <translation>Извлечение функций</translation>
    </message>
</context>
<context>
    <name>FunctionsWidget</name>
    <message>
        <source>Add comment</source>
        <translation type="vanished">Добавить комментарий</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="442"/>
        <source>Rename</source>
        <translation>Переименовать</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="443"/>
        <source>Undefine</source>
        <translation>Неопределенный</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="447"/>
        <source>Functions</source>
        <translation>Функции</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="549"/>
        <source>Rename function %1</source>
        <translation type="unfinished">Переименовать функцию %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="550"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X-Refs</source>
        <translation type="vanished">Перекрестные ссылки</translation>
    </message>
    <message>
        <source>Cross references</source>
        <translation type="vanished">Перекрестные ссылки</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="444"/>
        <source>Horizontal</source>
        <translation>Горизонтально</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="445"/>
        <source>Vertical</source>
        <translation>Вертикально</translation>
    </message>
</context>
<context>
    <name>GraphOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="14"/>
        <source>Graph</source>
        <translation>Граф</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="22"/>
        <source>Graph Block Options </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="31"/>
        <source>The offset of the first instruction of a graph block is shown in the header of the respective graph block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="34"/>
        <source>Show offset of the first instruction in each graph block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="41"/>
        <source>Show offset for each instruction (graph.offset)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="50"/>
        <source>Maximum Line Length:</source>
        <translation>Максимальная длина линии:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="70"/>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="80"/>
        <source>Hide text when zooming out and it is smaller than the given value. Higher values can increase Performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="73"/>
        <source>Minimum Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="98"/>
        <source>Graph Layout Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="117"/>
        <source>Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="140"/>
        <source>Horizontal</source>
        <translation type="unfinished">Горизонтально</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="176"/>
        <source>Block spacing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="183"/>
        <source>Edge spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="193"/>
        <source>Bitmap Export Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="202"/>
        <source>Export Transparent Bitmap Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="211"/>
        <source>Graph Bitmap Export Scale: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="218"/>
        <source>%</source>
        <translation type="unfinished">%</translation>
    </message>
    <message>
        <source>Show offsets (graph.offset)</source>
        <translation type="vanished">Показывать смещения (graph.offset)</translation>
    </message>
    <message>
        <source>Show offsets (graph.offset) </source>
        <translation type="vanished">Показать смещения (graph.offset) </translation>
    </message>
</context>
<context>
    <name>HeadersModel</name>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="55"/>
        <source>Offset</source>
        <translation>Смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="57"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="59"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="61"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>HeadersWidget</name>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="120"/>
        <source>Headers</source>
        <translation>Заголовки</translation>
    </message>
</context>
<context>
    <name>HexWidget</name>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="67"/>
        <source>Hexadecimal</source>
        <translation>Шестнадцатеричное</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="68"/>
        <source>Octal</source>
        <translation>Восьмеричная</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="69"/>
        <source>Decimal</source>
        <translation>Десятичное</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="70"/>
        <source>Signed decimal</source>
        <translation>Знаковое десятичное</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="71"/>
        <source>Float</source>
        <translation>С плавающей точкой</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="85"/>
        <source>Bytes per row</source>
        <translation>Байтов в строке</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="95"/>
        <source>Power of 2</source>
        <translation>Степень двойки</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="102"/>
        <source>Big Endian</source>
        <translation>Big Endian</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="107"/>
        <source>Bytes as pairs</source>
        <translation>Байты в виде пар</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="111"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="117"/>
        <source>Copy address</source>
        <translation>Скопировать адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="123"/>
        <source>Select range</source>
        <translation>Выбрать диапазон</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="130"/>
        <location filename="../../widgets/HexWidget.cpp" line="705"/>
        <source>Write string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="134"/>
        <source>Write length and string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="138"/>
        <location filename="../../widgets/HexWidget.cpp" line="842"/>
        <source>Write wide string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="142"/>
        <source>Write zero terminated string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="146"/>
        <source>Write De\Encoded Base64 string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="151"/>
        <location filename="../../widgets/HexWidget.cpp" line="745"/>
        <source>Write zeros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="155"/>
        <source>Write random bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="159"/>
        <source>Duplicate from offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="163"/>
        <source>Increment/Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="620"/>
        <source>Item size:</source>
        <translation>Размер элемента:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="622"/>
        <source>Item format:</source>
        <translation>Формат элемента:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="627"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="705"/>
        <location filename="../../widgets/HexWidget.cpp" line="826"/>
        <location filename="../../widgets/HexWidget.cpp" line="842"/>
        <location filename="../../widgets/HexWidget.cpp" line="857"/>
        <source>String:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="745"/>
        <source>Number of zeros:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="769"/>
        <source>Error</source>
        <translation type="unfinished">Ошибка</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="770"/>
        <source>Error occured during decoding your input.
Please, make sure, that it is a valid base64 string and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="793"/>
        <source>Write random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="793"/>
        <source>Number of bytes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="826"/>
        <source>Write Pascal string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="857"/>
        <source>Write zero-terminated string</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HexdumpRangeDialog</name>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="14"/>
        <source>Select Block</source>
        <translation>Выбрать блок</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="47"/>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="79"/>
        <source>Exclusive end address</source>
        <translation>Исключить конечный адрес</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="50"/>
        <source>End Address:</source>
        <translation>Конечный адрес:</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="65"/>
        <source>Start Address:</source>
        <translation>Начальный адрес:</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="99"/>
        <source>Length:</source>
        <translation>Длина:</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="115"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#ff8585;&quot;&gt;Big selection might cause a delay&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#ff8585;&quot;&gt;Большой выбор может вызвать задержку&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>HexdumpWidget</name>
    <message>
        <source>0  1  2  3 ...</source>
        <translation type="vanished">0  1  2  3 ...</translation>
    </message>
    <message>
        <source>0123...</source>
        <translation type="vanished">0123...</translation>
    </message>
    <message>
        <source>Offset</source>
        <translation type="vanished">Смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="70"/>
        <source>Parsing</source>
        <translation>Обработка</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="50"/>
        <source>Select bytes to display information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="143"/>
        <source>Disassembly</source>
        <translation>Дизассемблер</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="144"/>
        <source>String</source>
        <translation>Строка</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="145"/>
        <source>Assembler</source>
        <translation>Ассемблер</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="146"/>
        <source>C bytes</source>
        <translation>C bytes</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="147"/>
        <source>C bytes with instructions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="148"/>
        <source>C half-words (2 byte)</source>
        <translation>C half-words (2 byte)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="149"/>
        <source>C words (4 byte)</source>
        <translation>C words (4 byte)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="150"/>
        <source>C dwords (8 byte)</source>
        <translation>C dwords (8 byte)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="151"/>
        <source>Python</source>
        <translation>Python</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="152"/>
        <source>JSON</source>
        <translation>JSON</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="153"/>
        <source>JavaScript</source>
        <translation>JavaScript</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="154"/>
        <source>Yara</source>
        <translation>Yara</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="120"/>
        <source>Endian</source>
        <translation>Порядок байт</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="131"/>
        <source>Little</source>
        <translation>Little</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="136"/>
        <source>Big</source>
        <translation>Big (от старшего к младшему)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="191"/>
        <source>Arch</source>
        <translation>Архитектура</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="220"/>
        <source>Bits</source>
        <translation>Битность</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="231"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="236"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="241"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="318"/>
        <source>SHA256:</source>
        <translation type="unfinished">SHA1: {256:?}</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="357"/>
        <source>Copy SHA256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="396"/>
        <source>Copy CRC32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="466"/>
        <source>MD5:</source>
        <translation>MD5:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="479"/>
        <source>CRC32:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="350"/>
        <source>SHA1:</source>
        <translation>SHA1:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="434"/>
        <source>Entropy:</source>
        <translation>Энтропия:</translation>
    </message>
    <message>
        <source>Hexdump side panel</source>
        <translation type="vanished">Боковая панель дампа</translation>
    </message>
    <message>
        <source>Undefine</source>
        <translation type="obsolete">Undefine</translation>
    </message>
    <message>
        <source>Copy all</source>
        <translation type="vanished">Скопировать всё</translation>
    </message>
    <message>
        <source>Copy bytes</source>
        <translation type="vanished">Скопировать байты</translation>
    </message>
    <message>
        <source>Copy disasm</source>
        <translation type="vanished">Скопировать дизассемблерный листинг</translation>
    </message>
    <message>
        <source>Copy Hexpair</source>
        <translation type="vanished">Копировать шестнадцатеричную строку</translation>
    </message>
    <message>
        <source>Copy ASCII</source>
        <translation type="vanished">Скопировать ASCII</translation>
    </message>
    <message>
        <source>Copy Text</source>
        <translation type="vanished">Скопировать текст</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="vanished">2</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="vanished">4</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="vanished">8</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Редактировать</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Вставить</translation>
    </message>
    <message>
        <source>Insert Hex</source>
        <translation type="vanished">Вставить шестнадцатеричную строку</translation>
    </message>
    <message>
        <source>Insert String</source>
        <translation type="vanished">Вставить строку</translation>
    </message>
    <message>
        <source>Hex</source>
        <translation type="vanished">Hex</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation type="vanished">Восьмеричная</translation>
    </message>
    <message>
        <source>Half-word</source>
        <translation type="vanished">Half-word</translation>
    </message>
    <message>
        <source>Word</source>
        <translation type="vanished">Word</translation>
    </message>
    <message>
        <source>Quad-word</source>
        <translation type="vanished">Quad-word</translation>
    </message>
    <message>
        <source>Emoji</source>
        <translation type="vanished">Эмодзи</translation>
    </message>
    <message>
        <source>1 byte</source>
        <translation type="vanished">1 байт</translation>
    </message>
    <message>
        <source>2 bytes</source>
        <translation type="vanished">2 байта</translation>
    </message>
    <message>
        <source>4 bytes</source>
        <translation type="vanished">4 байта</translation>
    </message>
    <message>
        <source>Select Block...</source>
        <translation type="vanished">Выбрать блок...</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="219"/>
        <source>Hexdump</source>
        <translation>Шестнадцатеричный дамп</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation type="vanished">Столбцы</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">Формат</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Синхронизация/десинхронизация смещения</translation>
    </message>
    <message>
        <source>Error: Could not select range, end address is less then start address</source>
        <translation type="vanished">Ошибка: Не удалось выбрать диапазон, конечный адрес меньше, чем начальный адрес</translation>
    </message>
</context>
<context>
    <name>ImportsModel</name>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="45"/>
        <source>Unsafe</source>
        <translation>Небезопасный</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="71"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="73"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="75"/>
        <source>Safety</source>
        <translation>Безопасный</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="77"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="79"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="81"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>ImportsWidget</name>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="172"/>
        <source>Imports</source>
        <translation>Импорт</translation>
    </message>
</context>
<context>
    <name>IncrementDecrementDialog</name>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="14"/>
        <source>Increment/Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="24"/>
        <source>Interpret as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="38"/>
        <source>Value:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="54"/>
        <source>Increment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="64"/>
        <source>Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Byte</source>
        <translation type="unfinished">Byte</translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Dword</source>
        <translation type="unfinished">Dword</translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Qword</source>
        <translation type="unfinished">Qword</translation>
    </message>
</context>
<context>
    <name>InitialOptionsDialog</name>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="26"/>
        <source>Load Options</source>
        <translation>Параметры загрузки</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="84"/>
        <source>Program:</source>
        <translation>Программа:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="161"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="363"/>
        <source>Analysis: Enabled</source>
        <translation>Анализ: Включен</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="177"/>
        <source>Level: </source>
        <translation>Уровень: </translation>
    </message>
    <message>
        <source>Analyze all symbols (aa)</source>
        <translation type="vanished">Анализировать все символы (aa)</translation>
    </message>
    <message>
        <source>Analyze for references (aar)</source>
        <translation type="vanished">Анализ ссылок (aar)</translation>
    </message>
    <message>
        <source>Analyze function calls (aac)</source>
        <translation type="vanished">Анализ вызовов функции (aac)</translation>
    </message>
    <message>
        <source>Analyze all basic blocks (aab)</source>
        <translation type="vanished">Анализ всех основных блоков (aab)</translation>
    </message>
    <message>
        <source>Autorename functions based on context (aan)</source>
        <translation type="vanished">Авто-переименование функции на основе контекста (aan)</translation>
    </message>
    <message>
        <source>Experimental:</source>
        <translation type="vanished">Экспериментальный:</translation>
    </message>
    <message>
        <source>Emulate code to find computed references (aae)</source>
        <translation type="vanished">Эмуляция кода для нахождения вычисляемых ссылок (aae)</translation>
    </message>
    <message>
        <source>Analyze for consecutive function (aat)</source>
        <translation type="vanished">Анализ последовательности функции (aat)</translation>
    </message>
    <message>
        <source>Type and Argument matching analysis (afta)</source>
        <translation type="vanished">Анализ соответствия типов и аргументов (afta)</translation>
    </message>
    <message>
        <source>Analyze code after trap-sleds (aaT)</source>
        <translation type="vanished">Анализ кода после ловушек (aaT)</translation>
    </message>
    <message>
        <source>Analyze function preludes (aap)</source>
        <translation type="vanished">Анализировать пролог функции (aap)</translation>
    </message>
    <message>
        <source>Analyze jump tables in switch statements (e! anal.jmptbl)</source>
        <translation type="vanished">Анализ таблиц переходов в операторах switch (e! anal.jmptbl)</translation>
    </message>
    <message>
        <source>Analyze push+ret as jmp (e! anal.pushret)</source>
        <translation type="vanished">Анализировать push+ret как jmp (e! anal.pushret)</translation>
    </message>
    <message>
        <source>Continue analysis after each function (e! anal.hasnext)</source>
        <translation type="vanished">Продолжить анализ после каждой функции (e! anal.hasnext)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="322"/>
        <source>Load in write mode (-w)</source>
        <translation>Загрузить в режиме записи (-w)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="332"/>
        <source>Do not load bin information (-n)</source>
        <translation>Не загружать двоичную информацию (-n)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="345"/>
        <source>Use virtual addressing</source>
        <translation>Использовать виртуальную адресацию</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="355"/>
        <source>Import demangled symbols</source>
        <translation>Импортировать измененные символы</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="372"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="403"/>
        <source>Advanced options</source>
        <translation>Дополнительные настройки</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="433"/>
        <source>CPU options</source>
        <translation>CPU опции</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="448"/>
        <source>Architecture:</source>
        <translation>Архитектура:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="459"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="519"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="567"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="612"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="637"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="37"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="473"/>
        <source>CPU:</source>
        <translation>CPU:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="524"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="529"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="534"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="539"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="553"/>
        <source>Endianness: </source>
        <translation>Порядок байтов: </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="572"/>
        <source>Little</source>
        <translation>Little</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="577"/>
        <source>Big</source>
        <translation>Big</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="598"/>
        <source>Kernel: </source>
        <translation>Ядро: </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="626"/>
        <source>Format:</source>
        <translation>Формат:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="680"/>
        <source>Load bin offset (-B)</source>
        <translation>Загрузить двоичный файл по смещению (-B)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="699"/>
        <source>1024</source>
        <translation>1024</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="718"/>
        <source>Map offset (-m)</source>
        <translation>Смещение файла карты (-m)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="734"/>
        <source>0x40000</source>
        <translation>0x40000</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="741"/>
        <source>Load PDB</source>
        <translation>Загрузить PDB</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="766"/>
        <source>PDB File path</source>
        <translation>Путь к PDB-файлу</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="773"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="815"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="783"/>
        <source>Load script file</source>
        <translation>Загрузите скрипт</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="808"/>
        <source>Path to Rizin script file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path to radare2 script file</source>
        <translation type="vanished">Путь до radare2 скрипта</translation>
    </message>
    <message>
        <source>BasicBlock maxsize:</source>
        <translation type="vanished">Максимальный размер основного блока:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="871"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="884"/>
        <source>  Ok  </source>
        <translation>  Ok  </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="55"/>
        <source>Analyze all symbols</source>
        <translation>Анализировать все символы</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="56"/>
        <source>Analyze instructions for references</source>
        <translation>Анализ инструкций для ссылок</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="57"/>
        <source>Analyze function calls</source>
        <translation>Анализ вызовов функций</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="58"/>
        <source>Analyze all basic blocks</source>
        <translation>Анализ всех основных блоков</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="59"/>
        <source>Analyze all objc references</source>
        <translation>Анализ всех ссылок objc</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="60"/>
        <source>Recover class information from RTTI</source>
        <translation>Восстановить информацию о классах из RTTI</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="61"/>
        <source>Autoname functions based on context</source>
        <translation>Автоматическое именование функций на основе контекста</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="62"/>
        <source>Emulate code to find computed references</source>
        <translation>Эмуляция кода для нахождения вычисляемых ссылок</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="63"/>
        <source>Analyze all consecutive functions</source>
        <translation>Анализ последовательности функций</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="64"/>
        <source>Type and Argument matching analysis</source>
        <translation>Анализ соответствия типов и аргументов</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="65"/>
        <source>Analyze code after trap-sleds</source>
        <translation>Анализ кода после ловушек</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="66"/>
        <source>Analyze function preludes</source>
        <translation>Анализировать прологов функций</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="67"/>
        <source>Analyze jump tables in switch statements</source>
        <translation>Анализ таблиц переходов в операторах switch</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="70"/>
        <source>Analyze PUSH+RET as JMP</source>
        <translation>Анализировать PUSH+RET как JMP</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="71"/>
        <source>Continue analysis after each function</source>
        <translation>Продолжить анализ после каждой функции</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="343"/>
        <source>No analysis</source>
        <translation>Не анализировать</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="345"/>
        <source>Auto-Analysis (aaa)</source>
        <translation>Авто-анализ (aaa)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="347"/>
        <source>Auto-Analysis Experimental (aaaa)</source>
        <translation>Авто-анализ Экспериментальный (aaaa)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="349"/>
        <source>Advanced</source>
        <translation>Дополнительно</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="351"/>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="357"/>
        <source>Level</source>
        <translation>Уровень</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="360"/>
        <source>Analysis: Disabled</source>
        <translation>Анализ: Выключен</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="406"/>
        <source>Select PDB file</source>
        <translation>Выберите PDB файл</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="407"/>
        <source>PDB file (*.pdb)</source>
        <translation>PDB-файл (*.pdb)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="407"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="429"/>
        <source>All files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="428"/>
        <source>Select Rizin script file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="429"/>
        <source>Script file (*.rz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select radare2 script file</source>
        <translation type="vanished">Выберите radare2 скрипт</translation>
    </message>
    <message>
        <source>Script file (*.r2)</source>
        <translation type="vanished">Файл скрипта (*.r2)</translation>
    </message>
</context>
<context>
    <name>InitializationFileEditor</name>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.ui" line="14"/>
        <source>CutterRC Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.ui" line="20"/>
        <source>TextLabel</source>
        <translation type="unfinished">Текстовая метка</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.cpp" line="35"/>
        <source>Script is loaded from &lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JSDecDecompiler</name>
    <message>
        <location filename="../../common/Decompiler.cpp" line="40"/>
        <source>Failed to parse JSON from jsdec</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JupyterWebView</name>
    <message>
        <source>Jupyter</source>
        <translation type="vanished">Jupyter</translation>
    </message>
</context>
<context>
    <name>JupyterWidget</name>
    <message>
        <source>Jupyter</source>
        <translation type="obsolete">Jupyter</translation>
    </message>
    <message>
        <source>Cutter has been built without QtWebEngine.&lt;br /&gt;Open the following URL in your Browser to use Jupyter:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation type="vanished">Cutter был собран без QtWebEngine.&lt;br /&gt;Откройте URL в вашем браузере, чтобы использовать Jupyter:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <source>An error occurred while opening jupyter. Make sure Jupyter is installed system-wide.</source>
        <translation type="vanished">Произошла ошибка открытия Jupiter. Убедитесь, что Jupiter установлен в системе.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Ошибка</translation>
    </message>
</context>
<context>
    <name>LayoutManager</name>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="14"/>
        <source>Layout</source>
        <translation type="unfinished">Схема расположения</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="25"/>
        <source>Rename</source>
        <translation type="unfinished">Переименовать</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="32"/>
        <location filename="../../dialogs/LayoutManager.cpp" line="60"/>
        <source>Delete</source>
        <translation type="unfinished">Удалить</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="42"/>
        <source>Rename layout error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="43"/>
        <source>&apos;%1&apos; is already used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="45"/>
        <source>Save layout</source>
        <translation type="unfinished">Сохранить оформление</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="45"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="61"/>
        <source>Do you want to delete &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinkTypeDialog</name>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="26"/>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="43"/>
        <source>Enter Address</source>
        <translation>Введите адрес</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="50"/>
        <source>Structure Type</source>
        <translation>Тип структуры</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="73"/>
        <source>Address/Flag</source>
        <translation>Адрес/флаг</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="10"/>
        <source>Link type to address</source>
        <translation>Тип ссылки на адрес</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="13"/>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="57"/>
        <source>(No Type)</source>
        <translation>(Без типа)</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="75"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="75"/>
        <source>The given address is invalid</source>
        <translation>Введен направильный адрес</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="106"/>
        <source>Invalid Address</source>
        <translation>Неправильный адрес</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../core/MainWindow.ui" line="33"/>
        <source>Add extra...</source>
        <translation>Добавить дополнительно...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="61"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="68"/>
        <source>Set mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="94"/>
        <location filename="../../core/MainWindow.cpp" line="288"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="101"/>
        <source>Zoom</source>
        <translation>Масштаб</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="110"/>
        <source>Layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="130"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="138"/>
        <location filename="../../core/MainWindow.ui" line="498"/>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="147"/>
        <source>Windows</source>
        <translation>Окна</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="151"/>
        <location filename="../../core/MainWindow.cpp" line="142"/>
        <location filename="../../core/MainWindow.cpp" line="143"/>
        <source>Plugins</source>
        <translation>Плагины</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="156"/>
        <source>Info...</source>
        <translation>Информация...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="161"/>
        <source>Debug...</source>
        <translation>Отладка...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="176"/>
        <source>Debug</source>
        <translation>Отладка</translation>
    </message>
    <message>
        <source>Reset Layout</source>
        <translation type="vanished">Сбросить оформление</translation>
    </message>
    <message>
        <source>Reset layout</source>
        <translation type="vanished">Сбросить оформление</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="227"/>
        <source>Zen Mode</source>
        <translation>Режим &quot;Дзен&quot;</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="230"/>
        <source>Zen mode</source>
        <translation>Режим &quot;Дзен&quot;</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="235"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="240"/>
        <source>Report an issue</source>
        <translation>Сообщить об ошибке</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Создать</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="248"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="253"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="258"/>
        <location filename="../../core/MainWindow.ui" line="261"/>
        <location filename="../../core/MainWindow.ui" line="847"/>
        <location filename="../../core/MainWindow.cpp" line="1207"/>
        <source>Save layout</source>
        <translation>Сохранить оформление</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="266"/>
        <source>Documentation</source>
        <translation>Документация</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Открыть</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation type="vanished">Ctrl+O</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Сохранить</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="282"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="340"/>
        <source>Undo Seek</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="352"/>
        <source>Redo Seek</source>
        <translation>Повторить</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="287"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="222"/>
        <source>Reset to default layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="245"/>
        <source>New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="271"/>
        <source>Map File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="274"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="279"/>
        <location filename="../../core/MainWindow.cpp" line="712"/>
        <location filename="../../core/MainWindow.cpp" line="735"/>
        <source>Save Project</source>
        <translation type="unfinished">Сохранить проект</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="292"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="297"/>
        <location filename="../../core/MainWindow.ui" line="503"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="302"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="307"/>
        <location filename="../../core/MainWindow.ui" line="310"/>
        <source>Select all</source>
        <translation>Выделить все</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="315"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="320"/>
        <location filename="../../core/MainWindow.ui" line="323"/>
        <source>Find next</source>
        <translation>Найти далее</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="328"/>
        <location filename="../../core/MainWindow.ui" line="331"/>
        <source>Find previous</source>
        <translation>Найти предыдущее</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Назад</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="343"/>
        <source>Go back</source>
        <translation>Вернуться</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">Вперед</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="363"/>
        <source>Unlock Panels</source>
        <translation>Разблокировать панели</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="366"/>
        <source>Toggle panel locks</source>
        <translation>Переключить блокировку панелей</translation>
    </message>
    <message>
        <source>Lock/Unlock</source>
        <translation type="vanished">Блокировать/Разблокировать</translation>
    </message>
    <message>
        <source>Strings</source>
        <translation type="vanished">Строки</translation>
    </message>
    <message>
        <source>Show/Hide Strings panel</source>
        <translation type="vanished">Показать/скрыть панель строк</translation>
    </message>
    <message>
        <source>Sections</source>
        <translation type="vanished">Разделы</translation>
    </message>
    <message>
        <source>Show/Hide Sections panel</source>
        <translation type="vanished">Показать/скрыть панель секций</translation>
    </message>
    <message>
        <source>Segments</source>
        <translation type="vanished">Сегменты</translation>
    </message>
    <message>
        <source>Show/Hide Segments panel</source>
        <translation type="vanished">Показать/скрыть панель сегментов</translation>
    </message>
    <message>
        <source>Functions</source>
        <translation type="vanished">Функции</translation>
    </message>
    <message>
        <source>Show/Hide Functions panel</source>
        <translation type="vanished">Показать/скрыть панель функций</translation>
    </message>
    <message>
        <source>Imports</source>
        <translation type="vanished">Импорт</translation>
    </message>
    <message>
        <source>Show/Hide Imports panel</source>
        <translation type="vanished">Показать/скрыть панель импорта</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation type="vanished">Символы</translation>
    </message>
    <message>
        <source>Show/Hide Symbols panel</source>
        <translation type="vanished">Показать/скрыть панель символов</translation>
    </message>
    <message>
        <source>Relocs</source>
        <translation type="vanished">Relocs</translation>
    </message>
    <message>
        <source>Show/Hide Relocs panel</source>
        <translation type="vanished">Показать/скрыть панель передислокации</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="vanished">Флаги</translation>
    </message>
    <message>
        <source>Show/Hide Flags panel</source>
        <translation type="vanished">Показать/скрыть панель флагов</translation>
    </message>
    <message>
        <source>Memory</source>
        <translation type="vanished">Память</translation>
    </message>
    <message>
        <source>Show/Hide Memory panel</source>
        <translation type="vanished">Показать/скрыть панель памяти</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="385"/>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="400"/>
        <location filename="../../core/MainWindow.ui" line="403"/>
        <source>Tabs up/down</source>
        <translation>Верхние/Нижние вкладки</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="412"/>
        <source>Refresh</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <source>Comments</source>
        <translation type="vanished">Комментарии</translation>
    </message>
    <message>
        <source>Show/Hide comments</source>
        <translation type="vanished">Показать/скрыть комментарии</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="420"/>
        <source>Show Tabs at the Top</source>
        <translation>Показывать вкладки в верхней части</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="423"/>
        <source>Toggle tab position</source>
        <translation>Переключить позицию таба</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="428"/>
        <source>Dark Theme</source>
        <translation>Тёмная тема</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="433"/>
        <location filename="../../core/MainWindow.ui" line="436"/>
        <source>Load layout</source>
        <translation>Загрузить оформление</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="441"/>
        <source>Default Theme</source>
        <translation>Оформление по-умолчанию</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="446"/>
        <source>Bindiff</source>
        <translation>Bindiff</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="451"/>
        <source>Analysis</source>
        <translation>Анализ</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="456"/>
        <source>Test menu</source>
        <translation>Тестовое меню</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="461"/>
        <location filename="../../core/MainWindow.ui" line="464"/>
        <source>Copy hexpair</source>
        <translation>Копировать шестнадцатеричную строку</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="469"/>
        <location filename="../../core/MainWindow.ui" line="472"/>
        <source>Copy text</source>
        <translation>Скопировать текст</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="477"/>
        <source>Copy ASCII</source>
        <translation>Копировать ASCII</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="482"/>
        <location filename="../../core/MainWindow.ui" line="485"/>
        <source>Insert string</source>
        <translation>Вставить строку</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="490"/>
        <location filename="../../core/MainWindow.ui" line="493"/>
        <source>Insert hex</source>
        <translation>Вставить шестнадцатеричную строку</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="508"/>
        <source>Show/Hide bytes</source>
        <translation>Показать/скрыть байты</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="513"/>
        <source>Switch case</source>
        <translation>Switch case</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="518"/>
        <location filename="../../core/MainWindow.ui" line="521"/>
        <source>Copy all</source>
        <translation>Копировать все</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="526"/>
        <location filename="../../core/MainWindow.ui" line="529"/>
        <source>Copy bytes</source>
        <translation>Копировать байты</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="534"/>
        <location filename="../../core/MainWindow.ui" line="537"/>
        <location filename="../../core/MainWindow.ui" line="542"/>
        <location filename="../../core/MainWindow.ui" line="545"/>
        <source>Copy disasm</source>
        <translation>Скопировать дизассемблерный листинг</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="557"/>
        <location filename="../../core/MainWindow.ui" line="560"/>
        <source>Start web server</source>
        <translation>Запустить web-сервер</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="565"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="570"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="575"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="580"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="585"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="590"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="595"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="600"/>
        <source>Syntax AT&amp;T/Intel</source>
        <translation>Синтаксис AT&amp;T/Intel</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="605"/>
        <location filename="../../core/MainWindow.ui" line="615"/>
        <source>Rename</source>
        <translation>Переименовать</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="610"/>
        <location filename="../../core/MainWindow.ui" line="620"/>
        <source>Undefine</source>
        <translation>Неопределенный</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="625"/>
        <source>Add comment</source>
        <translation>Добавить комментарий</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="637"/>
        <location filename="../../core/MainWindow.ui" line="640"/>
        <source>Show/Hide bottom pannel</source>
        <translation>Показать/скрыть нижнюю панелью</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="645"/>
        <source>Run Rizin script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="717"/>
        <source>Save Project As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="735"/>
        <source>Analyze Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="809"/>
        <source>Commit changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="817"/>
        <source>Write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="820"/>
        <source>Open the file in write mode. Every change to the file will change the original file on disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="828"/>
        <source>Cache mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="831"/>
        <source>Enable cache mode. Changes to the file would not be applied to disk unless you specifically commit them. This is a safer option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="842"/>
        <source>Read-Only mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="852"/>
        <source>Manage layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SDB Browser</source>
        <translation type="vanished">SDB обозреватель</translation>
    </message>
    <message>
        <source>Run Script</source>
        <translation type="vanished">Запустить скрипт</translation>
    </message>
    <message>
        <source>Dashboard</source>
        <translation type="vanished">Панель мониторинга</translation>
    </message>
    <message>
        <source>Show/Hide Dashboard panel</source>
        <translation type="vanished">Показать/скрыть информационную панель</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="650"/>
        <source>Reset Settings</source>
        <translation>Сбросить настройки</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="653"/>
        <source>Reset settings</source>
        <translation>Сбросить настройки</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="658"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="661"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="669"/>
        <source>Exports</source>
        <translation>Экспорт</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="672"/>
        <source>Show/Hide Exports panel</source>
        <translation>Показать/скрыть панель экспорта</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="677"/>
        <source>Refresh Contents</source>
        <translation>Обновить содержимое</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="680"/>
        <source>Refresh contents</source>
        <translation>Обновить содержимое</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="688"/>
        <source>Show ESIL rather than assembly</source>
        <translation>Показать ESIL вместо ассемблера</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="696"/>
        <source>Show pseudocode rather than assembly</source>
        <translation>Показать псевдокод вместо ассемблера</translation>
    </message>
    <message>
        <source>Entry Points</source>
        <translation type="vanished">Точка входа</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="707"/>
        <source>Display offsets</source>
        <translation>Отобразить смещения</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="712"/>
        <source>Preferences</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <source>Save As...</source>
        <translation type="vanished">Сохранить как...</translation>
    </message>
    <message>
        <source>Graph Overview</source>
        <translation type="vanished">Обзор графа</translation>
    </message>
    <message>
        <source>Decompiler</source>
        <translation type="vanished">Декомпилятор</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="745"/>
        <source>Add Hexdump</source>
        <translation>Добавить шестнадцатеричный дамп</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="750"/>
        <source>Add Decompiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="755"/>
        <source>Add Disassembly</source>
        <translation>Добавить дизассемблерный листинг</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="760"/>
        <source>Add Graph</source>
        <translation>Добавить граф</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="771"/>
        <source>Grouped dock dragging</source>
        <translation>Группировка перетаскиванием</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="776"/>
        <source>Zoom In</source>
        <translation>Приблизить</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="779"/>
        <source>Ctrl++</source>
        <translation>Ctrl++</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="787"/>
        <source>Zoom Out</source>
        <translation>Отдалить</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="790"/>
        <source>Ctrl+-</source>
        <translation>Ctrl+-</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="798"/>
        <source>Reset</source>
        <translation>Сбросить</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="801"/>
        <source>Ctrl+=</source>
        <translation>Ctrl+=</translation>
    </message>
    <message>
        <source>Tmp</source>
        <translation type="vanished">Tmp</translation>
    </message>
    <message>
        <source>Disassembly</source>
        <translation type="vanished">Дизассемблер</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="725"/>
        <source>Graph</source>
        <translation>Граф</translation>
    </message>
    <message>
        <source>Pseudocode</source>
        <translation type="vanished">Псевдокод</translation>
    </message>
    <message>
        <source>Hexdump</source>
        <translation type="obsolete">Hexdump</translation>
    </message>
    <message>
        <source>Sidebar</source>
        <translation type="vanished">Боковая панель</translation>
    </message>
    <message>
        <source>Console</source>
        <translation type="vanished">Консоль</translation>
    </message>
    <message>
        <source>Stack</source>
        <translation type="vanished">Стек</translation>
    </message>
    <message>
        <source>Registers</source>
        <translation type="vanished">Регистры</translation>
    </message>
    <message>
        <source>Backtrace</source>
        <translation type="vanished">Обратная трассировка</translation>
    </message>
    <message>
        <source>Threads</source>
        <translation type="vanished">Потоки</translation>
    </message>
    <message>
        <source>Processes</source>
        <translation type="vanished">Процессы</translation>
    </message>
    <message>
        <source>Memory map</source>
        <translation type="vanished">Карта памяти</translation>
    </message>
    <message>
        <source>Breakpoints</source>
        <translation type="vanished">Точки останова</translation>
    </message>
    <message>
        <source>Register References</source>
        <translation type="vanished">Ссылки на регистр</translation>
    </message>
    <message>
        <source>Classes</source>
        <translation type="vanished">Классы</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="730"/>
        <source>Import PDB</source>
        <translation>Импорт PDB</translation>
    </message>
    <message>
        <source>Analyze</source>
        <translation type="vanished">Анализ</translation>
    </message>
    <message>
        <source>Resources</source>
        <translation type="vanished">Ресурсы</translation>
    </message>
    <message>
        <source>VTables</source>
        <translation type="vanished">Виртуальные таблицы</translation>
    </message>
    <message>
        <source>Show/Hide VTables panel</source>
        <translation type="vanished">Показать/скрыть панель виртуальных таблиц</translation>
    </message>
    <message>
        <source>Types</source>
        <translation type="vanished">Типы</translation>
    </message>
    <message>
        <source>Show/Hide Types panel</source>
        <translation type="vanished">Показать/скрыть панель типов</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Поиск</translation>
    </message>
    <message>
        <source>Show/Hide Search panel</source>
        <translation type="vanished">Показать/скрыть панель поиска</translation>
    </message>
    <message>
        <source>Headers</source>
        <translation type="vanished">Заголовки</translation>
    </message>
    <message>
        <source>Show/Hide Headers panel</source>
        <translation type="vanished">Показать/скрыть панель заголовков</translation>
    </message>
    <message>
        <source>Zignatures</source>
        <translation type="vanished">Сигнатуры</translation>
    </message>
    <message>
        <source>Show/Hide Zignatures panel</source>
        <translation type="vanished">Показать/скрыть панель сигнатур</translation>
    </message>
    <message>
        <source>Jupyter</source>
        <translation type="obsolete">Jupyter</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="740"/>
        <location filename="../../core/MainWindow.cpp" line="1697"/>
        <source>Export as code</source>
        <translation>Экспортировать как код</translation>
    </message>
    <message>
        <source>Hexdump view</source>
        <translation type="vanished">Дамп</translation>
    </message>
    <message>
        <source>Disassembly view</source>
        <translation type="vanished">Дизассемблер</translation>
    </message>
    <message>
        <source>Graph view</source>
        <translation type="vanished">Представление в виде графа</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="533"/>
        <source>Script loading</source>
        <translation>Загрузка скрипта</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="534"/>
        <source>Do you want to load the &apos;%1&apos; script?</source>
        <translation>Вы хотите загрузить &apos;%1&apos; скрипт?</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="552"/>
        <source>Cannot open file!</source>
        <translation>Не удается открыть файл!</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="553"/>
        <source>Could not open the file! Make sure the file exists and that you have the correct permissions.</source>
        <translation>Не удалось открыть файл! Убедитесь, что файл существует и у вас есть верные права.</translation>
    </message>
    <message>
        <source> &gt; Populating UI</source>
        <translation type="vanished"> &gt; Наполнение пользовательского интерфейса</translation>
    </message>
    <message>
        <source> &gt; Finished, happy reversing :)</source>
        <translation type="vanished"> &gt; Готово, счастливого реверса :)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="764"/>
        <source>Do you really want to exit?
Save your project before closing!</source>
        <translation>Вы действительно хотите выйти?
Сохраните проект перед закрытием!</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1038"/>
        <source>New disassembly</source>
        <translation>Добавить дизассемблерный листинг</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1040"/>
        <source>New graph</source>
        <translation>Добавить граф</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1042"/>
        <source>New hexdump</source>
        <translation>Добавить шестнадцатеричный дамп</translation>
    </message>
    <message>
        <source>Select radare2 script</source>
        <translation type="vanished">Выберите скрипт radare2</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="251"/>
        <source>No plugins are installed. Check the plugins section on Cutter documentation to learn more.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="256"/>
        <source>The installed plugins didn&apos;t add entries to this menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="616"/>
        <source>Failed to open project: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="619"/>
        <source>Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1043"/>
        <source>New Decompiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1204"/>
        <source>Save layout error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1205"/>
        <source>&apos;%1&apos; is not a valid name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1207"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1514"/>
        <source>Select Rizin script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1557"/>
        <source>Do you really want to clear all settings?</source>
        <translation>Вы действительно хотите сбросить все настройки?</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1651"/>
        <source>Select PDB file</source>
        <translation>Выберите PDB файл</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1652"/>
        <source>PDB file (*.pdb)</source>
        <translation>PDB-файл (*.pdb)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1652"/>
        <source>All files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1662"/>
        <source>%1 loaded.</source>
        <translation>%1 загружен.</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1672"/>
        <source>C uin8_t array (*.c)</source>
        <translation>C uin8_t array (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1674"/>
        <source>C uin16_t array (*.c)</source>
        <translation>C uin16_t array (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1676"/>
        <source>C uin32_t array (*.c)</source>
        <translation>C uin32_t array (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1678"/>
        <source>C uin64_t array (*.c)</source>
        <translation>C uin64_t array (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1680"/>
        <source>C string (*.c)</source>
        <translation>C string (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1682"/>
        <source>Shell-script that reconstructs the bin (*.sh)</source>
        <translation>Шелл-скрипт реконструирующий двоичный файл (*.sh)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1684"/>
        <source>JSON array (*.json)</source>
        <translation>JSON массив(*.json)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1686"/>
        <source>JavaScript array (*.js)</source>
        <translation>JavaScript массив(*.js)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1688"/>
        <source>Python array (*.py)</source>
        <translation>Python массив(*.py)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1690"/>
        <source>Print &apos;wx&apos; Rizin commands (*.rz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print &apos;wx&apos; r2 commands (*.r2)</source>
        <translation type="vanished">Введите &apos;wx&apos; r2 команды (*.r2)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1692"/>
        <source>GAS .byte blob (*.asm, *.s)</source>
        <translation>GAS .byte blob (*.asm, *.s)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1694"/>
        <source>.bytes with instructions in comments (*.txt)</source>
        <translation>.bytes c инструкциями в комментариях (*.txt)</translation>
    </message>
    <message>
        <source>Project saved: %1</source>
        <translation type="vanished">Проект сохранен: %1</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="736"/>
        <source>Failed to save project: %1</source>
        <translation>Не удалось сохранить проект: %1</translation>
    </message>
    <message>
        <source>Project saved:</source>
        <translation type="vanished">Проект сохранен:</translation>
    </message>
</context>
<context>
    <name>MapFileDialog</name>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="14"/>
        <source>Map New File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="26"/>
        <source>File:</source>
        <translation type="unfinished">Файл:</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="52"/>
        <location filename="../../dialogs/MapFileDialog.cpp" line="18"/>
        <source>Select file</source>
        <translation type="unfinished">Выберите файл</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="59"/>
        <source>Map address:</source>
        <translation type="unfinished">Начальный адрес:</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="72"/>
        <source>0x40000</source>
        <translation type="unfinished">0x40000</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.cpp" line="36"/>
        <source>Map new file file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.cpp" line="36"/>
        <source>Failed to map a new file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MemoryDockWidget</name>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Синхронизация/десинхронизация смещения</translation>
    </message>
</context>
<context>
    <name>MemoryMapModel</name>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="58"/>
        <source>Offset start</source>
        <translation>Начальное смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="60"/>
        <source>Offset end</source>
        <translation>Конечное смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="62"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="64"/>
        <source>Permissions</source>
        <translation>Права доступа</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="66"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>MemoryMapWidget</name>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="123"/>
        <source>Memory Map</source>
        <translation>Карта памяти</translation>
    </message>
</context>
<context>
    <name>MultitypeFileSaveDialog</name>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="24"/>
        <source>Detect type (*)</source>
        <translation>Определить тип (*)</translation>
    </message>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="63"/>
        <source>File save error</source>
        <translation>Ошибка сохранения файла</translation>
    </message>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="64"/>
        <source>Unrecognized extension &apos;%1&apos;</source>
        <translation>Неопознанное расширение &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>NativeDebugDialog</name>
    <message>
        <location filename="../../dialogs/NativeDebugDialog.ui" line="35"/>
        <source>Command line arguments:</source>
        <translation>Аргументы командной строки:</translation>
    </message>
</context>
<context>
    <name>NewFileDialog</name>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="20"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="149"/>
        <source>Open File</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="79"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="182"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="390"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="169"/>
        <source>&lt;b&gt;Select new file&lt;b&gt;</source>
        <translation>&lt;b&gt;Выберите новый файл&lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="202"/>
        <source>&lt;b&gt;IO&lt;/b&gt;</source>
        <translation>&lt;b&gt;IO&lt;/b&gt;</translation>
    </message>
    <message>
        <source>://</source>
        <translation type="vanished">://</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="296"/>
        <source>Don&apos;t open any file</source>
        <translation>Не открывать никакой файл</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="303"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="360"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="501"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="316"/>
        <source>Open Shellcode</source>
        <translation>Открыть шеллкод</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="328"/>
        <source>&lt;b&gt;Paste Shellcode&lt;b&gt;</source>
        <translation>&lt;b&gt;Вставить шеллкод&lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="370"/>
        <source>Projects</source>
        <translation>Проекты</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="410"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Open Project&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="534"/>
        <source>Clear all projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Projects path (dir.projects):&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Projects path (dir.projects):&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="519"/>
        <source>Remove item</source>
        <translation>Удалить элемент</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="524"/>
        <source>Clear all</source>
        <translation>Очистить все</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="529"/>
        <source>Delete project</source>
        <translation>Удалить проект</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="92"/>
        <source>Select file</source>
        <translation>Выберите файл</translation>
    </message>
    <message>
        <source>Select project path (dir.projects)</source>
        <translation type="vanished">Выбрать путь проекта (dir.projects)</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Отказано в доступе</translation>
    </message>
    <message>
        <source>You do not have write access to &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="vanished">У вас нет прав на запись в &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Delete the project &quot;%1&quot; from disk ?</source>
        <translation type="vanished">Удалить проект &quot;%1&quot; с диска ?</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="104"/>
        <source>Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="287"/>
        <source>Open a file with no extra treatment.</source>
        <translation>Открыть файл без дополнительной обработки.</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="319"/>
        <source>Select a new program or a previous one before continuing.</source>
        <translation>Выберите новую программу или последнюю перед тем, как продолжить.</translation>
    </message>
</context>
<context>
    <name>Omnibar</name>
    <message>
        <location filename="../../widgets/Omnibar.cpp" line="15"/>
        <source>Type flag name or address here</source>
        <translation>Введите здесь имя флага или адрес</translation>
    </message>
</context>
<context>
    <name>OpenFileDialog</name>
    <message>
        <source>Open file</source>
        <translation type="vanished">Открыть файл</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="vanished">Выберите файл</translation>
    </message>
    <message>
        <source>Map address:</source>
        <translation type="vanished">Начальный адрес:</translation>
    </message>
    <message>
        <source>File:</source>
        <translation type="vanished">Файл:</translation>
    </message>
    <message>
        <source>Map address</source>
        <translation type="vanished">Адрес карты</translation>
    </message>
    <message>
        <source>0x40000</source>
        <translation type="vanished">0x40000</translation>
    </message>
    <message>
        <source>Failed to open file</source>
        <translation type="vanished">Не удалось открыть файл</translation>
    </message>
</context>
<context>
    <name>PluginsOptionsWidget</name>
    <message>
        <source>Plugins are loaded from &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="vanished">Плагины загружаются из &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="28"/>
        <source>Plugins are loaded from &lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="48"/>
        <source>Show Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show radare2 plugin information</source>
        <translation type="vanished">Показать информацию о плагинах radare2</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.ui" line="6"/>
        <source>Preferences</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="28"/>
        <source>Disassembly</source>
        <translation>Дизассемблер</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="34"/>
        <source>Debug</source>
        <translation>Отладка</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="35"/>
        <source>Appearance</source>
        <translation>Оформление</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="36"/>
        <source>Plugins</source>
        <translation>Плагины</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="37"/>
        <source>Initialization Script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="39"/>
        <source>Analysis</source>
        <translation type="unfinished">Анализ</translation>
    </message>
</context>
<context>
    <name>ProcessModel</name>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="71"/>
        <source>PID</source>
        <translation>Идентификатор процесса</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="73"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="75"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="77"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
</context>
<context>
    <name>ProcessesWidget</name>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="26"/>
        <source>PID</source>
        <translation>Идентификатор процесса</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="27"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="28"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="29"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="172"/>
        <source>Unable to switch to the requested process.</source>
        <translation>Не удается переключиться на запрошенный процесс.</translation>
    </message>
</context>
<context>
    <name>PseudocodeWidget</name>
    <message>
        <source>Pseudocode</source>
        <translation type="vanished">Псевдокод</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Refresh</translation>
    </message>
    <message>
        <source>Decompiler:</source>
        <translation type="vanished">Декомпилятор:</translation>
    </message>
    <message>
        <source>r2dec</source>
        <translation type="vanished">r2dec</translation>
    </message>
    <message>
        <source>pdc</source>
        <translation type="vanished">pdc</translation>
    </message>
    <message>
        <source>Click Refresh to generate Pseudocode from current offset.</source>
        <translation type="vanished">Нажмите обновить для генерации псевдокода.</translation>
    </message>
    <message>
        <source>Cannot decompile at</source>
        <translation type="vanished">Не удается декомпилировать по</translation>
    </message>
    <message>
        <source>(Not a function?)</source>
        <translation type="vanished">(Нет функции?)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>A Qt and C++ GUI for radare2 reverse engineering framework</source>
        <translation type="vanished">Qt и C++ GUI для radare2 reverse engineering framework</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="97"/>
        <source>The version used to compile Cutter (%1) does not match the binary version of rizin (%2). This could result in unexpected behaviour. Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="317"/>
        <source>A Qt and C++ GUI for rizin reverse engineering framework</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="320"/>
        <source>Filename to open.</source>
        <translation>Имя файла для открытия.</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="324"/>
        <source>Automatically open file and optionally start analysis. Needs filename to be specified. May be a value between 0 and 2: 0 = no analysis, 1 = aaa, 2 = aaaa (experimental)</source>
        <translation>Автоматически открывать файл и при необходимости запускать анализ. Требуется указать имя файла. Значение может быть от 0 до 2: 0 = не анализировать, 1 = aaa, 2 = aaaa (экспериментальный)</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="327"/>
        <source>level</source>
        <translation>уровень</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="331"/>
        <source>Force using a specific file format (bin plugin)</source>
        <translation>Принудительное использование определенного формата файла (бинарный плагин)</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="332"/>
        <source>name</source>
        <translation>название</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="336"/>
        <source>Load binary at a specific base address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="337"/>
        <source>base address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="340"/>
        <source>Run script file</source>
        <translation>Запустить скрипт</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="340"/>
        <source>file</source>
        <translation>файл</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="343"/>
        <source>Load project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="344"/>
        <source>project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="348"/>
        <source>Open file in write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="352"/>
        <source>PYTHONHOME to use for embedded python interpreter</source>
        <translation>PYTHONHOME чтобы использовать встроенные интерпретатор Python</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="358"/>
        <source>Disable output redirection. Some of the output in console widget will not be visible. Use this option when debuging a crash or freeze and output  redirection is causing some messages to be lost.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="364"/>
        <source>Do not load plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="368"/>
        <source>Do not load Cutter plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="372"/>
        <source>Do not load rizin plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PYTHONHOME to use for Jupyter</source>
        <translation type="vanished">PYTHONHOME для использования Jupyter</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="96"/>
        <source>Version mismatch!</source>
        <translation>Конфликт версии!</translation>
    </message>
    <message>
        <source>The version used to compile Cutter (%1) does not match the binary version of radare2 (%2). This could result in unexpected behaviour. Are you sure you want to continue?</source>
        <translation type="vanished">Версия, используемая для компиляции Cutter (%1), не соответствует бинарной версии radare2 (%2). Это может привести к неожиданному поведению. Вы уверены что хотите продолжить?</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="386"/>
        <source>Invalid Analysis Level. May be a value between 0 and 2.</source>
        <translation>Неверный уровень анализа. Допустимое значение находится между 0 и 2.</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="406"/>
        <source>Filename must be specified to start analysis automatically.</source>
        <translation>Имя файла должно быть указано для автоматического запуска анализа.</translation>
    </message>
    <message>
        <source>Color of comment generated by radare2</source>
        <translation type="vanished">Цвет комментариев созданных radare2</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="410"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="410"/>
        <source>Color of comment generated by Rizin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="411"/>
        <source>Comment created by user</source>
        <translation>Комментарий созданный пользователем</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="411"/>
        <source>Color of user Comment</source>
        <translation>Цвет пользовательских комментариев</translation>
    </message>
    <message>
        <source>Color of function arguments</source>
        <translation type="vanished">Цвет аргументов функции</translation>
    </message>
    <message>
        <source>Arguments</source>
        <translation type="vanished">Аргументы</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="413"/>
        <source>Color of names of functions</source>
        <translation>Цвет для имени функций</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="413"/>
        <source>Function name</source>
        <translation>Имя функции</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="414"/>
        <source>Color of function location</source>
        <translation>Цвет расположения функции</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="414"/>
        <source>Function location</source>
        <translation>Расположение функции</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="416"/>
        <source>Color of ascii line in left side that shows what opcodes are belong to function</source>
        <translation>Цвет линии ascii в левой части показывает, какие опкоды принадлежат функции</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="418"/>
        <source>Function line</source>
        <translation>Линии функций</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="420"/>
        <source>Color of flags (similar to bookmarks for offset)</source>
        <translation>Цвет флагов (аналогично закладкам для смещения)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="420"/>
        <source>Flag</source>
        <translation>Флаг</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="421"/>
        <source>Label</source>
        <translation>Метка</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="422"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <source>flow</source>
        <translation type="vanished">flow</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="424"/>
        <source>flow2</source>
        <translation>flow2</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="425"/>
        <location filename="../../widgets/ColorThemeListView.cpp" line="427"/>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="425"/>
        <source>prompt</source>
        <translation>prompt</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="426"/>
        <source>Color of offsets</source>
        <translation>Цвет смещений</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="426"/>
        <source>Offset</source>
        <translation>Смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="427"/>
        <source>input</source>
        <translation>ввод</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="428"/>
        <source>Invalid opcode color</source>
        <translation>Неверный цвет опкода</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="428"/>
        <source>invalid</source>
        <translation>неверный</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="429"/>
        <source>other</source>
        <translation>другое</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="430"/>
        <source>0x00 opcode color</source>
        <translation>Цвет 0x00 опкода</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="431"/>
        <source>0x7f opcode color</source>
        <translation>Цвет 0x7f опкода</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="432"/>
        <source>0xff opcode color</source>
        <translation>Цвет 0xff опкода</translation>
    </message>
    <message>
        <source>arithmetic color (+, -, *, / etc.)</source>
        <translation type="vanished">цвет арифметических действий (+, -, *, / и т.д)</translation>
    </message>
    <message>
        <source>bin</source>
        <translation type="vanished">bin</translation>
    </message>
    <message>
        <source>btext</source>
        <translation type="vanished">btext</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="443"/>
        <source>push opcode color</source>
        <translation>Цвет push опкода</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="444"/>
        <source>pop opcode color</source>
        <translation>Цвет pop опкода</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="445"/>
        <source>Cryptographic color</source>
        <translation>Криптографический цвет</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="446"/>
        <source>jmp instructions color</source>
        <translation>цвет jmp инструкции</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="450"/>
        <source>call instructions color (ccall, rcall, call etc)</source>
        <translation>цвет инструкции call (ccall, rcall, call etc)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="451"/>
        <source>nop opcode color</source>
        <translation>цвет nop опкода</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="452"/>
        <source>ret opcode color</source>
        <translation>цвет ret опкода</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="453"/>
        <source>Color of interrupts</source>
        <translation>Цвет прерываний</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="453"/>
        <source>Interrupts</source>
        <translation>Прерывания</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="454"/>
        <source>swi opcode color</source>
        <translation>цвет swi опкода</translation>
    </message>
    <message>
        <source>cmp opcode color</source>
        <translation type="vanished">цвет cmp опкода</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="458"/>
        <source>Registers color</source>
        <translation>Цвет регистров</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="458"/>
        <source>Register</source>
        <translation>Регистр</translation>
    </message>
    <message>
        <source>Numeric constants color</source>
        <translation type="vanished">Цвет числовых констант</translation>
    </message>
    <message>
        <source>Numbers</source>
        <translation type="vanished">Цифры</translation>
    </message>
    <message>
        <source>mov instructions color (mov, movd, movw etc</source>
        <translation type="vanished">цвет mov инструкции (mov, movd, movw etc</translation>
    </message>
    <message>
        <source>mov</source>
        <translation type="vanished">mov</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="423"/>
        <source>Color of lines showing jump destination</source>
        <translation>Цвета линий показывают, показывающих место перехода</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="423"/>
        <source>Flow</source>
        <translation>Поток выполнения</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="434"/>
        <source>Color of arithmetic opcodes (add, div, mul etc)</source>
        <translation>Цвет арифметических команд (добавить, div, mul и т.д.)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="435"/>
        <source>Arithmetic</source>
        <translation>Арифметические</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="437"/>
        <source>Color of binary operations (and, or, xor etc).</source>
        <translation>Цвет бинарных операций (and, or, xor и т.д.).</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="437"/>
        <source>Binary</source>
        <translation>Бинарные</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="439"/>
        <source>Color of object names, commas between operators, squared brackets and operators inside them.</source>
        <translation>Цвет имен объектов, запятых между операторами, квадратных скобок и операторов внутри них.</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="442"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="448"/>
        <source>Color of conditional jump opcodes such as je, jg, jne etc</source>
        <translation>Цвет команд условного перехода, таких как je, jg, jne и т.д.</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="449"/>
        <source>Conditional jump</source>
        <translation>Условный переход</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="456"/>
        <source>Color of compare instructions such as test and cmp</source>
        <translation>Цвет инструкций сравнения, таких как test и cmp</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="457"/>
        <source>Compare instructions</source>
        <translation>Инструкции сравнения</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="461"/>
        <source>Color of numeric constants and object pointers</source>
        <translation>Цвет числовых констант и указателей на объекты</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="461"/>
        <source>Constants</source>
        <translation>Константы</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="463"/>
        <source>Color of move instructions such as mov, movd, lea etc</source>
        <translation>Цвет инструкций перемещения, таких как mov, movd, lea и т.д.</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="464"/>
        <source>Move instructions</source>
        <translation>Инструкции перемещения</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="465"/>
        <source>Function variable color</source>
        <translation>Цвет переменной функции</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="465"/>
        <source>Function variable</source>
        <translation>Переменная функции</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="467"/>
        <source>Function variable (local or argument) type color</source>
        <translation>Тип цвета переменной функции (локальной или аргумента)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="468"/>
        <source>Variable type</source>
        <translation>Тип переменной</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="470"/>
        <source>Function variable address color</source>
        <translation>Цвет адреса переменной функции</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="470"/>
        <source>Variable address</source>
        <translation>Адрес переменной</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="482"/>
        <source>In graph view jump arrow true</source>
        <translation>Стрелка истины в графическом представлении</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="482"/>
        <source>Arrow true</source>
        <translation>Стрелка истина</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="484"/>
        <source>In graph view jump arrow false</source>
        <translation>Стрелка лжи в графическом представлении</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="484"/>
        <source>Arrow false</source>
        <translation>Стрелка ложь</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="486"/>
        <source>In graph view jump arrow (no condition)</source>
        <translation>Стрелка прыжка в графическом представлении (без условия)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="486"/>
        <source>Arrow</source>
        <translation>Стрелка</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="490"/>
        <source>Background color of Graph Overview&apos;s node</source>
        <translation>Цвет фона узла в графе</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="491"/>
        <source>Graph Overview node</source>
        <translation>Узел графа</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="493"/>
        <source>Fill color of Graph Overview&apos;s selection</source>
        <translation>Цвет заливки выделения в графе</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="494"/>
        <source>Graph Overview fill</source>
        <translation>Цвет заливки графа</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="496"/>
        <source>Border color of Graph Overview&apos;s selection</source>
        <translation>Цвет границы выделения в просмотре графа</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="497"/>
        <source>Graph Overview border</source>
        <translation>Граница просмотра графа</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="500"/>
        <source>General background color</source>
        <translation>Общий цвет фона</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="500"/>
        <source>Background</source>
        <translation>Фон</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="502"/>
        <source>Background color of non-focused graph node</source>
        <translation>Цвет фона в узлах вне фокуса</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="503"/>
        <source>Node background</source>
        <translation>Фон узла</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="511"/>
        <source>Background color of selected word</source>
        <translation>Цвет фона выбранного слова</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="512"/>
        <source>Main function color</source>
        <translation>Основной цвет функции</translation>
    </message>
    <message>
        <source>Alt. background</source>
        <translation type="vanished">Альтернативный фон</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="505"/>
        <source>Background of current graph node</source>
        <translation>Фон текущего узла графа</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="505"/>
        <source>Current graph node</source>
        <translation>Текущий узел графа</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="507"/>
        <source>Color of node border in graph view</source>
        <translation>Цвет границы узла в просмотре графа</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="507"/>
        <source>Node border</source>
        <translation>Граница узла</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="509"/>
        <source>Selected line background color</source>
        <translation>Цвет фона выделенной линии</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="509"/>
        <source>Line highlight</source>
        <translation>Подсветка линии</translation>
    </message>
    <message>
        <source>Highlighted word text color</source>
        <translation type="vanished">Цвет текста выделенного слова</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="511"/>
        <source>Word higlight</source>
        <translation>Подсветка опечатки</translation>
    </message>
    <message>
        <source>Color of main function color</source>
        <translation type="vanished">Цвет основной функции</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="512"/>
        <source>Main</source>
        <translation>Main</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="521"/>
        <source>Code section color in navigation bar</source>
        <translation>Цвет секций кода в панели навигации</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="521"/>
        <source>Navbar code</source>
        <translation>Код панели навигации</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="523"/>
        <source>Empty section color in navigation bar</source>
        <translation>Цвет пустой секции в панели навигации</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="523"/>
        <source>Navbar empty</source>
        <translation>Пустая панель навигации</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="524"/>
        <source>ucall</source>
        <translation>ucall</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="525"/>
        <source>ujmp</source>
        <translation>ujmp</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="526"/>
        <source>Breakpoint background</source>
        <translation>Фон точки останова</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="100"/>
        <source>Crash</source>
        <translation>Сбой</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="101"/>
        <source>Cutter received a signal it can&apos;t handle and will close.&lt;br/&gt;Would you like to create a crash dump for a bug report?</source>
        <translation>Cutter получил сигнал об ошибке, который он не может обработать, и будет закрыт.&lt;br/&gt;Хотите создать дамп памяти для отчёта об ошибке?</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="104"/>
        <source>Create a Crash Dump</source>
        <translation>Создать дамп памяти</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="105"/>
        <location filename="../../common/CrashHandler.cpp" line="148"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="119"/>
        <source>Choose a directory to save the crash dump in</source>
        <translation>Выберите каталог для сохранения дампа памяти</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="124"/>
        <source>Minidump (*.dmp)</source>
        <translation>Минидамп (*.dmp)</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="133"/>
        <source>Save Crash Dump</source>
        <translation>Сохранить дамп памяти</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="134"/>
        <source>Failed to write to %1.&lt;br/&gt;Please make sure you have access to that directory and try again.</source>
        <translation>Не удалось записать в %1.&lt;br/&gt;Пожалуйста, убедитесь, что у вас есть доступ к этой папке и повторите попытку.</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="142"/>
        <source>Success</source>
        <translation>Успешно</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="143"/>
        <source>&lt;a href=&quot;%1&quot;&gt;Crash dump&lt;/a&gt; was successfully created.</source>
        <translation>&lt;a href=&quot;%1&quot;&gt;Дамп памяти&lt;/a&gt; был успешно создан.</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="147"/>
        <source>Open an Issue</source>
        <translation>Открыть тикет</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="156"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="157"/>
        <source>Error occurred during crash dump creation.</source>
        <translation>Произошла ошибка при создании дампа памяти.</translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="53"/>
        <source>Write error</source>
        <translation type="unfinished">Ошибка записи</translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="54"/>
        <source>Your file is opened in read-only mode. Editing is only available when the file is opened in either Write or Cache modes.

WARNING: In Write mode, any changes will be committed to the file on disk. For safety, please consider using Cache mode and then commit the changes manually via File -&gt; Commit modifications to disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="60"/>
        <source>Cancel</source>
        <translation type="unfinished">Отмена</translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="62"/>
        <source>Reopen in Write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="64"/>
        <source>Enable Cache mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="99"/>
        <source>Uncomitted changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="100"/>
        <source>It seems that you have changes or patches that are not committed to the file.
Do you want to commit them now?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickFilterView</name>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="41"/>
        <source>Quick Filter</source>
        <translation>Быстрая фильтрация</translation>
    </message>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="61"/>
        <source>X</source>
        <translation>X</translation>
    </message>
</context>
<context>
    <name>R2DecDecompiler</name>
    <message>
        <source>Failed to parse JSON from r2dec</source>
        <translation type="vanished">Ошибка парсинга JSON из r2dec</translation>
    </message>
</context>
<context>
    <name>R2PluginsDialog</name>
    <message>
        <source>radare2 plugin information</source>
        <translation type="vanished">сведения о плагине radare2</translation>
    </message>
    <message>
        <source>RBin</source>
        <translation type="vanished">RBin</translation>
    </message>
    <message>
        <source>RBin plugins</source>
        <translation type="vanished">RBin плагины</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Имя</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Описание</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">Лицензия</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Тип</translation>
    </message>
    <message>
        <source>RIO</source>
        <translation type="vanished">RIO</translation>
    </message>
    <message>
        <source>RIO plugins</source>
        <translation type="vanished">RIO плагины</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="vanished">Права доступа</translation>
    </message>
    <message>
        <source>RCore</source>
        <translation type="vanished">RCore</translation>
    </message>
    <message>
        <source>RCore plugins</source>
        <translation type="vanished">RCore плагины</translation>
    </message>
    <message>
        <source>RAsm</source>
        <translation type="vanished">RAsm</translation>
    </message>
    <message>
        <source>RAsm plugins</source>
        <translation type="vanished">RAsm плагины</translation>
    </message>
    <message>
        <source>Architecture</source>
        <translation type="vanished">Архитектура</translation>
    </message>
    <message>
        <source>CPU&apos;s</source>
        <translation type="vanished">CPU&apos;s</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="vanished">Версия</translation>
    </message>
    <message>
        <source>Author</source>
        <translation type="vanished">Автор</translation>
    </message>
    <message>
        <source>Cutter</source>
        <translation type="vanished">Cutter</translation>
    </message>
    <message>
        <source>Cutter plugins</source>
        <translation type="vanished">Cutter плагины</translation>
    </message>
</context>
<context>
    <name>R2TaskDialog</name>
    <message>
        <source>R2 Task</source>
        <translation type="vanished">Задача R2</translation>
    </message>
    <message>
        <source>R2 task in progress..</source>
        <translation type="vanished">Задача R2 в выполняется...</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">Время</translation>
    </message>
    <message>
        <source>Running for</source>
        <translation type="vanished">Выполняется</translation>
    </message>
    <message numerus="yes">
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation type="vanished">
            <numerusform>%n час</numerusform>
            <numerusform>%n часа</numerusform>
            <numerusform>%n часов</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation type="vanished">
            <numerusform>%n минута</numerusform>
            <numerusform>%n минуты</numerusform>
            <numerusform>%n минут</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation type="vanished">
            <numerusform>%n секунда</numerusform>
            <numerusform>%n секунды</numerusform>
            <numerusform>%n секунд</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RawAddrDock</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="500"/>
        <source>Raw</source>
        <translation>Исх. данные</translation>
    </message>
</context>
<context>
    <name>RegisterRefModel</name>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="67"/>
        <source>Register</source>
        <translation>Регистр</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="69"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="71"/>
        <source>Reference</source>
        <translation>Ссылка</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="73"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>RegisterRefsWidget</name>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="137"/>
        <source>Copy register value</source>
        <translation>Скопировать значение регистра</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="138"/>
        <source>Copy register reference</source>
        <translation>Скопировать ссылку регистра</translation>
    </message>
</context>
<context>
    <name>RelocsModel</name>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="54"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="56"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="58"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="60"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>RelocsWidget</name>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="131"/>
        <source>Relocs</source>
        <translation>Релокации</translation>
    </message>
</context>
<context>
    <name>RemoteDebugDialog</name>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="43"/>
        <source>Debugger:</source>
        <translation>Отладчик:</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="82"/>
        <source>IP or Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="159"/>
        <source>Remove item</source>
        <translation type="unfinished">Удалить элемент</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="164"/>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="167"/>
        <source>Remove all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GDB</source>
        <translation type="vanished">GDB</translation>
    </message>
    <message>
        <source>WinDbg - Pipe</source>
        <translation type="vanished">WinDbg - Pipe</translation>
    </message>
    <message>
        <source>IP:</source>
        <translation type="vanished">IP:</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="75"/>
        <source>Port:</source>
        <translation>Порт:</translation>
    </message>
    <message>
        <source>Path:</source>
        <translation type="vanished">Путь:</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="57"/>
        <source>Invalid debugger</source>
        <translation>Неверный отладчик</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="68"/>
        <source>Invalid IP address</source>
        <translation>Некорректный IP-адрес</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="81"/>
        <source>Path does not exist</source>
        <translation>Путь не существует</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="94"/>
        <source>Invalid port</source>
        <translation>Некорректный порт</translation>
    </message>
</context>
<context>
    <name>RenameDialog</name>
    <message>
        <source>Name:</source>
        <translation type="vanished">Имя:</translation>
    </message>
</context>
<context>
    <name>ResourcesModel</name>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="76"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="78"/>
        <source>Vaddr</source>
        <translation>Виртуальный адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="80"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="82"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="84"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="86"/>
        <source>Lang</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="88"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>ResourcesWidget</name>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="118"/>
        <source>Resources</source>
        <translation>Ресурсы</translation>
    </message>
</context>
<context>
    <name>RizinGraphWidget</name>
    <message>
        <location filename="../../widgets/RizinGraphWidget.ui" line="57"/>
        <source>ag...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="21"/>
        <source>Data reference graph (aga)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="22"/>
        <source>Global data references graph (agA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="26"/>
        <source>Imports graph (agi)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="27"/>
        <source>References graph (agr)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="28"/>
        <source>Global references graph (agR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="29"/>
        <source>Cross references graph (agx)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="30"/>
        <source>Custom graph (agg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="31"/>
        <source>User command</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RizinPluginsDialog</name>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="14"/>
        <source>Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="24"/>
        <source>RzBin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="30"/>
        <source>RzBin plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="41"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="82"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="123"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="154"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="46"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="87"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="128"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="174"/>
        <source>Description</source>
        <translation type="unfinished">Описание</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="51"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="92"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="179"/>
        <source>License</source>
        <translation type="unfinished">Лицензия</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="56"/>
        <source>Type</source>
        <translation type="unfinished">Тип</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="65"/>
        <source>RzIO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="71"/>
        <source>RzIO plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="97"/>
        <source>Permissions</source>
        <translation type="unfinished">Права доступа</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="106"/>
        <source>RzCore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="112"/>
        <source>RzCore plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="137"/>
        <source>RzAsm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="143"/>
        <source>RzAsm plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="159"/>
        <source>Architecture</source>
        <translation type="unfinished">Архитектура</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="164"/>
        <source>CPU&apos;s</source>
        <translation type="unfinished">CPU&apos;s</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="169"/>
        <source>Version</source>
        <translation type="unfinished">Версия</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="184"/>
        <source>Author</source>
        <translation type="unfinished">Автор</translation>
    </message>
</context>
<context>
    <name>RizinTaskDialog</name>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="14"/>
        <source>Rizin Task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="20"/>
        <source>Rizin task in progress..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="27"/>
        <source>Time</source>
        <translation type="unfinished">Время</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="32"/>
        <source>Running for</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="34"/>
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="38"/>
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation type="unfinished">
            <numerusform>%n минута</numerusform>
            <numerusform>%n минуты</numerusform>
            <numerusform>%n минут</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="41"/>
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation type="unfinished">
            <numerusform>%n секунда</numerusform>
            <numerusform>%n секунды</numerusform>
            <numerusform>%n секунд</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RunScriptTask</name>
    <message>
        <location filename="../../common/RunScriptTask.cpp" line="18"/>
        <source>Executing script...</source>
        <translation>Выполнение скрипта...</translation>
    </message>
    <message>
        <location filename="../../common/RunScriptTask.h" line="15"/>
        <source>Run Script</source>
        <translation>Запустить скрипт</translation>
    </message>
</context>
<context>
    <name>SaveProjectDialog</name>
    <message>
        <source>Save Project</source>
        <translation type="vanished">Сохранить проект</translation>
    </message>
    <message>
        <source>Project name (prj.name):</source>
        <translation type="vanished">Имя проекта (prj.name):</translation>
    </message>
    <message>
        <source>Projects path (dir.projects):</source>
        <translation type="vanished">Путь проектов (dir.projects):</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Выбрать</translation>
    </message>
    <message>
        <source>Use simple project saving style (prj.simple, recommended)</source>
        <translation type="vanished">Использовать простой стиль сохранения проекта (prj.simple, рекомендуется)</translation>
    </message>
    <message>
        <source>Save the target binary inside the project directory (prj.files)</source>
        <translation type="vanished">Сохранить исследуемый файл внутри директории проекта (prj.files)</translation>
    </message>
    <message>
        <source>Project is a git repo and saving is committing (prj.git)</source>
        <translation type="vanished">Проект представляет собой git репозиторий и сохранение эквивалентно коммиту (prj.git)</translation>
    </message>
    <message>
        <source>Use ZIP format for project files (prj.zip)</source>
        <translation type="vanished">Использовать ZIP формат для проектов (prj.zip)</translation>
    </message>
    <message>
        <source>Select project path (dir.projects)</source>
        <translation type="vanished">Выбрать путь проекта (dir.projects)</translation>
    </message>
    <message>
        <source>Save project</source>
        <translation type="vanished">Сохранить проект</translation>
    </message>
    <message>
        <source>Invalid project name.</source>
        <translation type="vanished">Недопустимое имя проекта.</translation>
    </message>
</context>
<context>
    <name>SdbDock</name>
    <message>
        <source>Key</source>
        <translation type="vanished">Ключ</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Значение</translation>
    </message>
</context>
<context>
    <name>SdbWidget</name>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="17"/>
        <source>SDB Browser</source>
        <translation>Браузер SDB</translation>
    </message>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="120"/>
        <source>Key</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="125"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
</context>
<context>
    <name>SearchModel</name>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="96"/>
        <source>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Предварительный просмотр&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="115"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="117"/>
        <source>Offset</source>
        <translation>Смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="119"/>
        <source>Code</source>
        <translation>Код</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="121"/>
        <source>Data</source>
        <translation>Данные</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="123"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="83"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="90"/>
        <source>Search for:</source>
        <translation>Поиск по:</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="100"/>
        <source>Search in:</source>
        <translation>Искать в:</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="251"/>
        <source>asm code</source>
        <translation>ассемблерный код</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="252"/>
        <source>string</source>
        <translation>строка</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="253"/>
        <source>hex string</source>
        <translation>шестнадцатеричная строка</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="254"/>
        <source>ROP gadgets</source>
        <translation>ROP гаджеты</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="255"/>
        <source>32bit value</source>
        <translation>32bit значение</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="282"/>
        <source>No results found for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="285"/>
        <source>No Results Found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SectionsModel</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="93"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="95"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="101"/>
        <source>Virtual Size</source>
        <translation>Виртуальный размер</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="97"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="99"/>
        <source>End Address</source>
        <translation>Конечный адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="103"/>
        <source>Permissions</source>
        <translation>Права доступа</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="105"/>
        <source>Entropy</source>
        <translation>Энтропия</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="107"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>SegmentsModel</name>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="81"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="83"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="85"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="87"/>
        <source>End Address</source>
        <translation>Конечный адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="89"/>
        <source>Permissions</source>
        <translation>Права доступа</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="91"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>SetFunctionVarTypes</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Диалог</translation>
    </message>
    <message>
        <source>Set Type To:</source>
        <translation type="vanished">Установить тип для:</translation>
    </message>
    <message>
        <source>Set Name To:</source>
        <translation type="vanished">Установить имя для:</translation>
    </message>
    <message>
        <source>Modify:</source>
        <translation type="vanished">Редактировать:</translation>
    </message>
    <message>
        <source>You must be in a function to define variable types.</source>
        <translation type="vanished">Вы должны быть в функции для определения типов переменных.</translation>
    </message>
</context>
<context>
    <name>SetToDataDialog</name>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="14"/>
        <source>Set to Data</source>
        <translation>Отобразить как данные</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="20"/>
        <location filename="../../dialogs/SetToDataDialog.ui" line="34"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="27"/>
        <source>Start address</source>
        <translation>Начальный адрес</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="41"/>
        <source>End address</source>
        <translation>Конечный адрес</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="48"/>
        <source>Item size</source>
        <translation>Размер элемента</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="55"/>
        <source>Number of items</source>
        <translation>Количество элементов</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="62"/>
        <location filename="../../dialogs/SetToDataDialog.ui" line="69"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>SideBar</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Form</translation>
    </message>
    <message>
        <source>Script</source>
        <translation type="vanished">Скрипт</translation>
    </message>
    <message>
        <source>X</source>
        <translation type="vanished">X</translation>
    </message>
    <message>
        <source>example.py</source>
        <translation type="vanished">example.py</translation>
    </message>
    <message>
        <source>Execution finished</source>
        <translation type="vanished">Выполнение завершено</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation type="vanished">Калькулятор</translation>
    </message>
    <message>
        <source>Assembler</source>
        <translation type="obsolete">Assembler</translation>
    </message>
    <message>
        <source>Assembly</source>
        <translation type="vanished">Ассемблер</translation>
    </message>
    <message>
        <source>v</source>
        <translation type="vanished">v</translation>
    </message>
    <message>
        <source>^</source>
        <translation type="vanished">^</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation type="vanished">Шестнадцатеричный</translation>
    </message>
    <message>
        <source>Toogle resposiveness</source>
        <translation type="vanished">Переключение адаптивного интерфейса</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Настройки</translation>
    </message>
</context>
<context>
    <name>SidebarWidget</name>
    <message>
        <source> Function:</source>
        <translation type="vanished"> Функция:</translation>
    </message>
    <message>
        <source>Offset info:</source>
        <translation type="vanished">Информация о смещении:</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="vanished">Информация</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Значение</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Opcode description:</source>
        <translation type="vanished">Описание опкода:</translation>
    </message>
    <message>
        <source>Function registers info:</source>
        <translation type="vanished">Информация о регистрах функции:</translation>
    </message>
    <message>
        <source>X-Refs to current address:</source>
        <translation type="vanished">Перекрестные ссылки к текущему адресу:</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="vanished">Адрес</translation>
    </message>
    <message>
        <source>Instruction</source>
        <translation type="vanished">Инструкция</translation>
    </message>
    <message>
        <source>X-Refs from current address:</source>
        <translation type="vanished">Перекрестные ссылки от текущего адреса:</translation>
    </message>
</context>
<context>
    <name>SimpleTextGraphView</name>
    <message>
        <location filename="../../widgets/SimpleTextGraphView.cpp" line="29"/>
        <source>Copy</source>
        <translation type="unfinished">Копировать</translation>
    </message>
</context>
<context>
    <name>StackModel</name>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="216"/>
        <source>Offset</source>
        <translation>Смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="218"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="220"/>
        <source>Reference</source>
        <translation>Ссылка</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="222"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>StackWidget</name>
    <message>
        <source>Offset</source>
        <translation type="vanished">Смещение</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Значение</translation>
    </message>
    <message>
        <source>Reference</source>
        <translation type="obsolete">Reference</translation>
    </message>
    <message>
        <source>Seek to this offset</source>
        <translation type="vanished">Перейти к этому смещению</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="33"/>
        <source>Edit stack value...</source>
        <translation>Редактирование значение стека...</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="109"/>
        <source>Edit stack at %1</source>
        <translation>Редактирование стек по адресу %1</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="138"/>
        <source>Stack position</source>
        <translation>Позиция в стеке</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="140"/>
        <source>Pointed memory</source>
        <translation>Память по адресу</translation>
    </message>
</context>
<context>
    <name>StringsModel</name>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="67"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="69"/>
        <source>String</source>
        <translation>Строка</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="71"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="73"/>
        <source>Length</source>
        <translation>Длина</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="75"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="77"/>
        <source>Section</source>
        <translation>Раздел</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="79"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>StringsTask</name>
    <message>
        <location filename="../../common/StringsTask.h" line="13"/>
        <source>Searching for Strings</source>
        <translation>Поиск по строкам</translation>
    </message>
</context>
<context>
    <name>StringsWidget</name>
    <message>
        <source>Copy Address</source>
        <translation type="vanished">Копировать адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.ui" line="76"/>
        <source>Copy String</source>
        <translation>Ссылка на строку</translation>
    </message>
    <message>
        <source>Xrefs</source>
        <translation type="vanished">Внешние ссылки</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.ui" line="81"/>
        <source>Filter</source>
        <translation>Фильтр</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="161"/>
        <source>Section:</source>
        <translation>Раздел:</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="244"/>
        <source>(all)</source>
        <translation>(все)</translation>
    </message>
</context>
<context>
    <name>SymbolsModel</name>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="58"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="60"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="62"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="64"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>SymbolsWidget</name>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="123"/>
        <source>Symbols</source>
        <translation>Символы</translation>
    </message>
</context>
<context>
    <name>ThreadsWidget</name>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="24"/>
        <source>PID</source>
        <translation>Идентификатор процесса</translation>
    </message>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="25"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="26"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
</context>
<context>
    <name>TypesInteractionDialog</name>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="23"/>
        <source>Load From File:</source>
        <translation>Загрузить из файла:</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="37"/>
        <source>Select File</source>
        <translation>Выберите файл</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="50"/>
        <source>Enter Types Manually</source>
        <translation>Ввести тип вручную</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="32"/>
        <source>Select file</source>
        <translation>Выберите файл</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="40"/>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="68"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="69"/>
        <source>There was some error while loading new types</source>
        <translation>Произошла ошибка при загрузке новых типов</translation>
    </message>
</context>
<context>
    <name>TypesModel</name>
    <message>
        <source>Type</source>
        <translation type="vanished">Тип</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="62"/>
        <source>Type / Name</source>
        <translation>Тип / название</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="64"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="66"/>
        <source>Format</source>
        <translation>Формат</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="68"/>
        <source>Category</source>
        <translation>Категория</translation>
    </message>
</context>
<context>
    <name>TypesWidget</name>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="76"/>
        <location filename="../../widgets/TypesWidget.ui" line="79"/>
        <source>Export Types</source>
        <translation>Экспортировать типы</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="84"/>
        <location filename="../../widgets/TypesWidget.ui" line="87"/>
        <location filename="../../widgets/TypesWidget.cpp" line="286"/>
        <source>Load New Types</source>
        <translation>Загрузить новые типы</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="92"/>
        <location filename="../../widgets/TypesWidget.ui" line="95"/>
        <source>Delete Type</source>
        <translation>Удалить тип</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="100"/>
        <source>Link Type to Address</source>
        <translation>Тип ссылки на адрес</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="140"/>
        <source>Category</source>
        <translation>Категория</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="185"/>
        <source>View Type</source>
        <translation>Просмотреть тип</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="186"/>
        <source>Edit Type</source>
        <translation>Редактировать тип</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="217"/>
        <source>(All)</source>
        <translation>(Все)</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="266"/>
        <source>Save File</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="273"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="302"/>
        <source>Edit Type: </source>
        <translation>Редактировать тип: </translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="305"/>
        <location filename="../../widgets/TypesWidget.cpp" line="355"/>
        <source>View Type: </source>
        <translation>Просмотреть тип: </translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="305"/>
        <location filename="../../widgets/TypesWidget.cpp" line="355"/>
        <source> (Read Only)</source>
        <translation> (Только чтение)</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="324"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="324"/>
        <source>Are you sure you want to delete &quot;%1&quot;?</source>
        <translation>Вы уверены, что хотите удалить &quot;%1&quot;?</translation>
    </message>
</context>
<context>
    <name>UpdateWorker</name>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="34"/>
        <source>Time limit exceeded during version check. Please check your internet connection and try again.</source>
        <translation>Превышено время проверки версий. Пожалуйста, проверьте подключение к Интернету и повторите попытку.</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="81"/>
        <source>Version control</source>
        <translation>Управление версиями</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="82"/>
        <source>There is an update available for Cutter.&lt;br/&gt;</source>
        <translation>Доступно обновление для Cutter.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="82"/>
        <source>Current version:</source>
        <translation>Текущая версия:</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="83"/>
        <source>Latest version:</source>
        <translation>Последняя версия:</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="85"/>
        <source>For update, please check the link:&lt;br/&gt;</source>
        <translation>Для обновления перейдите по ссылке:&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="89"/>
        <source>or click &quot;Download&quot; to download latest version of Cutter.</source>
        <translation>или нажмите &quot;Загрузить&quot; для загрузки последней версии Cutter.</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="92"/>
        <source>Don&apos;t check for updates</source>
        <translation>Не проверять наличие обновлений</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="96"/>
        <source>Download</source>
        <translation>Загрузить</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="103"/>
        <source>Choose directory for downloading</source>
        <translation>Выберите каталог для загрузки</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="108"/>
        <source>Downloading update...</source>
        <translation>Загрузка обновления...</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="108"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="116"/>
        <source>Download finished!</source>
        <translation>Загрузка завершена!</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="117"/>
        <source>Latest version of Cutter was succesfully downloaded!</source>
        <translation>Последняя версия Cutter была успешно загружена!</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="119"/>
        <source>Open file</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="120"/>
        <source>Open download folder</source>
        <translation>Открыть папку загрузок</translation>
    </message>
</context>
<context>
    <name>VTableModel</name>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="63"/>
        <source>VTable</source>
        <translation>Таблица виртуальных методов</translation>
    </message>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="84"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="86"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
</context>
<context>
    <name>VersionInfoDialog</name>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="50"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="64"/>
        <source>TextLabel</source>
        <translation>Текстовая метка</translation>
    </message>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="94"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="126"/>
        <source>Key</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="99"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="131"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
</context>
<context>
    <name>VirtualAddrDock</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="508"/>
        <source>Virtual</source>
        <translation>Виртуальный</translation>
    </message>
</context>
<context>
    <name>VisualNavbar</name>
    <message>
        <location filename="../../widgets/VisualNavbar.cpp" line="29"/>
        <source>Visual navigation bar</source>
        <translation>Визуальная панель навигации</translation>
    </message>
</context>
<context>
    <name>WelcomeDialog</name>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="20"/>
        <source>Welcome to Cutter</source>
        <translation>Добро пожаловать в Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="83"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="104"/>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="19"/>
        <source>Version </source>
        <translation>Версия </translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="168"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="203"/>
        <source>Native Theme</source>
        <translation>Нативная тема</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="208"/>
        <source>Dark Theme</source>
        <translation>Тёмная тема</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="213"/>
        <source>Midnight Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="218"/>
        <source>Light Theme</source>
        <translation>Светлая тема</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="229"/>
        <source>Check for updates on start</source>
        <translation>Проверять обновления при старте</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="298"/>
        <source>Community</source>
        <translation>Сообщество</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="320"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/cutter_re&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@cutter_re&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/cutter_re&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@cutter_re &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="355"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;/span&gt;&lt;a href=&quot;https://github.com/rizinorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and report bugs or contribute code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Присоединиться к тысячам реверс инженеров в нашем сообществе:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;/span&gt;&lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and report bugs or contribute code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Хотите помочь нам сделать Cutter еще лучше?&lt;br/&gt;Посетите нашу &lt;/span&gt;&lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;страницу Github&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; и сообщайте об ошибках или вносите свой код.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="417"/>
        <source>Continue</source>
        <translation>Продолжить</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;	&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;#cutter on &lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Присоединяйтесь к тысячам реверс-инженеров в наше сообщество:&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;	&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;#cutter on &lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt; and report bugs or contribute code.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Хотите помочь нам сделать Cutter еще лучше?&lt;br/&gt;Посетите страницу &lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Github&lt;/span&gt;&lt;/a&gt; и сообщить об ошибках или внести код.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="394"/>
        <source>Contributing</source>
        <translation>Помощь проекту</translation>
    </message>
    <message>
        <source>Continue 🢒</source>
        <translation type="vanished">Продолжить</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="70"/>
        <source>Language settings</source>
        <translation>Настройки языка</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="71"/>
        <source>Language will be changed after next application start.</source>
        <translation>Язык будет изменен после следующего запуска приложения.</translation>
    </message>
</context>
<context>
    <name>XrefModel</name>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="287"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="291"/>
        <source>Code</source>
        <translation>Код</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="293"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="289"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
</context>
<context>
    <name>XrefsDialog</name>
    <message>
        <source>Address</source>
        <translation type="vanished">Адрес</translation>
    </message>
    <message>
        <source>Code</source>
        <translation type="vanished">Код</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Тип</translation>
    </message>
    <message>
        <source>X-Refs to %1:</source>
        <translation type="vanished">Перекрестные ссылки к %1:</translation>
    </message>
    <message>
        <source>X-Refs from %1:</source>
        <translation type="vanished">Перекрестные ссылки от %1:</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="145"/>
        <source>X-Refs to %1 (%2 results):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="147"/>
        <source>X-Refs from %1 (%2 results):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="152"/>
        <source>Writes to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="153"/>
        <source>Reads from %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="164"/>
        <location filename="../../dialogs/XrefsDialog.cpp" line="183"/>
        <source>X-Refs for %1</source>
        <translation>Перекрестные ссылки для %1</translation>
    </message>
</context>
<context>
    <name>ZignaturesModel</name>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="66"/>
        <source>Offset</source>
        <translation>Смещение</translation>
    </message>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="68"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="70"/>
        <source>Bytes</source>
        <translation>Байтов</translation>
    </message>
</context>
</TS>
