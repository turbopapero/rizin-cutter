<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca" sourcelanguage="en">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="14"/>
        <source>About Cutter</source>
        <translation>Quant a Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="69"/>
        <source>Check for updates on start</source>
        <translation>Verifica si hi ha noves actualitzacions a l&apos;obrir l&apos;aplicació</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="95"/>
        <source>Show version information</source>
        <translation>Mostrar informació sobre la versió</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="108"/>
        <source>Show Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by Rizin&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show radare2 plugin information</source>
        <translation type="vanished">Mostra la informació dels complements</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by radare2&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by radare2&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="128"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:28pt; font-weight:600;&quot;&gt;Cutter&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:28pt; font-weight:600;&quot;&gt;Cutter&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Show plugin information</source>
        <translation type="vanished">Mostrar informació de les extensions</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="82"/>
        <source>Check for updates</source>
        <translation>Buscar actualitzacions</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.ui" line="29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="28"/>
        <source>Version</source>
        <translation>Versió</translation>
    </message>
    <message>
        <source>Using r2-</source>
        <translation type="vanished">Utilitzant r2-</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="28"/>
        <source>Using rizin </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="30"/>
        <source>Optional Features:</source>
        <translation>Funcionalitats Opcionals:</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="47"/>
        <source>License</source>
        <translation>Llicència</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="48"/>
        <source>This Software is released under the GNU General Public License v3.0</source>
        <translation>Aquest programari és publica sota la llicència Publica General de GNU (GPLv3)</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="49"/>
        <source>Authors</source>
        <translation>Autors</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="50"/>
        <source>Cutter is developed by the community and maintained by its core and development teams.&lt;br/&gt;</source>
        <translation type="unfinished">Cutter is developed by the community and maintained by its core and development teams.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="52"/>
        <source>Check our &lt;a href=&apos;https://github.com/rizinorg/cutter/graphs/contributors&apos;&gt;contributors page&lt;/a&gt; for the full list of contributors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="76"/>
        <source>Rizin version information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check our &lt;a href=&apos;https://github.com/radareorg/cutter/graphs/contributors&apos;&gt;contributors page&lt;/a&gt; for the full list of contributors.</source>
        <translation type="obsolete">Check our &lt;a href=&apos;https://github.com/radareorg/cutter/graphs/contributors&apos;&gt;contributors page&lt;/a&gt; for the full list of contributors.</translation>
    </message>
    <message>
        <source>radare2 version information</source>
        <translation type="vanished">informació sobre la versió de radare2</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="99"/>
        <source>Checking for updates...</source>
        <translation>Buscant actualitzacions...</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="109"/>
        <source>Cutter is up to date!</source>
        <translation>Cutter està actualitzat!</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="150"/>
        <source>Based on Qt %1 (%2, %3 bit)</source>
        <translation type="unfinished">Based on Qt %1 (%2, %3 bit)</translation>
    </message>
    <message>
        <source>Timeout error!</source>
        <translation type="vanished">El temps d&apos;espera s&apos;ha esgotat!</translation>
    </message>
    <message>
        <source>Please check your internet connection and try again.</source>
        <translation type="vanished">Comprova la teva connexió a Internet i torna-ho a provar.</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="105"/>
        <source>Error!</source>
        <translation>Error!</translation>
    </message>
    <message>
        <location filename="../../dialogs/AboutDialog.cpp" line="108"/>
        <source>Version control</source>
        <translation>Control de versions</translation>
    </message>
    <message>
        <source>You have latest version and no need to update!</source>
        <translation type="vanished">Estas a l&apos;ultima versió, no cal que actualitzis!</translation>
    </message>
    <message>
        <source>Current version:</source>
        <translation type="vanished">Versió actual:</translation>
    </message>
    <message>
        <source>Latest version:</source>
        <translation type="vanished">Última versió:</translation>
    </message>
    <message>
        <source>For update, please check the link:</source>
        <translation type="vanished">Per actualitzar, premeu aquest enllaç:</translation>
    </message>
</context>
<context>
    <name>AddressableDockWidget</name>
    <message>
        <location filename="../../widgets/AddressableDockWidget.cpp" line="12"/>
        <source>Sync/unsync offset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddressableItemContextMenu</name>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="16"/>
        <source>Show in</source>
        <translation type="unfinished">Show in</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="17"/>
        <source>Copy address</source>
        <translation>Copia adreça</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="18"/>
        <source>Show X-Refs</source>
        <translation>Mostra X-Refs</translation>
    </message>
    <message>
        <location filename="../../menus/AddressableItemContextMenu.cpp" line="19"/>
        <source>Add comment</source>
        <translation>Afegeix un comentari</translation>
    </message>
</context>
<context>
    <name>AnalClassesModel</name>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="403"/>
        <source>class</source>
        <translation>classe</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="427"/>
        <source>base</source>
        <translation>base</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="458"/>
        <source>method</source>
        <translation>mètode</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="494"/>
        <source>vtable</source>
        <translation>vtable</translation>
    </message>
</context>
<context>
    <name>AnalOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="20"/>
        <source>Analysis</source>
        <translation type="unfinished">Anàlisi</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="55"/>
        <source>Show verbose information when performing analysis (analysis.verbose)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="68"/>
        <source>Analyze push+ret as jmp (analysis.pushret)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="81"/>
        <source>Verbose output from type analysis (analysis.types.verbose)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="94"/>
        <source>Speculatively set a name for the functions (analysis.autoname)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="107"/>
        <source>Search for new functions following already defined functions (analysis.hasnext)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="120"/>
        <source>Create references for unconditional jumps (analysis.jmp.ref)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="133"/>
        <source>Analyze jump tables in switch statements (analysis.jmp.tbl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="161"/>
        <source>Search boundaries for analysis (analysis.in): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="195"/>
        <source>Pointer depth (analysis.ptrdepth):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="236"/>
        <source>Functions Prelude (analysis.prelude):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AnalOptionsWidget.ui" line="269"/>
        <source>Analyze program</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AnalTask</name>
    <message>
        <location filename="../../common/AnalTask.cpp" line="26"/>
        <source>Analyzing Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="43"/>
        <source>Loading the file...</source>
        <translation>Carregant el fitxer...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="69"/>
        <source>Loading PDB file...</source>
        <translation>Carregant el fitxer PDB...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="78"/>
        <source>Loading shellcode...</source>
        <translation>Carregant shellcode...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="89"/>
        <source>Executing script...</source>
        <translation>Executant intèrpret...</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="98"/>
        <source>Executing analysis...</source>
        <translation>Executant anàlisis...</translation>
    </message>
    <message>
        <source>Analyzing...</source>
        <translation type="vanished">Analitzant...</translation>
    </message>
    <message>
        <source>Running</source>
        <translation type="vanished">Executant</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="107"/>
        <source>Analysis complete!</source>
        <translation>L&apos;anàlisi ha finalitzat!</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="109"/>
        <source>Skipping Analysis.</source>
        <translation>Ometent l&apos;anàlisi.</translation>
    </message>
    <message>
        <location filename="../../common/AnalTask.cpp" line="24"/>
        <source>Initial Analysis</source>
        <translation>Anàlisi inicial</translation>
    </message>
</context>
<context>
    <name>AppearanceOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="14"/>
        <source>Appearance</source>
        <translation>Aparença</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="28"/>
        <source>Font:</source>
        <translation>Tipografia:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="56"/>
        <source>Select font</source>
        <translation>Seleccionar tipografia</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="76"/>
        <source>Zoom</source>
        <translation>Amplia</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="95"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="116"/>
        <source>Language:</source>
        <translation>Llengua:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="133"/>
        <source>Interface Theme:</source>
        <translation type="unfinished">Interface Theme:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="160"/>
        <source>Color Theme:</source>
        <translation type="unfinished">Color Theme:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="179"/>
        <source>Edit Theme</source>
        <translation type="unfinished">Edit Theme</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="193"/>
        <source>Rename</source>
        <translation>Reanomena</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="241"/>
        <source>Export</source>
        <translation>Exporta</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="255"/>
        <source>Import</source>
        <translation>Importa</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="289"/>
        <source>Use information provided by decompiler when highlighting code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="292"/>
        <source>Decompiler based highlighting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Qt Theme:</source>
        <translation type="vanished">Tema de Qt:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="147"/>
        <source>Default</source>
        <translation>Per Defecte</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="152"/>
        <source>Dark</source>
        <translation>Fosc</translation>
    </message>
    <message>
        <source>Color Theme</source>
        <translation type="vanished">Tema de Colors</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="213"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="227"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="163"/>
        <source>Delete</source>
        <translation>Esborrar</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Idioma</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.ui" line="312"/>
        <source>Save as Default</source>
        <translation>Desar per Defecte</translation>
    </message>
    <message>
        <source>Enter scheme name</source>
        <translation type="vanished">Introdueix un nom per l&apos;esquema</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="136"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="221"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Are you sure you want to delete theme </source>
        <translation type="vanished">Vols eliminar aquest tema? </translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="125"/>
        <source>Theme Editor - &lt;%1&gt;</source>
        <translation type="unfinished">Theme Editor - &lt;%1&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="136"/>
        <source>Enter theme name</source>
        <translation>Introdueix el nom del tema</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="137"/>
        <source> - copy</source>
        <translation> - copia</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="143"/>
        <source>Theme Copy</source>
        <translation>Copia el tema</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="144"/>
        <source>Theme named %1 already exists.</source>
        <translation type="unfinished">Theme named %1 already exists.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="159"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="168"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="190"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="214"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="229"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="163"/>
        <source>Are you sure you want to delete &lt;b&gt;%1&lt;/b&gt;?</source>
        <translation type="unfinished">Are you sure you want to delete &lt;b&gt;%1&lt;/b&gt;?</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="185"/>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="211"/>
        <source>Success</source>
        <translation>Éxit</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="186"/>
        <source>Color theme &lt;b&gt;%1&lt;/b&gt; was successfully imported.</source>
        <translation type="unfinished">Color theme &lt;b&gt;%1&lt;/b&gt; was successfully imported.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="212"/>
        <source>Color theme &lt;b&gt;%1&lt;/b&gt; was successfully exported.</source>
        <translation type="unfinished">Color theme &lt;b&gt;%1&lt;/b&gt; was successfully exported.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="221"/>
        <source>Enter new theme name</source>
        <translation type="unfinished">Enter new theme name</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="240"/>
        <source>Language settings</source>
        <translation>Configuracions d&apos;idioma</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="241"/>
        <source>Language will be changed after next application start.</source>
        <translation>L&apos;idioma canviarà quan tornis a obrir l&apos;aplicació.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AppearanceOptionsWidget.cpp" line="245"/>
        <source>Cannot set language, not found in available ones</source>
        <translation type="unfinished">Cannot set language, not found in available ones</translation>
    </message>
</context>
<context>
    <name>AsmOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="14"/>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="65"/>
        <source>Disassembly</source>
        <translation>Desensamblar</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="35"/>
        <source>Style</source>
        <translation>Estil</translation>
    </message>
    <message>
        <source>Show ESIL instead of assembly (asm.esil)</source>
        <translation type="vanished">Mostrar ESIL enlloc de l&apos;ensamblador (asm.esil)</translation>
    </message>
    <message>
        <source>Show pseudocode instead of assembly (asm.pseudo)</source>
        <translation type="vanished">Mostrar pseudo-codi enlloc de l&apos;ensamblador (asm.pseudo)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="170"/>
        <source>Show offsets (asm.offset)</source>
        <translation>Mostrar adreces (asm.offset)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="71"/>
        <source>Display the bytes of each instruction (asm.bytes)</source>
        <translation type="unfinished">Display the bytes of each instruction (asm.bytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="291"/>
        <source>Comments</source>
        <translation type="unfinished">Comments</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="299"/>
        <source>Show opcode description (asm.describe)</source>
        <translation>Descriure les instruccions de l&apos;ensamblador (asm.describe)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="78"/>
        <source>Syntax (asm.syntax):</source>
        <translation>Sintaxi (asm.syntax):</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="198"/>
        <source>Lowercase</source>
        <translation>Tot Minúscules</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="203"/>
        <source>Uppercase (asm.ucase)</source>
        <translation>Tot Majúscules (asm.ucase)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="208"/>
        <source>Capitalize (asm.capitalize)</source>
        <translation>Capitalitzar (asm.capitalize)</translation>
    </message>
    <message>
        <source>Separate bytes with whitespace (asm.bytespace)</source>
        <translation type="vanished">Separar els bytes amb espais (asm.bytespace)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="121"/>
        <source>Indent disassembly based on reflines depth (asm.indent)</source>
        <translation type="unfinished">Indent disassembly based on reflines depth (asm.indent)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="98"/>
        <source>Show Disassembly as:</source>
        <translation type="unfinished">Show Disassembly as:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="190"/>
        <source>Show empty line after every basic block (asm.bb.line)</source>
        <translation type="unfinished">Show empty line after every basic block (asm.bb.line)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="152"/>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="307"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="157"/>
        <source>ESIL (asm.esil)</source>
        <translation type="unfinished">ESIL (asm.esil)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="162"/>
        <source>Pseudocode (asm.pseudo)</source>
        <translation>Pseudocodi (asm.pseudo)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="216"/>
        <source>Align bytes to the left (asm.lbytes)</source>
        <translation>Alinear els bytes a l&apos;esquerra (asm.lbytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="223"/>
        <source>Separate bytes with whitespace (asm.bytes.space)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="230"/>
        <source>Display flags&apos; real name (asm.flags.real)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="259"/>
        <source>Show offsets relative to:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="266"/>
        <source>Functions (asm.reloff)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="275"/>
        <source>Flags (asm.reloff.flags)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="312"/>
        <source>Above instructions</source>
        <translation type="unfinished">Above instructions</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="317"/>
        <source>Off</source>
        <translation type="unfinished">Off</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="325"/>
        <source>Show comments:</source>
        <translation type="unfinished">Show comments:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="475"/>
        <source>Substitute variables (asm.sub.var)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="482"/>
        <source>Substitute entire variable expressions with names (asm.sub.varonly)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="237"/>
        <source>Tabs in assembly (asm.tabs):</source>
        <translation>Tabular les instruccions i els operands (asm.tabs):</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="177"/>
        <source>Tabs before assembly (asm.tabs.off):</source>
        <translation>Quanta tabulació (asm.tabs.off):</translation>
    </message>
    <message>
        <source>Show empty line after every basic block (asm.bbline)</source>
        <translation type="vanished">Separar els blocks amb una linea en blanc (asm.bbline)</translation>
    </message>
    <message>
        <source>Show comments at right of assembly (asm.cmt.right)</source>
        <translation type="vanished">Mostrar comentaris a la dreta del desensamblat (asm.cmt.right)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="345"/>
        <source>Column to align comments (asm.cmt.col):</source>
        <translation>Columna per alinear comentaris (asm.cmt.col):</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="355"/>
        <source>Show x-refs (asm.xrefs)</source>
        <translation type="unfinished">Show x-refs (asm.xrefs)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="362"/>
        <source>Show refpointer information (asm.refptr)</source>
        <translation type="unfinished">Show refpointer information (asm.refptr)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="395"/>
        <source>Metadata</source>
        <translation>Metadades</translation>
    </message>
    <message>
        <source>Show stack pointer (asm.stackptr)</source>
        <translation type="vanished">Mostrar el punter de la pila (asm.stackptr)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="419"/>
        <source>Slow Analysis (asm.slow)</source>
        <translation>Anàlisi lent (asm.slow)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="426"/>
        <source>Show jump lines (asm.lines)</source>
        <translation>Mostrar línies de salt (asm.lines)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="433"/>
        <source>Show function boundary lines (asm.lines.fcn)</source>
        <translation>Mostra les línies de límits de funció (asm.lines.fcn)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="440"/>
        <source>Show offset before flags (asm.flags.off)</source>
        <translation>Mostrar adreça abans de les flags (asm.flags.off)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="447"/>
        <source>Run ESIL emulation analysis (asm.emu)</source>
        <translation>Executar l&apos;anàlisi d&apos;emulació ESIL (asm.emu)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="454"/>
        <source>Show only strings if any in the asm.emu output (emu.str)</source>
        <translation type="unfinished">Show only strings if any in the asm.emu output (emu.str)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="461"/>
        <source>Show size of opcodes in disassembly (asm.size)</source>
        <translation>Mostra la mida dels codis d&apos;instrucció al desemsamblat (asm.size)</translation>
    </message>
    <message>
        <source>Show bytes (asm.bytes)</source>
        <translation type="vanished">Mostra els bytes (asm.bytes)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="468"/>
        <source>Show variables summary instead of full list (asm.var.summary)</source>
        <translation>Mostra un resum de les variables enlloc de la llista completa (asm.var.summary)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="111"/>
        <source>Number of bytes to display (asm.nbytes):</source>
        <translation>Nombre de bytes a mostrar (asm.nbytes):</translation>
    </message>
    <message>
        <source>Substitute variables (asm.var.sub)</source>
        <translation type="vanished">Substituir variables (asm.var.sub)</translation>
    </message>
    <message>
        <source>Substitute entire variable expressions with names (asm.var.subonly)</source>
        <translation type="vanished">Substituir completament les expressions de variable amb noms (asm.var.subonly)</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/AsmOptionsWidget.ui" line="538"/>
        <source>Save as Default</source>
        <translation>Guardar com a valor per defecte</translation>
    </message>
</context>
<context>
    <name>AsyncTaskDialog</name>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.ui" line="14"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.ui" line="20"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="43"/>
        <source>Running for</source>
        <translation>Executant per</translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="45"/>
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation type="unfinished">
            <numerusform>%n hores</numerusform>
            <numerusform>%n hour</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="49"/>
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation type="unfinished">
            <numerusform>%n minuts</numerusform>
            <numerusform>%n minute</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/AsyncTaskDialog.cpp" line="52"/>
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation type="unfinished">
            <numerusform>%n segons</numerusform>
            <numerusform>%n seconds</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>AttachProcDialog</name>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="14"/>
        <source>Select process to attach...</source>
        <translation>Selecciona un procés al que enganxar-se...</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="46"/>
        <source>Processes with same name as currently open file:</source>
        <translation>Processos amb el mateix nom que el fitxer actual:</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="94"/>
        <source>All processes:</source>
        <translation>Tots els processos:</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.ui" line="138"/>
        <source>Quick Filter</source>
        <translation>Filtre ràpid</translation>
    </message>
</context>
<context>
    <name>BacktraceWidget</name>
    <message>
        <source>Func Name</source>
        <translation type="vanished">Nom de la funció</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="16"/>
        <source>Function</source>
        <translation>Funció</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="19"/>
        <source>Description</source>
        <translation>Descripció</translation>
    </message>
    <message>
        <location filename="../../widgets/BacktraceWidget.cpp" line="20"/>
        <source>Frame Size</source>
        <translation>Mida de la tramesa</translation>
    </message>
</context>
<context>
    <name>Base64EnDecodedWriteDialog</name>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="14"/>
        <source>Base64 Encode/Decode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="24"/>
        <source>String:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="38"/>
        <source>Decode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/Base64EnDecodedWriteDialog.ui" line="48"/>
        <source>Encode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BinClassesModel</name>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="121"/>
        <source>method</source>
        <translation>mètode</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="145"/>
        <source>field</source>
        <translation>camp</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="167"/>
        <source>base class</source>
        <translation type="unfinished">base class</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="187"/>
        <source>class</source>
        <translation>classe</translation>
    </message>
</context>
<context>
    <name>BreakpointModel</name>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="64"/>
        <source>HW %1</source>
        <translation type="unfinished">HW %1</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="66"/>
        <source>SW</source>
        <translation type="unfinished">SW</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="101"/>
        <source>Offset</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="103"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="105"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="109"/>
        <source>Enabled</source>
        <translation>Actiu</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="111"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="vanished">Permisos</translation>
    </message>
    <message>
        <source>Hardware bp</source>
        <translation type="vanished">Punts d&apos;interrupció de maquinari</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="107"/>
        <source>Tracing</source>
        <translation>Rastreig</translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="vanished">Actiu</translation>
    </message>
</context>
<context>
    <name>BreakpointWidget</name>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="58"/>
        <source>Add new breakpoint</source>
        <translation>Afegir un nou punt d&apos;interrupció</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="65"/>
        <location filename="../../widgets/BreakpointWidget.cpp" line="196"/>
        <source>Delete breakpoint</source>
        <translation>Eliminar un punt d&apos;interrupció</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.ui" line="72"/>
        <source>Delete all breakpoints</source>
        <translation>Suprimeix tots els punts d&apos;interrupció</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="202"/>
        <source>Toggle breakpoint</source>
        <translation>Commuta el punt d&apos;interrupció</translation>
    </message>
    <message>
        <location filename="../../widgets/BreakpointWidget.cpp" line="208"/>
        <source>Edit</source>
        <translation>Edita</translation>
    </message>
</context>
<context>
    <name>BreakpointsDialog</name>
    <message>
        <source>Add breakpoints</source>
        <translation type="vanished">Afegir punts d&apos;interrupció</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="14"/>
        <source>Add/Edit breakpoint</source>
        <translation>Afegeix/edita un un d&apos;interrupció</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="22"/>
        <source>Position</source>
        <translation>Posició</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="59"/>
        <source>Condition</source>
        <translation>Condició</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="91"/>
        <source>?v $.rax-0x6  # break when rax is 6</source>
        <translation type="unfinished">?v $.rax-0x6  # break when rax is 6</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="99"/>
        <source>Module</source>
        <translation>Mòdul</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="126"/>
        <source>Type/Options</source>
        <translation type="unfinished">Type/Options</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="132"/>
        <source>Enabled</source>
        <translation>Actiu</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="142"/>
        <source>Software</source>
        <translation type="unfinished">Software</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="152"/>
        <source>Hardware</source>
        <translation type="unfinished">Hardware</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="180"/>
        <source>Read</source>
        <translation>Lectura</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="187"/>
        <source>Write</source>
        <translation>Escriptura</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="194"/>
        <source>Execute</source>
        <translation type="unfinished">Execute</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="206"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="217"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="222"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="227"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="232"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="261"/>
        <source>Action</source>
        <translation type="unfinished">Action</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="267"/>
        <source>Trace</source>
        <translation type="unfinished">Trace</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.ui" line="276"/>
        <source>Command</source>
        <translation type="unfinished">Command</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="21"/>
        <source>Edit breakpoint</source>
        <translation type="unfinished">Edit breakpoint</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="23"/>
        <source>New breakpoint</source>
        <translation type="unfinished">New breakpoint</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="32"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="32"/>
        <source>Address or expression calculated when creating breakpoint</source>
        <translation type="unfinished">Address or expression calculated when creating breakpoint</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="34"/>
        <source>Named</source>
        <translation type="unfinished">Named</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="34"/>
        <source>Expression - stored as expression</source>
        <translation type="unfinished">Expression - stored as expression</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="35"/>
        <source>Module offset</source>
        <translation type="unfinished">Module offset</translation>
    </message>
    <message>
        <location filename="../../dialogs/BreakpointsDialog.cpp" line="35"/>
        <source>Offset relative to module</source>
        <translation type="unfinished">Offset relative to module</translation>
    </message>
</context>
<context>
    <name>CallGraphWidget</name>
    <message>
        <location filename="../../widgets/CallGraph.cpp" line="23"/>
        <source>Global Callgraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CallGraph.cpp" line="23"/>
        <source>Callgraph</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClassesModel</name>
    <message>
        <source>method</source>
        <translation type="vanished">mètode</translation>
    </message>
    <message>
        <source>field</source>
        <translation type="vanished">camp</translation>
    </message>
    <message>
        <source>class</source>
        <translation type="vanished">classe</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="19"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="21"/>
        <source>Type</source>
        <translation>Tipus</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="23"/>
        <source>Offset</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="25"/>
        <source>VTable</source>
        <translation type="unfinished">VTable</translation>
    </message>
</context>
<context>
    <name>ClassesWidget</name>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="79"/>
        <source>Source:</source>
        <translation>Codi:</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="93"/>
        <source>Binary Info (Fixed)</source>
        <translation>Informació del binari (arreglada)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="98"/>
        <source>Analysis (Editable)</source>
        <translation type="unfinished">Analysis (Editable)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="109"/>
        <source>Seek to VTable</source>
        <translation type="unfinished">Seek to VTable</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="114"/>
        <source>Edit Method</source>
        <translation type="unfinished">Edit Method</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="119"/>
        <source>Add Method</source>
        <translation type="unfinished">Add Method</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="124"/>
        <location filename="../../widgets/ClassesWidget.cpp" line="743"/>
        <source>Create new Class</source>
        <translation type="unfinished">Create new Class</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="129"/>
        <source>Rename Class</source>
        <translation type="unfinished">Rename Class</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.ui" line="134"/>
        <location filename="../../widgets/ClassesWidget.cpp" line="759"/>
        <source>Delete Class</source>
        <translation type="unfinished">Delete Class</translation>
    </message>
    <message>
        <source>Flags (Editable)</source>
        <translation type="vanished">Indicadors (Editable)</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="695"/>
        <source>Missing VTable in class</source>
        <translation type="unfinished">Missing VTable in class</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="696"/>
        <source>The class %1 does not have any VTable!</source>
        <translation type="unfinished">The class %1 does not have any VTable!</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="743"/>
        <source>Class Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="778"/>
        <source>Class name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Class Name</source>
        <translation type="obsolete">Class Name</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="760"/>
        <source>Are you sure you want to delete the class %1?</source>
        <translation type="unfinished">Are you sure you want to delete the class %1?</translation>
    </message>
    <message>
        <location filename="../../widgets/ClassesWidget.cpp" line="777"/>
        <source>Rename Class %1</source>
        <translation type="unfinished">Rename Class %1</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="87"/>
        <source>Val:</source>
        <translation type="unfinished">Val:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="118"/>
        <source>Sat:</source>
        <translation type="unfinished">Sat:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="149"/>
        <source>Hue:</source>
        <translation type="unfinished">Hue:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="184"/>
        <source>Red:</source>
        <translation type="unfinished">Red:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="215"/>
        <source>Green:</source>
        <translation type="unfinished">Green:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="246"/>
        <source>Blue:</source>
        <translation type="unfinished">Blue:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="281"/>
        <source>Hex:</source>
        <translation type="unfinished">Hex:</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="288"/>
        <source>\#HHHHHH</source>
        <translation type="unfinished">\#HHHHHH</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorPicker.ui" line="312"/>
        <source>Pick color from screen</source>
        <translation type="unfinished">Pick color from screen</translation>
    </message>
</context>
<context>
    <name>ColorSchemeFileSaver</name>
    <message>
        <source>Standard themes not found!</source>
        <translation type="vanished">Els temes per defecte no s&apos;han pogut trobar!</translation>
    </message>
    <message>
        <source>The radare2 standard themes could not be found! This probably means radare2 is not properly installed. If you think it is open an issue please.</source>
        <translation type="vanished">No s&apos;han pogut trobar els temes per defecte de radare2! Verifica la teva instal·lació de radare2.</translation>
    </message>
</context>
<context>
    <name>ColorSchemePrefWidget</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Formulari</translation>
    </message>
    <message>
        <source>Set Default</source>
        <translation type="vanished">Per Defecte</translation>
    </message>
</context>
<context>
    <name>ColorThemeEditDialog</name>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Quadre de diàleg</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="26"/>
        <source>Color Theme:</source>
        <translation type="unfinished">Color Theme:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.ui" line="55"/>
        <source>Search</source>
        <translation type="unfinished">Search</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="35"/>
        <source>Disassembly Preview</source>
        <translation type="unfinished">Disassembly Preview</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="79"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="93"/>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="144"/>
        <source>Unsaved changes</source>
        <translation>Canvis no desats</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="94"/>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="145"/>
        <source>Are you sure you want to exit without saving? All changes will be lost.</source>
        <translation>Segur que en voleu sortir sense desar? Tots els canvis es perdràn.</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/ColorThemeEditDialog.cpp" line="159"/>
        <source>Theme Editor - &lt;%1&gt;</source>
        <translation type="unfinished">Theme Editor - &lt;%1&gt;</translation>
    </message>
</context>
<context>
    <name>ColorThemeWorker</name>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="45"/>
        <source>Standard themes not found</source>
        <translation type="unfinished">Standard themes not found</translation>
    </message>
    <message>
        <source>The radare2 standard themes could not be found. Most likely, radare2 is not properly installed.</source>
        <translation type="obsolete">The radare2 standard themes could not be found. Most likely, radare2 is not properly installed.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="74"/>
        <location filename="../../common/ColorThemeWorker.cpp" line="202"/>
        <source>Theme &lt;b&gt;%1&lt;/b&gt; does not exist.</source>
        <translation type="unfinished">Theme &lt;b&gt;%1&lt;/b&gt; does not exist.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="84"/>
        <source>The file &lt;b&gt;%1&lt;/b&gt; cannot be opened.</source>
        <translation type="unfinished">The file &lt;b&gt;%1&lt;/b&gt; cannot be opened.</translation>
    </message>
    <message>
        <source>You can not delete standard radare2 color themes.</source>
        <translation type="obsolete">You can not delete standard radare2 color themes.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="46"/>
        <source>The Rizin standard themes could not be found in &apos;%1&apos;. Most likely, Rizin is not properly installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="199"/>
        <source>You can not delete standard Rizin color themes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="207"/>
        <source>You have no permission to write to &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="unfinished">You have no permission to write to &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="210"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; can not be opened.</source>
        <translation type="unfinished">File &lt;b&gt;%1&lt;/b&gt; can not be opened.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="213"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; can not be removed.</source>
        <translation type="unfinished">File &lt;b&gt;%1&lt;/b&gt; can not be removed.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="222"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; does not exist.</source>
        <translation type="unfinished">File &lt;b&gt;%1&lt;/b&gt; does not exist.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="228"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; could not be opened. Please make sure you have access to it and try again.</source>
        <translation type="unfinished">File &lt;b&gt;%1&lt;/b&gt; could not be opened. Please make sure you have access to it and try again.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="232"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; is not a Cutter color theme</source>
        <translation type="unfinished">File &lt;b&gt;%1&lt;/b&gt; is not a Cutter color theme</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="237"/>
        <source>A color theme named &lt;b&gt;%1&lt;/b&gt; already exists.</source>
        <translation type="unfinished">A color theme named &lt;b&gt;%1&lt;/b&gt; already exists.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="243"/>
        <source>Error occurred during importing. Please make sure you have an access to the directory &lt;b&gt;%1&lt;/b&gt; and try again.</source>
        <translation type="unfinished">Error occurred during importing. Please make sure you have an access to the directory &lt;b&gt;%1&lt;/b&gt; and try again.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="253"/>
        <source>A color theme named &lt;b&gt;&quot;%1&quot;&lt;/b&gt; already exists.</source>
        <translation type="unfinished">A color theme named &lt;b&gt;&quot;%1&quot;&lt;/b&gt; already exists.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="257"/>
        <source>You can not rename standard Rizin themes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can not rename standard radare2 themes.</source>
        <translation type="obsolete">You can not rename standard radare2 themes.</translation>
    </message>
    <message>
        <location filename="../../common/ColorThemeWorker.cpp" line="263"/>
        <source>Something went wrong during renaming. Please make sure you have access to the directory &lt;b&gt;&quot;%1&quot;&lt;/b&gt;.</source>
        <translation type="unfinished">Something went wrong during renaming. Please make sure you have access to the directory &lt;b&gt;&quot;%1&quot;&lt;/b&gt;.</translation>
    </message>
</context>
<context>
    <name>ComboQuickFilterView</name>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="32"/>
        <source>Quick Filter</source>
        <translation>Filtre ràpid</translation>
    </message>
    <message>
        <location filename="../../widgets/ComboQuickFilterView.ui" line="39"/>
        <source>TextLabel</source>
        <translation type="unfinished">TextLabel</translation>
    </message>
</context>
<context>
    <name>CommandTask</name>
    <message>
        <location filename="../../common/CommandTask.h" line="23"/>
        <source>Running Command</source>
        <translation>Executar Comanda</translation>
    </message>
</context>
<context>
    <name>CommentsDialog</name>
    <message>
        <location filename="../../dialogs/CommentsDialog.ui" line="14"/>
        <source>Comment</source>
        <translation>Comentari</translation>
    </message>
    <message>
        <location filename="../../dialogs/CommentsDialog.cpp" line="43"/>
        <source>Add Comment at %1</source>
        <translation type="unfinished">Add Comment at %1</translation>
    </message>
    <message>
        <location filename="../../dialogs/CommentsDialog.cpp" line="45"/>
        <source>Edit Comment at %1</source>
        <translation type="unfinished">Edit Comment at %1</translation>
    </message>
</context>
<context>
    <name>CommentsModel</name>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="157"/>
        <source>Function/Offset</source>
        <translation>Funció/Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="159"/>
        <location filename="../../widgets/CommentsWidget.cpp" line="170"/>
        <source>Comment</source>
        <translation>Comentari</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="166"/>
        <source>Offset</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="168"/>
        <source>Function</source>
        <translation>Funció</translation>
    </message>
</context>
<context>
    <name>CommentsWidget</name>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="236"/>
        <source>Horizontal</source>
        <translation>Horitzontal</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="239"/>
        <source>Comments</source>
        <translation type="unfinished">Comments</translation>
    </message>
    <message>
        <source>Horizontal view</source>
        <translation type="vanished">Vista Horitzontal</translation>
    </message>
    <message>
        <location filename="../../widgets/CommentsWidget.cpp" line="237"/>
        <source>Vertical</source>
        <translation>Vertical</translation>
    </message>
    <message>
        <source>Vertical view</source>
        <translation type="vanished">Vista Vertical</translation>
    </message>
</context>
<context>
    <name>Configuration</name>
    <message>
        <location filename="../../common/Configuration.cpp" line="138"/>
        <source>Critical!</source>
        <translation type="unfinished">Critical!</translation>
    </message>
    <message>
        <location filename="../../common/Configuration.cpp" line="139"/>
        <source>!!! Settings are not writable! Make sure you have a write access to &quot;%1&quot;</source>
        <translation type="unfinished">!!! Settings are not writable! Make sure you have a write access to &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>ConsoleWidget</name>
    <message>
        <source>R2 Console</source>
        <translation type="obsolete">R2 Console</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="93"/>
        <source>Rizin Console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="98"/>
        <source>Debugee Input</source>
        <translation type="unfinished">Debugee Input</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="115"/>
        <source> Type &quot;?&quot; for help</source>
        <translation> Escriu &quot;?&quot; per l&apos;ajuda</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="137"/>
        <source> Enter input for the debugee</source>
        <translation type="unfinished"> Enter input for the debugee</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="153"/>
        <source>Execute command</source>
        <translation>Executar comanda</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.ui" line="159"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.cpp" line="75"/>
        <source>Clear Output</source>
        <translation>Sortida en pla</translation>
    </message>
    <message>
        <location filename="../../widgets/ConsoleWidget.cpp" line="84"/>
        <source>Wrap Lines</source>
        <translation type="unfinished">Wrap Lines</translation>
    </message>
</context>
<context>
    <name>CutterCore</name>
    <message>
        <location filename="../../core/Cutter.cpp" line="1717"/>
        <source>Starting native debug...</source>
        <translation type="unfinished">Starting native debug...</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1760"/>
        <source>Starting emulation...</source>
        <translation type="unfinished">Starting emulation...</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1814"/>
        <source>Connecting to: </source>
        <translation type="unfinished">Connecting to: </translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="1853"/>
        <source>Attaching to process (</source>
        <translation type="unfinished">Attaching to process (</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2209"/>
        <source>Creating debug tracepoint...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2245"/>
        <source>Stopping debug session...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2279"/>
        <source>Breakpoint error</source>
        <translation type="unfinished">Breakpoint error</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="2279"/>
        <source>Failed to create breakpoint</source>
        <translation type="unfinished">Failed to create breakpoint</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="3144"/>
        <source>Unknown (%1)</source>
        <translation>Desconegut (%1)</translation>
    </message>
    <message>
        <location filename="../../core/Cutter.cpp" line="3396"/>
        <source>Primitive</source>
        <translation type="unfinished">Primitive</translation>
    </message>
</context>
<context>
    <name>CutterGraphView</name>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="30"/>
        <location filename="../../widgets/CutterGraphView.cpp" line="442"/>
        <source>Export Graph</source>
        <translation type="unfinished">Exportar gràfic</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="42"/>
        <source>Layout</source>
        <translation type="unfinished">Layout</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="43"/>
        <source>Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="47"/>
        <source>Grid narrow</source>
        <translation type="unfinished">Grid narrow</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="48"/>
        <source>Grid medium</source>
        <translation type="unfinished">Grid medium</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="49"/>
        <source>Grid wide</source>
        <translation type="unfinished">Grid wide</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="63"/>
        <source>Graphviz polyline</source>
        <translation type="unfinished">Graphviz polyline</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="64"/>
        <source>Graphviz ortho</source>
        <translation type="unfinished">Graphviz ortho</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="65"/>
        <source>Graphviz sfdp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="66"/>
        <source>Graphviz neato</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="67"/>
        <source>Graphviz twopi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="68"/>
        <source>Graphviz circo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="409"/>
        <source>PNG (*.png)</source>
        <translation type="unfinished">PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="410"/>
        <source>JPEG (*.jpg)</source>
        <translation type="unfinished">JPEG (*.jpg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="411"/>
        <source>SVG (*.svg)</source>
        <translation type="unfinished">SVG (*.svg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="417"/>
        <source>Graphviz dot (*.dot)</source>
        <translation type="unfinished">Graphviz dot (*.dot)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="418"/>
        <source>Graph Modelling Language (*.gml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="420"/>
        <source>RZ JSON (*.json)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="421"/>
        <source>SDB key-value (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="427"/>
        <source>Graphviz json (*.json)</source>
        <translation type="unfinished">Graphviz json (*.json)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="429"/>
        <source>Graphviz gif (*.gif)</source>
        <translation type="unfinished">Graphviz gif (*.gif)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="431"/>
        <source>Graphviz png (*.png)</source>
        <translation type="unfinished">Graphviz png (*.png)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="433"/>
        <source>Graphviz jpg (*.jpg)</source>
        <translation type="unfinished">Graphviz jpg (*.jpg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="435"/>
        <source>Graphviz PostScript (*.ps)</source>
        <translation type="unfinished">Graphviz PostScript (*.ps)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="437"/>
        <source>Graphviz svg (*.svg)</source>
        <translation type="unfinished">Graphviz svg (*.svg)</translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="460"/>
        <source>Graph Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/CutterGraphView.cpp" line="461"/>
        <source>Do you really want to export %1 x %2 = %3 pixel bitmap image? Consider using different format.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CutterSeekable</name>
    <message>
        <location filename="../../widgets/AddressableDockWidget.cpp" line="45"/>
        <source> (unsynced)</source>
        <translation> (no sincronitzat)</translation>
    </message>
    <message>
        <location filename="../../common/CutterSeekable.cpp" line="73"/>
        <source>More than one (%1) references here. Weird behaviour expected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CutterTreeWidget</name>
    <message>
        <location filename="../../widgets/CutterTreeWidget.cpp" line="19"/>
        <source>%1 Items</source>
        <translation>%1 elements</translation>
    </message>
</context>
<context>
    <name>Dashboard</name>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="116"/>
        <source>OVERVIEW</source>
        <translation>VISIÓ DE CONJUNT</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="140"/>
        <source>Info</source>
        <translation>Informació</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="178"/>
        <source>File:</source>
        <translation>Fitxer:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="188"/>
        <location filename="../../widgets/Dashboard.ui" line="217"/>
        <location filename="../../widgets/Dashboard.ui" line="246"/>
        <location filename="../../widgets/Dashboard.ui" line="291"/>
        <location filename="../../widgets/Dashboard.ui" line="320"/>
        <location filename="../../widgets/Dashboard.ui" line="349"/>
        <location filename="../../widgets/Dashboard.ui" line="378"/>
        <location filename="../../widgets/Dashboard.ui" line="391"/>
        <location filename="../../widgets/Dashboard.ui" line="436"/>
        <location filename="../../widgets/Dashboard.ui" line="465"/>
        <location filename="../../widgets/Dashboard.ui" line="494"/>
        <location filename="../../widgets/Dashboard.ui" line="523"/>
        <location filename="../../widgets/Dashboard.ui" line="552"/>
        <location filename="../../widgets/Dashboard.ui" line="581"/>
        <location filename="../../widgets/Dashboard.ui" line="610"/>
        <location filename="../../widgets/Dashboard.ui" line="655"/>
        <location filename="../../widgets/Dashboard.ui" line="668"/>
        <location filename="../../widgets/Dashboard.ui" line="710"/>
        <location filename="../../widgets/Dashboard.ui" line="739"/>
        <location filename="../../widgets/Dashboard.ui" line="768"/>
        <location filename="../../widgets/Dashboard.ui" line="797"/>
        <location filename="../../widgets/Dashboard.ui" line="826"/>
        <location filename="../../widgets/Dashboard.ui" line="855"/>
        <location filename="../../widgets/Dashboard.ui" line="884"/>
        <location filename="../../widgets/Dashboard.ui" line="913"/>
        <location filename="../../widgets/Dashboard.ui" line="942"/>
        <source>--</source>
        <translation>--</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="207"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="236"/>
        <source>Mode:</source>
        <translation>Mode:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="265"/>
        <source>Size:</source>
        <translation>Mida:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="281"/>
        <source>Type:</source>
        <translation>Tipus:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="310"/>
        <source>Class:</source>
        <translation>Classe:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="339"/>
        <source>Language:</source>
        <translation>Llenguatge:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="368"/>
        <source>Bits:</source>
        <translation>Bits:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="426"/>
        <source>FD:</source>
        <translation>FD:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="455"/>
        <source>Base addr:</source>
        <translation>Adreça Base:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="484"/>
        <source>Virtual addr:</source>
        <translation>Adreça Virtual:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="513"/>
        <source>Canary:</source>
        <translation>Canari:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="542"/>
        <source>Crypto:</source>
        <translation>Crypto:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="571"/>
        <source>NX bit:</source>
        <translation>NX:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="600"/>
        <source>PIC:</source>
        <translation>PIC:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="629"/>
        <source>Static:</source>
        <translation>Static:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="645"/>
        <source>Relro:</source>
        <translation>Relro:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="700"/>
        <source>Architecture:</source>
        <translation>Arquitectura:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="729"/>
        <source>Machine:</source>
        <translation>Màquina:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="758"/>
        <source>OS:</source>
        <translation>SO:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="787"/>
        <source>Subsystem:</source>
        <translation>Subsistema:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="816"/>
        <source>Stripped:</source>
        <translation>Desposseït:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="845"/>
        <source>Relocs:</source>
        <translation>Reubicacions:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="874"/>
        <source>Endianness:</source>
        <translation>Endian:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="903"/>
        <source>Compiled:</source>
        <translation>Compilat:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="932"/>
        <source>Compiler:</source>
        <translation type="unfinished">Compiler:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="993"/>
        <source>Certificates</source>
        <translation>Certificats</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1006"/>
        <source>Version info</source>
        <translation>Informació sobre Versions</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1034"/>
        <source>Hashes</source>
        <translation>Sumes de comprovació</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1051"/>
        <source>Libraries</source>
        <translation>Llibreries</translation>
    </message>
    <message>
        <source>MD5:</source>
        <translation type="vanished">MD5:</translation>
    </message>
    <message>
        <source>SHA1:</source>
        <translation type="vanished">SHA1:</translation>
    </message>
    <message>
        <source>Entropy:</source>
        <translation type="vanished">Entropia:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1083"/>
        <source>Analysis info</source>
        <translation type="unfinished">Analysis info</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1107"/>
        <source>Functions:</source>
        <translation type="unfinished">Functions:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1133"/>
        <source>X-Refs:</source>
        <translation type="unfinished">X-Refs:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1159"/>
        <source>Calls:</source>
        <translation>Crides:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1185"/>
        <source>Strings:</source>
        <translation type="unfinished">Strings:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1211"/>
        <source>Symbols:</source>
        <translation type="unfinished">Symbols:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1237"/>
        <source>Imports:</source>
        <translation type="unfinished">Imports:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1263"/>
        <source>Analysis coverage:</source>
        <translation type="unfinished">Analysis coverage:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1289"/>
        <source>Code size:</source>
        <translation type="unfinished">Code size:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.ui" line="1315"/>
        <source>Coverage percent:</source>
        <translation type="unfinished">Coverage percent:</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="124"/>
        <source>&lt;b&gt;Entropy:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="236"/>
        <location filename="../../widgets/Dashboard.cpp" line="256"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="251"/>
        <source>True</source>
        <translation>Cert</translation>
    </message>
    <message>
        <location filename="../../widgets/Dashboard.cpp" line="253"/>
        <source>False</source>
        <translation>Fals</translation>
    </message>
</context>
<context>
    <name>DebugActions</name>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="56"/>
        <source>Start debug</source>
        <translation>Inicia la depuració</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="37"/>
        <source>Start emulation</source>
        <translation>Inicia l&apos;emulació</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="38"/>
        <source>Attach to process</source>
        <translation>Adjunta un procés</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="40"/>
        <source>Stop debug</source>
        <translation>Atura la depuració</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="41"/>
        <source>Stop emulation</source>
        <translation>Atura l&apos;emulació</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="55"/>
        <source>Restart program</source>
        <translation>Reinicia el programa</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="42"/>
        <source>Restart emulation</source>
        <translation>Reinicia l&apos;emulació</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="54"/>
        <source>Continue</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="43"/>
        <source>Continue until main</source>
        <translation type="unfinished">Continue until main</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="39"/>
        <source>Connect to a remote debugger</source>
        <translation>Connecta un depurador remot</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="44"/>
        <source>Continue until call</source>
        <translation>Continua fins a la crida</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="45"/>
        <source>Continue until syscall</source>
        <translation>Continua fins a la crida de sistema</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="46"/>
        <source>Continue backwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="47"/>
        <source>Step</source>
        <translation>Pas</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="48"/>
        <source>Step over</source>
        <translation type="unfinished">Step over</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="49"/>
        <source>Step out</source>
        <translation type="unfinished">Step out</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="50"/>
        <source>Step backwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="51"/>
        <source>Start trace session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="52"/>
        <source>Stop trace session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="53"/>
        <source>Suspend the process</source>
        <translation type="unfinished">Suspend the process</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="147"/>
        <source>Debugged process exited (</source>
        <translation type="unfinished">Debugged process exited (</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="267"/>
        <source>Debug is currently in beta.
</source>
        <translation>La depuració està en fase beta actualment.
</translation>
    </message>
    <message>
        <source>If you encounter any problems or have suggestions, please submit an issue to https://github.com/radareorg/cutter/issues</source>
        <translation type="obsolete">If you encounter any problems or have suggestions, please submit an issue to https://github.com/radareorg/cutter/issues</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="295"/>
        <source>Error connecting.</source>
        <translation type="unfinished">Error connecting.</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="377"/>
        <source>File &apos;%1&apos; does not have executable permissions.</source>
        <translation type="unfinished">File &apos;%1&apos; does not have executable permissions.</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="348"/>
        <source>Error attaching. No process selected!</source>
        <translation type="unfinished">Error attaching. No process selected!</translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="268"/>
        <source>If you encounter any problems or have suggestions, please submit an issue to https://github.com/rizinorg/cutter/issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DebugActions.cpp" line="282"/>
        <location filename="../../widgets/DebugActions.cpp" line="357"/>
        <source>Detach from process</source>
        <translation type="unfinished">Detach from process</translation>
    </message>
</context>
<context>
    <name>DebugOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="14"/>
        <source>Debug</source>
        <translation>Depurar</translation>
    </message>
    <message>
        <source>Debug Plugin:</source>
        <translation type="vanished">Connector de depuració:</translation>
    </message>
    <message>
        <source>Program Arguments:</source>
        <translation type="vanished">Arguments del programa:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="30"/>
        <source>Debug plugin:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="42"/>
        <source>ESIL options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="69"/>
        <source>ESIL stack address:</source>
        <translation>Adreça de la pila d&apos;ESIL:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="79"/>
        <source>Hide text when zooming out and it is smaller than the given value. Higher values can increase Performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="82"/>
        <source>ESIL stack size:</source>
        <translation>Mida de la pila d&apos;ESIL:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="100"/>
        <source>Trace options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="109"/>
        <source>Trace each step during continue in a trace session (dbg.trace_continue)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="112"/>
        <source>Disabling this option means that stepping back after continue will return to the previous PC. Significantly improves performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/DebugOptionsWidget.ui" line="54"/>
        <source>Break esil execution when instruction is invalid (esil.breakoninvalid)</source>
        <translation>Atura la execució d&apos;ESIL quan la instrucció sigui invàlida (esil.breakoninvalid)</translation>
    </message>
</context>
<context>
    <name>DebugToolbar</name>
    <message>
        <source>Start debug</source>
        <translation type="vanished">Inicia la depuració</translation>
    </message>
    <message>
        <source>Start emulation</source>
        <translation type="vanished">Inicia la emulació</translation>
    </message>
    <message>
        <source>Attach to process</source>
        <translation type="vanished">Enganxar-se a un procés</translation>
    </message>
    <message>
        <source>Stop debug</source>
        <translation type="vanished">Aturar la depuració</translation>
    </message>
    <message>
        <source>Stop emulation</source>
        <translation type="vanished">Aturar la emulació</translation>
    </message>
    <message>
        <source>Restart program</source>
        <translation type="vanished">Reinicia el programa</translation>
    </message>
    <message>
        <source>Restart emulation</source>
        <translation type="vanished">Reinicia la emulació</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">Continua</translation>
    </message>
    <message>
        <source>Continue until main</source>
        <translation type="vanished">Continuar execució fins al main</translation>
    </message>
    <message>
        <source>Continue until call</source>
        <translation type="vanished">Continua fins a la crida</translation>
    </message>
    <message>
        <source>Continue until syscall</source>
        <translation type="vanished">Continua fins a la crida de sistema</translation>
    </message>
    <message>
        <source>Step</source>
        <translation type="vanished">Pas</translation>
    </message>
    <message>
        <source>Step over</source>
        <translation type="vanished">Passa al següent</translation>
    </message>
    <message>
        <source>Step out</source>
        <translation type="vanished">Passar per sobre</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; does not have executable permissions.</source>
        <translation type="vanished">El fitxer &apos;%1&apos; no té permissos d&apos;execució.</translation>
    </message>
    <message>
        <source>Error attaching. No process selected!</source>
        <translation type="vanished">Error! No hi ha cap procés seleccionat!</translation>
    </message>
    <message>
        <source>Detach from process</source>
        <translation type="vanished">Separar-se del procés</translation>
    </message>
</context>
<context>
    <name>DecompilerContextMenu</name>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="26"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="27"/>
        <source>Copy instruction address (&lt;address&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="28"/>
        <source>Copy address of [flag] (&lt;address&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="29"/>
        <source>Show in</source>
        <translation type="unfinished">Show in</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="30"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="161"/>
        <source>Add Comment</source>
        <translation type="unfinished">Afegir Comentari</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="31"/>
        <source>Delete comment</source>
        <translation type="unfinished">Esborrar comentari</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="32"/>
        <source>Rename function at cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="33"/>
        <source>Delete &lt;name&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="34"/>
        <source>Edit variable &lt;name of variable&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="35"/>
        <source>Show X-Refs</source>
        <translation type="unfinished">Mostra X-Refs</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="36"/>
        <source>Add/remove breakpoint</source>
        <translation type="unfinished">Afegir/eliminar punt d&apos;interrupció</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="37"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="192"/>
        <source>Advanced breakpoint</source>
        <translation type="unfinished">Advanced breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="39"/>
        <source>Continue until line</source>
        <translation type="unfinished">Continuar fins la línia</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="40"/>
        <source>Set PC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="164"/>
        <source>Edit Comment</source>
        <translation type="unfinished">Editar comentari</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="180"/>
        <source>Add breakpoint</source>
        <translation type="unfinished">Add breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="182"/>
        <source>Remove breakpoint</source>
        <translation type="unfinished">Remove breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="184"/>
        <source>Remove all breakpoints in line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="191"/>
        <source>Edit breakpoint</source>
        <translation type="unfinished">Edit breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="195"/>
        <source>Set %1 here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="207"/>
        <source>Rename function %1</source>
        <translation type="unfinished">Renombrar funció %1</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="212"/>
        <source>Rename %1</source>
        <translation type="unfinished">Reanomena %1</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="213"/>
        <source>Remove %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="216"/>
        <source>Add name to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="221"/>
        <source>Copy instruction address (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="227"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="232"/>
        <source>Copy address of %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="236"/>
        <source>Copy address (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="252"/>
        <source>Edit variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="254"/>
        <source>Rename variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="316"/>
        <source>Can&apos;t rename this variable.&lt;br&gt;Only local variables defined in disassembly can be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="335"/>
        <source>Can&apos;t edit this variable.&lt;br&gt;Only local variables defined in disassembly can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="407"/>
        <source>Define this function at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="408"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="415"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="414"/>
        <source>Rename function %2</source>
        <translation type="unfinished">Renombrar funció %2</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="424"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="449"/>
        <source>Rename %2</source>
        <translation type="unfinished">Reanomena %2</translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="425"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="432"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="450"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="432"/>
        <source>Add name to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="443"/>
        <source>Rename local variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="444"/>
        <source>Can&apos;t rename this variable. Only local variables defined in disassembly can be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="468"/>
        <source>Edit local variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="469"/>
        <source>Can&apos;t edit this variable. Only local variables defined in disassembly can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="535"/>
        <source>Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="545"/>
        <source>Debug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="575"/>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="577"/>
        <source>Show %1 in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DecompilerContextMenu.cpp" line="582"/>
        <source>%1 (%2)</source>
        <translation type="unfinished">%1 (%2)</translation>
    </message>
</context>
<context>
    <name>DecompilerWidget</name>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="14"/>
        <location filename="../../widgets/DecompilerWidget.cpp" line="452"/>
        <source>Decompiler</source>
        <translation>Decompilador</translation>
    </message>
    <message>
        <source>Auto Refresh</source>
        <translation type="vanished">Autorefresca</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Refresca</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="53"/>
        <source>Decompiling...</source>
        <translation>Descompilant...</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.ui" line="75"/>
        <source>Decompiler:</source>
        <translation type="unfinished">Decompiler:</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="33"/>
        <source>Choose an offset and refresh to get decompiled code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="69"/>
        <source>No Decompiler available.</source>
        <translation type="unfinished">No Decompiler available.</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="262"/>
        <source>No function found at this offset. Seek to a function or define one in order to decompile it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <source>Click Refresh to generate Decompiler from current offset.</source>
        <translation type="obsolete">Click Refresh to generate Decompiler from current offset.</translation>
    </message>
    <message>
        <location filename="../../widgets/DecompilerWidget.cpp" line="309"/>
        <source>Cannot decompile at this address (Not a function?)</source>
        <translation type="unfinished">Cannot decompile at this address (Not a function?)</translation>
    </message>
</context>
<context>
    <name>DisassemblerGraphView</name>
    <message>
        <source>Export Graph</source>
        <translation type="vanished">Exportar gràfic</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Sincronitzar adreces</translation>
    </message>
    <message>
        <source>Grid narrow</source>
        <translation type="obsolete">Grid narrow</translation>
    </message>
    <message>
        <source>Grid medium</source>
        <translation type="obsolete">Grid medium</translation>
    </message>
    <message>
        <source>Grid wide</source>
        <translation type="obsolete">Grid wide</translation>
    </message>
    <message>
        <source>Graphviz polyline</source>
        <translation type="obsolete">Graphviz polyline</translation>
    </message>
    <message>
        <source>Graphviz polyline LR</source>
        <translation type="obsolete">Graphviz polyline LR</translation>
    </message>
    <message>
        <source>Graphviz ortho</source>
        <translation type="obsolete">Graphviz ortho</translation>
    </message>
    <message>
        <source>Graphviz ortho LR</source>
        <translation type="obsolete">Graphviz ortho LR</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation type="obsolete">Layout</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="91"/>
        <source>Highlight block</source>
        <translation type="unfinished">Highlight block</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="109"/>
        <source>Unhighlight block</source>
        <translation type="unfinished">Unhighlight block</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="119"/>
        <source>Highlight instruction</source>
        <translation type="unfinished">Highlight instruction</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="123"/>
        <source>Unhighlight instruction</source>
        <translation type="unfinished">Unhighlight instruction</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="199"/>
        <source>No function detected. Cannot display graph.</source>
        <translation>No s&apos;ha detectat cap funció. No es pot visualitzar el graf.</translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblerGraphView.cpp" line="214"/>
        <source>Graph</source>
        <translation>Graf</translation>
    </message>
    <message>
        <source>Graphviz dot (*.dot)</source>
        <translation type="obsolete">Graphviz dot (*.dot)</translation>
    </message>
    <message>
        <source>Graphviz json (*.json)</source>
        <translation type="obsolete">Graphviz json (*.json)</translation>
    </message>
    <message>
        <source>Graphviz gif (*.gif)</source>
        <translation type="obsolete">Graphviz gif (*.gif)</translation>
    </message>
    <message>
        <source>Graphviz png (*.png)</source>
        <translation type="obsolete">Graphviz png (*.png)</translation>
    </message>
    <message>
        <source>Graphviz jpg (*.jpg)</source>
        <translation type="obsolete">Graphviz jpg (*.jpg)</translation>
    </message>
    <message>
        <source>Graphviz PostScript (*.ps)</source>
        <translation type="obsolete">Graphviz PostScript (*.ps)</translation>
    </message>
    <message>
        <source>Graphviz svg (*.svg)</source>
        <translation type="obsolete">Graphviz svg (*.svg)</translation>
    </message>
    <message>
        <source>Graphiz dot (*.dot)</source>
        <translation type="vanished">Format Graphviz (*.dot)</translation>
    </message>
    <message>
        <source>GIF (*.gif)</source>
        <translation type="vanished">GIF (*.gif)</translation>
    </message>
    <message>
        <source>PNG (*.png)</source>
        <translation type="vanished">PNG (*.png)</translation>
    </message>
    <message>
        <source>JPEG (*.jpg)</source>
        <translation type="vanished">JPEG (*.jpg)</translation>
    </message>
    <message>
        <source>PostScript (*.ps)</source>
        <translation type="vanished">PostScript (*.ps)</translation>
    </message>
    <message>
        <source>SVG (*.svg)</source>
        <translation type="vanished">SVG (*.svg)</translation>
    </message>
    <message>
        <source>JSON (*.json)</source>
        <translation type="vanished">JSON (*.json)</translation>
    </message>
</context>
<context>
    <name>DisassemblyContextMenu</name>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="72"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="75"/>
        <source>Copy address</source>
        <translation>Copia adreça</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="79"/>
        <source>Show in</source>
        <translation type="unfinished">Show in</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="84"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="549"/>
        <source>Add Comment</source>
        <translation>Afegir Comentari</translation>
    </message>
    <message>
        <source>Add Flag</source>
        <translation type="vanished">Afegir flag</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">Renombrar</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="96"/>
        <source>Edit function</source>
        <translation>Editar funció</translation>
    </message>
    <message>
        <source>Rename Flag/Fcn/Var Used Here</source>
        <translation type="vanished">Reanomena Flag/Fcn/Var utilitzats aquí</translation>
    </message>
    <message>
        <source>Re-type function local vars</source>
        <translation type="vanished">Canviar el tipus de les variables locals de la funció</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="100"/>
        <source>Delete comment</source>
        <translation>Esborrar comentari</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="104"/>
        <source>Delete flag</source>
        <translation>Esborrar flag</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="107"/>
        <source>Undefine function</source>
        <translation>Funció no definida</translation>
    </message>
    <message>
        <source>Define function here...</source>
        <translation type="vanished">Defineix una funció aquí...</translation>
    </message>
    <message>
        <source>Set to Code</source>
        <translation type="vanished">Ajustar al Codi</translation>
    </message>
    <message>
        <source>Set as Code</source>
        <translation type="vanished">Definir com a Codi</translation>
    </message>
    <message>
        <source>Set as String</source>
        <translation type="vanished">Definir com a Text</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="133"/>
        <source>Show X-Refs</source>
        <translation>Mostra X-Refs</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="137"/>
        <source>X-Refs for local variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="141"/>
        <source>Show Options</source>
        <translation>Mostra Opcions</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="172"/>
        <source>Set Immediate Base to...</source>
        <translation>Ajustar la base immediata a...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="174"/>
        <source>Binary</source>
        <translation>Binari</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="178"/>
        <source>Octal</source>
        <translation>Octal</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="182"/>
        <source>Decimal</source>
        <translation>Decimal</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="186"/>
        <source>Hexadecimal</source>
        <translation>Hexadecimal</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="190"/>
        <source>Network Port</source>
        <translation>Port de xarxa</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="194"/>
        <source>IP Address</source>
        <translation>Adreça IP</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="198"/>
        <source>Syscall</source>
        <translation>Crida de sistema</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="202"/>
        <source>String</source>
        <translation>Cadena de caràcters</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="209"/>
        <source>Set current bits to...</source>
        <translation>Posar els bits actuals a...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="424"/>
        <source>Rename local &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="429"/>
        <source>Rename flag &quot;%1&quot; (used here)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="826"/>
        <source>New function at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="827"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="841"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="840"/>
        <source>Rename function %2</source>
        <translation type="unfinished">Renombrar funció %2</translation>
    </message>
    <message>
        <source>Set to Data...</source>
        <translation type="vanished">Definir com a dades...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="111"/>
        <source>Define function here</source>
        <translation type="unfinished">Define function here</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="121"/>
        <source>Structure offset</source>
        <translation type="unfinished">Structure offset</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="125"/>
        <source>Link Type to Address</source>
        <translation type="unfinished">Link Type to Address</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="226"/>
        <source>Set as...</source>
        <translation type="unfinished">Set as...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="228"/>
        <source>Code</source>
        <translation type="unfinished">Code</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="232"/>
        <source>String...</source>
        <translation type="unfinished">String...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="234"/>
        <source>Auto-detect</source>
        <translation type="unfinished">Auto-detect</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="236"/>
        <source>Remove</source>
        <translation type="unfinished">Remove</translation>
    </message>
    <message>
        <source>Adanced</source>
        <translation type="obsolete">Adanced</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="250"/>
        <source>Data...</source>
        <translation type="unfinished">Data...</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="252"/>
        <source>Byte</source>
        <translation>Byte</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="256"/>
        <source>Word</source>
        <translation>Paraula</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="260"/>
        <source>Dword</source>
        <translation>Dword</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="264"/>
        <source>Qword</source>
        <translation>Qword</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="279"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="281"/>
        <source>Instruction</source>
        <translation>Instrucció</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="285"/>
        <source>Nop Instruction</source>
        <translation>Nopejar instrucció</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="289"/>
        <source>Bytes</source>
        <translation>Octets</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="292"/>
        <source>Reverse Jump</source>
        <translation>Salt invers</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="298"/>
        <source>Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="310"/>
        <source>Debug</source>
        <translation>Depurar</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="300"/>
        <source>Add/remove breakpoint</source>
        <translation>Afegir/eliminar punt d&apos;interrupció</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="88"/>
        <source>Rename or add flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="92"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="875"/>
        <source>Re-type Local Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="238"/>
        <source>Advanced</source>
        <translation type="unfinished">Avançat</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="303"/>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="587"/>
        <source>Advanced breakpoint</source>
        <translation type="unfinished">Advanced breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="312"/>
        <source>Continue until line</source>
        <translation>Continuar fins la línia</translation>
    </message>
    <message>
        <source>%1 (used here)</source>
        <translation type="obsolete">%1 (used here)</translation>
    </message>
    <message>
        <source>%1 (%2)</source>
        <translation type="obsolete">%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="552"/>
        <source>Edit Comment</source>
        <translation>Editar comentari</translation>
    </message>
    <message>
        <source>Rename function &quot;%1&quot;</source>
        <translation type="vanished">Reanomenar la funció &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Rename flag &quot;%1&quot;</source>
        <translation type="vanished">Renombrar flag &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="568"/>
        <source>Edit function &quot;%1&quot;</source>
        <translation>Editar funció &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="414"/>
        <source>Add flag at %1 (used here)</source>
        <translation>Afegir flag a %1 (utilitzat aquí)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="419"/>
        <source>Rename &quot;%1&quot;</source>
        <translation>Canviar el nom de &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Rename &quot;%1&quot; (used here)</source>
        <translation type="vanished">Renombrar &quot;%1&quot; (aquí)</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="585"/>
        <source>Remove breakpoint</source>
        <translation type="unfinished">Remove breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="585"/>
        <source>Add breakpoint</source>
        <translation type="unfinished">Add breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="586"/>
        <source>Edit breakpoint</source>
        <translation type="unfinished">Edit breakpoint</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="601"/>
        <source>X-Refs for %1</source>
        <translation type="unfinished">Referències a %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="701"/>
        <source>Edit Instruction at %1</source>
        <translation>Modificar la instrucció a %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="755"/>
        <source>Edit Bytes at %1</source>
        <translation>Canviar els bytes a %1</translation>
    </message>
    <message>
        <source>Write error</source>
        <translation type="vanished">Error d&apos;escriptura</translation>
    </message>
    <message>
        <source>Unable to complete write operation. Consider opening in write mode. 

WARNING: In write mode any changes will be commited to disk</source>
        <translation type="vanished">No s&apos;ha pogut completar la operació d&apos;escriptura. Prova re-obrint en mode d&apos;escriptura.

ADVERTÈNCIA: En mode escriptura, qualsevol canvi s&apos;aplica directament en el fitxer.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">D&apos;acord</translation>
    </message>
    <message>
        <source>Reopen in write mode and try again</source>
        <translation type="vanished">Re-obre en mode d&apos;escriptura i prova de nou</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="938"/>
        <source>Wrong address</source>
        <translation type="unfinished">Wrong address</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="939"/>
        <source>Can&apos;t edit string at this address</source>
        <translation type="unfinished">Can&apos;t edit string at this address</translation>
    </message>
    <message>
        <source>Add Comment at %1</source>
        <translation type="vanished">Afegir comentari a %1</translation>
    </message>
    <message>
        <source>Edit Comment at %1</source>
        <translation type="vanished">Editar Comentari a %1</translation>
    </message>
    <message>
        <source>Analyze function at %1</source>
        <translation type="vanished">Analitzar funció a %1</translation>
    </message>
    <message>
        <source>Function name</source>
        <translation type="vanished">Nom de la funció</translation>
    </message>
    <message>
        <source>Rename function %1</source>
        <translation type="vanished">Renombrar funció %1</translation>
    </message>
    <message>
        <source>Rename flag %1</source>
        <translation type="vanished">Renombrar flag %1</translation>
    </message>
    <message>
        <source>Add flag at %1</source>
        <translation type="vanished">Afegir flag a %1</translation>
    </message>
    <message>
        <source>Rename %1</source>
        <translation type="vanished">Reanomena %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="876"/>
        <source>You must be in a function to define variable types.</source>
        <translation type="unfinished">You must be in a function to define variable types.</translation>
    </message>
    <message>
        <source>Set Variable Types for Function: %1</source>
        <translation type="vanished">Ajustar els tipus de les variables per a la funció: %1</translation>
    </message>
    <message>
        <location filename="../../menus/DisassemblyContextMenu.cpp" line="1020"/>
        <source>Edit function %1</source>
        <translation>Editar funció %1</translation>
    </message>
</context>
<context>
    <name>DisassemblyWidget</name>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="670"/>
        <source>More than one (%1) references here. Weird behaviour expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="676"/>
        <source>offsetFrom (%1) differs from refs.at(0).from (%(2))</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/DisassemblyWidget.cpp" line="721"/>
        <source>Disassembly</source>
        <translation type="unfinished">Disassembly</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Sincronitzar l&apos;adreça</translation>
    </message>
</context>
<context>
    <name>DuplicateFromOffsetDialog</name>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="20"/>
        <source>Duplicate from offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="28"/>
        <source>Offset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/DuplicateFromOffsetDialog.ui" line="49"/>
        <source>N bytes:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditFunctionDialog</name>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="14"/>
        <source>Edit Function</source>
        <translation>Editar funció</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="43"/>
        <source>Name of function</source>
        <translation>Nom de funció</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="53"/>
        <source>Start address</source>
        <translation type="unfinished">Start address</translation>
    </message>
    <message>
        <source>End address</source>
        <translation type="obsolete">End address</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="63"/>
        <source>Stack size</source>
        <translation>Mida de la pila</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditFunctionDialog.ui" line="73"/>
        <source>Calling convention</source>
        <translation>Convencions per les crides</translation>
    </message>
</context>
<context>
    <name>EditInstructionDialog</name>
    <message>
        <location filename="../../dialogs/EditInstructionDialog.ui" line="14"/>
        <source>Edit Instruction</source>
        <translation>Editar instrucció</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditInstructionDialog.ui" line="76"/>
        <source>Unknown Instruction</source>
        <translation>Instrucció desconeguda</translation>
    </message>
</context>
<context>
    <name>EditMethodDialog</name>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="25"/>
        <source>Class:</source>
        <translation type="unfinished">Class:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="32"/>
        <source>Name:</source>
        <translation type="unfinished">Name:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="42"/>
        <source>Address:</source>
        <translation type="unfinished">Address:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="52"/>
        <source>Virtual:</source>
        <translation type="unfinished">Virtual:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.ui" line="66"/>
        <source>Offset in VTable:</source>
        <translation type="unfinished">Offset in VTable:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.cpp" line="152"/>
        <source>Create Method</source>
        <translation type="unfinished">Create Method</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditMethodDialog.cpp" line="167"/>
        <source>Edit Method</source>
        <translation type="unfinished">Edit Method</translation>
    </message>
</context>
<context>
    <name>EditStringDialog</name>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="23"/>
        <source>Edit string</source>
        <translation type="unfinished">Edit string</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="66"/>
        <source>Address:</source>
        <translation type="unfinished">Address:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="52"/>
        <source>Size:</source>
        <translation type="unfinished">Size:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="59"/>
        <source>Type:</source>
        <translation type="unfinished">Type:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditStringDialog.ui" line="99"/>
        <source>Auto</source>
        <translation type="unfinished">Auto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>EditVariablesDialog</name>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialog</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="22"/>
        <source>Modify:</source>
        <translation type="unfinished">Modify:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="35"/>
        <source>Name:</source>
        <translation type="unfinished">Name:</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.ui" line="42"/>
        <source>Type:</source>
        <translation type="unfinished">Type:</translation>
    </message>
    <message>
        <source>Set Variable Types for Function: %1</source>
        <translation type="obsolete">Set Variable Types for Function: %1</translation>
    </message>
    <message>
        <location filename="../../dialogs/EditVariablesDialog.cpp" line="19"/>
        <source>Edit Variables in Function: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportsModel</name>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="60"/>
        <source>Address</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="62"/>
        <source>Size</source>
        <translation>Mida</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="64"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="66"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="68"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportsWidget</name>
    <message>
        <location filename="../../widgets/ExportsWidget.cpp" line="138"/>
        <source>Exports</source>
        <translation type="unfinished">Exports</translation>
    </message>
</context>
<context>
    <name>FlagDialog</name>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="14"/>
        <source>Add Flag</source>
        <translation type="unfinished">Add Flag</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="20"/>
        <source>Add flag at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="38"/>
        <source>Flag:</source>
        <translation>Flag:</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="61"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.ui" line="80"/>
        <source>Size:</source>
        <translation type="unfinished">Size:</translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.cpp" line="24"/>
        <source>Edit flag at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/FlagDialog.cpp" line="26"/>
        <source>Add flag at %1</source>
        <translation type="unfinished">Afegir flag a %1</translation>
    </message>
</context>
<context>
    <name>FlagsModel</name>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="64"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="66"/>
        <source>Offset</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="68"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="70"/>
        <source>Real Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="72"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FlagsWidget</name>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="79"/>
        <source>Quick Filter</source>
        <translation>Filtre ràpid</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="86"/>
        <source>Flagspace:</source>
        <translation>Espais de flags:</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="99"/>
        <source>Rename</source>
        <translation type="unfinished">Rename</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="102"/>
        <source>N</source>
        <translation type="unfinished">N</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="110"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.ui" line="113"/>
        <source>Del</source>
        <translation type="unfinished">Del</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="214"/>
        <source>Rename flag %1</source>
        <translation type="unfinished">Renombrar flag %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="215"/>
        <source>Flag name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FlagsWidget.cpp" line="244"/>
        <source>(all)</source>
        <translation>(tots)</translation>
    </message>
</context>
<context>
    <name>FunctionModel</name>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="123"/>
        <source>Offset: %1</source>
        <translation>Adreça: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="125"/>
        <source>Size: %1</source>
        <translation>Mida: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="127"/>
        <source>Import: %1</source>
        <translation>Import: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="128"/>
        <source>true</source>
        <translation>true</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="128"/>
        <source>false</source>
        <translation>false</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="130"/>
        <source>Nargs: %1</source>
        <translation>Nargs: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="132"/>
        <source>Nbbs: %1</source>
        <translation>Nbbs: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="134"/>
        <source>Nlocals: %1</source>
        <translation>Nlocals %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="281"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cyclomatic complexity: %1</source>
        <translation type="vanished">Complexitat ciclomàtica: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="136"/>
        <source>Call type: %1</source>
        <translation>Tipus de Crida: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="138"/>
        <source>Edges: %1</source>
        <translation>Arestes: %1</translation>
    </message>
    <message>
        <source>Cost: %1</source>
        <translation type="vanished">Cost: %1</translation>
    </message>
    <message>
        <source>Calls/OutDegree: %1</source>
        <translation type="vanished">GrauDeCrides: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="140"/>
        <source>StackFrame: %1</source>
        <translation>ContextDePila: %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="142"/>
        <source>Comment: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="221"/>
        <source>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="unfinished">&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="226"/>
        <source>&lt;div&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="unfinished">&lt;div&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;div&gt;&lt;strong&gt;Summary&lt;/strong&gt;:&lt;br&gt;</source>
        <translation type="vanished">&lt;div&gt;&lt;strong&gt;Sumari&lt;/strong&gt;:&lt;br&gt;</translation>
    </message>
    <message>
        <source>Size:&amp;nbsp;%1,&amp;nbsp;Cyclomatic complexity:&amp;nbsp;%2,&amp;nbsp;Basic blocks:&amp;nbsp;%3</source>
        <translation type="vanished">Mida:&amp;nbsp;%1,&amp;nbsp;Complexitat Ciclomàtica:&amp;nbsp;%2,&amp;nbsp;Blocs bàsics:&amp;nbsp;%3</translation>
    </message>
    <message>
        <source>&lt;/div&gt;&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="vanished">&lt;/div&gt;&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Vista prèvia del desensamblat&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="vanished">&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Destacats&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="257"/>
        <location filename="../../widgets/FunctionsWidget.cpp" line="261"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="263"/>
        <source>Size</source>
        <translation>Mida</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="265"/>
        <source>Imp.</source>
        <translation>Imp.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="267"/>
        <source>Offset</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="269"/>
        <source>Nargs</source>
        <translation>Nargs</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="273"/>
        <source>Nbbs</source>
        <translation>Nbbs</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="271"/>
        <source>Nlocals</source>
        <translation>Nlocals</translation>
    </message>
    <message>
        <source>Cyclo. Comp.</source>
        <translation type="vanished">Cyclo. Comp.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="275"/>
        <source>Call type</source>
        <translation>Tipus de Crida</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="277"/>
        <source>Edges</source>
        <translation>Arestes</translation>
    </message>
    <message>
        <source>Cost</source>
        <translation type="vanished">Cost</translation>
    </message>
    <message>
        <source>Calls/OutDeg.</source>
        <translation type="vanished">GrauDeCrides.</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="279"/>
        <source>StackFrame</source>
        <translation>Context de Pila</translation>
    </message>
</context>
<context>
    <name>FunctionsTask</name>
    <message>
        <location filename="../../common/FunctionsTask.h" line="13"/>
        <source>Fetching Functions</source>
        <translation>Cercant funcions</translation>
    </message>
</context>
<context>
    <name>FunctionsWidget</name>
    <message>
        <source>Add comment</source>
        <translation type="vanished">Afegir un comentari</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="442"/>
        <source>Rename</source>
        <translation type="unfinished">Rename</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="443"/>
        <source>Undefine</source>
        <translation>Indefinir</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="447"/>
        <source>Functions</source>
        <translation type="unfinished">Functions</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="549"/>
        <source>Rename function %1</source>
        <translation type="unfinished">Renombrar funció %1</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="550"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X-Refs</source>
        <translation type="vanished">X-Refs</translation>
    </message>
    <message>
        <source>Cross references</source>
        <translation type="vanished">Referències creuades</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="444"/>
        <source>Horizontal</source>
        <translation type="unfinished">Horizontal</translation>
    </message>
    <message>
        <location filename="../../widgets/FunctionsWidget.cpp" line="445"/>
        <source>Vertical</source>
        <translation type="unfinished">Vertical</translation>
    </message>
</context>
<context>
    <name>GraphOptionsWidget</name>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="14"/>
        <source>Graph</source>
        <translation>Gràf</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="22"/>
        <source>Graph Block Options </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="31"/>
        <source>The offset of the first instruction of a graph block is shown in the header of the respective graph block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="34"/>
        <source>Show offset of the first instruction in each graph block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="41"/>
        <source>Show offset for each instruction (graph.offset)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="50"/>
        <source>Maximum Line Length:</source>
        <translation>Longitut Màxima de les Linies:</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="70"/>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="80"/>
        <source>Hide text when zooming out and it is smaller than the given value. Higher values can increase Performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="73"/>
        <source>Minimum Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="98"/>
        <source>Graph Layout Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="117"/>
        <source>Vertical</source>
        <translation type="unfinished">Vertical</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="140"/>
        <source>Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="176"/>
        <source>Block spacing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="183"/>
        <source>Edge spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="193"/>
        <source>Bitmap Export Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="202"/>
        <source>Export Transparent Bitmap Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="211"/>
        <source>Graph Bitmap Export Scale: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/GraphOptionsWidget.ui" line="218"/>
        <source>%</source>
        <translation type="unfinished">%</translation>
    </message>
    <message>
        <source>Show offsets (graph.offset)</source>
        <translation type="obsolete">Show offsets (graph.offset)</translation>
    </message>
    <message>
        <source>Show offsets (graph.offset) </source>
        <translation type="vanished">Mostrar adreces (graph.offset) </translation>
    </message>
</context>
<context>
    <name>HeadersModel</name>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="55"/>
        <source>Offset</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="57"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="59"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="61"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HeadersWidget</name>
    <message>
        <location filename="../../widgets/HeadersWidget.cpp" line="120"/>
        <source>Headers</source>
        <translation type="unfinished">Headers</translation>
    </message>
</context>
<context>
    <name>HexWidget</name>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="67"/>
        <source>Hexadecimal</source>
        <translation type="unfinished">Hexadecimal</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="68"/>
        <source>Octal</source>
        <translation type="unfinished">Octal</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="69"/>
        <source>Decimal</source>
        <translation type="unfinished">Decimal</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="70"/>
        <source>Signed decimal</source>
        <translation type="unfinished">Signed decimal</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="71"/>
        <source>Float</source>
        <translation type="unfinished">Float</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="85"/>
        <source>Bytes per row</source>
        <translation type="unfinished">Bytes per row</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="95"/>
        <source>Power of 2</source>
        <translation type="unfinished">Power of 2</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="102"/>
        <source>Big Endian</source>
        <translation type="unfinished">Big Endian</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="107"/>
        <source>Bytes as pairs</source>
        <translation type="unfinished">Bytes as pairs</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="111"/>
        <source>Copy</source>
        <translation type="unfinished">Copy</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="117"/>
        <source>Copy address</source>
        <translation type="unfinished">Copy address</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="123"/>
        <source>Select range</source>
        <translation type="unfinished">Select range</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="130"/>
        <location filename="../../widgets/HexWidget.cpp" line="705"/>
        <source>Write string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="134"/>
        <source>Write length and string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="138"/>
        <location filename="../../widgets/HexWidget.cpp" line="842"/>
        <source>Write wide string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="142"/>
        <source>Write zero terminated string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="146"/>
        <source>Write De\Encoded Base64 string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="151"/>
        <location filename="../../widgets/HexWidget.cpp" line="745"/>
        <source>Write zeros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="155"/>
        <source>Write random bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="159"/>
        <source>Duplicate from offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="163"/>
        <source>Increment/Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="620"/>
        <source>Item size:</source>
        <translation type="unfinished">Item size:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="622"/>
        <source>Item format:</source>
        <translation type="unfinished">Item format:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="627"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="705"/>
        <location filename="../../widgets/HexWidget.cpp" line="826"/>
        <location filename="../../widgets/HexWidget.cpp" line="842"/>
        <location filename="../../widgets/HexWidget.cpp" line="857"/>
        <source>String:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="745"/>
        <source>Number of zeros:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="769"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="770"/>
        <source>Error occured during decoding your input.
Please, make sure, that it is a valid base64 string and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="793"/>
        <source>Write random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="793"/>
        <source>Number of bytes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="826"/>
        <source>Write Pascal string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexWidget.cpp" line="857"/>
        <source>Write zero-terminated string</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HexdumpRangeDialog</name>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="14"/>
        <source>Select Block</source>
        <translation>Selecciona un bloc</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="47"/>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="79"/>
        <source>Exclusive end address</source>
        <translation type="unfinished">Exclusive end address</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="50"/>
        <source>End Address:</source>
        <translation>Adreça Final:</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="65"/>
        <source>Start Address:</source>
        <translation>Adreça Inicial:</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="99"/>
        <source>Length:</source>
        <translation>Longitut:</translation>
    </message>
    <message>
        <location filename="../../dialogs/HexdumpRangeDialog.ui" line="115"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#ff8585;&quot;&gt;Big selection might cause a delay&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#ff8585;&quot;&gt;Big selection might cause a delay&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>HexdumpWidget</name>
    <message>
        <source>0  1  2  3 ...</source>
        <translation type="vanished">0 1 2 3 ...</translation>
    </message>
    <message>
        <source>0123...</source>
        <translation type="vanished">0123...</translation>
    </message>
    <message>
        <source>Offset</source>
        <translation type="vanished">Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="70"/>
        <source>Parsing</source>
        <translation>Llegir</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="50"/>
        <source>Select bytes to display information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="143"/>
        <source>Disassembly</source>
        <translation type="unfinished">Disassembly</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="144"/>
        <source>String</source>
        <translation type="unfinished">String</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="145"/>
        <source>Assembler</source>
        <translation>Assemblador</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="146"/>
        <source>C bytes</source>
        <translation>Array en C</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="147"/>
        <source>C bytes with instructions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="148"/>
        <source>C half-words (2 byte)</source>
        <translation>C half-words (2 octets)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="149"/>
        <source>C words (4 byte)</source>
        <translation>C words (4 octets)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="150"/>
        <source>C dwords (8 byte)</source>
        <translation>C dwords (8 octets)</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="151"/>
        <source>Python</source>
        <translation>Python</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="152"/>
        <source>JSON</source>
        <translation>JSON</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="153"/>
        <source>JavaScript</source>
        <translation>JavaScript</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="154"/>
        <source>Yara</source>
        <translation type="unfinished">Yara</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="120"/>
        <source>Endian</source>
        <translation>Endian</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="131"/>
        <source>Little</source>
        <translation>Petit</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="136"/>
        <source>Big</source>
        <translation>Gran</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="191"/>
        <source>Arch</source>
        <translation>Arch</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="220"/>
        <source>Bits</source>
        <translation>Bits</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="231"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="236"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="241"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="318"/>
        <source>SHA256:</source>
        <translation type="unfinished">SHA1: {256:?}</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="357"/>
        <source>Copy SHA256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="396"/>
        <source>Copy CRC32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="466"/>
        <source>MD5:</source>
        <translation>MD5:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="479"/>
        <source>CRC32:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="350"/>
        <source>SHA1:</source>
        <translation>SHA1:</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.ui" line="434"/>
        <source>Entropy:</source>
        <translation>Entropia:</translation>
    </message>
    <message>
        <source>Hexdump side panel</source>
        <translation type="vanished">Panell lateral de bolcat hexadecimal</translation>
    </message>
    <message>
        <source>Undefine</source>
        <translation type="obsolete">Undefine</translation>
    </message>
    <message>
        <source>Copy all</source>
        <translation type="vanished">Copiar-ho tot</translation>
    </message>
    <message>
        <source>Copy bytes</source>
        <translation type="vanished">Còpia octets</translation>
    </message>
    <message>
        <source>Copy disasm</source>
        <translation type="vanished">Còpia desensamblat</translation>
    </message>
    <message>
        <source>Copy Hexpair</source>
        <translation type="vanished">Còpia cadena hexadecimal</translation>
    </message>
    <message>
        <source>Copy ASCII</source>
        <translation type="vanished">Còpia ASCII</translation>
    </message>
    <message>
        <source>Copy Text</source>
        <translation type="vanished">Còpia text</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="vanished">2</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="vanished">4</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="vanished">8</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Editar</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Enganxar</translation>
    </message>
    <message>
        <source>Insert Hex</source>
        <translation type="vanished">Insertar Hexadecimal</translation>
    </message>
    <message>
        <source>Insert String</source>
        <translation type="vanished">Insertar Cadena de Text</translation>
    </message>
    <message>
        <source>Hex</source>
        <translation type="vanished">Hex</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation type="obsolete">Octal</translation>
    </message>
    <message>
        <source>Half-word</source>
        <translation type="vanished">Mitja Paraula</translation>
    </message>
    <message>
        <source>Word</source>
        <translation type="vanished">Word</translation>
    </message>
    <message>
        <source>Quad-word</source>
        <translation type="vanished">Quad-Word</translation>
    </message>
    <message>
        <source>Emoji</source>
        <translation type="vanished">Emoji</translation>
    </message>
    <message>
        <source>1 byte</source>
        <translation type="vanished">1 octet</translation>
    </message>
    <message>
        <source>2 bytes</source>
        <translation type="vanished">2 octets</translation>
    </message>
    <message>
        <source>4 bytes</source>
        <translation type="vanished">4 octets</translation>
    </message>
    <message>
        <source>Select Block...</source>
        <translation type="vanished">Selecciona un bloc...</translation>
    </message>
    <message>
        <location filename="../../widgets/HexdumpWidget.cpp" line="219"/>
        <source>Hexdump</source>
        <translation>Bolcat hexadecimal</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation type="vanished">Columnes</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">Format</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="obsolete">Sync/unsync offset</translation>
    </message>
    <message>
        <source>Error: Could not select range, end address is less then start address</source>
        <translation type="vanished">Error: No s&apos;ha pogut seleccionar cap rang, l&apos;adreça final es menor a la inicial</translation>
    </message>
</context>
<context>
    <name>ImportsModel</name>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="45"/>
        <source>Unsafe</source>
        <translation>Insegur</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="71"/>
        <source>Address</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="73"/>
        <source>Type</source>
        <translation>Tipus</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="75"/>
        <source>Safety</source>
        <translation>Seguretat</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="77"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="79"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="81"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportsWidget</name>
    <message>
        <location filename="../../widgets/ImportsWidget.cpp" line="172"/>
        <source>Imports</source>
        <translation type="unfinished">Imports</translation>
    </message>
</context>
<context>
    <name>IncrementDecrementDialog</name>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="14"/>
        <source>Increment/Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="24"/>
        <source>Interpret as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="38"/>
        <source>Value:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="54"/>
        <source>Increment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/IncrementDecrementDialog.ui" line="64"/>
        <source>Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Byte</source>
        <translation type="unfinished">Byte</translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Dword</source>
        <translation type="unfinished">Dword</translation>
    </message>
    <message>
        <location filename="../../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Qword</source>
        <translation type="unfinished">Qword</translation>
    </message>
</context>
<context>
    <name>InitialOptionsDialog</name>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="26"/>
        <source>Load Options</source>
        <translation>Opcions de càrrega</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="84"/>
        <source>Program:</source>
        <translation>Programa:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="161"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="363"/>
        <source>Analysis: Enabled</source>
        <translation>Anàlisi: Habilitat</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="177"/>
        <source>Level: </source>
        <translation>Nivell: </translation>
    </message>
    <message>
        <source>Analyze all symbols (aa)</source>
        <translation type="vanished">Analitzar tots els simbols (aa)</translation>
    </message>
    <message>
        <source>Analyze for references (aar)</source>
        <translation type="vanished">Analitzar referències (aar)</translation>
    </message>
    <message>
        <source>Analyze function calls (aac)</source>
        <translation type="vanished">Analitzar crides a funcions (aac)</translation>
    </message>
    <message>
        <source>Analyze all basic blocks (aab)</source>
        <translation type="vanished">Analitzar tots els blocs bàsics (aab)</translation>
    </message>
    <message>
        <source>Autorename functions based on context (aan)</source>
        <translation type="vanished">Anomenar funcions automàticament (aan)</translation>
    </message>
    <message>
        <source>Experimental:</source>
        <translation type="vanished">Experimental:</translation>
    </message>
    <message>
        <source>Emulate code to find computed references (aae)</source>
        <translation type="vanished">Emular codi linealment per buscar referències computades (aae)</translation>
    </message>
    <message>
        <source>Analyze for consecutive function (aat)</source>
        <translation type="vanished">Analitzar funcions de forma consecutiva (aat)</translation>
    </message>
    <message>
        <source>Type and Argument matching analysis (afta)</source>
        <translation type="vanished">Analisi d&apos;arguments i detecció de tipus (afta)</translation>
    </message>
    <message>
        <source>Analyze code after trap-sleds (aaT)</source>
        <translation type="vanished">Analitzar codi a continuació dels trap-sleds (aaT)</translation>
    </message>
    <message>
        <source>Analyze function preludes (aap)</source>
        <translation type="vanished">Analitzar preludis de funcions (aap)</translation>
    </message>
    <message>
        <source>Analyze jump tables in switch statements (e! anal.jmptbl)</source>
        <translation type="vanished">Analitzar taules de salt pels switch (anal.jmptbl)</translation>
    </message>
    <message>
        <source>Analyze push+ret as jmp (e! anal.pushret)</source>
        <translation type="vanished">Analitzar push+ret com si fós un jmp (anal.pushret)</translation>
    </message>
    <message>
        <source>Continue analysis after each function (e! anal.hasnext)</source>
        <translation type="vanished">Continuar analitzant al final de cada funció (anal.hasnext)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="322"/>
        <source>Load in write mode (-w)</source>
        <translation>Obrir en mode d&apos;escriptura (-w)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="332"/>
        <source>Do not load bin information (-n)</source>
        <translation>No carregar la informació del binari (-n)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="345"/>
        <source>Use virtual addressing</source>
        <translation>Utilitzar adreces virtuals</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="355"/>
        <source>Import demangled symbols</source>
        <translation>Importar símbols descodificats</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="372"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="403"/>
        <source>Advanced options</source>
        <translation>Opcions avançades</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="433"/>
        <source>CPU options</source>
        <translation>Opcions de CPU</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="448"/>
        <source>Architecture:</source>
        <translation type="unfinished">Architecture:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="459"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="519"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="567"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="612"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="637"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="37"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="473"/>
        <source>CPU:</source>
        <translation>CPU:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="524"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="529"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="534"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="539"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="553"/>
        <source>Endianness: </source>
        <translation>Endian: </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="572"/>
        <source>Little</source>
        <translation type="unfinished">Little</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="577"/>
        <source>Big</source>
        <translation type="unfinished">Big</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="598"/>
        <source>Kernel: </source>
        <translation>Nucli: </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="626"/>
        <source>Format:</source>
        <translation type="unfinished">Format:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="680"/>
        <source>Load bin offset (-B)</source>
        <translation>Definir adreça base (-B)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="699"/>
        <source>1024</source>
        <translation>1024</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="718"/>
        <source>Map offset (-m)</source>
        <translation>Adreça de mapeig (-m)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="734"/>
        <source>0x40000</source>
        <translation type="unfinished">0x40000</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="741"/>
        <source>Load PDB</source>
        <translation>Carregar PDB</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="766"/>
        <source>PDB File path</source>
        <translation>Ruta del fitxer PDB</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="773"/>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="815"/>
        <source>Select</source>
        <translation>Seleccionar</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="783"/>
        <source>Load script file</source>
        <translation>Carregar fitxer d&apos;script</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="808"/>
        <source>Path to Rizin script file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path to radare2 script file</source>
        <translation type="vanished">Ruta a l&apos;script de radare2</translation>
    </message>
    <message>
        <source>BasicBlock maxsize:</source>
        <translation type="vanished">Màxim tamany de bloc bàsic:</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="871"/>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.ui" line="884"/>
        <source>  Ok  </source>
        <translation>  D&apos;acord  </translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="55"/>
        <source>Analyze all symbols</source>
        <translation type="unfinished">Analyze all symbols</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="56"/>
        <source>Analyze instructions for references</source>
        <translation type="unfinished">Analyze instructions for references</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="57"/>
        <source>Analyze function calls</source>
        <translation type="unfinished">Analyze function calls</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="58"/>
        <source>Analyze all basic blocks</source>
        <translation type="unfinished">Analyze all basic blocks</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="59"/>
        <source>Analyze all objc references</source>
        <translation type="unfinished">Analyze all objc references</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="60"/>
        <source>Recover class information from RTTI</source>
        <translation type="unfinished">Recover class information from RTTI</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="61"/>
        <source>Autoname functions based on context</source>
        <translation type="unfinished">Autoname functions based on context</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="62"/>
        <source>Emulate code to find computed references</source>
        <translation type="unfinished">Emulate code to find computed references</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="63"/>
        <source>Analyze all consecutive functions</source>
        <translation type="unfinished">Analyze all consecutive functions</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="64"/>
        <source>Type and Argument matching analysis</source>
        <translation type="unfinished">Type and Argument matching analysis</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="65"/>
        <source>Analyze code after trap-sleds</source>
        <translation type="unfinished">Analyze code after trap-sleds</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="66"/>
        <source>Analyze function preludes</source>
        <translation type="unfinished">Analyze function preludes</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="67"/>
        <source>Analyze jump tables in switch statements</source>
        <translation type="unfinished">Analyze jump tables in switch statements</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="70"/>
        <source>Analyze PUSH+RET as JMP</source>
        <translation type="unfinished">Analyze PUSH+RET as JMP</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="71"/>
        <source>Continue analysis after each function</source>
        <translation type="unfinished">Continue analysis after each function</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="343"/>
        <source>No analysis</source>
        <translation>No analitzar</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="345"/>
        <source>Auto-Analysis (aaa)</source>
        <translation>Auto-Anàlisi (aaa)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="347"/>
        <source>Auto-Analysis Experimental (aaaa)</source>
        <translation>Auto-Anàlisi Experimental (aaaa)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="349"/>
        <source>Advanced</source>
        <translation>Avançat</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="351"/>
        <source>Unknown</source>
        <translation>Desconegut</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="357"/>
        <source>Level</source>
        <translation>Nivell</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="360"/>
        <source>Analysis: Disabled</source>
        <translation>Anàlisi: Deshabilitat</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="406"/>
        <source>Select PDB file</source>
        <translation>Seleccionar fitxer PDB</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="407"/>
        <source>PDB file (*.pdb)</source>
        <translation>Fitxer PDB (*.pdb)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="407"/>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="429"/>
        <source>All files (*)</source>
        <translation>Tots els fitxers (*)</translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="428"/>
        <source>Select Rizin script file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/InitialOptionsDialog.cpp" line="429"/>
        <source>Script file (*.rz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select radare2 script file</source>
        <translation type="vanished">Sel·leccionar script de radare2</translation>
    </message>
    <message>
        <source>Script file (*.r2)</source>
        <translation type="vanished">Fitxer d&apos;script (*.r2)</translation>
    </message>
</context>
<context>
    <name>InitializationFileEditor</name>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.ui" line="14"/>
        <source>CutterRC Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.ui" line="20"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/InitializationFileEditor.cpp" line="35"/>
        <source>Script is loaded from &lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JSDecDecompiler</name>
    <message>
        <location filename="../../common/Decompiler.cpp" line="40"/>
        <source>Failed to parse JSON from jsdec</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JupyterWebView</name>
    <message>
        <source>Jupyter</source>
        <translation type="vanished">Jupyter</translation>
    </message>
</context>
<context>
    <name>JupyterWidget</name>
    <message>
        <source>Jupyter</source>
        <translation type="vanished">Jupyter</translation>
    </message>
    <message>
        <source>Cutter has been built without QtWebEngine.&lt;br /&gt;Open the following URL in your Browser to use Jupyter:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation type="vanished">Cutter s&apos;ha compilat sense QtWebEngine.&lt;br /&gt;Obre l&apos;adreça en el navegador per utilitzar Jupyter:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <source>An error occurred while opening jupyter. Make sure Jupyter is installed system-wide.</source>
        <translation type="vanished">Ha ocorregut un error mentre s&apos;obria Jupyter. Assegura&apos;t que Jupyter esta instal·lat en el sistema.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Error</translation>
    </message>
</context>
<context>
    <name>LayoutManager</name>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="14"/>
        <source>Layout</source>
        <translation type="unfinished">Layout</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="25"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.ui" line="32"/>
        <location filename="../../dialogs/LayoutManager.cpp" line="60"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="42"/>
        <source>Rename layout error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="43"/>
        <source>&apos;%1&apos; is already used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="45"/>
        <source>Save layout</source>
        <translation type="unfinished">Desar la disposició</translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="45"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/LayoutManager.cpp" line="61"/>
        <source>Do you want to delete &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinkTypeDialog</name>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialog</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="26"/>
        <source>=</source>
        <translation type="unfinished">=</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="43"/>
        <source>Enter Address</source>
        <translation type="unfinished">Enter Address</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="50"/>
        <source>Structure Type</source>
        <translation type="unfinished">Structure Type</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.ui" line="73"/>
        <source>Address/Flag</source>
        <translation type="unfinished">Address/Flag</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="10"/>
        <source>Link type to address</source>
        <translation type="unfinished">Link type to address</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="13"/>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="57"/>
        <source>(No Type)</source>
        <translation type="unfinished">(No Type)</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="75"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="75"/>
        <source>The given address is invalid</source>
        <translation type="unfinished">The given address is invalid</translation>
    </message>
    <message>
        <location filename="../../dialogs/LinkTypeDialog.cpp" line="106"/>
        <source>Invalid Address</source>
        <translation type="unfinished">Invalid Address</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../core/MainWindow.ui" line="33"/>
        <source>Add extra...</source>
        <translation>Afegir un altre...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="61"/>
        <source>File</source>
        <translation>Fitxer</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="68"/>
        <source>Set mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="94"/>
        <location filename="../../core/MainWindow.cpp" line="288"/>
        <source>View</source>
        <translation>Veure</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="101"/>
        <source>Zoom</source>
        <translation type="unfinished">Zoom</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="110"/>
        <source>Layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="130"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="138"/>
        <location filename="../../core/MainWindow.ui" line="498"/>
        <source>Edit</source>
        <translation type="unfinished">Edit</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="147"/>
        <source>Windows</source>
        <translation>Finestres</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="151"/>
        <location filename="../../core/MainWindow.cpp" line="142"/>
        <location filename="../../core/MainWindow.cpp" line="143"/>
        <source>Plugins</source>
        <translation type="unfinished">Plugins</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="156"/>
        <source>Info...</source>
        <translation>Informació...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="161"/>
        <source>Debug...</source>
        <translation>Depurar...</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="176"/>
        <source>Debug</source>
        <translation type="unfinished">Debug</translation>
    </message>
    <message>
        <source>Reset Layout</source>
        <translation type="vanished">Restaurar Disposició Original</translation>
    </message>
    <message>
        <source>Reset layout</source>
        <translation type="vanished">Restaurar disposició original</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="227"/>
        <source>Zen Mode</source>
        <translation>Mode Zen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="230"/>
        <source>Zen mode</source>
        <translation>Mode Zen</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="235"/>
        <source>About</source>
        <translation>En quant a</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="240"/>
        <source>Report an issue</source>
        <translation type="unfinished">Report an issue</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Nou</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="248"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="253"/>
        <source>Close</source>
        <translation>Tancar</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="258"/>
        <location filename="../../core/MainWindow.ui" line="261"/>
        <location filename="../../core/MainWindow.ui" line="847"/>
        <location filename="../../core/MainWindow.cpp" line="1207"/>
        <source>Save layout</source>
        <translation>Desar la disposició</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="266"/>
        <source>Documentation</source>
        <translation>Documentació</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Obrir</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation type="vanished">Ctrl+O</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Desar</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="282"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="340"/>
        <source>Undo Seek</source>
        <translation>Desfer el desplaçament</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="352"/>
        <source>Redo Seek</source>
        <translation>Refer l&apos;últim desplaçament</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="287"/>
        <source>Cut</source>
        <translation>Tallar</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="222"/>
        <source>Reset to default layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="245"/>
        <source>New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="271"/>
        <source>Map File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="274"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="279"/>
        <location filename="../../core/MainWindow.cpp" line="712"/>
        <location filename="../../core/MainWindow.cpp" line="735"/>
        <source>Save Project</source>
        <translation type="unfinished">Guardar el projecte</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="292"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="297"/>
        <location filename="../../core/MainWindow.ui" line="503"/>
        <source>Paste</source>
        <translation type="unfinished">Paste</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="302"/>
        <source>Delete</source>
        <translation type="unfinished">Delete</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="307"/>
        <location filename="../../core/MainWindow.ui" line="310"/>
        <source>Select all</source>
        <translation>Seleccionar tot</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="315"/>
        <source>Find</source>
        <translation>Cercar</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="320"/>
        <location filename="../../core/MainWindow.ui" line="323"/>
        <source>Find next</source>
        <translation>Cercar el següent</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="328"/>
        <location filename="../../core/MainWindow.ui" line="331"/>
        <source>Find previous</source>
        <translation>Cercar l&apos;anterior</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Tornar</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="343"/>
        <source>Go back</source>
        <translation>Tornar Enrera</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">Endavant</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="363"/>
        <source>Unlock Panels</source>
        <translation>Desbloquejar els Panells</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="366"/>
        <source>Toggle panel locks</source>
        <translation>Blocar/Desblocar els panells</translation>
    </message>
    <message>
        <source>Lock/Unlock</source>
        <translation type="vanished">Bloquejar/Desbloquejar</translation>
    </message>
    <message>
        <source>Strings</source>
        <translation type="vanished">Cadenes de text</translation>
    </message>
    <message>
        <source>Show/Hide Strings panel</source>
        <translation type="vanished">Mostrar/Amagar el panell de cadenes de text</translation>
    </message>
    <message>
        <source>Sections</source>
        <translation type="vanished">Seccions</translation>
    </message>
    <message>
        <source>Show/Hide Sections panel</source>
        <translation type="vanished">Mostrar/Amagar el panell de seccions</translation>
    </message>
    <message>
        <source>Segments</source>
        <translation type="vanished">Segments</translation>
    </message>
    <message>
        <source>Show/Hide Segments panel</source>
        <translation type="vanished">Mostrar/Amagar el panell de Segments</translation>
    </message>
    <message>
        <source>Functions</source>
        <translation type="vanished">Funcions</translation>
    </message>
    <message>
        <source>Show/Hide Functions panel</source>
        <translation type="vanished">Mostrar/Amagar el panell de funcions</translation>
    </message>
    <message>
        <source>Imports</source>
        <translation type="vanished">Imports</translation>
    </message>
    <message>
        <source>Show/Hide Imports panel</source>
        <translation type="vanished">Mostrar/Amagar panell d&apos;importacions</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation type="vanished">Simbols</translation>
    </message>
    <message>
        <source>Show/Hide Symbols panel</source>
        <translation type="vanished">Mostar/Amagar el panell de simbols</translation>
    </message>
    <message>
        <source>Relocs</source>
        <translation type="vanished">Reubicacions</translation>
    </message>
    <message>
        <source>Show/Hide Relocs panel</source>
        <translation type="vanished">Mostrar/Amagar panell de reubicacions</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="vanished">Flags</translation>
    </message>
    <message>
        <source>Show/Hide Flags panel</source>
        <translation type="vanished">Mostrar/Amagar panell de flags</translation>
    </message>
    <message>
        <source>Memory</source>
        <translation type="vanished">Memòria</translation>
    </message>
    <message>
        <source>Show/Hide Memory panel</source>
        <translation type="vanished">Mostrar/Amagar panell de memòria</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="385"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="400"/>
        <location filename="../../core/MainWindow.ui" line="403"/>
        <source>Tabs up/down</source>
        <translation>Pestanyes adalt/abaix</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="412"/>
        <source>Refresh</source>
        <translation>Recarregar</translation>
    </message>
    <message>
        <source>Comments</source>
        <translation type="vanished">Comentaris</translation>
    </message>
    <message>
        <source>Show/Hide comments</source>
        <translation type="vanished">Mostrar/Amagar comentaris</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="420"/>
        <source>Show Tabs at the Top</source>
        <translation>Mostrar les pestanyes adalt</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="423"/>
        <source>Toggle tab position</source>
        <translation>Canviar la posició de les pestanyes</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="428"/>
        <source>Dark Theme</source>
        <translation>Tema Fosc</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="433"/>
        <location filename="../../core/MainWindow.ui" line="436"/>
        <source>Load layout</source>
        <translation>Carregar disposició</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="441"/>
        <source>Default Theme</source>
        <translation>Tema per Defecte</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="446"/>
        <source>Bindiff</source>
        <translation>Diferències binaries</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="451"/>
        <source>Analysis</source>
        <translation>Anàlisi</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="456"/>
        <source>Test menu</source>
        <translation>Menú de proves</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="461"/>
        <location filename="../../core/MainWindow.ui" line="464"/>
        <source>Copy hexpair</source>
        <translation>Copiar cadena hexadecimal</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="469"/>
        <location filename="../../core/MainWindow.ui" line="472"/>
        <source>Copy text</source>
        <translation>Copiar text</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="477"/>
        <source>Copy ASCII</source>
        <translation type="unfinished">Copy ASCII</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="482"/>
        <location filename="../../core/MainWindow.ui" line="485"/>
        <source>Insert string</source>
        <translation>Insertar cadena de text</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="490"/>
        <location filename="../../core/MainWindow.ui" line="493"/>
        <source>Insert hex</source>
        <translation>Insertar cadena hexadecimal</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="508"/>
        <source>Show/Hide bytes</source>
        <translation>Mostrar/Amagar octets</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="513"/>
        <source>Switch case</source>
        <translation>Switch case</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="518"/>
        <location filename="../../core/MainWindow.ui" line="521"/>
        <source>Copy all</source>
        <translation type="unfinished">Copy all</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="526"/>
        <location filename="../../core/MainWindow.ui" line="529"/>
        <source>Copy bytes</source>
        <translation type="unfinished">Copy bytes</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="534"/>
        <location filename="../../core/MainWindow.ui" line="537"/>
        <location filename="../../core/MainWindow.ui" line="542"/>
        <location filename="../../core/MainWindow.ui" line="545"/>
        <source>Copy disasm</source>
        <translation type="unfinished">Copy disasm</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="557"/>
        <location filename="../../core/MainWindow.ui" line="560"/>
        <source>Start web server</source>
        <translation>Iniciar servidor web</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="565"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="570"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="575"/>
        <source>4</source>
        <translation type="unfinished">4</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="580"/>
        <source>8</source>
        <translation type="unfinished">8</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="585"/>
        <source>16</source>
        <translation type="unfinished">16</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="590"/>
        <source>32</source>
        <translation type="unfinished">32</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="595"/>
        <source>64</source>
        <translation type="unfinished">64</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="600"/>
        <source>Syntax AT&amp;T/Intel</source>
        <translation>Sintaxi AT&amp;T/Intel</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="605"/>
        <location filename="../../core/MainWindow.ui" line="615"/>
        <source>Rename</source>
        <translation type="unfinished">Rename</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="610"/>
        <location filename="../../core/MainWindow.ui" line="620"/>
        <source>Undefine</source>
        <translation type="unfinished">Undefine</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="625"/>
        <source>Add comment</source>
        <translation type="unfinished">Add comment</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="637"/>
        <location filename="../../core/MainWindow.ui" line="640"/>
        <source>Show/Hide bottom pannel</source>
        <translation>Mostar/Amagar panell inferior</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="645"/>
        <source>Run Rizin script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="717"/>
        <source>Save Project As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="735"/>
        <source>Analyze Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="809"/>
        <source>Commit changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="817"/>
        <source>Write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="820"/>
        <source>Open the file in write mode. Every change to the file will change the original file on disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="828"/>
        <source>Cache mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="831"/>
        <source>Enable cache mode. Changes to the file would not be applied to disk unless you specifically commit them. This is a safer option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="842"/>
        <source>Read-Only mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="852"/>
        <source>Manage layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SDB Browser</source>
        <translation type="vanished">Cercador SDB</translation>
    </message>
    <message>
        <source>Run Script</source>
        <translation type="vanished">Executar Script</translation>
    </message>
    <message>
        <source>Dashboard</source>
        <translation type="vanished">Panell Principal</translation>
    </message>
    <message>
        <source>Show/Hide Dashboard panel</source>
        <translation type="vanished">Mostrar/Amagar panell principal</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="650"/>
        <source>Reset Settings</source>
        <translation>Restaurar Configuració per Defecte</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="653"/>
        <source>Reset settings</source>
        <translation>Restaurar la Configuració per defecte</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="658"/>
        <source>Quit</source>
        <translation>Sortir</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="661"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="669"/>
        <source>Exports</source>
        <translation>Exportacions</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="672"/>
        <source>Show/Hide Exports panel</source>
        <translation>Mostrar/Amagar panell d&apos;exportacions</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="677"/>
        <source>Refresh Contents</source>
        <translation>Refrescar Continguts</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="680"/>
        <source>Refresh contents</source>
        <translation>Refrescar continguts</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="688"/>
        <source>Show ESIL rather than assembly</source>
        <translation>Mostrar ESIL enlloc del desensamblat</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="696"/>
        <source>Show pseudocode rather than assembly</source>
        <translation>Mostrar pseudo-codi enlloc de l&apos;ensamblador</translation>
    </message>
    <message>
        <source>Entry Points</source>
        <translation type="vanished">Punts d&apos;Entrada</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="707"/>
        <source>Display offsets</source>
        <translation>Visualitzar adreces</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="712"/>
        <source>Preferences</source>
        <translation>Preferències</translation>
    </message>
    <message>
        <source>Save As...</source>
        <translation type="vanished">Desar Com...</translation>
    </message>
    <message>
        <source>Graph Overview</source>
        <translation type="obsolete">Graph Overview</translation>
    </message>
    <message>
        <source>Decompiler</source>
        <translation type="obsolete">Decompiler</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="745"/>
        <source>Add Hexdump</source>
        <translation type="unfinished">Add Hexdump</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="750"/>
        <source>Add Decompiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="755"/>
        <source>Add Disassembly</source>
        <translation type="unfinished">Add Disassembly</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="760"/>
        <source>Add Graph</source>
        <translation type="unfinished">Add Graph</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="771"/>
        <source>Grouped dock dragging</source>
        <translation type="unfinished">Grouped dock dragging</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="776"/>
        <source>Zoom In</source>
        <translation type="unfinished">Zoom In</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="779"/>
        <source>Ctrl++</source>
        <translation type="unfinished">Ctrl++</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="787"/>
        <source>Zoom Out</source>
        <translation type="unfinished">Zoom Out</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="790"/>
        <source>Ctrl+-</source>
        <translation type="unfinished">Ctrl+-</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="798"/>
        <source>Reset</source>
        <translation type="unfinished">Reset</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="801"/>
        <source>Ctrl+=</source>
        <translation type="unfinished">Ctrl+=</translation>
    </message>
    <message>
        <source>Tmp</source>
        <translation type="vanished">Tmp</translation>
    </message>
    <message>
        <source>Disassembly</source>
        <translation type="vanished">Desassemblar</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="725"/>
        <source>Graph</source>
        <translation type="unfinished">Graph</translation>
    </message>
    <message>
        <source>Pseudocode</source>
        <translation type="vanished">Pseudo-codi</translation>
    </message>
    <message>
        <source>Hexdump</source>
        <translation type="obsolete">Hexdump</translation>
    </message>
    <message>
        <source>Sidebar</source>
        <translation type="vanished">Barra Lateral</translation>
    </message>
    <message>
        <source>Console</source>
        <translation type="vanished">Consola</translation>
    </message>
    <message>
        <source>Stack</source>
        <translation type="vanished">Pila</translation>
    </message>
    <message>
        <source>Registers</source>
        <translation type="vanished">Registres</translation>
    </message>
    <message>
        <source>Backtrace</source>
        <translation type="vanished">Traça Inversa</translation>
    </message>
    <message>
        <source>Threads</source>
        <translation type="obsolete">Threads</translation>
    </message>
    <message>
        <source>Processes</source>
        <translation type="obsolete">Processes</translation>
    </message>
    <message>
        <source>Memory map</source>
        <translation type="vanished">Mapes de Memòria</translation>
    </message>
    <message>
        <source>Breakpoints</source>
        <translation type="vanished">Punts de Ruptura</translation>
    </message>
    <message>
        <source>Register References</source>
        <translation type="vanished">Referències dels Registres</translation>
    </message>
    <message>
        <source>Classes</source>
        <translation type="vanished">Classes</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="730"/>
        <source>Import PDB</source>
        <translation>Importar PDB</translation>
    </message>
    <message>
        <source>Analyze</source>
        <translation type="vanished">Analitzar</translation>
    </message>
    <message>
        <source>Resources</source>
        <translation type="vanished">Recursos</translation>
    </message>
    <message>
        <source>VTables</source>
        <translation type="vanished">VTables</translation>
    </message>
    <message>
        <source>Show/Hide VTables panel</source>
        <translation type="vanished">Mostrar/Amagar panell de VTables</translation>
    </message>
    <message>
        <source>Types</source>
        <translation type="vanished">Tipus</translation>
    </message>
    <message>
        <source>Show/Hide Types panel</source>
        <translation type="vanished">Mostrar/Amagar panell de Tipus</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Cercar</translation>
    </message>
    <message>
        <source>Show/Hide Search panel</source>
        <translation type="vanished">Mostrar/Amagar panell de Cerca</translation>
    </message>
    <message>
        <source>Headers</source>
        <translation type="vanished">Capçaleres</translation>
    </message>
    <message>
        <source>Show/Hide Headers panel</source>
        <translation type="vanished">Mostrar/Amagar panell de Capçaleres</translation>
    </message>
    <message>
        <source>Zignatures</source>
        <translation type="vanished">Zignatures</translation>
    </message>
    <message>
        <source>Show/Hide Zignatures panel</source>
        <translation type="vanished">Mostrar/Amagar el panell de Zignatures</translation>
    </message>
    <message>
        <source>Jupyter</source>
        <translation type="obsolete">Jupyter</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.ui" line="740"/>
        <location filename="../../core/MainWindow.cpp" line="1697"/>
        <source>Export as code</source>
        <translation>Exportar com a codi</translation>
    </message>
    <message>
        <source>Hexdump view</source>
        <translation type="vanished">Vista de volcat hexadecimal</translation>
    </message>
    <message>
        <source>Disassembly view</source>
        <translation type="vanished">Vista de desensamblat</translation>
    </message>
    <message>
        <source>Graph view</source>
        <translation type="vanished">Vista de graf</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="533"/>
        <source>Script loading</source>
        <translation>Carregant scripts</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="534"/>
        <source>Do you want to load the &apos;%1&apos; script?</source>
        <translation>Vols carregar l&apos;script &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="552"/>
        <source>Cannot open file!</source>
        <translation>No s&apos;ha pogut obrir el fitxer!</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="553"/>
        <source>Could not open the file! Make sure the file exists and that you have the correct permissions.</source>
        <translation>No s&apos;ha pogut obrir el fitxer! Revisa que el fitxer existeix i tens els permissos adequats.</translation>
    </message>
    <message>
        <source> &gt; Populating UI</source>
        <translation type="vanished"> &gt; Popul·lant la interfaç d&apos;usuari</translation>
    </message>
    <message>
        <source> &gt; Finished, happy reversing :)</source>
        <translation type="vanished"> &gt; Ja he acabat! Feliç enginyeria inversa!</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="764"/>
        <source>Do you really want to exit?
Save your project before closing!</source>
        <translation>Vols sortir? Desa el projecte abans de tancar!</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1038"/>
        <source>New disassembly</source>
        <translation type="unfinished">New disassembly</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1040"/>
        <source>New graph</source>
        <translation type="unfinished">New graph</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1042"/>
        <source>New hexdump</source>
        <translation type="unfinished">New hexdump</translation>
    </message>
    <message>
        <source>Select radare2 script</source>
        <translation type="vanished">Seleccionar script de radare2</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="251"/>
        <source>No plugins are installed. Check the plugins section on Cutter documentation to learn more.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="256"/>
        <source>The installed plugins didn&apos;t add entries to this menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="616"/>
        <source>Failed to open project: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="619"/>
        <source>Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1043"/>
        <source>New Decompiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1204"/>
        <source>Save layout error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1205"/>
        <source>&apos;%1&apos; is not a valid name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1207"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1514"/>
        <source>Select Rizin script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1557"/>
        <source>Do you really want to clear all settings?</source>
        <translation>Vols esborrar totes les configuracions?</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1651"/>
        <source>Select PDB file</source>
        <translation>Seleccionar fitxer PDB</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1652"/>
        <source>PDB file (*.pdb)</source>
        <translation>Fitxer PDB (*.pdb)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1652"/>
        <source>All files (*)</source>
        <translation>Tots els fitxers (*)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1662"/>
        <source>%1 loaded.</source>
        <translation>%1 carregat.</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1672"/>
        <source>C uin8_t array (*.c)</source>
        <translation>Array d&apos;enters de 8 bits (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1674"/>
        <source>C uin16_t array (*.c)</source>
        <translation>Array d&apos;enters de 16 bits (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1676"/>
        <source>C uin32_t array (*.c)</source>
        <translation>Array d&apos;enters de 32 bits (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1678"/>
        <source>C uin64_t array (*.c)</source>
        <translation>Array d&apos;enters de 64 bits (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1680"/>
        <source>C string (*.c)</source>
        <translation>Cadena en C (*.c)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1682"/>
        <source>Shell-script that reconstructs the bin (*.sh)</source>
        <translation>Script de shell (*.sh)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1684"/>
        <source>JSON array (*.json)</source>
        <translation>Array JSON (*.json)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1686"/>
        <source>JavaScript array (*.js)</source>
        <translation>Array en JavaScript (*.js)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1688"/>
        <source>Python array (*.py)</source>
        <translation>Array en Python (*.py)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1690"/>
        <source>Print &apos;wx&apos; Rizin commands (*.rz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print &apos;wx&apos; r2 commands (*.r2)</source>
        <translation type="vanished">Mostrar en comandes de r2 (*.r2)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1692"/>
        <source>GAS .byte blob (*.asm, *.s)</source>
        <translation>GAS .byte. (*.asm, *.s)</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="1694"/>
        <source>.bytes with instructions in comments (*.txt)</source>
        <translation>.octets amb instruccions en comentaris (*.txt)</translation>
    </message>
    <message>
        <source>Project saved: %1</source>
        <translation type="vanished">Projecte desat: %1</translation>
    </message>
    <message>
        <location filename="../../core/MainWindow.cpp" line="736"/>
        <source>Failed to save project: %1</source>
        <translation>Hi ha hagut un error al desar el projecte: %1</translation>
    </message>
    <message>
        <source>Project saved:</source>
        <translation type="vanished">Projecte desat:</translation>
    </message>
</context>
<context>
    <name>MapFileDialog</name>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="14"/>
        <source>Map New File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="26"/>
        <source>File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="52"/>
        <location filename="../../dialogs/MapFileDialog.cpp" line="18"/>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="59"/>
        <source>Map address:</source>
        <translation type="unfinished">Map address:</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.ui" line="72"/>
        <source>0x40000</source>
        <translation type="unfinished">0x40000</translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.cpp" line="36"/>
        <source>Map new file file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/MapFileDialog.cpp" line="36"/>
        <source>Failed to map a new file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MemoryDockWidget</name>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="obsolete">Sync/unsync offset</translation>
    </message>
</context>
<context>
    <name>MemoryMapModel</name>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="58"/>
        <source>Offset start</source>
        <translation>Adreça Inicial</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="60"/>
        <source>Offset end</source>
        <translation>Adreça Final</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="62"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="64"/>
        <source>Permissions</source>
        <translation>Permisos</translation>
    </message>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="66"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MemoryMapWidget</name>
    <message>
        <location filename="../../widgets/MemoryMapWidget.cpp" line="123"/>
        <source>Memory Map</source>
        <translation type="unfinished">Memory Map</translation>
    </message>
</context>
<context>
    <name>MultitypeFileSaveDialog</name>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="24"/>
        <source>Detect type (*)</source>
        <translation type="unfinished">Detect type (*)</translation>
    </message>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="63"/>
        <source>File save error</source>
        <translation type="unfinished">File save error</translation>
    </message>
    <message>
        <location filename="../../dialogs/MultitypeFileSaveDialog.cpp" line="64"/>
        <source>Unrecognized extension &apos;%1&apos;</source>
        <translation type="unfinished">Unrecognized extension &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>NativeDebugDialog</name>
    <message>
        <location filename="../../dialogs/NativeDebugDialog.ui" line="35"/>
        <source>Command line arguments:</source>
        <translation type="unfinished">Command line arguments:</translation>
    </message>
</context>
<context>
    <name>NewFileDialog</name>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="20"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="149"/>
        <source>Open File</source>
        <translation>Obrir Fitxer</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="79"/>
        <source>About</source>
        <translation type="unfinished">About</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="182"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="390"/>
        <source>Select</source>
        <translation>Seleccionar</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="169"/>
        <source>&lt;b&gt;Select new file&lt;b&gt;</source>
        <translation>&lt;b&gt;Seleccionar nou fitxer&lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="202"/>
        <source>&lt;b&gt;IO&lt;/b&gt;</source>
        <translation>&lt;b&gt;Entrada/Sortida&lt;/b&gt;</translation>
    </message>
    <message>
        <source>://</source>
        <translation type="vanished">://</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="296"/>
        <source>Don&apos;t open any file</source>
        <translation>No obrir cap fitxer</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="303"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="360"/>
        <location filename="../../dialogs/NewFileDialog.ui" line="501"/>
        <source>Open</source>
        <translation type="unfinished">Open</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="316"/>
        <source>Open Shellcode</source>
        <translation>Obrir Shellcode</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="328"/>
        <source>&lt;b&gt;Paste Shellcode&lt;b&gt;</source>
        <translation>&lt;b&gt;Enganxar shellcode&lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="370"/>
        <source>Projects</source>
        <translation>Projectes</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="410"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Open Project&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="534"/>
        <source>Clear all projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Projects path (dir.projects):&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ruta pels projectes (dir.projects):&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="519"/>
        <source>Remove item</source>
        <translation>Eliminar Element</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="524"/>
        <source>Clear all</source>
        <translation>Esborrar-ho tot</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.ui" line="529"/>
        <source>Delete project</source>
        <translation>Esborrar Projecte</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="92"/>
        <source>Select file</source>
        <translation>Seleccionar Fitxer</translation>
    </message>
    <message>
        <source>Select project path (dir.projects)</source>
        <translation type="vanished">Selecciona la ruta pels projectes (dir.projects)</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="obsolete">Permission denied</translation>
    </message>
    <message>
        <source>You do not have write access to &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="obsolete">You do not have write access to &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Delete the project &quot;%1&quot; from disk ?</source>
        <translation type="vanished">Esborrar el projecte &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="104"/>
        <source>Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="287"/>
        <source>Open a file with no extra treatment.</source>
        <translation>Obrir un fitxer sense tractament addicional.</translation>
    </message>
    <message>
        <location filename="../../dialogs/NewFileDialog.cpp" line="319"/>
        <source>Select a new program or a previous one before continuing.</source>
        <translation>Seleccionar un programa abans de continuar.</translation>
    </message>
</context>
<context>
    <name>Omnibar</name>
    <message>
        <location filename="../../widgets/Omnibar.cpp" line="15"/>
        <source>Type flag name or address here</source>
        <translation>Escriu el nom de la flag o l&apos;adreça aqui</translation>
    </message>
</context>
<context>
    <name>OpenFileDialog</name>
    <message>
        <source>Open file</source>
        <translation type="vanished">Obrir Fitxer</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="obsolete">Select file</translation>
    </message>
    <message>
        <source>Map address:</source>
        <translation type="obsolete">Map address:</translation>
    </message>
    <message>
        <source>File:</source>
        <translation type="obsolete">File:</translation>
    </message>
    <message>
        <source>Map address</source>
        <translation type="vanished">Adreça de Mapeig</translation>
    </message>
    <message>
        <source>0x40000</source>
        <translation type="vanished">0x40000</translation>
    </message>
    <message>
        <source>Failed to open file</source>
        <translation type="obsolete">Failed to open file</translation>
    </message>
</context>
<context>
    <name>PluginsOptionsWidget</name>
    <message>
        <source>Plugins are loaded from &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="obsolete">Plugins are loaded from &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="28"/>
        <source>Plugins are loaded from &lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Version</source>
        <translation type="unfinished">Version</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Author</source>
        <translation type="unfinished">Author</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PluginsOptionsWidget.cpp" line="48"/>
        <source>Show Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show radare2 plugin information</source>
        <translation type="obsolete">Show radare2 plugin information</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.ui" line="6"/>
        <source>Preferences</source>
        <translation>Preferències</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="28"/>
        <source>Disassembly</source>
        <translation type="unfinished">Disassembly</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="34"/>
        <source>Debug</source>
        <translation type="unfinished">Debug</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="35"/>
        <source>Appearance</source>
        <translation type="unfinished">Appearance</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="36"/>
        <source>Plugins</source>
        <translation type="unfinished">Plugins</translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="37"/>
        <source>Initialization Script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/preferences/PreferencesDialog.cpp" line="39"/>
        <source>Analysis</source>
        <translation type="unfinished">Anàlisi</translation>
    </message>
</context>
<context>
    <name>ProcessModel</name>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="71"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="73"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="75"/>
        <source>Status</source>
        <translation>Estat</translation>
    </message>
    <message>
        <location filename="../../dialogs/AttachProcDialog.cpp" line="77"/>
        <source>Path</source>
        <translation>Ruta</translation>
    </message>
</context>
<context>
    <name>ProcessesWidget</name>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="26"/>
        <source>PID</source>
        <translation type="unfinished">PID</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="27"/>
        <source>UID</source>
        <translation type="unfinished">UID</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="28"/>
        <source>Status</source>
        <translation type="unfinished">Status</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="29"/>
        <source>Path</source>
        <translation type="unfinished">Path</translation>
    </message>
    <message>
        <location filename="../../widgets/ProcessesWidget.cpp" line="172"/>
        <source>Unable to switch to the requested process.</source>
        <translation type="unfinished">Unable to switch to the requested process.</translation>
    </message>
</context>
<context>
    <name>PseudocodeWidget</name>
    <message>
        <source>Pseudocode</source>
        <translation type="obsolete">Pseudocode</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Refresh</translation>
    </message>
    <message>
        <source>Decompiler:</source>
        <translation type="vanished">Decompilador:</translation>
    </message>
    <message>
        <source>r2dec</source>
        <translation type="vanished">r2dec</translation>
    </message>
    <message>
        <source>pdc</source>
        <translation type="vanished">pdc</translation>
    </message>
    <message>
        <source>Click Refresh to generate Pseudocode from current offset.</source>
        <translation type="vanished">Feu clic a refrescar per a generar pseudocodi des de l&apos;adreça actual.</translation>
    </message>
    <message>
        <source>Cannot decompile at</source>
        <translation type="vanished">No s&apos;ha pogut decompilar a</translation>
    </message>
    <message>
        <source>(Not a function?)</source>
        <translation type="vanished">(No és una funció?)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>A Qt and C++ GUI for radare2 reverse engineering framework</source>
        <translation type="vanished">Una interfície gràfica en Qt i C++ per a radare2</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="97"/>
        <source>The version used to compile Cutter (%1) does not match the binary version of rizin (%2). This could result in unexpected behaviour. Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="317"/>
        <source>A Qt and C++ GUI for rizin reverse engineering framework</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="320"/>
        <source>Filename to open.</source>
        <translation>Fitxer a obrir.</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="324"/>
        <source>Automatically open file and optionally start analysis. Needs filename to be specified. May be a value between 0 and 2: 0 = no analysis, 1 = aaa, 2 = aaaa (experimental)</source>
        <translation>Obrir fitxer automàticament i analitzar-lo opcionalment. Cal especificar un fitxer. Els valors poden ser entre 0 i 2: 0 = no analitzar, 1 = aaa, 2 = aaaa (experimental)</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="327"/>
        <source>level</source>
        <translation>nivell</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="331"/>
        <source>Force using a specific file format (bin plugin)</source>
        <translation type="unfinished">Force using a specific file format (bin plugin)</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="332"/>
        <source>name</source>
        <translation type="unfinished">name</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="336"/>
        <source>Load binary at a specific base address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="337"/>
        <source>base address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="340"/>
        <source>Run script file</source>
        <translation>Executar un script</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="340"/>
        <source>file</source>
        <translation>fitxer</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="343"/>
        <source>Load project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="344"/>
        <source>project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="348"/>
        <source>Open file in write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="352"/>
        <source>PYTHONHOME to use for embedded python interpreter</source>
        <translation type="unfinished">PYTHONHOME to use for embedded python interpreter</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="358"/>
        <source>Disable output redirection. Some of the output in console widget will not be visible. Use this option when debuging a crash or freeze and output  redirection is causing some messages to be lost.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="364"/>
        <source>Do not load plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="368"/>
        <source>Do not load Cutter plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="372"/>
        <source>Do not load rizin plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PYTHONHOME to use for Jupyter</source>
        <translation type="vanished">PYTHONHOME utilitzat per Jupyter</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="96"/>
        <source>Version mismatch!</source>
        <translation>Les versions no encaixen!</translation>
    </message>
    <message>
        <source>The version used to compile Cutter (%1) does not match the binary version of radare2 (%2). This could result in unexpected behaviour. Are you sure you want to continue?</source>
        <translation type="vanished">La versió utilitzada per Cutter (%1) no encaixa amb la versió de radare2 (%2). Això podria causar comportaments inesperats. Vols continuar?</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="386"/>
        <source>Invalid Analysis Level. May be a value between 0 and 2.</source>
        <translation>Nivell d&apos;anàlisi incorrecte. Ha de ser un valor entre 0 i 2.</translation>
    </message>
    <message>
        <location filename="../../CutterApplication.cpp" line="406"/>
        <source>Filename must be specified to start analysis automatically.</source>
        <translation>Cal especificar un fitxer per tal de realitzar l&apos;anàlisi automàtic.</translation>
    </message>
    <message>
        <source>Color of comment generated by radare2</source>
        <translation type="vanished">Color pels comentaris generats per radare2</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="410"/>
        <source>Comment</source>
        <translation type="unfinished">Comment</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="410"/>
        <source>Color of comment generated by Rizin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="411"/>
        <source>Comment created by user</source>
        <translation>Comentari creat per l&apos;usuari</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="411"/>
        <source>Color of user Comment</source>
        <translation>Color del comentari del usuari</translation>
    </message>
    <message>
        <source>Color of function arguments</source>
        <translation type="vanished">Color dels arguments d&apos;una funció</translation>
    </message>
    <message>
        <source>Arguments</source>
        <translation type="vanished">Arguments</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="413"/>
        <source>Color of names of functions</source>
        <translation>Color del nom de les funcions</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="413"/>
        <source>Function name</source>
        <translation type="unfinished">Function name</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="414"/>
        <source>Color of function location</source>
        <translation>Color de la localització de la funció</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="414"/>
        <source>Function location</source>
        <translation>Localització de la funció</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="416"/>
        <source>Color of ascii line in left side that shows what opcodes are belong to function</source>
        <translation type="unfinished">Color of ascii line in left side that shows what opcodes are belong to function</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="418"/>
        <source>Function line</source>
        <translation type="unfinished">Function line</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="420"/>
        <source>Color of flags (similar to bookmarks for offset)</source>
        <translation>Color per les flags</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="420"/>
        <source>Flag</source>
        <translation>Flag</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="421"/>
        <source>Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="422"/>
        <source>Help</source>
        <translation type="unfinished">Help</translation>
    </message>
    <message>
        <source>flow</source>
        <translation type="vanished">flow</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="424"/>
        <source>flow2</source>
        <translation>flow2</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="425"/>
        <location filename="../../widgets/ColorThemeListView.cpp" line="427"/>
        <source>Info</source>
        <translation type="unfinished">Info</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="425"/>
        <source>prompt</source>
        <translation>prompt</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="426"/>
        <source>Color of offsets</source>
        <translation>Color de les adreces</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="426"/>
        <source>Offset</source>
        <translation type="unfinished">Offset</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="427"/>
        <source>input</source>
        <translation>entrada</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="428"/>
        <source>Invalid opcode color</source>
        <translation>Color per les instruccions invàlides</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="428"/>
        <source>invalid</source>
        <translation>invàlid</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="429"/>
        <source>other</source>
        <translation>altres</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="430"/>
        <source>0x00 opcode color</source>
        <translation>0x00 Color pels octets nuls</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="431"/>
        <source>0x7f opcode color</source>
        <translation>0x7f Color pels octets</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="432"/>
        <source>0xff opcode color</source>
        <translation>0xFF Color pels octets</translation>
    </message>
    <message>
        <source>arithmetic color (+, -, *, / etc.)</source>
        <translation type="vanished">color pels operadors aritmètics (+, -, *, / etc.)</translation>
    </message>
    <message>
        <source>bin</source>
        <translation type="vanished">bin</translation>
    </message>
    <message>
        <source>btext</source>
        <translation type="vanished">btext</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="443"/>
        <source>push opcode color</source>
        <translation>color pel &apos;push&apos;</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="444"/>
        <source>pop opcode color</source>
        <translation>color per les instruccions Pop</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="445"/>
        <source>Cryptographic color</source>
        <translation>Color per les instruccions criptogràfiques</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="446"/>
        <source>jmp instructions color</source>
        <translation>color per les instruccions de salt</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="450"/>
        <source>call instructions color (ccall, rcall, call etc)</source>
        <translation>color per les instruccions de crida (call, ccall, rcall,...)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="451"/>
        <source>nop opcode color</source>
        <translation>color per les instruccions que no fan res</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="452"/>
        <source>ret opcode color</source>
        <translation>color per les instruccions Ret</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="453"/>
        <source>Color of interrupts</source>
        <translation>Color de les interrupcions</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="453"/>
        <source>Interrupts</source>
        <translation>Interrupcions</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="454"/>
        <source>swi opcode color</source>
        <translation>color per les interrupcions de programari</translation>
    </message>
    <message>
        <source>cmp opcode color</source>
        <translation type="vanished">color per les instruccions de comparació</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="458"/>
        <source>Registers color</source>
        <translation>Color pels registres</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="458"/>
        <source>Register</source>
        <translation type="unfinished">Register</translation>
    </message>
    <message>
        <source>Numeric constants color</source>
        <translation type="vanished">Color pels nombres constants</translation>
    </message>
    <message>
        <source>Numbers</source>
        <translation type="vanished">Números</translation>
    </message>
    <message>
        <source>mov instructions color (mov, movd, movw etc</source>
        <translation type="vanished">Color per les instruccions MOV</translation>
    </message>
    <message>
        <source>mov</source>
        <translation type="vanished">mov</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="423"/>
        <source>Color of lines showing jump destination</source>
        <translation type="unfinished">Color of lines showing jump destination</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="423"/>
        <source>Flow</source>
        <translation type="unfinished">Flow</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="434"/>
        <source>Color of arithmetic opcodes (add, div, mul etc)</source>
        <translation type="unfinished">Color of arithmetic opcodes (add, div, mul etc)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="435"/>
        <source>Arithmetic</source>
        <translation type="unfinished">Arithmetic</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="437"/>
        <source>Color of binary operations (and, or, xor etc).</source>
        <translation type="unfinished">Color of binary operations (and, or, xor etc).</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="437"/>
        <source>Binary</source>
        <translation type="unfinished">Binary</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="439"/>
        <source>Color of object names, commas between operators, squared brackets and operators inside them.</source>
        <translation type="unfinished">Color of object names, commas between operators, squared brackets and operators inside them.</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="442"/>
        <source>Text</source>
        <translation type="unfinished">Text</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="448"/>
        <source>Color of conditional jump opcodes such as je, jg, jne etc</source>
        <translation type="unfinished">Color of conditional jump opcodes such as je, jg, jne etc</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="449"/>
        <source>Conditional jump</source>
        <translation type="unfinished">Conditional jump</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="456"/>
        <source>Color of compare instructions such as test and cmp</source>
        <translation type="unfinished">Color of compare instructions such as test and cmp</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="457"/>
        <source>Compare instructions</source>
        <translation type="unfinished">Compare instructions</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="461"/>
        <source>Color of numeric constants and object pointers</source>
        <translation type="unfinished">Color of numeric constants and object pointers</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="461"/>
        <source>Constants</source>
        <translation type="unfinished">Constants</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="463"/>
        <source>Color of move instructions such as mov, movd, lea etc</source>
        <translation type="unfinished">Color of move instructions such as mov, movd, lea etc</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="464"/>
        <source>Move instructions</source>
        <translation type="unfinished">Move instructions</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="465"/>
        <source>Function variable color</source>
        <translation>Color per les variables de funció</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="465"/>
        <source>Function variable</source>
        <translation>Variable de funció</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="467"/>
        <source>Function variable (local or argument) type color</source>
        <translation>Color per les variables de funció (local o argument)</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="468"/>
        <source>Variable type</source>
        <translation>Tipus de Variable</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="470"/>
        <source>Function variable address color</source>
        <translation>Color per les adreces de les variables de funció</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="470"/>
        <source>Variable address</source>
        <translation>Adreça de la variable</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="482"/>
        <source>In graph view jump arrow true</source>
        <translation>Arestes pels salts que es compleixen</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="482"/>
        <source>Arrow true</source>
        <translation>Fletxa certa</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="484"/>
        <source>In graph view jump arrow false</source>
        <translation>Color per les arestes dels salts que no es compleixen</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="484"/>
        <source>Arrow false</source>
        <translation>Fletxa falsa</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="486"/>
        <source>In graph view jump arrow (no condition)</source>
        <translation>Color de les arestes sense condicions en el graf</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="486"/>
        <source>Arrow</source>
        <translation>Fletxa</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="490"/>
        <source>Background color of Graph Overview&apos;s node</source>
        <translation type="unfinished">Background color of Graph Overview&apos;s node</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="491"/>
        <source>Graph Overview node</source>
        <translation type="unfinished">Graph Overview node</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="493"/>
        <source>Fill color of Graph Overview&apos;s selection</source>
        <translation type="unfinished">Fill color of Graph Overview&apos;s selection</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="494"/>
        <source>Graph Overview fill</source>
        <translation type="unfinished">Graph Overview fill</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="496"/>
        <source>Border color of Graph Overview&apos;s selection</source>
        <translation type="unfinished">Border color of Graph Overview&apos;s selection</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="497"/>
        <source>Graph Overview border</source>
        <translation type="unfinished">Graph Overview border</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="500"/>
        <source>General background color</source>
        <translation>Fons general</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="500"/>
        <source>Background</source>
        <translation>Fons</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="502"/>
        <source>Background color of non-focused graph node</source>
        <translation type="unfinished">Background color of non-focused graph node</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="503"/>
        <source>Node background</source>
        <translation type="unfinished">Node background</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="511"/>
        <source>Background color of selected word</source>
        <translation type="unfinished">Background color of selected word</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="512"/>
        <source>Main function color</source>
        <translation type="unfinished">Main function color</translation>
    </message>
    <message>
        <source>Alt. background</source>
        <translation type="vanished">Fons Alternatiu</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="505"/>
        <source>Background of current graph node</source>
        <translation>Fons del node seleccionat en el graf</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="505"/>
        <source>Current graph node</source>
        <translation>Node actual del graf</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="507"/>
        <source>Color of node border in graph view</source>
        <translation type="unfinished">Color of node border in graph view</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="507"/>
        <source>Node border</source>
        <translation type="unfinished">Node border</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="509"/>
        <source>Selected line background color</source>
        <translation>Selecionar color de fons de les linies</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="509"/>
        <source>Line highlight</source>
        <translation>Resalar la línea</translation>
    </message>
    <message>
        <source>Highlighted word text color</source>
        <translation type="vanished">Color per resaltar paraules</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="511"/>
        <source>Word higlight</source>
        <translation>Resaltar Paraula</translation>
    </message>
    <message>
        <source>Color of main function color</source>
        <translation type="vanished">Color per la funció principal</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="512"/>
        <source>Main</source>
        <translation>Main</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="521"/>
        <source>Code section color in navigation bar</source>
        <translation>Seleccionar color pel codi dins de la barra de navegació</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="521"/>
        <source>Navbar code</source>
        <translation>Barra de navegació de codi</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="523"/>
        <source>Empty section color in navigation bar</source>
        <translation>Color per les seccions buides de la barra de navegació</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="523"/>
        <source>Navbar empty</source>
        <translation>Barra de navegació buida</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="524"/>
        <source>ucall</source>
        <translation type="unfinished">ucall</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="525"/>
        <source>ujmp</source>
        <translation type="unfinished">ujmp</translation>
    </message>
    <message>
        <location filename="../../widgets/ColorThemeListView.cpp" line="526"/>
        <source>Breakpoint background</source>
        <translation>Fons pels punts d&apos;interrupció</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="100"/>
        <source>Crash</source>
        <translation type="unfinished">Crash</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="101"/>
        <source>Cutter received a signal it can&apos;t handle and will close.&lt;br/&gt;Would you like to create a crash dump for a bug report?</source>
        <translation type="unfinished">Cutter received a signal it can&apos;t handle and will close.&lt;br/&gt;Would you like to create a crash dump for a bug report?</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="104"/>
        <source>Create a Crash Dump</source>
        <translation type="unfinished">Create a Crash Dump</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="105"/>
        <location filename="../../common/CrashHandler.cpp" line="148"/>
        <source>Quit</source>
        <translation type="unfinished">Quit</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="119"/>
        <source>Choose a directory to save the crash dump in</source>
        <translation type="unfinished">Choose a directory to save the crash dump in</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="124"/>
        <source>Minidump (*.dmp)</source>
        <translation type="unfinished">Minidump (*.dmp)</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="133"/>
        <source>Save Crash Dump</source>
        <translation type="unfinished">Save Crash Dump</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="134"/>
        <source>Failed to write to %1.&lt;br/&gt;Please make sure you have access to that directory and try again.</source>
        <translation type="unfinished">Failed to write to %1.&lt;br/&gt;Please make sure you have access to that directory and try again.</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="142"/>
        <source>Success</source>
        <translation type="unfinished">Success</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="143"/>
        <source>&lt;a href=&quot;%1&quot;&gt;Crash dump&lt;/a&gt; was successfully created.</source>
        <translation type="unfinished">&lt;a href=&quot;%1&quot;&gt;Crash dump&lt;/a&gt; was successfully created.</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="147"/>
        <source>Open an Issue</source>
        <translation type="unfinished">Open an Issue</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="156"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../../common/CrashHandler.cpp" line="157"/>
        <source>Error occurred during crash dump creation.</source>
        <translation type="unfinished">Error occurred during crash dump creation.</translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="53"/>
        <source>Write error</source>
        <translation type="unfinished">Error d&apos;escriptura</translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="54"/>
        <source>Your file is opened in read-only mode. Editing is only available when the file is opened in either Write or Cache modes.

WARNING: In Write mode, any changes will be committed to the file on disk. For safety, please consider using Cache mode and then commit the changes manually via File -&gt; Commit modifications to disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="60"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="62"/>
        <source>Reopen in Write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="64"/>
        <source>Enable Cache mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="99"/>
        <source>Uncomitted changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/IOModesController.cpp" line="100"/>
        <source>It seems that you have changes or patches that are not committed to the file.
Do you want to commit them now?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickFilterView</name>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="41"/>
        <source>Quick Filter</source>
        <translation type="unfinished">Quick Filter</translation>
    </message>
    <message>
        <location filename="../../widgets/QuickFilterView.ui" line="61"/>
        <source>X</source>
        <translation>X</translation>
    </message>
</context>
<context>
    <name>R2DecDecompiler</name>
    <message>
        <source>Failed to parse JSON from r2dec</source>
        <translation type="obsolete">Failed to parse JSON from r2dec</translation>
    </message>
</context>
<context>
    <name>R2PluginsDialog</name>
    <message>
        <source>radare2 plugin information</source>
        <translation type="vanished">informació sobre l&apos;extensió de radare2</translation>
    </message>
    <message>
        <source>RBin</source>
        <translation type="vanished">RBin</translation>
    </message>
    <message>
        <source>RBin plugins</source>
        <translation type="vanished">Extensions d&apos;RBin</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="obsolete">Description</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">Llicència</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Type</translation>
    </message>
    <message>
        <source>RIO</source>
        <translation type="vanished">RIO</translation>
    </message>
    <message>
        <source>RIO plugins</source>
        <translation type="vanished">Extensions de RIO</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="obsolete">Permissions</translation>
    </message>
    <message>
        <source>RCore</source>
        <translation type="vanished">RCore</translation>
    </message>
    <message>
        <source>RCore plugins</source>
        <translation type="vanished">Extensions de RCore</translation>
    </message>
    <message>
        <source>RAsm</source>
        <translation type="vanished">RAsm</translation>
    </message>
    <message>
        <source>RAsm plugins</source>
        <translation type="vanished">Extensions de RAsm</translation>
    </message>
    <message>
        <source>Architecture</source>
        <translation type="vanished">Arquitectura</translation>
    </message>
    <message>
        <source>CPU&apos;s</source>
        <translation type="vanished">CUP&apos;s</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="vanished">Versió</translation>
    </message>
    <message>
        <source>Author</source>
        <translation type="vanished">Autor</translation>
    </message>
    <message>
        <source>Cutter</source>
        <translation type="obsolete">Cutter</translation>
    </message>
    <message>
        <source>Cutter plugins</source>
        <translation type="vanished">Extensions de Cutter</translation>
    </message>
</context>
<context>
    <name>R2TaskDialog</name>
    <message>
        <source>R2 Task</source>
        <translation type="obsolete">R2 Task</translation>
    </message>
    <message>
        <source>R2 task in progress..</source>
        <translation type="obsolete">R2 task in progress..</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Time</translation>
    </message>
    <message>
        <source>Running for</source>
        <translation type="obsolete">Running for</translation>
    </message>
    <message numerus="yes">
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation type="obsolete">
            <numerusform>%n hour</numerusform>
            <numerusform>%n hour</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation type="obsolete">
            <numerusform>%n minute</numerusform>
            <numerusform>%n minute</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation type="obsolete">
            <numerusform>%n seconds</numerusform>
            <numerusform>%n seconds</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RawAddrDock</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="500"/>
        <source>Raw</source>
        <translation>En cru</translation>
    </message>
</context>
<context>
    <name>RegisterRefModel</name>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="67"/>
        <source>Register</source>
        <translation>Registre</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="69"/>
        <source>Value</source>
        <translation type="unfinished">Value</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="71"/>
        <source>Reference</source>
        <translation>Referència</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="73"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegisterRefsWidget</name>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="137"/>
        <source>Copy register value</source>
        <translation>Copiar valor del registre</translation>
    </message>
    <message>
        <location filename="../../widgets/RegisterRefsWidget.cpp" line="138"/>
        <source>Copy register reference</source>
        <translation>Copiar referencia del registre</translation>
    </message>
</context>
<context>
    <name>RelocsModel</name>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="54"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="56"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="58"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="60"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RelocsWidget</name>
    <message>
        <location filename="../../widgets/RelocsWidget.cpp" line="131"/>
        <source>Relocs</source>
        <translation type="unfinished">Relocs</translation>
    </message>
</context>
<context>
    <name>RemoteDebugDialog</name>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="43"/>
        <source>Debugger:</source>
        <translation type="unfinished">Debugger:</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="82"/>
        <source>IP or Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="159"/>
        <source>Remove item</source>
        <translation type="unfinished">Eliminar Element</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="164"/>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="167"/>
        <source>Remove all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GDB</source>
        <translation type="obsolete">GDB</translation>
    </message>
    <message>
        <source>WinDbg - Pipe</source>
        <translation type="obsolete">WinDbg - Pipe</translation>
    </message>
    <message>
        <source>IP:</source>
        <translation type="obsolete">IP:</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.ui" line="75"/>
        <source>Port:</source>
        <translation type="unfinished">Port:</translation>
    </message>
    <message>
        <source>Path:</source>
        <translation type="obsolete">Path:</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="57"/>
        <source>Invalid debugger</source>
        <translation type="unfinished">Invalid debugger</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="68"/>
        <source>Invalid IP address</source>
        <translation type="unfinished">Invalid IP address</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="81"/>
        <source>Path does not exist</source>
        <translation type="unfinished">Path does not exist</translation>
    </message>
    <message>
        <location filename="../../dialogs/RemoteDebugDialog.cpp" line="94"/>
        <source>Invalid port</source>
        <translation type="unfinished">Invalid port</translation>
    </message>
</context>
<context>
    <name>RenameDialog</name>
    <message>
        <source>Name:</source>
        <translation type="vanished">Nom:</translation>
    </message>
</context>
<context>
    <name>ResourcesModel</name>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="76"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="78"/>
        <source>Vaddr</source>
        <translation>AdreçaVirtual</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="80"/>
        <source>Index</source>
        <translation>Índex</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="82"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="84"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="86"/>
        <source>Lang</source>
        <translation>Llengua</translation>
    </message>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="88"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ResourcesWidget</name>
    <message>
        <location filename="../../widgets/ResourcesWidget.cpp" line="118"/>
        <source>Resources</source>
        <translation>Recursos</translation>
    </message>
</context>
<context>
    <name>RizinGraphWidget</name>
    <message>
        <location filename="../../widgets/RizinGraphWidget.ui" line="57"/>
        <source>ag...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="21"/>
        <source>Data reference graph (aga)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="22"/>
        <source>Global data references graph (agA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="26"/>
        <source>Imports graph (agi)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="27"/>
        <source>References graph (agr)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="28"/>
        <source>Global references graph (agR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="29"/>
        <source>Cross references graph (agx)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="30"/>
        <source>Custom graph (agg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/RizinGraphWidget.cpp" line="31"/>
        <source>User command</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RizinPluginsDialog</name>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="14"/>
        <source>Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="24"/>
        <source>RzBin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="30"/>
        <source>RzBin plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="41"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="82"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="123"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="154"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="46"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="87"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="128"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="174"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="51"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="92"/>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="179"/>
        <source>License</source>
        <translation type="unfinished">Llicència</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="56"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="65"/>
        <source>RzIO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="71"/>
        <source>RzIO plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="97"/>
        <source>Permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="106"/>
        <source>RzCore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="112"/>
        <source>RzCore plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="137"/>
        <source>RzAsm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="143"/>
        <source>RzAsm plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="159"/>
        <source>Architecture</source>
        <translation type="unfinished">Arquitectura</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="164"/>
        <source>CPU&apos;s</source>
        <translation type="unfinished">CUP&apos;s</translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="169"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinPluginsDialog.ui" line="184"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RizinTaskDialog</name>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="14"/>
        <source>Rizin Task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="20"/>
        <source>Rizin task in progress..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.ui" line="27"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="32"/>
        <source>Running for</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="34"/>
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="38"/>
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../dialogs/RizinTaskDialog.cpp" line="41"/>
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RunScriptTask</name>
    <message>
        <location filename="../../common/RunScriptTask.cpp" line="18"/>
        <source>Executing script...</source>
        <translation type="unfinished">Executing script...</translation>
    </message>
    <message>
        <location filename="../../common/RunScriptTask.h" line="15"/>
        <source>Run Script</source>
        <translation type="unfinished">Run Script</translation>
    </message>
</context>
<context>
    <name>SaveProjectDialog</name>
    <message>
        <source>Save Project</source>
        <translation type="vanished">Guardar el projecte</translation>
    </message>
    <message>
        <source>Project name (prj.name):</source>
        <translation type="vanished">Nom del Projecte (prj.name):</translation>
    </message>
    <message>
        <source>Projects path (dir.projects):</source>
        <translation type="vanished">Ruta pels Projectes (dir.projects):</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Seleccionar</translation>
    </message>
    <message>
        <source>Use simple project saving style (prj.simple, recommended)</source>
        <translation type="vanished">Utilitza projectes simples (prj.simple recommended)</translation>
    </message>
    <message>
        <source>Save the target binary inside the project directory (prj.files)</source>
        <translation type="vanished">Desa el binari dins del directori del projecte (prj.files)</translation>
    </message>
    <message>
        <source>Project is a git repo and saving is committing (prj.git)</source>
        <translation type="vanished">Gestionar els projectes amb Git (prj.jit)</translation>
    </message>
    <message>
        <source>Use ZIP format for project files (prj.zip)</source>
        <translation type="vanished">Comprimir els projectes en ZIP. (prj.zip)</translation>
    </message>
    <message>
        <source>Select project path (dir.projects)</source>
        <translation type="vanished">Ruta pels Projectes (dir.projects)</translation>
    </message>
    <message>
        <source>Save project</source>
        <translation type="vanished">Desar projecte</translation>
    </message>
    <message>
        <source>Invalid project name.</source>
        <translation type="vanished">El nom del projecte no es vàlid.</translation>
    </message>
</context>
<context>
    <name>SdbDock</name>
    <message>
        <source>Key</source>
        <translation type="vanished">Clau</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Value</translation>
    </message>
</context>
<context>
    <name>SdbWidget</name>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="17"/>
        <source>SDB Browser</source>
        <translation type="unfinished">SDB Browser</translation>
    </message>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="120"/>
        <source>Key</source>
        <translation type="unfinished">Key</translation>
    </message>
    <message>
        <location filename="../../widgets/SdbWidget.ui" line="125"/>
        <source>Value</source>
        <translation type="unfinished">Value</translation>
    </message>
</context>
<context>
    <name>SearchModel</name>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="96"/>
        <source>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="unfinished">&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="115"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="117"/>
        <source>Offset</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="119"/>
        <source>Code</source>
        <translation>Codi</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="121"/>
        <source>Data</source>
        <translation>Dades</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="123"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="83"/>
        <source>Search</source>
        <translation type="unfinished">Search</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="90"/>
        <source>Search for:</source>
        <translation>Cercar per:</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.ui" line="100"/>
        <source>Search in:</source>
        <translation>Cercar en:</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="251"/>
        <source>asm code</source>
        <translation>codi ensamblador</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="252"/>
        <source>string</source>
        <translation>cadena de caràcters</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="253"/>
        <source>hex string</source>
        <translation>cadena hexadecimal</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="254"/>
        <source>ROP gadgets</source>
        <translation>Enginys de POR</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="255"/>
        <source>32bit value</source>
        <translation>Valor de 32 bits</translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="282"/>
        <source>No results found for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/SearchWidget.cpp" line="285"/>
        <source>No Results Found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SectionsModel</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="93"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="95"/>
        <source>Size</source>
        <translation>Mida</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="101"/>
        <source>Virtual Size</source>
        <translation>Mida virtual</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="97"/>
        <source>Address</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="99"/>
        <source>End Address</source>
        <translation>Adreça final</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="103"/>
        <source>Permissions</source>
        <translation>Permisos</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="105"/>
        <source>Entropy</source>
        <translation>Entropia</translation>
    </message>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="107"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SegmentsModel</name>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="81"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="83"/>
        <source>Size</source>
        <translation>Mida</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="85"/>
        <source>Address</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="87"/>
        <source>End Address</source>
        <translation>Adreça final</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="89"/>
        <source>Permissions</source>
        <translation>Permisos</translation>
    </message>
    <message>
        <location filename="../../widgets/SegmentsWidget.cpp" line="91"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetFunctionVarTypes</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Diàleg</translation>
    </message>
    <message>
        <source>Set Type To:</source>
        <translation type="vanished">Canviar Tipus:</translation>
    </message>
    <message>
        <source>Set Name To:</source>
        <translation type="vanished">Canviar Nom:</translation>
    </message>
    <message>
        <source>Modify:</source>
        <translation type="vanished">Modificar:</translation>
    </message>
    <message>
        <source>You must be in a function to define variable types.</source>
        <translation type="vanished">Cal estar dins d&apos;una funció per poder definir tipus de variables.</translation>
    </message>
</context>
<context>
    <name>SetToDataDialog</name>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="14"/>
        <source>Set to Data</source>
        <translation>Defineix com a dades</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="20"/>
        <location filename="../../dialogs/SetToDataDialog.ui" line="34"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="27"/>
        <source>Start address</source>
        <translation>Adreça inicial</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="41"/>
        <source>End address</source>
        <translation>Adreça final</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="48"/>
        <source>Item size</source>
        <translation>Mida de l&apos;element</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="55"/>
        <source>Number of items</source>
        <translation>Nombre d&apos;elements</translation>
    </message>
    <message>
        <location filename="../../dialogs/SetToDataDialog.ui" line="62"/>
        <location filename="../../dialogs/SetToDataDialog.ui" line="69"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>SideBar</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Formulari</translation>
    </message>
    <message>
        <source>Script</source>
        <translation type="vanished">Script</translation>
    </message>
    <message>
        <source>X</source>
        <translation type="vanished">X</translation>
    </message>
    <message>
        <source>example.py</source>
        <translation type="vanished">exemple.py</translation>
    </message>
    <message>
        <source>Execution finished</source>
        <translation type="vanished">Execució finalitzada</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation type="vanished">Calculadora</translation>
    </message>
    <message>
        <source>Assembler</source>
        <translation type="obsolete">Assembler</translation>
    </message>
    <message>
        <source>Assembly</source>
        <translation type="vanished">Ensamblador</translation>
    </message>
    <message>
        <source>v</source>
        <translation type="vanished">v</translation>
    </message>
    <message>
        <source>^</source>
        <translation type="vanished">^</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation type="vanished">Hexadecimal</translation>
    </message>
    <message>
        <source>Toogle resposiveness</source>
        <translation type="vanished">Commuta la responsivitat</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Preferències</translation>
    </message>
</context>
<context>
    <name>SidebarWidget</name>
    <message>
        <source> Function:</source>
        <translation type="vanished"> Funció:</translation>
    </message>
    <message>
        <source>Offset info:</source>
        <translation type="vanished">Informació de l&apos;adreça:</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="vanished">Informació</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Valor</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Opcode description:</source>
        <translation type="vanished">Descripció de la Instrucció:</translation>
    </message>
    <message>
        <source>Function registers info:</source>
        <translation type="vanished">Ús dels registres dins en la funció:</translation>
    </message>
    <message>
        <source>X-Refs to current address:</source>
        <translation type="vanished">Referències a aquí:</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="vanished">Adreça</translation>
    </message>
    <message>
        <source>Instruction</source>
        <translation type="vanished">Instrucció</translation>
    </message>
    <message>
        <source>X-Refs from current address:</source>
        <translation type="vanished">Referències des d&apos;aqui:</translation>
    </message>
</context>
<context>
    <name>SimpleTextGraphView</name>
    <message>
        <location filename="../../widgets/SimpleTextGraphView.cpp" line="29"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StackModel</name>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="216"/>
        <source>Offset</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="218"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="220"/>
        <source>Reference</source>
        <translation>Referència</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="222"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StackWidget</name>
    <message>
        <source>Offset</source>
        <translation type="vanished">Adreça</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Valor</translation>
    </message>
    <message>
        <source>Reference</source>
        <translation type="obsolete">Reference</translation>
    </message>
    <message>
        <source>Seek to this offset</source>
        <translation type="vanished">Desplaçar-se a aquesta adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="33"/>
        <source>Edit stack value...</source>
        <translation>Editar valor de la pila...</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="109"/>
        <source>Edit stack at %1</source>
        <translation>Editar la pila a %1</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="138"/>
        <source>Stack position</source>
        <translation type="unfinished">Stack position</translation>
    </message>
    <message>
        <location filename="../../widgets/StackWidget.cpp" line="140"/>
        <source>Pointed memory</source>
        <translation type="unfinished">Pointed memory</translation>
    </message>
</context>
<context>
    <name>StringsModel</name>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="67"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="69"/>
        <source>String</source>
        <translation type="unfinished">String</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="71"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="73"/>
        <source>Length</source>
        <translation>Llargària</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="75"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="77"/>
        <source>Section</source>
        <translation>Secció</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="79"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StringsTask</name>
    <message>
        <location filename="../../common/StringsTask.h" line="13"/>
        <source>Searching for Strings</source>
        <translation>Cercar cadenes de text</translation>
    </message>
</context>
<context>
    <name>StringsWidget</name>
    <message>
        <source>Copy Address</source>
        <translation type="vanished">Copiar l&apos;adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.ui" line="76"/>
        <source>Copy String</source>
        <translation>Copiar la cadena de text</translation>
    </message>
    <message>
        <source>Xrefs</source>
        <translation type="vanished">Referències</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.ui" line="81"/>
        <source>Filter</source>
        <translation>Filtres</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="161"/>
        <source>Section:</source>
        <translation>Secció:</translation>
    </message>
    <message>
        <location filename="../../widgets/StringsWidget.cpp" line="244"/>
        <source>(all)</source>
        <translation type="unfinished">(all)</translation>
    </message>
</context>
<context>
    <name>SymbolsModel</name>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="58"/>
        <source>Address</source>
        <translation type="unfinished">Address</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="60"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="62"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="64"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SymbolsWidget</name>
    <message>
        <location filename="../../widgets/SymbolsWidget.cpp" line="123"/>
        <source>Symbols</source>
        <translation type="unfinished">Symbols</translation>
    </message>
</context>
<context>
    <name>ThreadsWidget</name>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="24"/>
        <source>PID</source>
        <translation type="unfinished">PID</translation>
    </message>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="25"/>
        <source>Status</source>
        <translation type="unfinished">Status</translation>
    </message>
    <message>
        <location filename="../../widgets/ThreadsWidget.cpp" line="26"/>
        <source>Path</source>
        <translation type="unfinished">Path</translation>
    </message>
</context>
<context>
    <name>TypesInteractionDialog</name>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialog</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="23"/>
        <source>Load From File:</source>
        <translation type="unfinished">Load From File:</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="37"/>
        <source>Select File</source>
        <translation type="unfinished">Select File</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.ui" line="50"/>
        <source>Enter Types Manually</source>
        <translation type="unfinished">Enter Types Manually</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="32"/>
        <source>Select file</source>
        <translation type="unfinished">Select file</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="40"/>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="68"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../../dialogs/TypesInteractionDialog.cpp" line="69"/>
        <source>There was some error while loading new types</source>
        <translation type="unfinished">There was some error while loading new types</translation>
    </message>
</context>
<context>
    <name>TypesModel</name>
    <message>
        <source>Type</source>
        <translation type="obsolete">Type</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="62"/>
        <source>Type / Name</source>
        <translation type="unfinished">Type / Name</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="64"/>
        <source>Size</source>
        <translation type="unfinished">Size</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="66"/>
        <source>Format</source>
        <translation type="unfinished">Format</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="68"/>
        <source>Category</source>
        <translation type="unfinished">Category</translation>
    </message>
</context>
<context>
    <name>TypesWidget</name>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="76"/>
        <location filename="../../widgets/TypesWidget.ui" line="79"/>
        <source>Export Types</source>
        <translation type="unfinished">Export Types</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="84"/>
        <location filename="../../widgets/TypesWidget.ui" line="87"/>
        <location filename="../../widgets/TypesWidget.cpp" line="286"/>
        <source>Load New Types</source>
        <translation type="unfinished">Load New Types</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="92"/>
        <location filename="../../widgets/TypesWidget.ui" line="95"/>
        <source>Delete Type</source>
        <translation type="unfinished">Delete Type</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.ui" line="100"/>
        <source>Link Type to Address</source>
        <translation type="unfinished">Link Type to Address</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="140"/>
        <source>Category</source>
        <translation type="unfinished">Category</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="185"/>
        <source>View Type</source>
        <translation>Veure el tipus</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="186"/>
        <source>Edit Type</source>
        <translation>Edita el tipus</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="217"/>
        <source>(All)</source>
        <translation>(Tot)</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="266"/>
        <source>Save File</source>
        <translation>Desa el fitxer</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="273"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="302"/>
        <source>Edit Type: </source>
        <translation>Edita el tipus: </translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="305"/>
        <location filename="../../widgets/TypesWidget.cpp" line="355"/>
        <source>View Type: </source>
        <translation>Veure el tipus: </translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="305"/>
        <location filename="../../widgets/TypesWidget.cpp" line="355"/>
        <source> (Read Only)</source>
        <translation> (Només lectura)</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="324"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../../widgets/TypesWidget.cpp" line="324"/>
        <source>Are you sure you want to delete &quot;%1&quot;?</source>
        <translation>Estàs segur que vols eliminar &quot;%1&quot;?</translation>
    </message>
</context>
<context>
    <name>UpdateWorker</name>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="34"/>
        <source>Time limit exceeded during version check. Please check your internet connection and try again.</source>
        <translation>Límit de temps execit durant la verificació de la versió. Si us plau verifica la teva connexió a Internet and torna-ho a intentar.</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="81"/>
        <source>Version control</source>
        <translation>Control de versions</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="82"/>
        <source>There is an update available for Cutter.&lt;br/&gt;</source>
        <translation>Hi ha una nova actualització disponible.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="82"/>
        <source>Current version:</source>
        <translation>Versió actual:</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="83"/>
        <source>Latest version:</source>
        <translation>Última versió:</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="85"/>
        <source>For update, please check the link:&lt;br/&gt;</source>
        <translation>Per actualitzar, premeu aquest enllaç:&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="89"/>
        <source>or click &quot;Download&quot; to download latest version of Cutter.</source>
        <translation>or prem &quot;Descàrrega&quot; per baixar la última versió disponible.</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="92"/>
        <source>Don&apos;t check for updates</source>
        <translation>No verifiquis si hi ha noves actualizacions disponibles</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="96"/>
        <source>Download</source>
        <translation>Descàrrega</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="103"/>
        <source>Choose directory for downloading</source>
        <translation>Tria un directori per la descàrrega</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="108"/>
        <source>Downloading update...</source>
        <translation>Baixant actualització...</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="108"/>
        <source>Cancel</source>
        <translation>Cancel·la</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="116"/>
        <source>Download finished!</source>
        <translation>Descàrrega finalitzada!</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="117"/>
        <source>Latest version of Cutter was succesfully downloaded!</source>
        <translation>La última versió ha estat baixada amb èxit!</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="119"/>
        <source>Open file</source>
        <translation>Obre fitxer</translation>
    </message>
    <message>
        <location filename="../../common/UpdateWorker.cpp" line="120"/>
        <source>Open download folder</source>
        <translation>Obre directori de descàrrega</translation>
    </message>
</context>
<context>
    <name>VTableModel</name>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="63"/>
        <source>VTable</source>
        <translation>VTable</translation>
    </message>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="84"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../widgets/VTablesWidget.cpp" line="86"/>
        <source>Address</source>
        <translation>Adreça</translation>
    </message>
</context>
<context>
    <name>VersionInfoDialog</name>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="50"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="64"/>
        <source>TextLabel</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="94"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="126"/>
        <source>Key</source>
        <translation>Clau</translation>
    </message>
    <message>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="99"/>
        <location filename="../../dialogs/VersionInfoDialog.ui" line="131"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
</context>
<context>
    <name>VirtualAddrDock</name>
    <message>
        <location filename="../../widgets/SectionsWidget.cpp" line="508"/>
        <source>Virtual</source>
        <translation>Virtual</translation>
    </message>
</context>
<context>
    <name>VisualNavbar</name>
    <message>
        <location filename="../../widgets/VisualNavbar.cpp" line="29"/>
        <source>Visual navigation bar</source>
        <translation>Barra de navegació visual</translation>
    </message>
</context>
<context>
    <name>WelcomeDialog</name>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="20"/>
        <source>Welcome to Cutter</source>
        <translation>Benvingut a Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="83"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="104"/>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="19"/>
        <source>Version </source>
        <translation>Versió </translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="168"/>
        <source>About</source>
        <translation>Quant a</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="203"/>
        <source>Native Theme</source>
        <translation>Tema Natiu</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="208"/>
        <source>Dark Theme</source>
        <translation>Tema fosc</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="213"/>
        <source>Midnight Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="218"/>
        <source>Light Theme</source>
        <translation>Tema clar</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="229"/>
        <source>Check for updates on start</source>
        <translation>Comprova si hi ha actualitzacions a l&apos;obrir l&apos;aplicació</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="298"/>
        <source>Community</source>
        <translation>Comunitat</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="320"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/cutter_re&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@cutter_re&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/cutter_re&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@cutter_re &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="355"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;/span&gt;&lt;a href=&quot;https://github.com/rizinorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and report bugs or contribute code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Uneix-te a milers de persones que practiquen enginyeria inversa en la nostra comunitat:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter a &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;/span&gt;&lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and report bugs or contribute code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Vols ajudar-nos a millorar Cutter?&lt;br/&gt;Visita la nostra pàgina &lt;/span&gt;&lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt; de GitHub&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; i reports errors o contibueix en el codi.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="417"/>
        <source>Continue</source>
        <translation>Continua</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;	&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;#cutter on &lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;	&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;#cutter on &lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt; and report bugs or contribute code.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt; and report bugs or contribute code.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.ui" line="394"/>
        <source>Contributing</source>
        <translation>Contribuïnt</translation>
    </message>
    <message>
        <source>Continue 🢒</source>
        <translation type="vanished">Continuar</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="70"/>
        <source>Language settings</source>
        <translation>Preferències d&apos;idioma</translation>
    </message>
    <message>
        <location filename="../../dialogs/WelcomeDialog.cpp" line="71"/>
        <source>Language will be changed after next application start.</source>
        <translation>L&apos;idioma canviarà quan tornis a obrir l&apos;aplicació.</translation>
    </message>
</context>
<context>
    <name>XrefModel</name>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="287"/>
        <source>Address</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="291"/>
        <source>Code</source>
        <translation>Codi</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="293"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="289"/>
        <source>Type</source>
        <translation>Tipus</translation>
    </message>
</context>
<context>
    <name>XrefsDialog</name>
    <message>
        <source>Address</source>
        <translation type="vanished">Adreça</translation>
    </message>
    <message>
        <source>Code</source>
        <translation type="obsolete">Code</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Tipus</translation>
    </message>
    <message>
        <source>X-Refs to %1:</source>
        <translation type="vanished">Referències a %1:</translation>
    </message>
    <message>
        <source>X-Refs from %1:</source>
        <translation type="vanished">Referències desde %1:</translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="145"/>
        <source>X-Refs to %1 (%2 results):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="147"/>
        <source>X-Refs from %1 (%2 results):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="152"/>
        <source>Writes to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="153"/>
        <source>Reads from %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dialogs/XrefsDialog.cpp" line="164"/>
        <location filename="../../dialogs/XrefsDialog.cpp" line="183"/>
        <source>X-Refs for %1</source>
        <translation>Referències a %1</translation>
    </message>
</context>
<context>
    <name>ZignaturesModel</name>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="66"/>
        <source>Offset</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="68"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../widgets/ZignaturesWidget.cpp" line="70"/>
        <source>Bytes</source>
        <translation>Octets</translation>
    </message>
</context>
</TS>
